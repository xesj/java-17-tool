package xesj.args;

/**
 * Kizárólag a felhasználó által hibásan összeállított program argumentumok esetén váltódik ki. 
 */
public class ArgumentException extends Exception {

  /**
   * Hiba lehetséges típusai.
   */
  public enum Type {

    /** 
     * Az argumentum nem tartalmaz egyenlőségjelet. 
     */
    EQUAL_SIGN,

    /**
     * Az argumentum név többször fordul elő.
     */
    MULTIPLE,

    /**
     * Az argumentum név nem használható, mert nincs rá szabály.
     */
    APPLICABLE,

    /**
     * Az argumentum megadása kötelező.
     */
    REQUIRED,

    /**
     * Az argumentum értéke nem engedélyezett.
     */
    VALUE_GRANT,

    /**
     * Az argumentum értéke nem konvertálható.
     */
    VALUE_CONVERT

  };

  /**
   * Hiba típusa.
   */
  private final Type exceptionType;

  /**
   * Hibát okozó argumentum neve.
   */
  private final String argumentName;

  /**
   * Konstruktor.
   * @param exceptionType Hiba típusa. 
   * @param argumentName Hibát okozó argumentum neve.
   */
  public ArgumentException(Type exceptionType, String argumentName) {

    this(exceptionType, argumentName, null, null);

  }

  /**
   * Konstruktor.
   * @param exceptionType Hiba típusa. 
   * @param argumentName Hibát okozó argumentum neve.
   * @param argumentValue Hibát okozó argumentum értéke.
   */
  public ArgumentException(Type exceptionType, String argumentName, String argumentValue) {

    this(exceptionType, argumentName, argumentValue, null);

  }

  /**
   * Konstruktor.
   * @param exceptionType Hiba típusa. 
   * @param argumentName Hibát okozó argumentum neve.
   * @param argumentValue Hibát okozó argumentum értéke.
   * @param argumentType Hibát okozó argumentum típusa.
   */
  public ArgumentException(Type exceptionType, String argumentName, String argumentValue, Class argumentType) {

    super(createMessage(exceptionType, argumentName, argumentValue, argumentType));
    this.argumentName = argumentName;
    this.exceptionType = exceptionType;

  }

  /**
   * Konstruktor. Ezt a konstruktort külső eljárás hívja, a külső eljárás állítja elő a teljes hibaszöveget.
   * @param message Hibaüzenet. 
   */
  public ArgumentException(String message) {

    super(message);
    this.argumentName = null;
    this.exceptionType = null;

  }

  /**
   * Hiba szövegének előállítása.
   * @param exceptionType Hiba típusa. 
   * @param argumentName Hibát okozó argumentum neve.
   * @param argumentValue Hibát okozó argumentum értéke.
   * @param argumentType Hibát okozó argumentum típusa.
   * @return Hiba szövege.
   */
  private static String createMessage(Type exceptionType, String argumentName, String argumentValue, Class argumentType) {

    String message = "'" + argumentName + "' argumentum ";

    switch (exceptionType) {
      case EQUAL_SIGN:
        message += "nem tartalmaz egyenlőségjelet.";
        break;
      case MULTIPLE:
        message += "név többször fordul elő.";
        break;
      case APPLICABLE:
        message += "név nem használható, mert nincs rá szabály.";
        break;
      case REQUIRED:
        message += "megadása kötelező.";
        break;
      case VALUE_GRANT:
        message += "értéke nem engedélyezett: '" + argumentValue + "'";
        break;
      case VALUE_CONVERT:
        message += "értéke nem konvertálható " + argumentType.getSimpleName() + " típusra: '" + argumentValue + "'";
        break;
    }

    return message;

  }

  /**
   * Hiba típusának lekérdezése.
   * @return Hiba típusa.
   */
  public Type getExceptionType() {

    return exceptionType;

  }

  /**
   * Hibát okozó argumentum nevének lekérdezése.
   * @return Hibát okozó argumentum neve.
   */
  public String getArgumentName() {

    return argumentName;

  }

  // ====
}
