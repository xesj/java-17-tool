package xesj.args;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Argumentumokat kezelő osztály.
 */
public class ArgumentHandler {

  /**
   * Szabályokat tároló map. A szabály név a kulcs.
   */
  private Map<String, ArgumentRule> ruleMap = new HashMap<>();

  /**
   * Szabály neveket tároló lista. A nevek sorrendje megegyezik azzal ahogy a szabályokat hozzáadtuk a handler-hez.
   */
  private List<String> ruleNames = new ArrayList<>();

  /**
   * Argumentumok eredeti formában.
   */
  private String[] args;

  /**
   * Argumentumok map-ként tárolva. Kulcs: argumentum név, érték: argumentum string érték.
   */
  private Map<String, String> argsMap = new HashMap<>();

  /**
   * Argumentumok típusra konvertált értékei map-ként tárolva. Kulcs: argumentum név, érték: konvertált argumentum érték.
   */
  private Map<String, Object> argsConvertedMap = new HashMap<>();

  /**
   * Jelzi hogy a check() metódus hívása megtörtént-e.
   */
  private boolean checked;

  /**
   * Jelzi hogy a check() metódus sikeresen futott-e le. 
   * True: lefutás sikeres, nem keletkezett HelpException, ArgumentException.
   */
  private boolean checkResult;

  /**
   * Szabály hozzáadása az argumentum kezelőhöz.
   * @param rule Hozzáadandó szabály.
   * @return Saját maga.
   */
  public ArgumentHandler addRule(ArgumentRule rule) {

    if (checked) throw new RuntimeException("addRule() metódus nem hívható meg check() metódus után.");

    if (rule == null) {
      throw new RuntimeException("A 'rule' paraméter kötelező.");
    }

    if (ruleMap.containsKey(rule.name)) {
      throw new RuntimeException("Az argumentum kezelő már tartalmaz '" + rule.name + "' nevű szabályt.");
    }

    rule.check();
    ruleNames.add(rule.name);
    ruleMap.put(rule.name, rule);
    return this;

  }

  /**
   * A paraméterként kapott argumentum tömb ellenőrzése a beállított szabályoknak megfelelően.
   * @param args Argumentum tömb.
   * @return Saját maga.
   * @throws HelpException Ha egyetlen "?" argumentum van. 
   *         Jelzi hogy az argumentumokat beállító (felhasználó) segítséget szeretne kapni 
   *         a lehetséges argumentumokról, a program használatáról.
   * @throws ArgumentException Ha a kapott argumentumok nem felelnek meg a beállított szabályoknak.
   */
  public ArgumentHandler check(String[] args) throws HelpException, ArgumentException {

    if (checked) {
      throw new RuntimeException("A check() metódus csak egyszer hívható meg.");
    }
    checked = true;

    if (args == null) {
      throw new RuntimeException("Az 'args' paraméter nem lehet null.");
    }
    this.args = args;

    // help ? 
    if (args.length == 1 && args[0].equals("?")) {
      throw new HelpException();
    }

    // args szétszedése, map-be töltése
    for (String arg: args) {
      int p = arg.indexOf('=');
      if (p == -1) throw new ArgumentException(ArgumentException.Type.EQUAL_SIGN, arg);
      String argName = arg.substring(0, p);
      String argValue = arg.substring(p + 1);
      String previous = argsMap.put(argName, argValue);
      if (previous != null) throw new ArgumentException(ArgumentException.Type.MULTIPLE, argName);
      if (!ruleMap.containsKey(argName)) throw new ArgumentException(ArgumentException.Type.APPLICABLE, argName);
    }
    
    // végighaladás a szabályokon
    for (String name: ruleNames) {
      ArgumentRule rule = ruleMap.get(name);
      String value = argsMap.get(name);
      if (value != null && value.isEmpty()) {
        value = null;
      }
      if (value == null) value = rule.defaultValue;
      // kötelezőség ellenőrzése
      if (rule.required) {
        if (value == null) {
          throw new ArgumentException(ArgumentException.Type.REQUIRED, name);
        } 
      }
      // az argumentum érték a megengedett értékek között van ?
      if (value != null && rule.enabledValues != null) {
        if (!rule.enabledValues.contains(value)) {
          throw new ArgumentException(ArgumentException.Type.VALUE_GRANT, name, value);
        }
      } 
      // argumentum érték konvertálása a kívánt típusra
      if (value != null) {
        Object converted = null;
        try {
          if (rule.type == String.class) converted = value;
          if (rule.type == Integer.class) converted = Integer.valueOf(value);
          if (rule.type == Long.class) converted = Long.valueOf(value);
          if (rule.type == Double.class) converted = Double.valueOf(value);
          if (rule.type == BigInteger.class) converted = new BigInteger(value);
          if (rule.type == BigDecimal.class) converted = new BigDecimal(value);
          if (rule.type == Date.class) converted = rule.dateFormat.parse(value);
          if (rule.type == Boolean.class) {
            converted = rule.booleanValues.get(value);
            if (converted == null) throw new NumberFormatException();  
          }
          argsConvertedMap.put(name, converted);
        }
        catch (NumberFormatException | ParseException e) {
          throw new ArgumentException(ArgumentException.Type.VALUE_CONVERT, name, value, rule.type);
        }
      }      
    }
    checkResult = true;
    return this;

  }

  /**
   * String típusú argumentum értékének lekérdezése.
   * @param name Argumentum név.
   * @return Argumentum értéke String típusként, vagy null ha az argumentum nem volt megadva.
   */
  public String getString(String name) {

    return getInner(name, String.class, "getString");

  }

  /**
   * Integer típusú argumentum értékének lekérdezése.
   * @param name Argumentum név.
   * @return Argumentum értéke Integer típusként, vagy null ha az argumentum nem volt megadva.
   */
  public Integer getInteger(String name) {

    return getInner(name, Integer.class, "getInteger");

  }

  /**
   * Long típusú argumentum értékének lekérdezése.
   * @param name Argumentum név.
   * @return Argumentum értéke Long típusként, vagy null ha az argumentum nem volt megadva.
   */
  public Long getLong(String name) {
    
    return getInner(name, Long.class, "getLong");

  }

  /**
   * Double típusú argumentum értékének lekérdezése.
   * @param name Argumentum név.
   * @return Argumentum értéke Double típusként, vagy null ha az argumentum nem volt megadva.
   */
  public Double getDouble(String name) {
    
    return getInner(name, Double.class, "getDouble");
    
  }
  
  /**
   * BigInteger típusú argumentum értékének lekérdezése.
   * @param name Argumentum név.
   * @return Argumentum értéke BigInteger típusként, vagy null ha az argumentum nem volt megadva.
   */
  public BigInteger getBigInteger(String name) {
    
    return getInner(name, BigInteger.class, "getBigInteger");
    
  }

  /**
   * BigDecimal típusú argumentum értékének lekérdezése.
   * @param name Argumentum név.
   * @return Argumentum értéke BigDecimal típusként, vagy null ha az argumentum nem volt megadva.
   */
  public BigDecimal getBigDecimal(String name) {
    
    return getInner(name, BigDecimal.class, "getBigDecimal");
    
  }

  /**
   * Date típusú argumentum értékének lekérdezése.
   * @param name Argumentum név.
   * @return Argumentum értéke Date típusként, vagy null ha az argumentum nem volt megadva.
   */
  public Date getDate(String name) {
    
    return getInner(name, Date.class, "getDate");
    
  }

  /**
   * Boolean típusú argumentum értékének lekérdezése.
   * @param name Argumentum név.
   * @return Argumentum értéke Boolean típusként, vagy null ha az argumentum nem volt megadva.
   */
  public Boolean getBoolean(String name) {
    
    return getInner(name, Boolean.class, "getBoolean");
    
  }

  /**
   * A getString(), getInteger(), getLong(), getDate(), ... közös eljárása.
   * @param name Argumentum név.
   * @param type Argumentum típus.
   * @param methodName Hívó metódus megnevezése.
   * @return Argumentum értéke type típusként, vagy null ha az argumentum nem volt megadva.
   */
  private <T> T getInner(String name, Class<T> type, String methodName) {
    
    if (!checked) throw new RuntimeException(methodName + "() metódus nem hívható check() metódus meghívása előtt.");
    if (!checkResult) throw new RuntimeException(methodName + "() metódus nem hívható sikertelen check() metódus után.");
    if (!ruleMap.containsKey(name)) throw new RuntimeException("'" + name + "' argumentum név nem használható.");
    if (ruleMap.get(name).type != type) {
      throw new RuntimeException("'" + name + "' argumentum nem " + type.getSimpleName() + " típusú.");
    }
    return (T)argsConvertedMap.get(name);
    
  }

  /**
   * Szöveges információ az argumentum szabályokról, mely segíti az argumentumok összeállítóját.
   * @return Információ az argumentum szabályokról.
   */
  public StringBuilder help() {
    
    StringBuilder builder = new StringBuilder();
    boolean print = false;
    for (String name: ruleNames) {
      ArgumentRule rule = ruleMap.get(name);
      if (print) builder.append("\n");
      builder.append(rule.help());
      print = true;
    }
    return builder;
    
  }

  // ====
}
