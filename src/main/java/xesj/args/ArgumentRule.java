package xesj.args;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import xesj.tool.StringTool;

/**
 * Argumentumokra vonatkozó szabályokat kezelő osztály.
 */
public class ArgumentRule {

  /**
   * Szabály név. Megegyezik az argumentumban szereplő névvel.
   * Például ha az argumentum: "log=naplo.txt", akkor erre a "log" nevű szabály vonatkozik.
   */
  String name;

  /**
   * Kötelező-e az argumentum kitöltése ?
   * Például ha az argumentum: "log=", akkor ki van töltve a "log" argumentum, értéke üres string.
   */
  boolean required;

  /**
   * Argumentum default értéke. Akkor lép életbe ha nincs ilyen argumentum.
   */
  String defaultValue;

  /**
   * Argumentum típusa. A kapott string típusú argumentumot erre a típusra kell konvertálni. 
   */
  Class type;

  /**
   * Dátum formátum, a string típusú argumentum érték Date típusra történő alakításához.
   */
  SimpleDateFormat dateFormat;

  /**
   * Igaznak vagy hamisnak értékelendő string-ek, Boolean típusú argumentumnál.
   */
  Map<String, Boolean> booleanValues = new HashMap<>();

  /**
   * Engedélyezett argumentum értékek.
   */
  Set<String> enabledValues;

  /**
   * Argumentum szöveges leírása. Ha be van állítva akkor a segítség-szöveg fogja tartalmazni.
   */
  String description;

  /**
   * Sorváltás karakter.
   */
  private final String LF = "\n";

  /**
   * Engedélyezett argumentum típusok.
   */
  static final Class[] ENABLED_TYPES = {

    String.class,
    Integer.class,
    Long.class,
    Double.class,
    BigInteger.class,
    BigDecimal.class,
    Date.class,
    Boolean.class

  };

  /**
   * Konstruktor.
   * @param name Szabály név. Megegyezik az argumentumban szereplő névvel (argumentumnNév=argumentumÉrték).
   * @param type Argumentum típusa. A kapott string típusú argumentumot erre a típusra kell konvertálni. 
   *             Lehetséges argumentum típusok: 
   *             <ul>
   *               <li>String.class</li>
   *               <li>Integer.class</li>
   *               <li>Long.class</li>
   *               <li>Double.class</li> 
   *               <li>BigInteger.class</li>
   *               <li>BigDecimal.class</li> 
   *               <li>Date.class</li>
   *               <li>Boolean.class</li>
   *             </ul>
   * @param defaultValue Argumentum default értéke. Akkor lép életbe ha nincs kitöltve az argumentum. 
   *                     Ha nincs default érték akkor: null.
   * @param required Kötelező-e az argumentum kitöltése ? 
   *                 Például: ha az argumentum: "log=", akkor már ki van töltve a "log" argumentum, értéke üres string.
   */
  public ArgumentRule(String name, Class type, String defaultValue, boolean required) {

    // ellenőrzések
    if (StringTool.isNullOrEmpty(name)) throw new RuntimeException("A 'name' paraméter kötelező.");
    if (type == null) throw new RuntimeException("A 'type' paraméter kötelező.");
    boolean typeOk = false;
    for (Class enabledType: ENABLED_TYPES) {
      if (type == enabledType) typeOk = true;
    }
    if (!typeOk) {
      throw new RuntimeException("A 'type' paraméter nem lehet " + type.getName() + ", mivel ez a típus nem támogatott.");
    }

    // ellenőrzések vége
    this.name = name;
    this.type = type;
    this.defaultValue = defaultValue;
    this.required = required;

  }

  /**
   * Dátum formátum beállítása java.util.Date típusú argumentumnál. 
   * A metódus kizárólag java.util.Date típusú argumentum esetén hívható!
   * @param dateFormat Dátum formátum. Nem lehet null.
   * @return Saját maga.
   */
  public ArgumentRule datePattern(SimpleDateFormat dateFormat) {

    if (type != Date.class) {
      throw new RuntimeException("datePattern() metódus csak java.util.Date típusú argumentumnál hívható.");
    }

    if (dateFormat == null) {
      throw new RuntimeException("A 'dateFormat' paraméter kötelező.");
    }

    this.dateFormat = dateFormat;
    return this;
  }

  /**
   * Igaznak értékelendő string-ek beállítása csak Boolean típusú argumentumnál.
   * A metódus az előzőleg beállított igaznak értékelendő stringeket törli.
   * @param values Igaznak értékelendő string-ek. 
   *               Legalább egy elem megadása kötelező, null nem szerepelhet az elemek között.
   * @return Saját maga.
   */
  public ArgumentRule trueValues(String... values) {

    booleanValues(true, values);
    return this;

  }

  /**
   * Hamisnak értékelendő string-ek beállítása csak Boolean típusú argumentumnál.
   * A metódus az előzőleg beállított hamisnak értékelendő stringeket törli.
   * @param values Hamisnak értékelendő string-ek. 
   *               Legalább egy elem megadása kötelező, null nem szerepelhet az elemek között.
   * @return Saját maga.
   */
  public ArgumentRule falseValues(String... values) {

    booleanValues(false, values);
    return this;

  }

  /**
   * trueValues() és falseValues() metódusok közös eljárása.
   * @param bool True: trueValues() hívta, false: falseValues() hívta.
   * @param values Igaznak vagy hamisnak értékelendő stringek.
   */
  private void booleanValues(boolean bool, String... values) {

    if (type != Boolean.class) {
      throw new RuntimeException(
        (bool ? "trueValues()" : "falseValues()") + " metódus csak Boolean típusú argumentumnál hívható."
      );
    }

    // varargs korrekció
    if (values == null) values = new String[]{null};
    if (values.length == 0) throw new RuntimeException("A 'values' paraméter kötelező.");

    // bool-tól függően adatok kitörlése a map-ből
    for (String key: booleanValues.keySet()) {
      if (booleanValues.get(key) == bool) booleanValues.remove(key);
    }

    // értékek betöltése
    for (String value: values) {
      if (StringTool.isNullOrEmpty(value)) {
        throw new RuntimeException(
          (bool ? "Igaznak" : "Hamisnak") + " értékelendő string-ek között nem lehet null vagy üres string."
        );
      }
      if (booleanValues.containsKey(value)) {
        throw new RuntimeException(
          (bool ? "Igaznak" : "Hamisnak") +
          " értékelendő string már szerepel " +
          (bool ? "hamisnak" : "igaznak") +
          " értékelendő string-ek között: '" + value + "'"
        );
      }
      booleanValues.put(value, bool);
    }

  }

  /**
   * Engedélyezett argumentum értékek beállítása. 
   * A metódus az előzőleg beállított engedélyezett argumentum értékeket törli.
   * Boolean típusú argumentum esetén nem hívható (helyette trueValues(), és falseValues() szükséges). 
   * @param values Engedélyezett string értékek. Az értékek között nem lehet null érték.
   * @return Saját maga.
   */
  public ArgumentRule enabledValues(String... values) {

    if (type == Boolean.class) {
      throw new RuntimeException("enabledValues() metódus nem hívható Boolean típusú argumentumnál.");
    }
    enabledValues = new HashSet<>();

    // varargs korrekció
    if (values == null) values = new String[]{null};

    // betöltés
    for (String value: values) {
      if (StringTool.isNullOrEmpty(value)) {
        throw new RuntimeException("Engedélyezett attribútum értékek között nem lehet null vagy üres string.");
      }
      enabledValues.add(value);
    }
    return this;

  }

  /**
   * Argumentum szöveges leírásának beállítása. 
   * A szöveg az ArgumentHandler.help() metódusának hívásakor kerül be a segítség-szövegbe.
   * @param description Argumentum szöveges leírása.
   * @return Saját maga.
   */
  public ArgumentRule description(String description) {

    this.description = description;
    return this;

  }

  /**
   * Szöveges információ az argumentum szabályról, mely segíti az argumentumok összeállítóját.
   * @return Információ az argumentum szabályról.
   */
  StringBuilder help() {

    StringBuilder builder = new StringBuilder();
    builder.append("Argumentumnév: ").append(name).append(LF);
    if (description != null) {
      builder.append("Leírás: ").append(description).append(LF);
    }
    builder.append("Típus: ").append(type.getSimpleName()).append(LF);
    builder.append("Kötelező ? ").append(required ? "igen" : "nem").append(LF);
    if (defaultValue != null) {
      builder.append("Default érték: ").append("'").append(defaultValue).append("'").append(LF);
    }
    if (dateFormat != null) {
      builder.append("Dátum formátum: ").append(dateFormat.toPattern()).append(LF);
    }
    if (!booleanValues.isEmpty()) {
      helpBooleanValues(builder, true);
      helpBooleanValues(builder, false);
    }
    if (enabledValues != null) {
      builder.append("Engedélyezett értékek: ");
      boolean print = false;
      for (String s: enabledValues) {
        if (print) builder.append(", ");
        builder.append("'").append(s).append("'");
        print = true;
      }
      builder.append(LF);
    }
    return builder;

  }

  /**
   * Belső eljárás a help() igaz és hamis értékeinek kiírásához. 
   */
  private void helpBooleanValues(StringBuilder builder, boolean trueFalse) {

    builder.append(trueFalse ? "Igaz" : "Hamis").append(" értékek: ");
    boolean print = false;
    for (String key: booleanValues.keySet()) {
      if (booleanValues.get(key) == trueFalse) {
        if (print) builder.append(", ");
        builder.append("'").append(key).append("'");
        print = true;
      }
    }
    builder.append(LF);

  }

  /**
   * Az ArgumentRule példány helyességének ellenőrzése.
   * @throws RuntimeException Ha az ArgumentRule példány nincs megfelelően beállítva. 
   */
  void check() {

    if (type == Date.class && dateFormat == null) {
      throw new RuntimeException("Date típusú argumentumnál kötelező a dátum formátum megadása.");
    }

    if (type == Boolean.class && booleanValues.isEmpty()) {
      throw new RuntimeException(
        "Boolean típusú argumentumnál kötelező igaznak, vagy hamisnak értékelendő string-ek megadása."
      );
    }

  }

  // ====
}
