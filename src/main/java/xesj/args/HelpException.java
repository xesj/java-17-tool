package xesj.args;

/**
 * HelpException váltódik ki, ha egy program egyetlen "?" argumentummal lett elindítva, 
 * jelezve hogy a felhasználó a program lehetséges argumentumairól szeretne információkat kapni.
 */
public class HelpException extends Exception {
}
