package xesj.json;

/**
 * Json value konverziós hiba.<br/> 
 * Akkor váltódik ki, ha az adatot más adattípusba szeretnénk konvertálni, mint amilyet a json tartalmaz.
 */
public class JsonConversionException extends RuntimeException {

  /**
   * Konstruktor
   */
  public JsonConversionException(String message) {

    super(message);

  }
  
  // ==== 
}
