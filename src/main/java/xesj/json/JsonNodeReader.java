package xesj.json;
import com.fasterxml.jackson.databind.JsonNode;
import java.math.BigInteger;
import lombok.Getter;
import lombok.NonNull;

/**
 * Értékek olvasása JsonNode objektumból fejlettebb módon.<br/>
 * A metódusokban szereplő "path" paraméter elemei: kulcsot String-ként, tömbelemet int-ként kell megadni, például:<br/>
 * <code>jsonNodeReader.getInteger("tomb", 3)</code><br/>
 * BigDecimal típusú érték lekérdezése nincs megvalósítva, mert a JsonNode osztály pontatlanul olvassa ki az értéket.
 */
public class JsonNodeReader {
  
  /**
   * Belső objektum, mely a konstruktorban lett megadva.
   */
  @Getter
  private JsonNode jsonNode;
  
  /**
   * Konstruktor
   * @param jsonNode Ezen az objektumon lesznek a lekérdezés műveletek végrehajtva.
   */
  public JsonNodeReader(@NonNull JsonNode jsonNode) {
    
    this.jsonNode = jsonNode;
    
  }
  
  /**
   * Path alapján a gyökér JsonNode-on belüli JsonNode lekérdezése.
   */
  private JsonNode nodeFromPath(Object... path) {
    
    // Iteráció a path elemeken, node meghatározása
    JsonNode n = jsonNode;
    for (Object p: path) {
      if (p instanceof String s) {
        n = n.path(s);
      }
      else if (p instanceof Integer i) {
        n = n.path(i);
      }
      else {
        throw new RuntimeException("A path elemei csak String vagy int típusúak lehetnek!");
      }
    }
    
    // Válasz
    return n;
    
  }
  
  /**
   * Létezik a path a json-ben ?
   * @param path Json path, melynek a létezését lekérdezzük. String és int elemekből állhat.
   * @return True: Létezik a path, meg van adva a json-ben (az értéke lehet null is). False: Nem létezik a path.
   */
  public boolean exist(@NonNull Object... path) {
    
    return !nodeFromPath(path).isMissingNode();
    
  }
  
  /**
   * Integer érték lekérdezése.
   * @param path Json path, melynek az értékét lekérdezzük. String és int elemekből állhat.
   * @return Integer érték. Ha a path nem létezik, vagy az értéke null, akkor null.
   * @throws JsonConversionException Ha az érték nem Integer típusú.
   */
  public Integer getInteger(@NonNull Object... path) throws JsonConversionException {
    
    // Node meghatározása
    JsonNode node = nodeFromPath(path);
    
    // Ha a node nem létezik, vagy null értékű
    if (node.isMissingNode() || node.isNull()) {
      return null;
    }
    
    // Érték lekérdezése
    if (node.isInt()) {
      return node.asInt();
    }
    else {
      throw new JsonConversionException("Az adat típusa nem Integer!");
    }
    
  }

  /**
   * Long érték lekérdezése.
   * @param path Json path, melynek az értékét lekérdezzük. String és int elemekből állhat.
   * @return Long érték. Ha a path nem létezik, vagy az értéke null, akkor null.
   * @throws JsonConversionException Ha az érték nem Long típusú.
   */
  public Long getLong(@NonNull Object... path) throws JsonConversionException {
    
    // Node meghatározása
    JsonNode node = nodeFromPath(path);
    
    // Ha a node nem létezik, vagy null értékű
    if (node.isMissingNode() || node.isNull()) {
      return null;
    }
    
    // Érték lekérdezése
    if (node.isInt() || node.isLong()) {
      return node.asLong();
    }
    else {
      throw new JsonConversionException("Az adat típusa nem Long!");
    }
    
  }

  /**
   * Double érték lekérdezése.
   * @param path Json path, melynek az értékét lekérdezzük. String és int elemekből állhat.
   * @return Double érték. Ha a path nem létezik, vagy az értéke null, akkor null.
   * @throws JsonConversionException Ha az érték nem Double típusú.
   */
  public Double getDouble(@NonNull Object... path) throws JsonConversionException {
    
    // Node meghatározása
    JsonNode node = nodeFromPath(path);
    
    // Ha a node nem létezik, vagy null értékű
    if (node.isMissingNode() || node.isNull()) {
      return null;
    }
    
    // Érték lekérdezése
    if (node.isInt() || node.isLong() || node.isDouble()) {
      return node.asDouble();
    }
    else {
      throw new JsonConversionException("Az adat típusa nem Double!");
    }
    
  }

  /**
   * BigInteger érték lekérdezése.
   * @param path Json path, melynek az értékét lekérdezzük. String és int elemekből állhat.
   * @return BigInteger érték. Ha a path nem létezik, vagy az értéke null, akkor null.
   * @throws JsonConversionException Ha az érték nem BigInteger típusú.
   */
  public BigInteger getBigInteger(@NonNull Object... path) throws JsonConversionException {
    
    // Node meghatározása
    JsonNode node = nodeFromPath(path);
    
    // Ha a node nem létezik, vagy null értékű
    if (node.isMissingNode() || node.isNull()) {
      return null;
    }
    
    // Érték lekérdezése
    if (node.isInt() || node.isLong() || node.isBigInteger()) {
      return node.bigIntegerValue();
    }
    else {
      throw new JsonConversionException("Az adat típusa nem BigInteger!");
    }
    
  }

  /**
   * String érték lekérdezése.
   * @param path Json path, melynek az értékét lekérdezzük. String és int elemekből állhat.
   * @return String érték. Ha a path nem létezik, vagy az értéke null, akkor null.
   * @throws JsonConversionException Ha az érték nem String típusú.
   */
  public String getString(@NonNull Object... path) throws JsonConversionException {
    
    // Node meghatározása
    JsonNode node = nodeFromPath(path);
    
    // Ha a node nem létezik, vagy null értékű
    if (node.isMissingNode() || node.isNull()) {
      return null;
    }
    
    // Érték lekérdezése
    if (node.isTextual()) {
      return node.asText();
    }
    else {
      throw new JsonConversionException("Az adat típusa nem String!");
    }
    
  }

  /**
   * Boolean érték lekérdezése.
   * @param path Json path, melynek az értékét lekérdezzük. String és int elemekből állhat.
   * @return Boolean érték. Ha a path nem létezik, vagy az értéke null, akkor null.
   * @throws JsonConversionException Ha az érték nem Boolean típusú.
   */
  public Boolean getBoolean(@NonNull Object... path) throws JsonConversionException {
    
    // Node meghatározása
    JsonNode node = nodeFromPath(path);
    
    // Ha a node nem létezik, vagy null értékű
    if (node.isMissingNode() || node.isNull()) {
      return null;
    }
    
    // Érték lekérdezése
    if (node.isBoolean()) {
      return node.asBoolean();
    }
    else {
      throw new JsonConversionException("Az adat típusa nem Boolean!");
    }
    
  }
  
  // ====
}
