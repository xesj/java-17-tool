package xesj.long_process;

/**
 * Exception: jelzi hogy a hosszan tartó folyamat BREAK státuszba kerül.
 */
public class BreakException extends Exception {}