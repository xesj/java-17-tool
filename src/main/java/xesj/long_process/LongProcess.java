package xesj.long_process;
import java.time.LocalDateTime;
import xesj.tool.ExceptionTool;
import xesj.tool.ThreadTool;

/**
 * Hosszan tartó folyamat ős osztálya.
 * Hívási lánc, melyből a leszármazott csak a runInner()-t valósítja meg: 
 * run() -> abstract runInner() -> listen()
 */
public abstract class LongProcess implements Runnable {
  
  /**
   * Státusz
   */
  private Status status;

  /**
   * Kérés
   */
  private Request request;
  
  /**
   * Folyamat kezdésének ideje
   */
  private LocalDateTime start;

  /**
   * Folyamat befejezésének ideje
   */
  private LocalDateTime end;

  /**
   * Üzenet
   */
  protected String message;

  /**
   * Folyamat haladása százalékban
   */
  protected Integer progress;
  
  /**
   * Folyamat hibás befejezése esetén a stack trace
   */
  private String trace;
  
  /**
   * Konstruktor
   */
  protected LongProcess() {
    
    status = Status.NEW;
    
  };
  
  /**
   * Folyamat státusza.
   */
  public final Status getStatus() {
    
    return status;
    
  };
  
  /**
   * Kérés törlése. Nem okoz problémát, ha nem volt még kérés.
   */
  public final void clearRequest() {
    
    request = null;
    
  }

  /**
   * Kérés a folyamathoz, lépjen át RUN státuszba. 
   * Csak akkor kérhető, ha a folyamat NEW, SUCCESS, ERROR, vagy BREAK státuszban van.
   * @throws RequestException Ha a kérés és a folyamat státusza nem felel meg egymásnak.
   */
  public final void runRequest() throws RequestException {
    
    // Kérés ellenőrzése
    if (status == Status.RUN || status == Status.PAUSE) {
      throw new RequestException(Request.RUN, status);
    }
    
    // Kérés bejegyzése és végrehajtása 
    request = Request.RUN;
    Thread thread = new Thread(this);
    thread.start();
    
  }
  
  /**
   * Kérés a folyamathoz, lépjen át PAUSE státuszba. 
   * Csak akkor kérhető, ha a folyamat RUN státuszban van.
   * @throws RequestException Ha a kérés és a folyamat státusza nem felel meg egymásnak.
   */
  public final void pauseRequest() throws RequestException {
    
    // Kérés ellenőrzése
    if (status != Status.RUN) {
      throw new RequestException(Request.PAUSE, status);
    }
    
    // Kérés bejegyzése
    request = Request.PAUSE;
    
  }

  /**
   * Kérés a folyamathoz, lépjen át PAUSE státuszból RUN státuszba. 
   * Csak akkor kérhető, ha a folyamat PAUSE státuszban van.
   * @throws RequestException Ha a kérés és a folyamat státusza nem felel meg egymásnak.
   */
  public final void continueRequest() throws RequestException {
    
    // Kérés ellenőrzése
    if (status != Status.PAUSE) {
      throw new RequestException(Request.CONTINUE, status);
    }
    
    // Kérés bejegyzése
    request = Request.CONTINUE;
    
  }

  /**
   * Kérés a folyamathoz, szakítsa meg a további műveletek végzését, és álljon meg (lépjen át BREAK státuszba). 
   * Csak akkor kérhető, ha a folyamat RUN, vagy PAUSE státuszban van.
   * @throws RequestException Ha a kérés és a folyamat státusza nem felel meg egymásnak.
   */
  public final void breakRequest() throws RequestException {
    
    // Kérés ellenőrzése
    if (status != Status.RUN && status != Status.PAUSE) {
      throw new RequestException(Request.BREAK, status);
    }
    
    // Kérés bejegyzése
    request = Request.BREAK;
    
  }

  /**
   * Kérés lekérdezése. Ha nincs ilyen, akkor null.
   */   
  public final Request getRequest() {
    
    return request;
    
  };

  /**
   * Folyamat indításának ideje.
   */  
  public final LocalDateTime getStart() {
    
    return start;
    
  }
  
  /**
   * Folyamat befejezésének (megszakításának) az ideje.      
   */
  public final LocalDateTime getEnd() {
    
    return end;
    
  }

  /**
   * A folyamat haladásának lekérdezése százalékban. Ha nem határozható meg, akkor null.
   */  
  public final Integer getProgress() {
   
    return progress;
    
  }

  /**
   * Az épp most folyó tevékenység megnevezése, leírása.
   */  
  public final String getMessage() {
    
    return message;
    
  }
  
  /**
   * Stack trace lekérdezése, ha a folyamat ERROR státuszba került.
   */ 
  public final String getTrace() {
    
    return trace;
    
  }

  /**
   * Státusz módosítása, és a kérés törlése
   */
  private void setStatus(Status status) {
    
    this.status = status;
    this.request = null;
    
  }
  
  /**
   * Futás
   */
  @Override
  public final void run() {
    
    // Előkészítés
    setStatus(Status.RUN);
    start = LocalDateTime.now();
    end = null;
    trace = null;

    // Belső eljárás futtatása
    try {    
      runInner();
      setStatus(Status.SUCCESS);
    }  
    catch (BreakException be) {
      setStatus(Status.BREAK);
    }
    catch (Exception e) {
      trace = ExceptionTool.traceString(e);
      setStatus(Status.ERROR);
    }
    end = LocalDateTime.now();
    
  }
  
  /**
   * Státusz kérések figyelése, kiszolgálása.
   * A metódust csak a runInner() hívhatja meg futás közben.
   */
  protected final void listen() throws BreakException {
    
    // A kérés BREAK ?
    if (request == Request.BREAK) {
      throw new BreakException();
    }

    // A kérés PAUSE ?
    if (request == Request.PAUSE) {
      setStatus(Status.PAUSE);
      while (true) {
        // Várakozás
        ThreadTool.sleep(2000);
        // Átváltott a kérés BREAK-re ?
        if (request == Request.BREAK) {
          throw new BreakException();
        }
        // Átváltott a kérés CONTINUE-ra ?
        if (request == Request.CONTINUE) {
          setStatus(Status.RUN);
          return;
        }
      }      
    }
    
  }
  
  /**
   * Folyamat megnevezése, rövid leírása. 
   * Állandó érték, nem változik.
   */
  public abstract String getName();
  
  /**
   * A folyamathoz tartozó jogosultság. 
   * Ha nincs hozzá jog, akkor null. 
   * Időben nem változik, egy felhasználóhoz állandó.
   */
  public abstract Permission getPermission();
  
  /**
   * A folyamat naplójának lekérdezése, mely az egész tevékenységét tartalmazza.
   * Vagy hivatkozás egy másik helyre, például adatbázisra vagy fájlra. 
   * A napló megvalósítása tetszőleges, nem biztos hogy StringBuilder-rel oldja meg a leszármazott.  
   */
  public abstract String getLog();
  
  /**
   * A folyamat belső magja. 
   * Csak a "message", "percent" változókat, és a naplót kezeli, és rendszeresen meghívja a listen() metódust. 
   * A listen()-t csak akkor szabad meghívnia, ha meg lehet szakítani a folyamatot!
   * Fel kell készülnie arra, hogy lefutás után ismét meghívhatják.
   */
  protected abstract void runInner() throws BreakException;
  
  // ====
}
