package xesj.long_process;

/**
 * Jogosultság a hosszan tartó folyamathoz.
 */
public enum Permission {

  /**
   * A folyamat csak megtekinthető, semmiféle módosítás nem engedélyezett.
   */  
  READ("megtekintés"),
  
  /**
   * A folyamaton bármely művelet elvégezhető.
   */
  ALL("teljes hozzáférés");
  
  /**
   * Leírás
   */
  public final String description;

  /**
   * Konstruktor
   */
  private Permission(String description) {
    
    this.description = description;
    
  }  
  
  // ====
}
