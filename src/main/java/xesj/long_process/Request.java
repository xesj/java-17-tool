package xesj.long_process;

/**
 * Kérés a hosszan tartó folyamathoz
 */
public enum Request {
  
  /**
   * Futtatás kérés
   */
  RUN("futtatás"),
  
  /**
   * Szünet kérés
   */
  PAUSE("szünet"),

  /**
   * Folytatás kérés
   */
  CONTINUE("folytatás"),
  
  /**
   * Megszakítás kérés
   */
  BREAK("megszakítás"),

  /**
   * Kérés törlése
   */
  CLEAR("kérés törlése");
  
  /**
   * Leírás
   */
  public final String description;

  /**
   * Konstruktor
   */
  private Request(String description) {
    
    this.description = description;
    
  }  
  
  // ====
}
