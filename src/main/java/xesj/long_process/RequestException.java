package xesj.long_process;

/**
 * Exception: nem engélyezett kérés
 */
public class RequestException extends Exception {
  
  /**
   * Konstruktor: egy kérés és a státusz egymásnak nem megfelelő.
   */
  public RequestException(Request request, Status status) {
    
    super(
      "A hosszan tartó folyamat státusza '" + status.description + "'" +
      ", ezért nem tud fogadni '" + request.description + "' kérést!"
    );
    
  }
  
  // ====  
}
