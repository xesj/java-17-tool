package xesj.long_process;

/**
 * Hosszan tartó folyamat státusza
 */
public enum Status {
  
  /**
   * Még sosem volt elindítva
   */
  NEW("új"),
  
  /**
   * Fut
   */
  RUN("fut"),
  
  /**
   * Szünetel. Futott, de meg lett állítva egy időre.
   */
  PAUSE("szünetel"),
  
  /**
   * Sikeresen lefutott
   */
  SUCCESS("sikeresen véget ért"),

  /**
   * A futó folyamatban exception keletkezett, emiatt a folyamat megszakadt
   */
  ERROR("hibával ért véget"),
  
  /**
   * A futó folyamat a felhasználó által meg lett szakítva
   */
  BREAK("megszakítással ért véget");
  
  /**
   * Leírás
   */
  public final String description;

  /**
   * Konstruktor
   */
  private Status(String description) {
    
    this.description = description;
    
  }  
  
  // ====
}
