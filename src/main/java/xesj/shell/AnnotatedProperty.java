package xesj.shell;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Annotációval ellátott osztályok jellemzőit tartalmazza.
 */
public class AnnotatedProperty implements Serializable {

  /**
   * Osztály melyből készült a property; 
   */
  Class cls;

  /** 
   * A @Table annotáció táblanév paramétere. Ha ilyen annotációval nincs ellátva az osztály, akkor null. 
   */
  String tableName;

  /** 
   * A @View annotáció viewnév paramétere. Ha ilyen annotációval nincs ellátva az osztály, akkor null. 
   */
  String viewName;

  /**
   * Oszlopok és tulajdonságaik.
   */
  List<ColumnProperty> columnPropertyList = new ArrayList<>();

  /**
   * A columnPropertyList-et indexelő tömb, mely id-oszlopokra mutat. A @Id annotációk alapján lett meghatározva.
   */
  int[] ids;

  /**
   * SQL-select parancs. Csak akkor létezik ha van @Table vagy @View annotáció, és @Id annotáció is, különben null.
   */
  String select;  

  /**
   * SQL-insert parancs. Csak akkor létezik ha van @Table vagy @View annotáció, különben null.
   */
  String insert;  

  /**
   * SQL-update parancs. Csak akkor létezik ha van @Table vagy @View annotáció, és @Id annotáció is, különben null.
   */
  String update;

  /**
   * SQL-delete parancs. Csak akkor létezik ha van @Table vagy @View annotáció, és @Id annotáció is, különben null.
   */
  String delete;

  /**
   * Konstruktor
   */
  AnnotatedProperty(Class cls) {

    this.cls = cls;

  }
  
  /**
   * Táblanév vagy viewnév lekérdezése, ha egyik sincs akkor null.
   */
  String getTableViewName() {

    if (tableName != null) return tableName;
    if (viewName != null) return viewName;
    return null;

  }

  /**
   * Létezik-e id ?
   */
  boolean isId() {

    return (ids != null && ids.length > 0);

  }

  // ====
}
