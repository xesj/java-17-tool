package xesj.shell;
import java.io.Serializable;

/**
 * Oszlop tulajdonságai.
 */
public class ColumnProperty implements Serializable {

  /**
   * Az oszlop neve ahogy az SQL-select -ből kapjuk, vagy a @Column annotációval ellátott mezőnév.
   */
  public final String name;

  /**
   * Az oszlop java típusa (az az osztály melybe be kell majd olvasni az adatot).
   */
  public final Class cls;

  /**
   * Az oszlop java-SQL típusa.
   */
  public Integer sqlType;

  /**
   * Az oszlophoz tartozó precision.
   */
  public Integer precision;

  /**
   * Az oszlophoz tartozó scale.
   */
  public Integer scale;

  /**
   * Az oszlop adatbázis specifikus típus neve.
   */
  public String dbType;

  /**
   * Konstruktor.
   * @param name Oszlop neve.
   * @param cls Oszlop típusa(osztálya).
   * @param sqlType Oszlop java-SQL típusa.
   * @param precision Oszlophoz tartozó precision.
   * @param scale Oszlophoz tartozó scale.
   */
  public ColumnProperty(String name, Class cls, String dbType, int precision, int scale, int sqlType) {

    this.name = name;
    this.cls = cls;
    this.dbType = dbType;
    this.precision = precision;
    this.scale = scale;
    this.sqlType = sqlType;

  }

  /**
   * Konstruktor.
   * @param name Oszlop neve.
   * @param cls Oszlop típusa(osztálya).
   */
  public ColumnProperty(String name, Class cls) {

    this.name = name;
    this.cls = cls;

  }

  // ====
}
