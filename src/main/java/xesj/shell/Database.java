package xesj.shell;
import java.io.Serializable;

/**
 * Adatbázis jellemzők.
 */
public class Database implements Serializable {

  /**
   * Adatbázis neve.
   */
  private String name;

  /**
   * Adatbázisra alkalmazható null-stratégia.
   */
  private NullStrategy nullStrategy;

  /**
   * Oracle adatbázis típus.
   */
  public static final Database Oracle;

  /**
   * PostgreSQL adatbázis típus.
   */
  public static final Database PostgreSQL;

  /**
   * Static blokk.
   */
  static {

    Oracle = new Database("Oracle", NullStrategy.SET_NULL_WITH_NULL_TYPE);
    PostgreSQL = new Database("PostgreSQL", NullStrategy.SET_OBJECT_WITH_NULL);

  }

  /**
   * Konstruktor.
   * Akkor használjuk ha nem előre definiált adatbázis típussal dolgozunk.
   * @param name Adatbázis megnevezése.
   * @param nullStrategy Meghatározza hogy null érték adatbázisba töltése esetén milyen módon történjen a művelet.
   */
  public Database(String name, NullStrategy nullStrategy) {

    if (nullStrategy == null) {
      throw new ShellException("A 'nullStrategy' paraméter nem lehet null.");
    }
    this.name = name;
    this.nullStrategy = nullStrategy;

  }

  /**
   * Adatbázis megnevezésének lekérdezése.
   * @return Adatbázis megnevezése.
   */
  public String getName() {

    return name;

  }

  /**
   * Null érték adatbázisba töltésének módját kérdezi le.
   * @return Null érték adatbázisba töltésének módja.
   */
  public NullStrategy getNullStrategy() {

    return nullStrategy;

  }

  // ====
}
