package xesj.shell;
import java.io.Serializable;

/**
 * Ez az exception akkor váltódik ki, amikor az SQL osztály getRow() metódusa egynél több eredménysort talál,
 * és a maximális sorok száma is egynél nagyobbra van állítva.
 */
public class MoreRowsException extends ShellException implements Serializable {

  /**
   * Konstruktor.
   * @param message Hibaüzenet. 
   */
  MoreRowsException(String message) {

    super(message);

  }

  // ====
}
