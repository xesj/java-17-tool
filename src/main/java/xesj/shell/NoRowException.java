package xesj.shell;
import java.io.Serializable;

/**
 * Ez az exception akkor váltódik ki, amikor az SQL osztály getRow() metódusa egyetlen eredménysort sem talál.
 */
public class NoRowException extends ShellException implements Serializable {

  /**
   * Konstruktor.
   * @param message Hibaüzenet. 
   */
  NoRowException(String message) {

    super(message);

  }

  // ====
}
