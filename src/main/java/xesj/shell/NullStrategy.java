package xesj.shell;
import java.io.Serializable;

/**
 * Meghatározza hogy null érték adatbázisba töltése esetén milyen módon történjen a művelet.
 */
public enum NullStrategy implements Serializable {

  /**
   * Adatbázis művelet null érték írásakor: <code>PreparedStatement.setNull(..., java.sql.Types.NULL)</code>
   */
  SET_NULL_WITH_NULL_TYPE,

  /**
   * Adatbázis művelet null érték írásakor: <code>PreparedStatement.setObject(..., null)</code>
   */
  SET_OBJECT_WITH_NULL

}
