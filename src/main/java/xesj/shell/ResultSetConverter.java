package xesj.shell;
import java.sql.ResultSet;

/**
 * ResultSet konvertáló interface.
 * @param <T> Objektum típus melyre konvertálódik a ResultSet egy sora.
 */
public interface ResultSetConverter<T> {

  /**
   * ResultSet konvertálása T-objektumra.
   * @param resultSet Konvertálandó ResultSet.
   * @param rowNumber Lekérdezéssel kapott sor sorszáma (1-től kezdődik).
   * @return Konvertálás után előállt T-objektum.
   */
  public T convert(ResultSet resultSet, int rowNumber);

  // ====
}
