package xesj.shell;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import xesj.tool.DateTool;
import xesj.tool.StringTool;

/**
 * SQL parancs tároló, végrehajtó osztály.
 */
public class SQL implements Serializable, Closeable {

  /** 
   * Shell, melynek környezetében fut az sql.
   */
  private final Shell shell;

  /** 
   * Eredeti SQL-parancs. 
   */
  private final String originalSql; 

  /** 
   * Eredeti SQL-parancs JDBC kérdőjeles formára alakítva.
   */
  private String jdbcSql; 

  /** 
   * Paraméterek. Nem tartalmazzák a kettőspontot.
   */
  private final ArrayList<String> parameters = new ArrayList<>(); 

  /** 
   * Paraméterek feltöltve, setParameter(), setAllParameters() metódusok által ? 
   */
  private final ArrayList<Boolean> parametersOk = new ArrayList<>(); 

  /** 
   * SQL-select paranccsal kapott sorok számának maximuma. Legalábbb 1-nek kell lenni. 
   * Hatással van a getRow() és getList() metódusokra.
   */
  private long maximumRows = Long.MAX_VALUE;

  /**
   * Végrehajtás statement-je.
   */
  private PreparedStatement statement; 

  /**
   * SQL-select eredményének ResultSet-e.
   */
  private ResultSet resultSet; 

  /**
   * Már sikeresen lefutott végrehajtás metódusa pl: "execute()", "getRow()".
   */
  private String method; 

  /**
   * Megnyitott InputStream-ek.
   */
  private final ArrayList<InputStream> openedStream = new ArrayList<>(); 

  /**
   * Megnyitott Reader-ek.
   */
  private final ArrayList<Reader> openedReader = new ArrayList<>(); 

  /**
   * Az SQL parancs SELECT ?
   */
  private boolean isSelect;

  /**
   * Jelzi hogy getList() esetén több sort kapnánk a lekérdezéssel mint amit a maximumRows paraméter előírt.
   */
  private boolean isMoreRows;

  /**
   * execute() metódus végrehajtásával érintett sorok száma.
   */
  private Long updateCount;

  /**
   * Oszlopok és jellemzőik, mely alapján a beolvasást el kell végezni a ResultSet-ből.
   */
  private List<ColumnProperty> columnPropertyList;

  /**
   * Konstruktor.
   * @param shell Adatbázis shell.
   * @param originalSql SQL-parancs.
   */
  SQL(Shell shell, String originalSql) {

    this.shell = shell;
    this.originalSql = originalSql;
    if (shell == null) throw new ShellException("A 'shell' paraméter null.");
    if (originalSql == null || originalSql.isEmpty()) {
      throw new ShellException("Az SQL parancs nem lehet null, vagy üres.");
    }
    if (originalSql.trim().toLowerCase().startsWith("select")) isSelect = true;
    // kettősponttal jelölt paraméterek meghatározása
    boolean e = true;
    // jelzi hogy az adott string pozició előtt páros számú aposztróf volt-e
    boolean[] even = new boolean[originalSql.length()]; 
    for (int i = 0; i < originalSql.length(); i++) {
      even[i] = e;
      if (originalSql.charAt(i) == '\'') e = !e;
    }
    //
    StringBuilder jdbcSqlBuilder = new StringBuilder();
    int copy = 0;
    Matcher matcher = Pattern.compile(":[a-z_0-9]+", Pattern.CASE_INSENSITIVE).matcher(originalSql);
    while (matcher.find()) {
      int start = matcher.start();
      int end = matcher.end();
      if (even[start]) {
        parameters.add(originalSql.substring(start + 1, end));
        parametersOk.add(false);
        jdbcSqlBuilder.append(originalSql.substring(copy, start)).append("?");
        copy = end;
      }  
    }
    jdbcSqlBuilder.append(originalSql.substring(copy));
    // jdbcSql előállítása
    jdbcSql = jdbcSqlBuilder.toString();
    // PreparedStatement létrehozása
    try {
      statement = shell.getConnection().prepareStatement(jdbcSql);
    }
    catch (SQLException ex) {
      throw new RuntimeException(ex);
    }

  }

  /**
   * SQL parancs egy paraméterének beállítása.
   * @param parameterName Paraméter az SQL-parancsban (kettőspont nélkül kell megadni).
   * @param parameterObject Paraméter helyére illesztendő objektum.
   * @return Saját maga.
   */
  public SQL setParameter(String parameterName, Object parameterObject) {

    setParameterInner(parameterName, null, parameterObject);
    return this;

  }

  /**
   * SQL parancs egy paraméterének beállítása.
   * @param parameterName Paraméter az SQL-arancsban (például ':23' esetén 23-at kell megadni).
   * @param parameterObject Paraméter helyére illesztendő objektum.
   * @return Saját maga.
   */
  public SQL setParameter(int parameterName, Object parameterObject) {

    setParameterInner(String.valueOf(parameterName), null, parameterObject);
    return this;

  }

  /**
   * SQL parancs összes paraméterének beállítása egyben.
   * @param parameterObjects Paraméterek helyére illesztendő objektumok.
   * @return Saját maga.
   */
  public SQL setAllParameters(Object... parameterObjects) {

    // setAllParameters(null) helyzet lekezelése
    if (parameterObjects == null) parameterObjects = new Object[]{null}; 
    // ellenőrzés
    if (parameterObjects.length != parameters.size()) {
      throw new ShellException(
        "Az SQL-parancs " + parameters.size() + " paraméterrel rendelkezik, de a setAllParameters() metódusban " + 
          parameterObjects.length + " paraméter van megadva."
      );
    }
    // behelyettesítés
    for (int parameterIndex = 1; parameterIndex <= parameterObjects.length; parameterIndex++) {
      setParameterInner(null, parameterIndex, parameterObjects[parameterIndex - 1]);
    }
    return this;

  }

  /**
   * SQL parancs paraméterének beállítása. A paraméter név és index közül pontosan az egyiket kell megadni.
   * @param parameterName Paraméter neve (kettőspont nélkül).
   * @param parameterIndex Paraméter indexe (1-től kezdődik).
   * @param parameterObject Paraméter helyére illesztendő objektum.
   */
  private void setParameterInner(String parameterName, Integer parameterIndex, Object parameterObject) {

    // már végre lett hajtva az SQL ?
    if (method != null) {
      throw new ShellException(
        "Paraméter beállítás nem lehetséges, mert az SQL már végre lett hajtva " + method + " metódussal."
      );
    }
    // a parameterName és parameterIndex közül pontosan az egyik van kitöltve ?
    if (parameterName == null && parameterIndex == null || parameterName != null && parameterIndex != null) {
      throw new ShellException("A parameterName és parameterIndex közül pontosan az egyiknek kell kitöltve lennie.");
    }
    // behelyettesítés
    if (parameterIndex != null) {
      setParameterInner2(parameterIndex, parameterObject);
    }
    else {
      // létezik ilyen nevű paraméter ?
      if (!parameters.contains(parameterName)) {
        throw new ShellException("Nem létezik '" + parameterName + "' nevű paraméter.");
      }
      for (int i = 0; i < parameters.size(); i++) {
        if (parameters.get(i).equals(parameterName)) {
          setParameterInner2(i + 1, parameterObject);
        }  
      }  
    }

  }

  /**
   * SQL-parancs adott indexű paraméterének beállítása. Csak a setParameterInner()-ből hívjuk !
   * @param parameterIndex Paraméter indexe (1-től kezdődik).
   * @param parameterObject Paraméter helyére illesztendő objektum.
   */
  private void setParameterInner2(int parameterIndex, Object parameterObject) {

    String parameterName = parameters.get(parameterIndex - 1);
    if (parametersOk.get(parameterIndex - 1)) {
      throw new ShellException("A(z) '" + parameterName + "' paraméter már be lett állítva.");
    }
    try {
      if (parameterObject == null) {
        // obj == null
        NullStrategy ns = shell.getDatabase().getNullStrategy();
        if (ns == NullStrategy.SET_NULL_WITH_NULL_TYPE) {
          statement.setNull(parameterIndex, java.sql.Types.NULL);
        }
        else if (ns == NullStrategy.SET_OBJECT_WITH_NULL) {
          statement.setObject(parameterIndex, null);
        }
        else {
          throw new ShellException("Ismeretlen NullStrategy: " + ns + ".");
        }
      }
      else {
        // obj != null
        if (parameterObject instanceof Short) {
          statement.setShort(parameterIndex, (Short)parameterObject);
        }
        else if (parameterObject instanceof Integer) {
          statement.setInt(parameterIndex, (Integer)parameterObject);
        }
        else if (parameterObject instanceof Long) {
          statement.setLong(parameterIndex, (Long)parameterObject);
        }
        else if (parameterObject instanceof Float) {
          statement.setFloat(parameterIndex, (Float)parameterObject);
        }
        else if (parameterObject instanceof Double) {
          statement.setDouble(parameterIndex, (Double)parameterObject);
        }
        else if (parameterObject instanceof BigDecimal) {
          statement.setBigDecimal(parameterIndex, (BigDecimal)parameterObject);
        }
        else if (parameterObject instanceof BigInteger) {
          statement.setBigDecimal(parameterIndex, new BigDecimal((BigInteger)parameterObject));
        }
        else if (parameterObject instanceof Time) {
          statement.setTime(parameterIndex, (Time)parameterObject);
        }
        else if (parameterObject instanceof Timestamp) {
          statement.setTimestamp(parameterIndex, (Timestamp)parameterObject);
        }
        else if (parameterObject instanceof java.sql.Date) {
          statement.setDate(parameterIndex, (java.sql.Date)parameterObject);
        }
        else if (parameterObject instanceof java.util.Date) {
          statement.setDate(parameterIndex, DateTool.toSqlDate((java.util.Date)parameterObject));
        }
        else if (parameterObject instanceof Boolean) {
          statement.setBoolean(parameterIndex, (Boolean)parameterObject);
        }
        else if (parameterObject instanceof String) {
          statement.setString(parameterIndex, (String)parameterObject);
        }
        else if (parameterObject instanceof Blob) {
          statement.setBlob(parameterIndex, (Blob)parameterObject);
        }
        else if (parameterObject instanceof Clob) {
          statement.setClob(parameterIndex, (Clob)parameterObject);
        }
        else if (parameterObject instanceof Reader) {
          Reader reader = (Reader)parameterObject;
          openedReader.add(reader);
          statement.setCharacterStream(parameterIndex, reader);
        }
        else if (parameterObject instanceof File) {
          File file = (File)parameterObject;
          String tmpMessage = "A(z) '" + parameterName + "' paraméter által címzett '" + file.getPath() + "' fájl ";
          if (!file.exists()) throw new ShellException(tmpMessage + "nem létezik.");
          if (file.isDirectory()) throw new ShellException(tmpMessage + "egy könyvtár.");
          if (!file.canRead()) throw new ShellException(tmpMessage + "nem olvasható.");
          int length = (int)file.length();
          FileInputStream fis = new FileInputStream(file);
          openedStream.add(fis);
          statement.setBinaryStream(parameterIndex, fis, length);
        }
        else {
          statement.setObject(parameterIndex, parameterObject);
        }
      }
    }
    catch (SQLException | FileNotFoundException e) {
      throw new RuntimeException(e);
    }
    parametersOk.set(parameterIndex - 1, true);

  }

  /**
   * Az SQL-parancsban szerepel-e az adott paraméter ?
   * @param parameterName Vizsgálandó paraméter név (kettőspont nélkül).
   * @return True: a paraméter szerepel az SQL parancsban, különben false.
   */
  public boolean isParameter(String parameterName) {

    return parameters.contains(parameterName);

  }

  /**
   * Az SQL-parancsban szerepel-e az adott paraméter ?
   * @param parameterName A vizsgálandó paraméter név (például ':23' esetén 23-at kell megadni).
   * @return True: a paraméter szerepel az SQL parancsban, különben false.
   */
  public boolean isParameter(int parameterName) {

    return isParameter(String.valueOf(parameterName));    

  }

  /**
   * Egy paraméter már be van állítva az SQL-parancsban ? 
   * @param parameterName A vizsgálandó paraméter név.
   * @return True: a paraméter szerepel az SQL parancsban és be is van állítva. 
   *         False: a paraméter nem szerepel az SQL parancsban vagy nincs beállítva.
   */
  public boolean isParameterSet(String parameterName) {

    int index = parameters.indexOf(parameterName);
    return (index == -1 ? false : parametersOk.get(index));

  }

  /**
   * Egy paraméter már be van állítva az SQL-parancsban ? 
   * @param parameterName A vizsgálandó paraméter név (például ':23' esetén 23-at kell megadni).
   * @return True: a paraméter szerepel az SQL parancsban és be is van állítva. 
   *         False: a paraméter nem szerepel az SQL parancsban vagy nincs beállítva.
   */
  public boolean isParameterSet(int parameterName) {

    return isParameterSet(String.valueOf(parameterName));

  }

  /**
   * Eredeti SQL-parancs lekérdezése.
   * @return Eredeti SQL-parancs (a paraméterek ':'-tal vannak jelölve).
   */
  public String getOriginalSql() {

    return originalSql;

  }

  /**
   * JDBC SQL-parancs lekérdezése.
   * @return JDBC SQL-parancs (a paraméterek '?'-lel vannak jelölve).
   */
  public String getJdbcSql() {

    return jdbcSql;

  }

  /**
   * Nem SQL-select parancs végrehajtása (például: INSERT, UPDATE, DROP TABLE, GRANT).
   * @return A művelettel érintett sorok száma.
   */
  public long execute() {

    try {
      String plannedMethod = "execute()";
      if (isSelect) throw new ShellException(plannedMethod + " metódus nem hívható SQL-select parancs esetén.");
      beforeCheck(plannedMethod);
      updateCount = Long.valueOf(statement.executeUpdate());
      method = plannedMethod;
      return updateCount;
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }
    finally {
      close();
    }

  }

  /**
   * ResultSet kérése SQL-select parancs esetén.
   * @return SQL-select eredménye ResultSet-ben.
   */
  public ResultSet getResultSet() {

    try {
      String plannedMethod = "getResultSet()";
      beforeCheck(plannedMethod);
      if (!isSelect) throw new ShellException(plannedMethod + " metódus csak SQL-select parancs esetén hívható.");
      ResultSet r = statement.executeQuery();
      method = plannedMethod;
      return r;
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * SQL-select paranccsal kapott sorok maximális számának beállítása. 
   * Legalábbb 1-nek kell lennie. 
   * Hatással van a getRow() és getList() metódusokra.
   * @param maximumRows Sorok maximális száma, értéke legalább 1.
   * @return Saját maga.
   */
  public SQL setMaximumRows(long maximumRows) {

    if (maximumRows < 1) throw new ShellException("A beállítható maximális sorok számának legalább 1-nek kell lennie.");
    this.maximumRows = maximumRows;
    return this;

  } 

  /**
   * SQL-select paranccsal kapott sorok maximális számának lekérdezése.
   * @return Maximális sorok száma.
   */
  public long getMaximumRows() {

    return maximumRows;

  } 

  /**
   * SQL-parancs timeout idejének beállítása.
   * @param seconds Timeout ideje másodpercben. 0 esetén végtelen.
   * @return Saját maga.
   */
  public SQL setTimeout(int seconds) {

    try {
      statement.setQueryTimeout(seconds);
      return this;
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * SQL-parancs timeout idejének lekérdezése.
   * @return Timeout ideje másodpercben.
   */
  public int getTimeout() {

    try {
      return statement.getQueryTimeout();
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * Egy sor beolvasása ShellMap objektumba.
   * @return ShellMap objektum.
   * @throws NoRowException Az eredmény egyetlen sort sem tartalmaz.
   * @throws MoreRowsException Az eredmény több mint egy sort tartalmaz.
   */ 
  public ShellMap getRow() throws NoRowException, MoreRowsException {

    return (ShellMap)getRowAndList(true, null, null, null);

  }

  /**
   * Egy sor beolvasása T-objektumba ResultSetConverter-rel.
   * @param <T> Típus meghatározás.
   * @param resultSetConverter ResultSet-et T-objektumra konvertáló. 
   * @return A konvertált T-objektum.
   * @throws NoRowException Az eredmény egyetlen sort sem tartalmaz.
   * @throws MoreRowsException Az eredmény több mint egy sort tartalmaz.
   */ 
  public <T> T getRow(ResultSetConverter<T> resultSetConverter) throws NoRowException, MoreRowsException {

    return (T)getRowAndList(true, null, null, resultSetConverter);

  }

  /**
   * Egy sor beolvasása a paraméterként megadott osztály egy példányába.
   * @param <T> Típus meghatározás.
   * @param anc Annotated Class (ANC). Ebből az osztályból készült példány fogja tartalmazni a beolvasott sort.
   * @return A paraméterként kapott osztály egy példánya, mely tartalmazza a beolvasás eredményét.
   * @throws NoRowException Az eredmény egyetlen sort sem tartalmaz.
   * @throws MoreRowsException Az eredmény több mint egy sort tartalmaz.
   */ 
  public <T> T getRow(Class<T> anc) throws NoRowException, MoreRowsException {

    return (T)getRowAndList(true, anc, null, null);

  }

  /**
   * Egy sor beolvasása a paraméterként megadott példányba.
   * @param <T> Típus meghatározás.
   * @param ano Annotated Object (ANO). Ebbe az objektumba kerül a beolvasott sor.
   * @return A beolvasás eredményét tartalmazó objektum.
   * @throws NoRowException Az eredmény egyetlen sort sem tartalmaz.
   * @throws MoreRowsException Az eredmény több mint egy sort tartalmaz.
   */ 
  public <T> T getRow(T ano) throws NoRowException, MoreRowsException {

    return (T)getRowAndList(true, null, ano, null);

  }

  /**
   * Sorok beolvasása ShellMap objektumokat tartalmazó listába.
   * @return ShellMap objektumok listája.
   */
  public List<ShellMap> getList() {

    return (List<ShellMap>)getRowAndList(false, null, null, null);

  }

  /**
   * Sorok beolvasása T-objektumokat tartalmazó listába, ResultSetConverter-rel.
   * @param <T> Típus meghatározás.
   * @param resultSetConverter ResultSet-et T-objektumra konvertáló. 
   * @return Konvertált T-objektumok listája.
   *
   */
  public <T> List<T> getList(ResultSetConverter<T> resultSetConverter) {

    return (List<T>)getRowAndList(false, null, null, resultSetConverter);

  } 

  /**
   * Sorok beolvasása paraméterként megadott osztály példányainak listájába.
   * @param <T> Típus meghatározás.
   * @param anc Annotated Class (ANC). Ebből az osztályból készült példányok fogják tartalmazni a beolvasott sorokat.
   * @return Paraméterként kapott osztály példányainak listája, melyek tartalmazzák a beolvasás eredményét.
   */
  public <T> List<T> getList(Class<T> anc) {

    return (List<T>)getRowAndList(false, anc, null, null);

  }

  /**
   * getRow() és getList() metódusok közös végrehajtása.
   * @param isGetRow True: getRow() metódus hívta. False: getList() metódus hívta.
   * @param targetClass Cél osztály. Ha nem null akkor ennek az osztálynak a példányába (példányaiba) 
   *                    kell a lekérdezés eredményét tölteni. 
   * @param targetObject Cél objektum. Ha nem null akkor ebbe az objektumba kell a lekérdezés eredményét tölteni. 
   * @param resultSetConverter Ha nem null akkor a ResultSet-et soronként ennek a konvertálónak kell átadni.
   */
  private Object getRowAndList(

    boolean isGetRow, Class targetClass, Object targetObject, ResultSetConverter resultSetConverter) {
    try {
      String plannedMethod = (isGetRow ? "getRow()" : "getList()");
      // ellenőrzés
      beforeCheck(plannedMethod);
      if (!isSelect) throw new ShellException(plannedMethod + " metódus csak SQL-select esetén hívható.");
      resultSet = statement.executeQuery();
      // beolvasandó oszlopok meghatározása
      if (resultSetConverter == null) {
        if (targetClass == null && targetObject == null) {
          columnPropertyList = Tool.getColumnsFromMetaData(resultSet.getMetaData());
        }
        else if (targetClass != null) {
          columnPropertyList = Tool.getAnnotatedProperty(targetClass).columnPropertyList;
        }
        else {
          columnPropertyList = Tool.getAnnotatedProperty(targetObject.getClass()).columnPropertyList;
        }
      }
      // ResultSet ciklus
      Object issue = null;
      ArrayList issueList = new ArrayList();
      int rowNumber = 0;
      while (resultSet.next()) {
        rowNumber++;
        if (rowNumber > maximumRows) {
          isMoreRows = true;
          break;
        }
        if (isGetRow && rowNumber == 2) {
          throw new MoreRowsException("getRow() metódus végrehajtásakor egynél több sor az eredmény.");
        }
        if (resultSetConverter == null) {
          Object t;
          if (targetClass == null && targetObject == null) t = new ShellMap(this.columnPropertyList);
          else if (targetClass != null) t = Tool.newInstance(targetClass);
          else t = targetObject;
          issue = Tool.resultSetToTargetObject(resultSet, t, columnPropertyList);
        }
        else {
          issue = resultSetConverter.convert(resultSet, rowNumber);
        }
        issueList.add(issue);
      }
      // ResultSet ciklus vége
      if (isGetRow && rowNumber == 0) {
        throw new NoRowException("getRow() metódus esetén a lekérdezés egyetlen sort sem adott vissza.");
      }
      method = plannedMethod;
      return (isGetRow ? issue : issueList);
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }
    finally {
      close();
    }

  }

  /**
   * SQL-parancs végrehajtása előtti ellenőrzés.
   * @param plannedMethod Tervezett végrehajtandó metódus.
   */
  private void beforeCheck(String plannedMethod) throws SQLException {

    if (statement.isClosed()) {
      throw new ShellException(
        "Az SQL-parancs már le van zárva, ezért " + plannedMethod + " metódus nem hajtható végre rajta."
      );
    }
    if (method != null) {
      throw new ShellException("Az SQL-parancs már végre lett hajtva " + method + " metódussal, nem hajtható végre ismét.");
    }
    for (int i = 0; i < parameters.size(); i++) {
      if (!parametersOk.get(i)) {
        throw new ShellException("A(z) '" + parameters.get(i) + "' paraméterhez még nincs paraméter-objektum hozzárendelve.");
      }
    }

  }

  /**
   * Az SQL példány zárása (ResultSet, PreparedStatement, stream-ek zárása). 
   * execute(), getRow(), getList() metódusok közvetlenül hívják.
   * getResultSet() metódus után nem hívódik, ezért amikor a külső hívó befejezte a ResultSet feldolgozását, 
   * neki kell meghívnia.
   */
  @Override
  public void close() {

    try {
      // zárások
      try {
        closeOpenedStream(); 
        closeOpenedReader();
      }
      finally {
        if (resultSet != null) resultSet.close(); 
        statement.close(); 
      } 
      // zárások vége
    }
    catch (IOException | SQLException e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * Az SQL példány lezárt állapotának lekérdezése.
   * @return True: már le van zárva. False: még nincs lezárva.
   */
  public boolean isClosed() {

    try {
      return statement.isClosed();
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * Nyitott stream-ek lezárása.
   */
  private void closeOpenedStream() throws IOException {

    for (InputStream stream: openedStream) {
      stream.close();
    }  
    openedStream.clear();

  }

  /**
   * Nyitott reader-ek lezárása.
   */
  private void closeOpenedReader() throws IOException {

    for (Reader reader: openedReader) {
      reader.close();
    }
    openedReader.clear();

  }

  /**
   * Egy SQL-select getRow() vagy getList() matódusokkal való végrehajtása után lett volna-e még további eredmény sor, 
   * melyet nem kaptunk meg a maximumRows korlátozás miatt ?
   * @return True: lett volna még eredménysor. False: nem volt több eredménysor.
   */
  public boolean isMoreRows() {

    return isMoreRows;

  }

  /**
   * Az execute() paranccsal érintett sorok számának lekérdezése.
   * @return Érintett sorok száma.
   */
  public Long updateCount() {

    return updateCount;

  }

  /**
   * Információ az SQL objektum belső állapotáról.
   * @return Információk formázottan.
   */
  public StringBuilder info() {

    final String NL = "\n";
    StringBuilder s = new StringBuilder();
    String vonal = StringTool.multiply("-", 25);
    try {
      s.append(vonal).append(" SQL.info() start ").append(vonal).append(NL);
      s.append("EREDETI SQL KEZDETE ->").append(NL).append(originalSql).append("\n<- EREDETI SQL VÉGE").append(NL);
      s.append("JDBC SQL KEZDETE ->").append(NL).append(jdbcSql).append("\n<- JDBC SQL VÉGE").append(NL);
      s.append("Paraméterek: ");
      for (int i = 0; i < parameters.size(); i++) {
        s.append("'").append(parameters.get(i)).append("'");
        if (i < parameters.size() -1) s.append(", ");
      }
      s.append(NL);
      s.append("SQL-select parancs ? ").append(isSelect ? "igen" : "nem").append(NL);
      s.append("PreparedStatement zárva ? ").append(statement.isClosed() ? "igen" : "nem").append(NL);
      s.append("ResultSet létezik ? ").append(resultSet == null ? "nem" : "igen").append(NL);
      if (resultSet != null) {
        s.append("ResultSet zárva ? ").append(resultSet.isClosed() ? "igen" : "nem").append(NL);
      }
      if (isSelect) {
        // SQL-select esetén
        if (columnPropertyList != null) {
          s.append(NL).append("Oszlopok: ").append(NL);
          for (int i = 0; i < columnPropertyList.size(); i++) {
            ColumnProperty cp = columnPropertyList.get(i);
            s.append("  ").append(StringTool.rightPad(String.valueOf(i+1) + ". oszlop: ", 16)).append(cp.name).append(NL);
            s.append("  DB típus:       ").append(StringTool.str(cp.dbType, "")).append(NL);
            s.append("  precision:      ").append(StringTool.str(cp.precision, "")).append(NL);
            s.append("  scale:          ").append(StringTool.str(cp.scale, "")).append(NL);
            s.append("  java-SQL típus: ").append(StringTool.str(Tool.getSQLTypeName(cp.sqlType, true), "")).append(NL);
            s.append("  java osztály:   ").append(cp.cls.getName()).append(NL);
            s.append(NL);
          }
        }
        s.append("Több sor van ? ").append(isMoreRows ? "igen" : "nem").append(NL);
      }
      s.append("Végre van hajtva ? ").append(method == null ? "nem" : "igen").append(NL);
      if (method != null) {
        s.append("Végrehajtás metódusa: ").append(method).append(NL);
        if (!isSelect) {
          s.append("execute() által érintett sorok száma: ").append(updateCount).append(NL);
        }
      }
      s.append("Nyitva maradt stream-ek száma: ").append(openedStream.size()).append(NL);
      s.append(vonal).append(" SQL.info() end ").append(vonal).append(NL);
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return s;

  }

  // ====
}
