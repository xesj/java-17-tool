package xesj.shell;
import java.io.Closeable;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import xesj.tool.StringTool;

/**
 * Adatbázis Shell.
 */
public class Shell implements Serializable, Closeable {

  /**
   * Annotált osztályokhoz tartozó jellemzők cache-területe. 
   */
  static final Map<Class,AnnotatedProperty> annotatedCache = new HashMap<>();

  /** 
   * Adatbázis connection. 
   */
  private final Connection connection;

  /** 
   * Adatbázis. 
   */
  private final Database database;

  /** 
   * Az utoljára létrehozott SQL-objektum. 
   */
  private SQL lastSQL; 

  /**
   * Konstruktor.
   * @param connection Adatbázis kapcsolat.
   * @param database Adatbázis típus.
   */
  public Shell(Connection connection, Database database) {

    if (connection == null) {
      throw new ShellException("A Shell objektum nem hozható létre mert a 'connection' paraméter null.");
    }
    if (database == null) {
      throw new ShellException("A Shell objektum nem hozható létre mert a 'database' paraméter null.");
    }
    this.connection = connection;
    this.database = database;

  }

  /**
   * Adatbázis kapcsolat lekérdezése.
   * @return Adatbázis kapcsolat.
   */
  public Connection getConnection() {

    return connection;

  }

  /**
   * Adatbázis típus lekérdezése.
   * @return Adatbázis típus.
   */
  public Database getDatabase() {

    return database;

  }

  /**
   * SQL-commit-ot hajt végre a konstruktorban megadott adatbázis kapcsolaton.
   */
  public void commit() {

    checkClosed("commit()");
    try {
      connection.commit();
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * SQL-rollback-et hajt végre a konstruktorban megadott adatbázis kapcsolaton.
   */
  public void rollback() {

    checkClosed("rollback()");
    try {
      connection.rollback();
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * Lezárja a konstruktorban megadott adatbázis kapcsolatot, de előtte egy SQL-rollback-et hajt végre, 
   * hogy a nem lezárt tranzakciókat visszavonja.
   */
  @Override
  public void close() {

    try {
      if (!connection.isClosed()) {
        // zárás
        try {
          connection.rollback();
        }
        finally {
          connection.close();  
        }  
        // zárás vége
      }
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * A Shell példány lezárt állapotának lekérdezése.
   * @return True: már le van zárva. False: még nincs lezárva.
   */
  public boolean isClosed() {

    try {
      return connection.isClosed();
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * SQL objektum létrehozása.
   * @param sqlCommand SQL-parancs.
   * @return Az SQL-parancsot magába foglaló SQL objektum.
   */
  public SQL sql(String sqlCommand) {

    checkClosed("sql()");
    lastSQL = new SQL(this, sqlCommand);
    return lastSQL;

  }

  /**
   * Utoljára létrehozott SQL objektum lekérdezése.
   * @return Utoljára létrehozott SQL objektum.
   */
  public SQL lastSQL() {

    return lastSQL; 

  }

  /**
   * Ellenőrzi hogy már zárt-e a shell. Ha zárt, akkor hibát dob.
   */
  private void checkClosed(String method) {

    try {
      if (connection.isClosed()) {
        throw new ShellException("A Shell már zárva van, nem hívható " + method + " metódus.");
      }
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * Információ a Shell belső állapotáról.
   * @return Információk formázottan.
   */
  public StringBuilder info() {

    final String NL = "\n";
    String vonal = StringTool.multiply("-", 25);
    StringBuilder s = new StringBuilder();
    try {
      s.append(vonal).append(" Shell.info() start ").append(vonal).append(NL);
      s.append("Adatbázis név: ").append(database.getName()).append(NL);
      s.append("Adatbázis null-stratégia: ").append(database.getNullStrategy()).append(NL);
      s.append("Adatbázis kapcsolat auto-commit ? ").append(connection.getAutoCommit() ? "igen" : "nem").append(NL);
      s.append("Adatbázis kapcsolat zárva ? ").append(connection.isClosed() ? "igen" : "nem").append(NL);
      s.append("SQL objektum volt már létrehozva ? ").append(lastSQL == null ? "nem" : "igen").append(NL);
      s.append(vonal).append(" Shell.info() end ").append(vonal).append(NL);
    }
    catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return s;
    
  }

  /**
   * Database Interface Class (DIC) új példányának feltöltése egy adatbázis sor tartalmával.
   * @param <T> Típus meghatározás.
   * @param dic Database Interface Class (DIC).
   * @param parameters Az osztály @Id annotációval ellátott mezőihez tartozó paraméterek, 
   *                   melynek sorrendje megegyezik a @Id(1), @Id(2), ... annotációval. 
   * @return Az feltöltött példány. Ha az adott paraméterekhez nincs sor akkor null.
   */
  public <T> T getObject(Class<T> dic, Object... parameters) {

    if (dic == null) {
      throw new ShellException("A 'dic' paraméter null.");
    }
    Object dio = Tool.newInstance(dic);
    return (T)getObject(dio, parameters);

  }

  /**
   * Database Interface Object feltöltése egy adatbázis sor tartalmával.
   * @param <T> Típus meghatározás.
   * @param dio Database Interface Object (DIO).
   * @param parameters Az osztály @Id annotációval ellátott mezőihez tartozó paraméterek, 
   *                   melynek sorrendje megegyezik a @Id(1), @Id(2), ... annotációval. 
   * @return Az feltöltött példány. Ha az adott paraméterekhez nincs sor akkor null.
   */
  public <T> T getObject(T dio, Object... parameters) {

    if (dio == null) {
      throw new ShellException("A 'dio' paraméter null.");
    }
    Class dic = dio.getClass();
    AnnotatedProperty ap = Tool.getAnnotatedProperty(dic);
    if (ap.select == null) {
      throw new ShellException(
        "A(z) '" + dic.getName() + "' osztály a @Table/@View és @Id annotáció egyikét nem tartalmazza."
      );
    }
    SQL sql = sql(ap.select);
    sql.setAllParameters(parameters);
    try {
      return sql.getRow(dio);
    }
    catch (NoRowException nre) {
      return null;    
    }

  }

  /**
   * Database Interface Object (DIO) beszúrása SQL-insert művelettel az adatbázisba.
   * @param dio Database Interface Object (DIO).
   * @return A művelettel érintett sorok száma.
   */
  public long insertObject(Object dio) {

    if (dio == null) {
      throw new ShellException("A 'dio' paraméter null.");
    }
    Class dic = dio.getClass();
    AnnotatedProperty ap = Tool.getAnnotatedProperty(dic);
    if (ap.insert == null) {
      throw new ShellException("A(z) '" + dic.getName() + "' osztály a @Table/@View annotáció egyikét sem tartalmazza.");
    }
    SQL sql = sql(ap.insert);
    for (ColumnProperty cp: ap.columnPropertyList) {
      try {
        Field field = dic.getField(cp.name);
        Object parameterObject = field.get(dio);
        sql.setParameter(cp.name, parameterObject);
      } 
      catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }
    return sql.execute();

  }

  /**
   * Database Interface Object (DIO) felülírása SQL-update művelettel az adatbázisban.
   * @param dio Database Interface Object (DIO).
   * @return A művelettel érintett sorok száma.
   */
  public long updateObject(Object dio) {

    if (dio == null) {
      throw new ShellException("A 'dio' paraméter null.");
    }
    Class dic = dio.getClass();
    AnnotatedProperty ap = Tool.getAnnotatedProperty(dic);
    if (ap.update == null) {
      throw new ShellException(
        "A(z) '" + dic.getName() + "' osztály a @Table/@View és @Id annotáció egyikét nem tartalmazza."
      );
    }
    SQL sql = sql(ap.update);
    for (ColumnProperty cp: ap.columnPropertyList) {
      try {
        Field field = dic.getField(cp.name);
        Object parameterObject = field.get(dio);
        sql.setParameter(cp.name, parameterObject);
      } 
      catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }
    return sql.execute();

  } 

  /**
   * Database Interface Object (DIO) beszúrása SQL-insert művelettel, vagy felülírása SQL-update művelettel az adatbázisba.
   * @param dio Database Interface Object (DIO).
   * @param isInsert A végrehajtandó művelet SQL-insert ?
   * @return A művelettel érintett sorok száma.
   */
  public long insertOrUpdateObject(Object dio, boolean isInsert) {

    if (isInsert) {
      return insertObject(dio);
    }
    else {
      return updateObject(dio);
    }

  }

  /**
   * Database Interface Object (DIO) törlése SQL-delete művelettel az adatbázisból.
   * @param dio Database Interface Object (DIO).
   * @return A művelettel érintett sorok száma.
   */
  public long deleteObject(Object dio) {

    if (dio == null) {
      throw new ShellException("A 'dio' paraméter null.");
    }
    Class dic = dio.getClass();
    AnnotatedProperty ap = Tool.getAnnotatedProperty(dic);
    if (ap.delete == null) {
      throw new ShellException(
        "A(z) '" + dic.getName() + "' osztály a @Table/@View és @Id annotáció egyikét nem tartalmazza."
      );
    }
    SQL sql = sql(ap.delete);
    for (int i: ap.ids) {
      ColumnProperty cp = ap.columnPropertyList.get(i);
      try {
        Field field = dic.getField(cp.name);
        Object parameterObject = field.get(dio);
        sql.setParameter(cp.name, parameterObject);
      }  
      catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e) {
        throw new RuntimeException(e);
      }  
    }
    return sql.execute();

  }

  // ====
}
