package xesj.shell;
import java.io.Serializable;

/**
 * Shell-hez tartozó speciális exception. 
 */
public class ShellException extends RuntimeException implements Serializable {

  /**
   * Konstruktor.
   * @param message Exception szövege.
   */
  public ShellException(String message) {

    super(message);

  }

  /**
   * Konstruktor.
   * @param message Exception szövege.
   * @param cause Eredeti exception.
   */
  public ShellException(String message, Throwable cause) {

    super(message, cause);

  }

  // ====
}
