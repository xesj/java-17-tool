package xesj.shell;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import xesj.tool.NumberTool;
import xesj.tool.StringTool;

/**
 * Az SQL osztály getRow() és getList() eredményeit tároló map. 
 */
public class ShellMap extends HashMap implements Serializable {

  /**
   * Oszlopok és jellemzőik. Kulcs: oszlopnév, érték: az oszlop összes jellemzője.
   */
  private final Map<String,ColumnProperty> columnMap = new HashMap<>();

  /**
   * Konstruktor.
   * @param columnPropertyList Oszlopok és jellemzőik.
   */
  ShellMap(List<ColumnProperty> columnPropertyList) {

    for (ColumnProperty cp: columnPropertyList) {
      columnMap.put(cp.name, cp);
    }

  }

  /**
   * A tárolt objektum lekérdezése Long típusként.
   * @param columnName SQL-select oszlopnév.
   * @return Tárolt objektum Long típusként.
   */
  public Long getLong(String columnName) {

    return common(columnName, Long.class);

  }

  /**
   * A tárolt objektum lekérdezése BigDecimal típusként.
   * @param columnName SQL-select oszlopnév.
   * @return Tárolt objektum BigDecimal típusként.
   */
  public BigDecimal getBigDecimal(String columnName) {

    return common(columnName, BigDecimal.class);

  }

  /**
   * A tárolt objektum lekérdezése BigInteger típusként.
   * @param columnName SQL-select oszlopnév.
   * @return Tárolt objektum BigInteger típusként.
   */
  public BigInteger getBigInteger(String columnName) {

    return common(columnName, BigInteger.class);

  }

  /**
   * A tárolt objektum lekérdezése java.sql.Date típusként.
   * @param columnName SQL-select oszlopnév.
   * @return Tárolt objektum java.sql.Date típusként.
   */
  public Date getDate(String columnName) {

    return common(columnName, Date.class);

  }

  /**
   * A tárolt objektum lekérdezése Time típusként.
   * @param columnName SQL-select oszlopnév.
   * @return Tárolt objektum Time típusként.
   */
  public Time getTime(String columnName) {

    return common(columnName, Time.class);

  }

  /**
   * A tárolt objektum lekérdezése Timestamp típusként.
   * @param columnName SQL-select oszlopnév.
   * @return Tárolt objektum Timestamp típusként.
   */
  public Timestamp getTimestamp(String columnName) {

    return common(columnName, Timestamp.class);

  }

  /**
   * A tárolt objektum lekérdezése Boolean típusként.
   * @param columnName SQL-select oszlopnév.
   * @return Tárolt objektum Boolean típusként.
   */
  public Boolean getBoolean(String columnName) {

    return common(columnName, Boolean.class);

  }

  /**
   * A tárolt objektum lekérdezése String típusként.
   * @param columnName SQL-select oszlopnév.
   * @return Tárolt objektum String típusként.
   */
  public String getString(String columnName) {

    return common(columnName, String.class);

  }

  /**
   * A tárolt objektum lekérdezése File típusként.
   * @param columnName SQL-select oszlopnév.
   * @return Tárolt objektum File típusként.
   */
  public File getFile(String columnName) {

    return common(columnName, File.class);

  }

  /**
   * Bármilyen típusú tárolt objektum lekérdezése.
   * @param columnName SQL-select oszlopnév.
   * @return Tárolt objektum.
   */
  public Object getAny(String columnName) {

    return common(columnName, Object.class);

  }

  /**
   * Közös ellenőrző, beolvasó, konvertáló eljárás.
   * @param columnName Oszlopnév. 
   * @param methodClass Ilyen adattípusra lesz alakítva az adat.
   * @return Tárolt objektum a paraméterként kapott típusra alakítva. 
   */
  private <T> T common(String columnName, Class<T> methodClass) {

    // ellenőrzések
    if (columnName == null) {
      throw new ShellException("Az oszlopnév nem lehet null.");
    }
    if (!columnMap.containsKey(columnName)) {
      throw new ShellException("A(z) '" + columnName + "' oszlopnév nem szerepel az SQL-select parancsban.");
    }

    // ellenőrzés vége
    Class columnClass = columnMap.get(columnName).cls;

    // getAny() esete
    if (methodClass == Object.class) {
      return (T)get(columnName);
    }

    // megegyező osztály esete
    if (columnClass == methodClass) {
      return (T)get(columnName);
    }

    // cross-conversion esetek
    if (methodClass == Long.class && columnClass == BigInteger.class) {
      return (T)NumberTool.toLong((BigInteger)get(columnName));
    }
    if (methodClass == Long.class && columnClass == BigDecimal.class) {
      return (T)NumberTool.toLong((BigDecimal)get(columnName));
    }
    if (methodClass == BigInteger.class && columnClass == Long.class) {
      return (T)NumberTool.toBigInteger((Long)get(columnName));
    }
    if (methodClass == BigInteger.class && columnClass == BigDecimal.class) {
      return (T)NumberTool.toBigInteger((BigDecimal)get(columnName));
    }
    if (methodClass == BigDecimal.class && columnClass == Long.class) {
      return (T)NumberTool.toBigDecimal((Long)get(columnName));
    }
    if (methodClass == BigDecimal.class && columnClass == BigInteger.class) {
      return (T)NumberTool.toBigDecimal((BigInteger)get(columnName));
    }

    // hibás beolvasó metódus esete 
    throw new ShellException(
      "A(z) '" + columnName + "' oszlop típusa '" + columnClass.getName() + "', így ezzel a metódussal nem olvasható be."
    );

  }

  /**
   * Információ a ShellMap belső állapotáról.
   * @return Információk formázottan.
   */
  public StringBuilder info() {

    final String NL = "\n";
    StringBuilder s = new StringBuilder();
    String vonal = StringTool.multiply("-", 25);
    s.append(vonal).append(" ShellMap.info() start ").append(vonal).append(NL);
    s.append("ShellMap adatok száma:").append(columnMap.size()).append(NL);
    for (String columnName: columnMap.keySet()) {
      s.append(NL);
      s.append("kulcs neve:   ").append(columnName).append(NL);
      s.append("érték típusa: ").append(columnMap.get(columnName).cls.getName()).append(NL);
    }
    s.append(vonal).append(" ShellMap.info() end ").append(vonal).append(NL);
    return s;

  }

  // ====
}
