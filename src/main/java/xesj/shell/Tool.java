package xesj.shell;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import xesj.shell.annotation.Column;
import xesj.shell.annotation.Id;
import xesj.shell.annotation.Table;
import xesj.shell.annotation.View;
import xesj.tool.FileTool;

/**
 * Általános tool.
 */
public class Tool implements Serializable {

  /**
   * SQL-típusok kódjai, és hozzátartozó megnevezések.
   */
  private static Map<Integer,String> sqlTypeMap = new HashMap<>();

  /**
   * Osztály inicializátor.
   */
  static {
    sqlTypeMap.put(Types.ARRAY, "ARRAY");
    sqlTypeMap.put(Types.BIGINT, "BIGINT");
    sqlTypeMap.put(Types.BINARY, "BINARY");
    sqlTypeMap.put(Types.BIT, "BIT");
    sqlTypeMap.put(Types.BLOB, "BLOB");
    sqlTypeMap.put(Types.BOOLEAN, "BOOLEAN");
    sqlTypeMap.put(Types.CHAR, "CHAR");
    sqlTypeMap.put(Types.CLOB, "CLOB");
    sqlTypeMap.put(Types.DATALINK, "DATALINK");
    sqlTypeMap.put(Types.DATE, "DATE");
    sqlTypeMap.put(Types.DECIMAL, "DECIMAL");
    sqlTypeMap.put(Types.DISTINCT, "DISTINCT");
    sqlTypeMap.put(Types.DOUBLE, "DOUBLE");
    sqlTypeMap.put(Types.FLOAT, "FLOAT");
    sqlTypeMap.put(Types.INTEGER, "INTEGER");
    sqlTypeMap.put(Types.JAVA_OBJECT, "JAVA_OBJECT");
    sqlTypeMap.put(Types.LONGNVARCHAR, "LONGNVARCHAR");
    sqlTypeMap.put(Types.LONGVARBINARY, "LONGVARBINARY");
    sqlTypeMap.put(Types.LONGVARCHAR, "LONGVARCHAR");
    sqlTypeMap.put(Types.NCHAR, "NCHAR");
    sqlTypeMap.put(Types.NCLOB, "NCLOB");
    sqlTypeMap.put(Types.NULL, "NULL");
    sqlTypeMap.put(Types.NUMERIC, "NUMERIC");
    sqlTypeMap.put(Types.NVARCHAR, "NVARCHAR");
    sqlTypeMap.put(Types.OTHER, "OTHER");
    sqlTypeMap.put(Types.REAL, "REAL");
    sqlTypeMap.put(Types.REF, "REF");
    sqlTypeMap.put(Types.ROWID, "ROWID");
    sqlTypeMap.put(Types.SMALLINT, "SMALLINT");
    sqlTypeMap.put(Types.SQLXML, "SQLXML");
    sqlTypeMap.put(Types.STRUCT, "STRUCT");
    sqlTypeMap.put(Types.TIME, "TIME");
    sqlTypeMap.put(Types.TIMESTAMP, "TIMESTAMP");
    sqlTypeMap.put(Types.TINYINT, "TINYINT");
    sqlTypeMap.put(Types.VARBINARY, "VARBINARY");
    sqlTypeMap.put(Types.VARCHAR, "VARCHAR");
  }

  /**
   * Osztály példányosítása.
   * @param cls Példányosítandó osztály.
   * @return Osztály példánya.
   */
  public static Object newInstance(Class cls) {

    Object object;
    try {
      object = cls.newInstance();
    }
    catch (InstantiationException | IllegalAccessException e) {
      throw new ShellException("A(z) '" + cls.getName() + "' osztály nem példányosítható.", e);
    }
    return object;

  }

  /**
   * Annotált osztály jellemzőinek meghatározása. Ha lehetséges akkor cache-ből adja vissza a jellemzőket.
   * @param cls Annotált osztály.
   * @return Annotált osztály jellemzői.
   */
  public static AnnotatedProperty getAnnotatedProperty(Class<?> cls) {

    // cache-ben már létezik ?
    if (Shell.annotatedCache.containsKey(cls)) {
      return Shell.annotatedCache.get(cls);
    }
    AnnotatedProperty ap = new AnnotatedProperty(cls);

    // @Table annotáció vizsgálata
    if (cls.isAnnotationPresent(Table.class)) {
      Table table = cls.getAnnotation(Table.class);
      ap.tableName = table.value();
    }  

    // @View annotáció vizsgálata
    if (cls.isAnnotationPresent(View.class)) {
      View view = cls.getAnnotation(View.class);
      ap.viewName = view.value();
    }  
    if (ap.tableName != null && ap.viewName != null) {
      throw new ShellException("A(z) '" + cls.getName() + "' osztály @Table és @View annotációt is tartalmaz.");
    }

    // @Column, @Id annotációk vizsgálata
    Map<Integer,Integer> idMap = new HashMap<>();
    Field[] fields = cls.getDeclaredFields();
    for (Field field: fields) {
      // @Column
      if (field.isAnnotationPresent(Column.class)) {
        if ((field.getModifiers() & Modifier.PUBLIC) == 0) {
          throw new ShellException(
            "A(z) '" + cls.getName() + "' osztály @Column annotációval ellátott '" + 
              field.getName() + "' mezője nem publikus."
          );
        }
        if ((field.getModifiers() & Modifier.STATIC) != 0) {
          throw new ShellException(
            "A(z) '" + cls.getName() + "' osztály @Column annotációval ellátott '" + field.getName() + "' mezője statikus."
          );
        }
        ap.columnPropertyList.add(new ColumnProperty(field.getName(), field.getType()));
      }
      // @Id
      if (field.isAnnotationPresent(Id.class)) {
        if (!field.isAnnotationPresent(Column.class)) {
          throw new ShellException(
            "A(z) '" + cls.getName() + "' osztály @Id annotációval ellátott '" + 
              field.getName() + "' mezője nincs ellátva @Column annotációval."
          );
        }  
        Id id = field.getAnnotation(Id.class);
        int idValue = id.value();
        if (idMap.containsKey(idValue)) {
          throw new ShellException(
            "A(z) '" + cls.getName() + "' osztály @Id(" + idValue +") annotációja többször szerepel."
          );
        }
        idMap.put(idValue, ap.columnPropertyList.size() - 1);
      }
    }
    if (ap.columnPropertyList.isEmpty()) {
      throw new ShellException("A(z) '" + cls.getName() + "' osztály egyetlen @Column annotációt sem tartalmaz.");
    }

    // id-map ellenőrzése: 1-től kezdődően szigorúan egyesével növekvő ?
    ap.ids = new int[idMap.size()];
    for (int i = 1; i <= idMap.size(); i++) {
      if (!idMap.containsKey(i)) {
        throw new ShellException("A(z) '" + cls.getName() + "' osztály nem tartalmazza a @Id(" + i +") annotációt.");
      }
      ap.ids[i-1] = idMap.get(i);
    }

    // SQL-oszlopnevek összeállítása: 
    // xColumns: "col1, col2, col3"
    // yColumns: ":col1, :col2, :col3"
    // zColumns: "col1 = :col1, col2 = :col2, col3 = :col3"
    StringBuilder xColumns = new StringBuilder(), yColumns = new StringBuilder(), zColumns = new StringBuilder(); 
    for (ColumnProperty cp: ap.columnPropertyList) {
      if (xColumns.length() > 0) {
        xColumns.append(", ");
        yColumns.append(", ");
        zColumns.append(", ");
      }
      xColumns.append(cp.name);
      yColumns.append(":").append(cp.name);
      zColumns.append(cp.name).append(" = :").append(cp.name);
    }    

    // SQL-where összeállítása: "col1 = :col1 and col2 = :col2 and col3 = :col3"  
    StringBuilder where = new StringBuilder();
    for (int i: ap.ids) {
      if (where.length() > 0) {
        where.append(" and ");
      }
      String colName = ap.columnPropertyList.get(i).name;
      where.append(colName);
      where.append(" = :");
      where.append(colName);
    }

    // SQL-select összeállítása
    if (ap.getTableViewName() != null && ap.isId()) {
      ap.select = "SELECT " + xColumns + " FROM " + ap.getTableViewName() + " WHERE " + where;
    }  

    // SQL-insert összeállítása
    if (ap.getTableViewName() != null) {
      ap.insert = "INSERT INTO " + ap.getTableViewName() + "(" + xColumns + ") VALUES(" + yColumns + ")";
    }  

    // SQL-update összeállítása
    if (ap.getTableViewName() != null && ap.isId()) {
      ap.update = "UPDATE " + ap.getTableViewName() + " SET " + zColumns + " WHERE " + where;
    }  

    // SQL-delete összeállítása
    if (ap.getTableViewName() != null && ap.isId()) {
      ap.delete = "DELETE FROM " + ap.getTableViewName() + " WHERE " + where;
    }  

    // Tárolás a cache-ben
    Shell.annotatedCache.put(cls, ap);
    return ap;

  } 

  /**
   * A kapott metadata alapján összeállítja az összes oszlop tulajdonságát.
   * @param meta ResultSetMetadata, melynek minden oszlopa felhasználásra kerül.
   * @return Oszlopok jellemzői.
   */
  public static List<ColumnProperty> getColumnsFromMetaData(ResultSetMetaData meta) {

    List<ColumnProperty> columnPropertyList = new ArrayList<>();
    Set<String> columnNameSet = new HashSet<>();
    try {
      for (int i = 1; i <= meta.getColumnCount(); i++) {
        String name = meta.getColumnName(i).toLowerCase();
        if (!columnNameSet.add(name)) {
          throw new ShellException(
            "Az SQL-select parancsban az oszlopnevek kisbetűre alakításakor többször fordul elő a(z) '" + 
              name + "' oszlopnév."
          );
        }
        int sqlType = meta.getColumnType(i);
        int precision = meta.getPrecision(i);
        int scale = meta.getScale(i);
        String dbType = meta.getColumnTypeName(i);
        Class cls;
        switch (sqlType) {
          case Types.BIGINT:      cls = Long.class;        break;
          case Types.BINARY:      cls = File.class;        break;
          case Types.BIT:         cls = Boolean.class;     break;
          case Types.BLOB:        cls = File.class;        break;
          case Types.BOOLEAN:     cls = Boolean.class;     break;
          case Types.CHAR:        cls = String.class;      break;
          case Types.CLOB:        cls = String.class;      break;
          case Types.DATE:        cls = Date.class;        break;
          case Types.DOUBLE:      cls = BigDecimal.class;  break;
          case Types.FLOAT:       cls = BigDecimal.class;  break;
          case Types.INTEGER:     cls = Long.class;        break;
          case Types.SMALLINT:    cls = Long.class;        break;
          case Types.TIME:        cls = Time.class;        break;
          case Types.TIMESTAMP:   cls = Timestamp.class;   break;
          case Types.TINYINT:     cls = Long.class;        break;
          case Types.VARCHAR:     cls = String.class;      break;
          // decimal, numeric
          case Types.DECIMAL:
          case Types.NUMERIC:
            if (precision <= 0 || scale != 0) {
              cls = BigDecimal.class;
            }
            else {
              // precision >= 1 && scale = 0
              cls = (precision <= 18 ? Long.class : BigInteger.class);
            }
            break;
          // egyéb típusok -> Object.class  
          case Types.ARRAY:
          case Types.DATALINK:
          case Types.DISTINCT:
          case Types.JAVA_OBJECT:
          case Types.LONGNVARCHAR:
          case Types.LONGVARBINARY:
          case Types.LONGVARCHAR:
          case Types.NCHAR:
          case Types.NCLOB:
          case Types.NULL:
          case Types.NVARCHAR:
          case Types.OTHER:
          case Types.REAL:
          case Types.REF:
          case Types.ROWID:
          case Types.SQLXML:
          case Types.STRUCT:
          case Types.VARBINARY:
            cls = Object.class;
            break;
          default:
            throw new ShellException("A(z) " + sqlType + " értékű java.sql.Types még nem támogatott");
        }
        columnPropertyList.add(new ColumnProperty(name, cls, dbType, precision, scale, sqlType));
      }
    } 
    catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return columnPropertyList;

  }

  /**
   * A ResultSet egy konkrét sorát a cél objektumba tárolja az oszlop jellemzőknek (ColumnProperty) megfelelően.
   * @param resultSet ResultSet egy konkrét sorra.
   * @param targetObject Cél objektum, melybe az adatok be fognak töltődni.
   * @param columnPropertyList Oszlopok és jellemzőik.
   * @return Az adatokkal feltöltött cél objektum.
   */
  public static Object resultSetToTargetObject(

    ResultSet resultSet, Object targetObject, List<ColumnProperty> columnPropertyList) {
    String colName = null;
    try {
      for (ColumnProperty columnProperty: columnPropertyList) {
        Class colClass = columnProperty.cls;
        colName = columnProperty.name;
        Object getObject;
        if (colClass == Short.class)               getObject = resultSet.getShort(colName);
        else if (colClass == Integer.class)        getObject = resultSet.getInt(colName);
        else if (colClass == Long.class)           getObject = resultSet.getLong(colName);
        else if (colClass == Float.class)          getObject = resultSet.getFloat(colName);
        else if (colClass == Double.class)         getObject = resultSet.getDouble(colName);
        else if (colClass == BigDecimal.class)     getObject = resultSet.getBigDecimal(colName);
        else if (colClass == Boolean.class)        getObject = resultSet.getBoolean(colName);
        else if (colClass == String.class)         getObject = resultSet.getString(colName);
        else if (colClass == java.sql.Date.class)  getObject = resultSet.getDate(colName);
        else if (colClass == java.util.Date.class) getObject = resultSet.getDate(colName);
        else if (colClass == Time.class)           getObject = resultSet.getTime(colName);
        else if (colClass == Timestamp.class)      getObject = resultSet.getTimestamp(colName);
        else if (colClass == Blob.class)           getObject = resultSet.getBlob(colName);
        else if (colClass == Clob.class)           getObject = resultSet.getClob(colName);
        else if (colClass == File.class)           getObject = getFileFromResultSet(resultSet.getBinaryStream(colName));
        else if (colClass == Object.class)         getObject = resultSet.getObject(colName);
        else if (colClass == BigInteger.class) {
          BigDecimal tmp = resultSet.getBigDecimal(colName);
          getObject = (tmp == null ? null : tmp.toBigIntegerExact());
        }
        else {
          throw new ShellException(
            "A resultSet-ből a(z) '" + colName + "' oszlop beolvasása '" + 
              colClass.getName() + "' osztály szerint nem támogatott."
          );
        }
        // null vizsgálat
        if (resultSet.wasNull()) getObject = null;
        // tárolás a targetObject-be
        if (targetObject instanceof ShellMap) {
          ((ShellMap)targetObject).put(colName, getObject);
        }
        else {
          targetObject.getClass().getDeclaredField(colName).set(targetObject, getObject);
        }
      }
    }
    catch (SQLException | NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
      if (colName == null) {
        throw new RuntimeException(e);
      }
      else {
        throw new RuntimeException("Hiba a(z) '" + colName + "' oszlop beolvasásakor.", e);
      }
    }
    return targetObject;

  }

  /**
   * A kapott InputStream-ből beolvassa az adatokat és fájlt készít belőle a temporális könyvtárban. 
   * @param is InputStream melyből be kell olvasni.
   * @return Elkészített fájl a temporális könyvtárban.
   */
  public static File getFileFromResultSet(InputStream is) {

    if (is == null) return null;
    File file = FileTool.tmpFile(null);
    BufferedOutputStream bos = null;
    try (BufferedInputStream bis = new BufferedInputStream(is)) {
      bos = new BufferedOutputStream(new FileOutputStream(file));
      byte[] bytes = new byte[1024];
      int byteCount;
      while ((byteCount = bis.read(bytes)) != -1) {
        bos.write(bytes, 0, byteCount);
      }
    }
    catch (IOException e) {
      throw new RuntimeException(e);
    }
    finally {
      try { 
        bos.close(); 
      } 
      catch (IOException e) {
        throw new RuntimeException(e);
      }
    }  
    return file;

  }

  /**
   * Lekérdezi az SQL-típus értéke alapján az SQL-típus nevét. 
   * @param value SQL-típus értéke.
   * @param plusValue A visszaadott névhez hozzáírja-e az értéket is zárójelben ?
   * @return SQL-típus neve. Null érték esetén null-t ad vissza.
   */
  public static String getSQLTypeName(Integer value, boolean plusValue) {

    if (value == null) return null;
    String name = sqlTypeMap.get(value);
    if (name == null) {
      throw new ShellException("Ismeretlen SQL-típus kód: " + value);
    }
    return name + (plusValue ? " (" + value + ")" : "");

  }

  // ====
}
