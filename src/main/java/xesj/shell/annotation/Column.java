package xesj.shell.annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Adatbázis tábla/view/lekérdezés oszlopnak megfelelő java mező annotációja.
 * Csak publikus, nem statikus mező jelzésére használatos. 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
}
