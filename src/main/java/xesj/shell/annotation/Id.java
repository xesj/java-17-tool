package xesj.shell.annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Adatbázis tábla/view primary key oszlopoknak megfelelő mező(ke)t jelző annotációja.
 * Csak publikus, nem statikus mező jelzésére használatos. 
 * Egyetlen mező esetén a használatos forma: @Id. 
 * Több mező esetén a használatos forma: @Id(1), @Id(2), ...
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Id {

  public int value() default 1;

}
