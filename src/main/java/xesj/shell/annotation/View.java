package xesj.shell.annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Adatbázis view-nak megfelelő java osztály annotációja.
 * Kötelező paraméterként megadni a view nevét, ahogyan az SQL-parancsban szerepelnie kell.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface View {

  public String value();

}
