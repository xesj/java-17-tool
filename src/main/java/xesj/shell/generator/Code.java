package xesj.shell.generator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import xesj.shell.ColumnProperty;
import xesj.shell.MyException;
import xesj.shell.Shell;
import xesj.shell.Tool;
import xesj.tool.ExceptionTool;
import xesj.tool.StringTool;

/**
 * Forráskód generáló.
 */
class Code {

  /**
   * XML fájl adatok.
   */
  private final Xml xml;

  /**
   * Tábla/view adatok.
   */
  private TableView tableView;

  /**
   * Import sorok.
   */
  private TreeSet<String> imports = new TreeSet<>();

  /**
   * Teljes forráskód.
   */
  private StringBuffer code = new StringBuffer();

  /**
   * Konstruktor
   * @param shell Shell, melyen az SQL-parancs végrehajtódik.
   * @param xml XML fájl adatok.
   * @param tableView Tábla/view adatok.
   */
  Code(Shell shell, Xml xml, TableView tableView) {

    this.xml = xml;
    this.tableView = tableView;

    // +---------------------+
    // | metadata beolvasása |
    // +---------------------+
    String command = "SELECT * FROM " + tableView.accessName + " WHERE 0 = 1";
    ResultSetMetaData meta = null;
    try {
      meta = shell.sql(command).getResultSet().getMetaData();
    }
    catch (Exception e) {
      throw new MyException("Nem sikerült a tábla/view struktúrát lekérdező SQL-parancs végrehajtása:\n\n" + 
        command + "\n\nA kapott hibaüzenet:\n\n" + ExceptionTool.traceString(e));

    }

    // +-------------------------+
    // | imports alapbeállítások |
    // +-------------------------+
    imports.add("java.io.Serializable");
    imports.add("xesj.shell.annotation." + (tableView.isTable ? "Table" : "View"));
    imports.add("xesj.shell.annotation.Column");
    if (!tableView.primaryKeyList.isEmpty()) {
      imports.add("xesj.shell.annotation.Id");
    }
    
    // +-----------------------+
    // | egyéb alapbeállítások |
    // +-----------------------+
    final String LS = xml.lineSeparator; // sor váltás
    StringBuilder fields = new StringBuilder(); // az osztály mezői
    final String indent = StringTool.space(xml.indent); // beljebb kezdés
    List<String> primaryKeyListRunOut = new ArrayList<>(); // ebből a listából törlődnie kell az összes elemnek
    primaryKeyListRunOut.addAll(tableView.primaryKeyList);
    List<String> skipListRunOut = new ArrayList<>(); // ebből a listából törlődnie kell az összes elemnek
    skipListRunOut.addAll(tableView.skipList);
    StringBuilder comment = new StringBuilder();
    comment.append("/*").append(LS);
    // comment: "Osztalynev osztalynev = new Osztalynev();"
    String classInstanceName = tableView.className.substring(0,1).toLowerCase() + tableView.className.substring(1);
    comment.append(tableView.className).append(" ").append(classInstanceName).append(" = new ")
      .append(tableView.className).append("();").append(LS);

    // +----------------------+
    // | ciklus az oszlopokon |
    // +----------------------+
    List<ColumnProperty> columnPropertyList = Tool.getColumnsFromMetaData(meta);
    for (ColumnProperty cp: columnPropertyList) {
      String 
        packageName = cp.cls.getPackage().getName(),
        className = cp.cls.getName(),
        classSimpleName = cp.cls.getSimpleName(),
        columnName = cp.name;
      // ki kell hagyni az oszlopot (skip) ?
      if (tableView.skipList.contains(columnName)) {
        skipListRunOut.remove(columnName);
        continue; // az oszlop nem kerül feldolgozásra!
      }
      // imports
      if (!packageName.equals("java.lang")) {
        imports.add(className);
      }
      // fields
      if (fields.length() > 0) fields.append(LS);
      fields.append(indent).append("@Column");
      int pkIndex = tableView.primaryKeyList.indexOf(columnName);
      if (pkIndex >= 0) {
        // @Id berakása
        fields.append(" @Id");
        if (tableView.primaryKeyList.size() > 1) fields.append("(").append(pkIndex + 1).append(")");
        primaryKeyListRunOut.remove(columnName);
      }
      fields.append(LS).append(indent).append("public ").append(classSimpleName).append(" ").append(columnName).append(";");
      // comment: "osztalynev.mezonev ="
      comment.append(classInstanceName).append(".").append(columnName).append(" =").append(LS);
    } 
    // oszlopciklus vége
    comment.append("*/");
    if (!primaryKeyListRunOut.isEmpty()) {
      throw new MyException("A(z) '" + tableView.name + "' nevű " + (tableView.isTable ? "táblánál" : "viewnál") 
        + " nem értelmezhető a(z) '" + primaryKeyListRunOut.get(0) + "' primary key." );
    }
    if (!skipListRunOut.isEmpty()) {
      throw new MyException("A(z) '" + tableView.name + "' nevű " + (tableView.isTable ? "táblánál" : "viewnál") 
        + " nem értelmezhető a(z) '" + skipListRunOut.get(0) + "' skip." );
    }
    if (fields.length() == 0) {
      throw new MyException("A(z) '" + tableView.name + "' nevű " + (tableView.isTable ? "táblánál" : "viewnál") 
        + " nincs egyetlen generálandó oszlop sem."); 
    }

    // +------------------------+
    // | teljes kód összerakása |
    // +------------------------+
    code.append("package ").append(xml.packageName).append(";").append(LS);
    for (String imp: imports) {
      code.append("import ").append(imp).append(";").append(LS);
    }
    code
      .append("/**").append(LS)
      .append(" * @author Shell DIC generátor").append(LS)
      .append(" */").append(LS)
      .append(tableView.isTable ? "@Table(\"" : "@View(\"").append(tableView.name).append("\")").append(LS)
      .append("public class ").append(tableView.className).append(" implements Serializable {").append(LS)
      .append(fields).append(LS)
      .append("}").append(LS)
      .append(comment).append(LS);

  }

  /**
   * Forráskód kiírása fájlba
   */
  void toFile() throws FileNotFoundException, IOException {

    File file = new File(xml.targetDirectory, tableView.className + ".java");
    OutputStreamWriter writer;
    try {
      writer = new OutputStreamWriter(new FileOutputStream(file), xml.encode);
    } 
    catch (UnsupportedEncodingException ex) {
      throw new MyException("A(z) '" + xml.encode + "' <encode> nem támogatott.");
    }
    writer.write(code.toString());
    writer.close();

  }

  // ====
}
