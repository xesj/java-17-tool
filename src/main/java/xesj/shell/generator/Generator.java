package xesj.shell.generator;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.JTextComponent;
import xesj.args.ArgumentException;
import xesj.args.ArgumentHandler;
import xesj.args.ArgumentRule;
import xesj.args.HelpException;
import xesj.shell.MyException;
import xesj.tool.ExceptionTool;
import xesj.tool.ThreadTool;
import xesj.xml.LineRule;

/**
 * Java forráskód generátor fő osztály. 
 */
public class Generator extends JFrame implements ActionListener, KeyListener {

  /**
   * Program argumentum: Default könyvtár az XML fájl választó ablakban. Nem kötelező.
   */
  static String xmlBrowseArgument;

  /**
   * Program argumentum: Default XML fájlnév. Nem kötelező.
   */
  static String xmlFileArgument;

  /**
   * Program argumentum: Default könyvtár a cél könyvtár választó ablakban. Nem kötelező.
   */
  static String targetBrowseArgument;

  /**
   * Program argumentum: Default cél könyvtár. Nem kötelező.
   */
  static String targetDirArgument;

  /**
   * Generátor példány.
   */
  static Generator generator;

  /**
   * Adatbázis URL felirat.
   */
  JLabel dbUrlLabel;

  /**
   * Adatbázis felhasználó felirat.
   */
  JLabel dbUserLabel;

  /**
   * Adatbázis jelszó felirat.
   */
  JLabel dbPasswordLabel;

  /**
   * XML mező.
   */
  JTextField xmlField;

  /**
   * Adatbázis URL mező.
   */
  JTextField dbUrlField;

  /**
   * Adatbázis felhasználó mező.
   */
  JTextField dbUserField;

  /**
   * Cél könyvtár mező.
   */
  JTextField dirField;

  /**
   * Adatbázis jelszó mező.
   */
  JPasswordField dbPasswordField;

  /**
   * Kiíró terület scroll.
   */
  JScrollPane printAreaScroll;

  /**
   * Kiíró terrület.
   */
  JTextArea printArea;

  /**
   * Minta XML letöltés gomb.
   */
  JButton mintaButton;

  /**
   * XML fájl kiválasztó gomb.
   */
  JButton xmlButton;

  /**
   * Cél könyvtár kiválasztó gomb.
   */
  JButton dirButton;

  /**
   * Generálás gomb.
   */
  JButton generateButton;

  /**
   * Elemeket körülvevő általános margó.
   */
  Insets insets0 = new Insets(10, 10, 10, 10);

  /**
   * Mezők háttérszíne.
   */
  static final Color fieldNormalBackgroundColor = new Color(Integer.parseInt("FFFFCC", 16));

  /**
   * Az összes elem magassága (kivéve textarea).
   */
  static final int HEIGHT_ALL = 35; 

  /**
   * Generátor belépési pont.
   * @param args Program argumentumok.
   */
  public static void main(String[] args) throws Exception {

    ArgumentHandler handler = new ArgumentHandler()
      .addRule(
        new ArgumentRule("xml.browse", String.class, null, false)
        .description("Default könyvtár az XML fájl választó ablakban")
      )
      .addRule(
        new ArgumentRule("xml.file", String.class, null, false)
        .description("Default XML fájlnév")
      )
      .addRule(
        new ArgumentRule("target.browse", String.class, null, false)
        .description("Default könyvtár a cél könyvtár választó ablakban")
      )
      .addRule(
        new ArgumentRule("target.dir", String.class, null, false)
        .description("Default cél könyvtár")
      );
    try {
      handler.check(args);
    }
    catch (HelpException e) {
      System.out.println("Program argumentumokra vonatkozó szabályok:");
      System.out.print(handler.help());
      ThreadTool.sleep(Long.MAX_VALUE);
      System.exit(1);      
    }
    catch (ArgumentException e) {
      System.out.println("A program hibás argumentumokkal lett indítva:");
      System.out.println(e.getMessage());
      ThreadTool.sleep(Long.MAX_VALUE);
      System.exit(1);      
    }
    xmlBrowseArgument = handler.getString("xml.browse");
    xmlFileArgument = handler.getString("xml.file");
    targetBrowseArgument = handler.getString("target.browse");
    targetDirArgument = handler.getString("target.dir");
    // UI beállítás
    UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
    // generátor aktiválása
    new Generator();

  }

  /**
   * Konstruktor
   */
  public Generator() {

    super("Shell DIC generátor");
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setLayout(new GridBagLayout());
    getContentPane().setBackground(Color.WHITE);
    // mintaButton 
    mintaButton = new JButton("Minta XML letöltés");
    setButton(mintaButton);
    add(
      mintaButton, 
      new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // xmlButton
    xmlButton = new JButton("XML fájl:");
    setButton(xmlButton);
    add(
      xmlButton, 
      new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // xmlField
    xmlField = new JTextField(50);
    setField(xmlField);
    xmlField.addKeyListener(this);
    add(
      xmlField, 
      new GridBagConstraints(1, 1, 6, 1, 100, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    if (xmlFileArgument != null) {
      xmlField.setText(xmlFileArgument);
    }
    // dbUrlLabel
    dbUrlLabel = new JLabel("Adatbázis URL:");
    setLabel(dbUrlLabel);    
    add(
      dbUrlLabel, 
      new GridBagConstraints(1, 2, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // dbUrlField
    dbUrlField = new JTextField();
    setField(dbUrlField);
    dbUrlField.setEditable(false);
    add(
      dbUrlField, 
      new GridBagConstraints(2, 2, 1, 1, 70, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // dbUserLabel
    dbUserLabel = new JLabel("Felhasználó:");
    setLabel(dbUserLabel);    
    add(
      dbUserLabel, 
      new GridBagConstraints(3, 2, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // dbUserField
    dbUserField = new JTextField();
    setField(dbUserField);
    dbUserField.setEditable(false);
    add(
      dbUserField, 
      new GridBagConstraints(4, 2, 1, 1, 30, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // dbPasswordLabel
    dbPasswordLabel = new JLabel("Jelszó:");
    setLabel(dbPasswordLabel);
    add(
      dbPasswordLabel, 
      new GridBagConstraints(5, 2, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // dbPasswordField
    dbPasswordField = new JPasswordField(10);
    setField(dbPasswordField);
    add(
      dbPasswordField, 
      new GridBagConstraints(6, 2, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // dirButton
    dirButton = new JButton("Cél könyvtár:");
    setButton(dirButton);
    add(
      dirButton, 
      new GridBagConstraints(0, 3, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // dirField
    dirField = new JTextField(50);
    setField(dirField);
    add(
      dirField, 
      new GridBagConstraints(1, 3, 5, 1, 100, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    if (targetDirArgument != null) {
      dirField.setText(targetDirArgument);
    }
    // generateButton
    generateButton = new JButton("Generálás");
    setButton(generateButton);
    add(
      generateButton, 
      new GridBagConstraints(6, 3, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // printArea
    printArea = new JTextArea(20, 100);
    setField(printArea);
    printArea.setEditable(false);
    printArea.setBorder(new EmptyBorder(5, 5, 5, 5));
    // printAreaScroll
    printAreaScroll = new JScrollPane(printArea);
    add(
      printAreaScroll, 
      new GridBagConstraints(0, 4, 7, 1, 100, 100, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets0, 0, 0)
    );
    // pack, show
    pack();
    setMinimumSize(getPreferredSize());
    setLocationRelativeTo(null);
    setVisible(true);
    generator = this;
    // init
    try {
      Gtool.rules = Arrays.asList(
        // gyökérelem gyerekágak
        new LineRule("/database_driver", null, 1), 
        new LineRule("/database_url", 1, 1),
        new LineRule("/database_user", 1, 1),
        new LineRule("/sql", null, null),
        new LineRule("/encode", null, 1),
        new LineRule("/line_separator", null, 1),
        new LineRule("/indent", null, 1),
        new LineRule("/margin", null, 1),
        new LineRule("/package", 1, 1),
        new LineRule("/table", null, null),
        new LineRule("/view", null, null),
        // "/table" gyerekágak
        new LineRule("/table/name", 1, 1), 
        new LineRule("/table/access_name", null, 1), 
        new LineRule("/table/class_name", null, 1),
        new LineRule("/table/primary_key", null, 1),
        new LineRule("/table/skip", null, 1),
        // "/view" gyerekágak
        new LineRule("/view/name", 1, 1), 
        new LineRule("/view/access_name", null, 1), 
        new LineRule("/view/class_name", null, 1),
        new LineRule("/view/primary_key", null, 1),
        new LineRule("/view/skip", null, 1)
      );
    }
    catch (Exception e) {
      Gtool.printException(e);
    }

  }

  /**
   * Gomb általános beállítása.
   * @param button Beállítandó gomb.
   */
  private void setButton(JButton button) {

    button.setFont(new Font(Font.DIALOG, Font.PLAIN, 16));
    button.addActionListener(this);
    button.setFocusable(false);
    correctSize(button);

  }

  /**
   * Mező általános beállítása.
   * @param field Beállítandó mező.
   */
  private void setField(JTextComponent field) {

    field.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 16));
    field.setBackground(fieldNormalBackgroundColor);
    correctSize(field);

  }

  /**
   * Cimke általános beállítása.
   * @param label Beállítandó cimke.
   */
  private void setLabel(JLabel label) {

    label.setFont(new Font(Font.DIALOG, Font.PLAIN, 16));
    correctSize(label);

  }

  /**
   * Komponens méret korrekt beállítása.
   * Akkor nem fog összecsuklani a komponens ha a minimum, és a preferred méretei megegyeznek.
   * JTextArea-ra nem adható ki mert elveszíti a scrollozhatóságát!
   * @param component Beállítandó komponens.
   */
  private void correctSize(JComponent component) {

    if (component == printArea) return;
    Dimension dimension = new Dimension(component.getPreferredSize().width, HEIGHT_ALL);
    component.setPreferredSize(dimension); 
    component.setMinimumSize(dimension);

  }

  /**
   * Gombnyomások lekezelése.
   * @param event Esemény.
   */
  @Override
  public void actionPerformed(ActionEvent event) {

    try {
      if (event.getSource() == mintaButton) mintaButtonPress();
      if (event.getSource() == xmlButton) xmlButtonPress();
      if (event.getSource() == dirButton) dirButtonPress();
      if (event.getSource() == generateButton) generateButtonPress();
    }  
    catch (MyException me) {
      Gtool.printMyException(me);
    }
    catch (Exception e) {
      Gtool.printException(e);
    }

  }

  /**
   * Enter lekezelése az xml mezőn.
   * @param event Esemény.
   */
  @Override
  public void keyPressed(KeyEvent event) {

    try {
      if (event.getSource() == xmlField) {
        if (event.getKeyCode() == KeyEvent.VK_ENTER) {
          new Xml().readDatabaseData();
        }
      }
    } 
    catch (MyException me) {
      Gtool.printMyException(me);
    }
    catch (Exception e) {
      Gtool.printException(e);
    }

  }

  /**
   * Billentyű felengedése esemény kezelő.
   * @param event Esemény. 
   */
  @Override
  public void keyReleased(KeyEvent event) {}

  /**
   * Karakter begépelése esemény kezelő.
   * @param event Esemény. 
   */
  @Override
  public void keyTyped(KeyEvent event) {}

  /**
   * 'Minta XML letöltés' gombnyomás lekezelése
   */
  public void mintaButtonPress() {

    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setDialogTitle("Minta XML letöltés");
    fileChooser.setSelectedFile(new File("DIC-generator-minta.xml"));
    if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
      File targetFile = fileChooser.getSelectedFile();
      InputStream sourceStream = getClass().getResourceAsStream("DIC-generator-minta.xml");
      try {
        Files.copy(sourceStream, targetFile.toPath());
        Gtool.printMessage("A minta XML fájl letöltődött: " + targetFile.getAbsolutePath());
      } 
      catch (IOException e) {
        throw new MyException("A minta XML letöltése sikertelen.\n\n" + ExceptionTool.traceString(e));
      }
    }
    else {
      Gtool.printMessage(null);
    }

  }

  /**
   * 'XML fájl' gombnyomás lekezelése
   */
  public void xmlButtonPress() {

    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setFileFilter(new FileNameExtensionFilter("XML", "xml"));
    fileChooser.setDialogTitle("XML fájl kiválasztása");
    fileChooser.setApproveButtonText("Kiválasztás");
    if (xmlBrowseArgument != null) {
      fileChooser.setCurrentDirectory(new File(xmlBrowseArgument));
    }
    if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      File file = fileChooser.getSelectedFile();
      xmlField.setText(file.getAbsolutePath());
      new Xml().readDatabaseData();
    } 
    else {
      xmlField.setText(null);
      dbUrlField.setText(null);
      dbUserField.setText(null);
      Gtool.printMessage(null);
    }  

  }

  /**
   * 'Cél könyvtár' gombnyomás lekezelése
   */
  public void dirButtonPress() {

    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    fileChooser.setDialogTitle("Cél könyvtár kiválasztása");
    fileChooser.setApproveButtonText("Kiválasztás");
    if (targetBrowseArgument != null) {
      fileChooser.setCurrentDirectory(new File(targetBrowseArgument));
    }
    if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      File file = fileChooser.getSelectedFile();
      dirField.setText(file.getAbsolutePath());
    } 
    else {
      dirField.setText(null);
    }  

  }

  /**
   * 'Generálás' gombnyomás lekezelése
   */
  private void generateButtonPress() {

    try {
      new Xml().generateSourceCode();
    } 
    catch (MyException me) {
      Gtool.printMyException(me);
    }
    catch (Exception e) {
      Gtool.printException(e);
    }

  }

  // ====
}
