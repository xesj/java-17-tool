package xesj.shell.generator;
import java.awt.Color;
import java.nio.charset.Charset;
import java.util.List;
import java.util.SortedMap;
import javax.swing.JTextArea;
import xesj.shell.MyException;
import xesj.tool.ExceptionTool;
import xesj.xml.LineRule;

/**
 * Generátor tool.
 */
public class Gtool {

  /**
   * Engedélyezett karakterek az osztálynévben.
   */
  static final String classNameEnabledCharacters = 
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
    "abcdefghijklmnopqrstuvwxyz" + 
    "0123456789_";

  /**
   * XML ágaira vonatkozó szabályok. 
   */
  static List<LineRule> rules;

  /**
   * Normál üzenet kiírása print-area területre.
   * @param message Kiírandó üzenet.
   */
  static void printMessage(String message) {

    JTextArea out = Generator.generator.printArea;
    out.setText(message);
    out.setCaretPosition(0);
    out.setForeground(Color.black);
    out.setBackground(Generator.fieldNormalBackgroundColor);

  }

  /**
   * MyException kiírása print-area területre.
   * @param myException Kiírandó MyException példány.
   */
  static void printMyException(MyException myException) {

    JTextArea out = Generator.generator.printArea;
    out.setText("Hiba:\n\n" + myException.getMessage());
    out.setCaretPosition(0);
    out.setForeground(Color.red.darker());
    out.setBackground(Generator.fieldNormalBackgroundColor);

  }

  /**
   * Exception kiírása print-area területre.
   * @param exception Kiírandó Exception példány.
   */
  static void printException(Exception exception) {

    JTextArea out = Generator.generator.printArea;
    out.setText("Programhiba:\n\n" + ExceptionTool.traceString(exception));
    out.setCaretPosition(0);
    out.setForeground(Color.white);
    out.setBackground(Color.red.darker());

  }

  /**
   * Osztálynév létrehozása név alapján. 
   * @param name Az osztálynév alapjául szolgáló név.
   */
  public static String createClassName(String name) {

    name = name.toLowerCase();
    StringBuilder sb = new StringBuilder();
    boolean isUpper = true;
    for (int i = 0; i < name.length(); i++) {
      char c = name.charAt(i);
      if (c == '.' || c == '_') {
        isUpper = true;
      }
      else {
        if (isUpper) {
          c = Character.toUpperCase(c);
          isUpper = false;
        }
        sb.append(c);
      }  
    }
    return sb.toString();

  }

  /**
   * Osztálynév szintaktikai ellenőrzése. 
   * Szintaktikai hiba esetén MyException-t dob.
   * @param name Hiba kiíráshoz szükséges név.
   * @param className Ellenőrizendő osztálynév.
   */
  public static void checkClassName(String name, String className) {

    // legalább 1 karakteres ?
    if (className.isEmpty()) {
      throw new MyException("A <name>" + name + "</name> taghoz tartozó <class_name> üres.");
    }

    // az első karakter nagybetű ?
    if (className.charAt(0) != Character.toUpperCase(className.charAt(0))) {
      throw new MyException("A <name>" + name + "</name> taghoz tartozó <class_name> tag első karaktere nem nagybetű.");
    }

    // minden karakter helyes ?
    for (int i = 0; i < className.length(); i++) {
      char c = className.charAt(i);
      if (classNameEnabledCharacters.indexOf(c) == -1) {
        throw new MyException(
          "A <name>" + name + "</name> taghoz tartozó <class_name> tag hibás karaktert tartalmaz: '" + c + "'"
        );
      }
    }

    // számmal kezdődik ?
    char c = className.charAt(0);
    if (c >= 48 && c <= 57) {
      throw new MyException("A <name>" + name + "</name> taghoz tartozó <class_name> tag számmal kezdődik: '" + c + "'");
    }

  }

  /**
   * Karakter kódolás nevének ellenőrzése.
   * @param encode Karakter kódolás.
   * @return True: létező encode. False: hibás encode.
   */
  static boolean checkEncode(String encode) {

    SortedMap<String,Charset> map = Charset.availableCharsets();
    return map.containsKey(encode);

  }

  // ====
}
