package xesj.shell.generator;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import xesj.shell.MyException;
import xesj.xml.Line;

/**
 * Tábla/view adatok.
 */
class TableView {

  /**
   * Jelzi hogy a sor <table>-e ? (különben <view>).
   */
  boolean isTable;

  /**
   * <name> tag
   */
  String name;

  /**
   * <access_name> tag
   */
  String accessName;

  /**
   * <class_name> tag
   */
  String className;

  /**
   * <primary_key> tag
   */
  String primaryKeyTag;

  /**
   * Primary key értékek. Ha nincs ilyen, akkor üres lista.
   */
  List<String> primaryKeyList = new ArrayList<>();

  /**
   * <skip> tag
   */
  String skipTag;

  /**
   * Skip értékek. Ha nincs ilyen, akkor üres lista.
   */
  List<String> skipList = new ArrayList<>();

  /**
   * Konstruktor
   * @param line XML egy <table> vagy <view> ága.
   */
  TableView(Line line) {

    isTable = line.name().equals("table");
    name = line.child("name").text();
    accessName = line.child("access_name").text();
    className = line.child("class_name").text();
    primaryKeyTag = line.child("primary_key").text();
    skipTag = line.child("skip").text();

    // ellenőrzések
    if (accessName == null) {
      accessName = name;
    }
    if (className == null) {
      className = Gtool.createClassName(name);
    }
    Gtool.checkClassName(name, className);

    // primary key szétszedése
    if (primaryKeyTag != null) {
      StringTokenizer tokenizer = new StringTokenizer(primaryKeyTag, ",");
      while (tokenizer.hasMoreTokens()) {
        String token = tokenizer.nextToken().trim().toLowerCase();
        if (!token.isEmpty()) {
          if (primaryKeyList.contains(token)) {
            throw new MyException(
              "A <name>" + name + "</name> taghoz tartozó <primary_key> többszörösen tartalmazza ezt az elemet: '" + 
                token + "'"
            );
          } 
          primaryKeyList.add(token);
        }  
      }
    }

    // skip szétszedése
    if (skipTag != null) {
      StringTokenizer tokenizer = new StringTokenizer(skipTag, ",");
      while (tokenizer.hasMoreTokens()) {
        String token = tokenizer.nextToken().trim().toLowerCase();
        if (!token.isEmpty()) {
          if (skipList.contains(token)) {
            throw new MyException(
              "A <name>" + name + "</name> taghoz tartozó <skip> többszörösen tartalmazza ezt az elemet: '" + 
                token + "'"
            );
          } 
          if (primaryKeyList.contains(token)) {
            throw new MyException(
              "A <name>" + name + "</name> taghoz tartozó '" + 
                token + "' <skip> elem szerepel a <primary_key> tag-en belül is."
            );
          }
          skipList.add(token);
        }  
      }
    }

  }

  // ====
}
