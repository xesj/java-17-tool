package xesj.shell.generator;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import xesj.shell.Database;
import xesj.shell.MyException;
import xesj.shell.NullStrategy;
import xesj.shell.Shell;
import static xesj.shell.generator.Gtool.printMessage;
import xesj.tool.ExceptionTool;
import xesj.tool.StringTool;
import xesj.xml.Line;
import xesj.xml.Tree;
import xesj.xml.TreeCheckException;

/**
 * XML feldolgozó.
 */
class Xml {

  /**
   * Generátor
   */
  private final Generator generator = Generator.generator;

  /** 
   * Feldolgozandó XML fájl. 
   */
  private File xmlFile;

  /**
   * Cél könyvtár.
   */
  File targetDirectory;

  /**
   * Adatbázis driver.
   */
  private String databaseDriver;

  /**
   * Adatbázis URL.
   */
  private String databaseURL;

  /**
   * Adatbázis user.
   */
  private String databaseUser;

  /**
   * Adatbázis password.
   */
  private String databasePassword;

  /**
   * SQL-parancsok listája.
   */
  private List<String> sqlList;

  /**
   * Generált fájl karakter kódolása.
   */
  String encode;

  /**
   * Sor elválasztó karakter(ek).
   */
  private String lineSeparatorTag;

  /**
   * Sor elválasztó valós karakter(ek).
   */
  String lineSeparator;

  /**
   * Generált forráskód bekezdés.
   */
  private String indentTag;

  /**
   * Generált forráskód valós bekezdés.
   */
  int indent;

  /**
   * Generált forráskód margó.
   */
  private String marginTag;

  /**
   * Generált forráskód valós margó.
   */
  int margin;

  /**
   * Generált forráskód csomagnév (package).
   */
  String packageName;

  /**
   * XML-ben definiált táblák és nézetek.
   */
  private List<TableView> tableViewList;

  /**
   * XML beolvasása.
   */
  private void readXML() {

    // alap ellenőrzések
    String xmlFileStr = generator.xmlField.getText();
    if (xmlFileStr.isEmpty()) {
      throw new MyException("Az XML fájl kiválasztása kötelező.");
    }
    xmlFile = new File(xmlFileStr);
    if (!xmlFile.exists()) {
      throw new MyException("Az XML fájl nem létezik.");
    }
    if (!xmlFile.canRead()) {
      throw new MyException("Az XML fájl nem olvasható.");
    }
    Tree tree;
    try {
      tree = new Tree(xmlFile);
    } 
    catch (SAXException | IOException | ParserConfigurationException ex) {
      throw new MyException("Az megadott fájl nem XML formátumú.");
    }
    try {
      tree.check("generator", Gtool.rules, true);
    } 
    catch (TreeCheckException e) {
      throw new MyException("Az XML fájl struktúrája hibás:\n\n" + e.getMessage());
    }  
    // -- adatok beolvasása --

    // <database_url>
    databaseURL = tree.root().child("database_url").text();

    // <database_user>
    databaseUser = tree.root().child("database_user").text();

    // <database_driver>
    databaseDriver = tree.root().child("database_driver").text();

    // <sql>
    sqlList = new ArrayList<>();
    List<Line> sqlLines = tree.root().children("sql");
    for (Line sqlLine: sqlLines) {
      sqlList.add(sqlLine.text());
    }

    // <encode>
    encode = tree.root().child("encode").text();
    if (encode != null && !Gtool.checkEncode(encode)) {
      throw new MyException("A(z) '" + encode + "' <encode> nem támogatott.");
    }

    // <line_separator>
    lineSeparatorTag = tree.root().child("line_separator").text();
    if (!(lineSeparatorTag == null || lineSeparatorTag.toUpperCase().equals("UNIX") || 
      lineSeparatorTag.toUpperCase().equals("DOS"))) {
      throw new MyException("A <line_separator> tag csak 'UNIX' vagy 'DOS' lehet.");
    }
    if (lineSeparatorTag == null) {
      lineSeparator = System.getProperty("line.separator");
    }
    else if (lineSeparatorTag.toUpperCase().equals("UNIX")) {
      lineSeparator = "\n";
    }
    else if (lineSeparatorTag.toUpperCase().equals("DOS")) {
      lineSeparator = "\r\n";
    }

    // <indent>
    indentTag = tree.root().child("indent").text();
    if (indentTag == null) {
      indent = 2;
    }
    else {
      try {
        indent = Integer.parseInt(indentTag);
        if (indent < 0 || indent > 20) throw new NumberFormatException();
      }
      catch (NumberFormatException e) {
        throw new MyException("Az <indent> tag csak 0..20 értékű lehet.");
      }
    }

    // <margin>
    marginTag = tree.root().child("margin").text();
    if (marginTag == null) {
      margin = 150;
    }
    else {
      try {
        margin = Integer.parseInt(marginTag);
        if (margin < 100 || margin > 1000) throw new NumberFormatException();
      }
      catch (NumberFormatException e) {
        throw new MyException("A <margin> tag csak 100..1000 értékű lehet.");
      }
    }

    // <package>
    packageName = tree.root().child("package").text();

    // <table> és <view>
    tableViewList = new ArrayList<>();
    List<Line> tableViewLines = tree.root().children("table");
    tableViewLines.addAll(tree.root().children("view"));
    for (Line line: tableViewLines) {
      tableViewList.add(new TableView(line));
    }

  }

  /**
   * Adatbázis adatok olvasása az XML-ből.
   */
  void readDatabaseData() {

    // mezők kiürítése
    printMessage(null);
    generator.dbUrlField.setText(null);
    generator.dbUserField.setText(null);

    // XML beolvasása
    readXML();

    // mezők kitöltése
    generator.dbUrlField.setText(databaseURL);
    generator.dbUrlField.setCaretPosition(0);
    generator.dbUserField.setText(databaseUser);
    generator.dbUserField.setCaretPosition(0);
    generator.dbPasswordField.requestFocus();

  }

  /**
   * Java forráskód generálása.
   */
  void generateSourceCode() throws IOException {

    readDatabaseData();

    // ellenőrzések
    databasePassword = new String(generator.dbPasswordField.getPassword());
    if (StringTool.isNullOrEmpty(databasePassword)) {
      throw new MyException("A jelszó kitöltése kötelező.");
    }
    targetDirectory = new File(generator.dirField.getText());
    if (generator.dirField.getText().isEmpty()) {
      throw new MyException("A cél könyvtár kitöltése kötelező.");
    }
    if (!targetDirectory.exists()) {
      throw new MyException("A cél könyvtár nem létezik.");
    }
    if (!targetDirectory.isDirectory()) {
      throw new MyException("A cél könyvtár nem könyvtár.");
    }
    if (!targetDirectory.canRead()) {
      throw new MyException("A cél könyvtár nem olvasható.");
    }
    if (!targetDirectory.canWrite()) {
      throw new MyException("A cél könyvtár nem írható.");
    }

    // az osztálynevek egyediek ?
    Set<String> classNameSet = new HashSet<>();
    for (TableView tableView: tableViewList) {
      String className = tableView.className;
      // egyediség ?
      if (!classNameSet.add(className)) {
        throw new MyException("Az XML fájlban többször fordul elő ez az osztálynév: '" + className + "'");
      }
    }

    // az osztálynevekkel nincs egyező nevű java-fájl a cél könyvtárban ? 
    File[] javaFiles = targetDirectory.listFiles(f -> f.getName().endsWith(".java"));
    for (File javaFile: javaFiles) {
      String naturFilename = javaFile.getName().substring(0, javaFile.getName().length()-5); // '.java' nélküli fájlnév
      if (classNameSet.contains(naturFilename)) {
        throw new MyException("A cél könyvtár már tartalmaz egy '" + javaFile.getName() + "' nevű fájlt.");
      }
    }

    // csatlakozás az adatbázishoz
    if (!StringTool.isNullOrEmpty(databaseDriver)) {
      try {
        Class.forName(databaseDriver);
      } 
      catch (ClassNotFoundException ex) {
        throw new MyException(
          "A(z) '" + databaseDriver + "' adatbázis driver nem található.\n" + 
            "Ellenőrizd hogy a driver neve helyesen van-e megadva, " +
            "illetve hogy a driver-re hivatkozik-e a 'java classpath' beállítás."
        );
      }
    }  
    Shell shell = null;
    try {
      Connection connection = null;
      try {
        connection = DriverManager.getConnection(databaseURL, databaseUser, databasePassword);
      }
      catch (SQLException e) {
        throw new MyException(
          "Nem sikerült csatlakozni az adatbázishoz. A kapott hibaüzenet:\n\n" + ExceptionTool.traceString(e)
        );
      }
      shell = new Shell(connection, new Database("any", NullStrategy.SET_NULL_WITH_NULL_TYPE)); // tetszőleges null-stratégia

      // xml-ben definiált sql parancsok végrehajtása
      for (String command: sqlList) {
        try {
          shell.sql(command).execute();
        }
        catch (Exception e) {
          throw new MyException(
            "Nem sikerült az XML-ben definiált SQL-parancs végrehajtása:\n\n" + 
              command + "\n\nA kapott hibaüzenet:\n\n" + ExceptionTool.traceString(e)
          );
        }
      }

      // forráskódok generálása
      List<Code> codeList = new ArrayList<>();
      for (TableView tableView: tableViewList) {
        codeList.add(new Code(shell, this, tableView));
      }

      // forráskódok kiírása fájlokba
      for (Code code: codeList) {
        code.toFile();       
      }

      // generálás kész, üzenet a felhasználónak
      StringBuilder message = new StringBuilder();
      message.append("A generálás sikeresen befejeződött !\nA következő fájlok jöttek létre:\n\n");
      for (TableView tableView: tableViewList) {
        message.append(tableView.className).append(".java\n");
      }
      Gtool.printMessage(message.toString());
    } 
    finally {
      try { shell.close(); } catch (Exception e) {}
    }

  }

  // ====
}
