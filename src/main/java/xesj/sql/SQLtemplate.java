package xesj.sql;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import xesj.tool.StringTool;

/**
 * Az osztály a text-blokk használatával együtt segít az SQL-parancs előállításában úgy, hogy a konstruktorban
 * egy SQL-template megadása szükséges.<br/> 
 * A template-ben használható a "{...}" szöveg.<br/>
 * Ez arra utal, hogy később ezt a szöveget a replace() metódussal másra kell cserélni.<br/>
 * A template-ben a sorok végén használható a "--(...)" szöveg, melynek neve: <b>marker</b>.<br/>
 * Ez arra utal, hogy később ez a sor törlésre kerülhet az SQL-parancsból a delete() metódussal.
 */
@Getter
public class SQLtemplate {
  
  private List<String> templateList;

  /**
   * Konstruktor.
   * Ha a template sorváltás karakterrel ér véget, akkor azt az 1 karaktert törli (a text-blokk miatt).
   * @param template SQL-template.
   */
  public SQLtemplate(String template) {
    
    // Ellenőrzés
    if (template == null) throw new SQLtemplateException("A 'template' paraméter nem lehet null!");
    
    // Sorváltás karakterrel végződik ? Ha igen, akkor 1 karakter törlés a végéről.
    if (template.endsWith("\n")) template = template.substring(0, template.length() - 1);
    
    // Darabolás listába
    String[] templateArray = template.split("\n", -1);
    templateList = new ArrayList<>(Arrays.asList(templateArray));
    
  }
  
  /**
   * Szöveg cseréje a template-ben.<br/>
   * Nem dob exception-t ha a "{" + target + "}" egyetlen sorban sem létezik.<br/>  
   * @param target 
   *        Mit kell cserélni.<br/>
   *        Minden sorban lecserélődik a "{" + target + "}" szöveg a replacement szövegre.<br/> 
   *        A target nem lehet null, vagy üres string.
   * @param replacement 
   *        Mire kell cserélni.<br/>
   *        Ha a replacement null, az egyenértékű az üres stringgel, teljesen törlődni fog a {target} rész.<br/>
   *        Ha a replacement sorváltás karakterrel ér véget, akkor azt az 1 karaktert törli.<br/>
   *        A replacement hiába tartalmaz sorváltásokat, az eredeti templateList nem bővül új sorokkal, a "\n" karakterek
   *        is ebben a sorban maradnak!<br/>
   * @param condition 
   *        True: megtörténik a csere. False: nem történik csere.
   */
  public SQLtemplate replace(String target, String replacement, boolean condition) {
    
    // El kell végezni a cserét ?
    if (!condition) return this;
    
    // Ellenőrzés: target
    if (StringTool.isNullOrEmpty(target)) {
      throw new SQLtemplateException("A 'target' paraméter nem lehet null, vagy üres string!");
    }
    
    // Replacement módosítás, ha szükséges
    if (replacement == null) replacement = "";
    if (replacement.endsWith("\n")) replacement = replacement.substring(0, replacement.length() - 1);

    // Csere
    target = "{" + target + "}";
    for (int i = 0; i < templateList.size(); i++) {
      String r = templateList.get(i).replace(target, replacement);
      templateList.set(i, r);
    } 
    
    // Válasz
    return this;
    
  }

  /**
   * Lásd:<br/>
   * {@link SQLtemplate#replace(String target, String replacement, boolean condition)}<br/>
   * ahol a condition = true
   */
  public SQLtemplate replace(String target, String replacement) {
    
    return replace(target, replacement, true);
    
  }

  /**
   * Sorok törlése a template-ből.<br/>
   * Nem dob exceptiont ha a "--(" + marker + ")" egyetlen sorban sem létezik.
   * A marker nem lehet null, és üres string.
   * @param marker 
   *        Azok a sorok törlődnek a template-ből melyek "--(" + marker + ")" szöveget tartalmaznak.
   * @param condition 
   *        True: megtörténik a törlés. False: nem történik törlés.
   */
  public SQLtemplate delete(String marker, boolean condition) {
    
    // El kell végezni a törlést ?
    if (!condition) return this;
    
    // Ellenőrzés: marker
    if (StringTool.isNullOrEmpty(marker)) {
      throw new SQLtemplateException("A 'marker' paraméter nem lehet null, vagy üres string!");
    }

    // Törlés
    marker = "--(" + marker + ")";
    for (int i = templateList.size() - 1; i >= 0; i--) {
      if (templateList.get(i).contains(marker)) {
       templateList.remove(i);
      }
    } 
    
    // Válasz
    return this;
    
  }
  
  /**
   * Lásd:<br/>
   * {@link SQLtemplate#delete(String marker, boolean condition)}<br/>
   * ahol a condition = true
   */
  public SQLtemplate delete(String marker) {

    return delete(marker, true);
    
  }
  
  /**
   * Template sorok darabszámának lekérdezése. 
   * A template sorai csökkenhetnek a delete() metódus hatására, de a replace() nem módosítja a sorok számát. 
   */
  public int getLineCount() {

    return templateList.size();
    
  }

  /**
   * Konvertálás string-re.<br/> 
   * A templateList elemeit egy stringbe fűzi össze úgy, hogy minden lista elem után egy sorváltás karaktert tesz,
   * kivéve az utolsó listaelemet.  
   */  
  @Override
  public String toString() {
    
    // Előkészítés
    StringBuilder builder = new StringBuilder();
    
    // Összefűzés
    for (int i = 0; i < templateList.size(); i++) {
      builder.append(templateList.get(i));
      if (i < templateList.size() - 1) builder.append("\n");
    } 
    
    // Válasz
    return builder.toString();
    
  }
  
  // ====
}
