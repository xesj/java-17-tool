package xesj.sql;

/**
 * SQLtemplate osztály által kiváltott exception.
 */
public class SQLtemplateException extends RuntimeException {
  
  /**
   * Konstruktor
   * @param message Hibaüzenet.
   */
  public SQLtemplateException(String message) {
    
    super(message);
    
  }

  // ====
}
