package xesj.tool;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Bájt tool
 */
public class ByteTool {
  
  /**
   * Hexadecimális pattern
   */
  public static final Pattern HEX_PATTERN = Pattern.compile("[0123456789abcdefABCDEF]*");

  /**
   * Bájt konvertálása 2 karakteres nagybetűs hexadecimális értékre pl: "A9"
   * @param bajt Konvertálandó bájt.
   * @return Konvertált érték, például: "00", "F9", "3D". Null paraméter esetén null-t ad vissza.
   */
  public static String byteToHex(Byte bajt) {

    if (bajt == null) return null;
    return String.format("%02X", bajt);

  }

  /**
   * 1-2 karakteres hexadecimális string konvertálása bájtra.
   * @param hex 1-2 karakteres hexadecimális string pl: "5", "d", "00", "f9", "F9", "3D", "3d". 
   * @throws NumberFormatException Ha a paraméter nem 1-2 karakter hosszú, vagy hibás hexadecimális string. 
   * @return Bájtra konvertált érték. Null paraméter esetén null-t ad vissza.
   */
  public static Byte hexToByte(String hex) throws NumberFormatException {

    if (hex == null) return null;
    if (hex.length() < 1 || hex.length() > 2) {
      throw new NumberFormatException("A 'hex' paraméter csak 1 vagy 2 karakterből állhat");
    }
    return (byte)Integer.parseInt(hex, 16);

  }

  /**
   * Bájt konvertálása 8 karakteres bináris értékre pl: "10110111"
   * @param bajt Konvertálandó bájt.
   * @return Konvertált érték, például: "00000000", "10101010". Null paraméter esetén null-t ad vissza.
   */
  public static String byteToBin(Byte bajt) {

    if (bajt == null) return null;
    return StringTool.leftPad(Integer.toBinaryString((int)bajt & 0xFF), 8, '0');

  }

  /**
   * 1-8 karakteres bináris string konvertálása bájtra.
   * @param bin 1-8 karakteres bináris string string pl: "11111111", "0110", "0". 
   * @throws NumberFormatException Ha a paraméter nem 1-8 karakter hosszú, vagy hibás bináris string. 
   * @return Bájtra konvertált érték. Null paraméter esetén null-t ad vissza.
   */
  public static Byte binToByte(String bin) throws NumberFormatException {

    if (bin == null) return null;
    if (bin.length() < 1 || bin.length() > 8) {
      throw new NumberFormatException("A 'bin' paraméter csak 1-8 karakterből állhat");
    }
    return (byte)Integer.parseInt(bin, 2);

  }
  
  /**
   * Bájt konvertálása pozitív Integer-re.
   * @param bajt Konvertálandó bájt.
   * @return 0 - 255 közötti konvertált érték. Null paraméter esetén null-t ad vissza.
   */
  public static Integer byteToInt(Byte bajt) {
    
    if (bajt == null) return null;
    return (bajt < 0 ? 256 + bajt : bajt);
    
  }
  
  /**
   * Pozitív integer konvertálása bájtra.
   * @param number 0 - 255 közötti egész szám. 
   * @return Bájtra konvertált érték. Null paraméter esetén null-t ad vissza.
   */
  public static Byte intToByte(Integer number) {
    
    // Null ?
    if (number == null) return null;
    
    // Ellenőrzés: 0 - 255 közötti ? 
    if (number < 0 || number > 255) {
      throw new RuntimeException("A 'number' paraméter értéke csak 0 - 255 között lehet!");
    }
    
    // Konverzió
    return Byte.parseByte(String.valueOf(number < 128 ? number : number - 256));
    
  }
  
  /**
   * Tetszőlegesen sok, páros számú hexadecimális karaktert tartalmazó string konvertálása bájt tömbre.
   * @param hex Hexadecimális string, például: null, "", "05", "cD80", "1F5cBa97". 
   * @throws NumberFormatException Ha a paraméter nem páros számú karakterből áll, vagy hibás karaktert tartalmaz. 
   * @return Bájt tömbre konvertált érték. Null paraméter esetén null-t ad vissza. Üres paraméter esetén üres tömböt ad vissza. 
   */
  public static byte[] hexToByteArray(String hex) throws NumberFormatException {
    
    // Null ?
    if (hex == null) return null;
    
    // Üres hexa string ?
    if (hex.isEmpty()) return new byte[0];
    
    // Ellenőrzés: páros hosszúságú ?
    if (hex.length() % 2 != 0) {
      throw new NumberFormatException("A hexadecimális érték hossza nem páros!");
    }
    
    // Ellenőrzés: csak a megengedett karakterekből áll ?
    Matcher matcher = HEX_PATTERN.matcher(hex);
    if (!matcher.matches()) {
      throw new NumberFormatException("A hexadecimális érték hibás karaktert tartalmaz!");
    }
    
    // Konvertálás
    byte[] byteArray = new byte[hex.length() / 2];
    for (int i = 0; i < hex.length(); i += 2) {
      byteArray[i / 2] = hexToByte(hex.substring(i, i + 2));
    } 
    
    // Válasz
    return byteArray;
    
  }
  
  /**
   * Bájt tömb konvertálása nagybetűs hexadecimális stringre, melynek hossza mindig páros szám, például: 
   * null, "", "0C", "5C7650FB", "BA7816BF8F01CFEA414140DE5DAE2223B00361A396177A9CB410FF61f20015AD"
   * @param byteArray Konvertálandó bájt tömb. Lehet null, és üres tömb is!
   * @return Konvertált érték. Null paraméter esetén null-t ad vissza. Üres tömb esetén üres stringet ad vissza.
   */
  public static String byteArrayToHex(byte[] byteArray) {

    // Null ?
    if (byteArray == null) return null;
    
    // Üres tömb ?
    if (byteArray.length == 0) return "";
    
    // Konvertálás
    StringBuilder builder = new StringBuilder();
    for (byte b: byteArray) {
      builder.append(byteToHex(b));
    }
    
    // Válasz
    return builder.toString();
    
  }

  // ====
}
