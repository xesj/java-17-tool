package xesj.tool;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import lombok.NonNull;

/**
 * Date tool
 */
public class DateTool {
  
  /** ÉÉÉÉ.HH.NN ÓÓ:PP:MM.MMM, vagyis a legpontosabb milliszekundumos dátum formátum: "yyyy.MM.dd HH:mm:ss.S" */
  public static final SimpleDateFormat FULL_FORMAT = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss.S");

  /** ÉÉÉÉ.HH.NN ÓÓ:PP:MM dátum formátum: "yyyy.MM.dd HH:mm:ss" */
  public static final SimpleDateFormat SEC_FORMAT = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

  /** ÉÉÉÉ.HH.NN dátum formátum: "yyyy.MM.dd" */
  public static final SimpleDateFormat DAY_FORMAT = new SimpleDateFormat("yyyy.MM.dd");

  /** ÓÓ:PP:MM dátum formátum: "HH:mm:ss" */
  public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");

  /** Hét napja magyarul (hétfő, kedd, ...) dátum formátum: "EEEEEEEEEE" */
  public static final SimpleDateFormat WEEK_DAY_FORMAT = new SimpleDateFormat("EEEEEEEEEE", LocaleTool.LOCALE_HU);

  /** Csak év dátum formátum: "yyyy" */
  public static final SimpleDateFormat ONLY_YEAR_FORMAT = new SimpleDateFormat("yyyy");

  /** Csak hónap dátum formátum: "MM" */
  public static final SimpleDateFormat ONLY_MONTH_FORMAT = new SimpleDateFormat("MM");

  /** Csak nap dátum formátum: "dd" */
  public static final SimpleDateFormat ONLY_DAY_FORMAT = new SimpleDateFormat("dd");

  /**
   * static
   */
  static {
    
    FULL_FORMAT.setLenient(false);
    SEC_FORMAT.setLenient(false);
    DAY_FORMAT.setLenient(false);
    TIME_FORMAT.setLenient(false);
    WEEK_DAY_FORMAT.setLenient(false);
    ONLY_YEAR_FORMAT.setLenient(false);
    ONLY_MONTH_FORMAT.setLenient(false);
    ONLY_DAY_FORMAT.setLenient(false);

  }
  
  /**
   * Teljes string formátumban adja vissza a dátumot.
   * @param date Dátum.
   * @return A dátum teljes string formátumban, null esetén null-t ad vissza.
   */
  public static String formatFull(Date date) {

    if (date == null) return null;
    return FULL_FORMAT.format(date);

  }
  
  /**
   * "éééé.hh.nn" string formátumban adja vissza a dátumot.
   * @param date Dátum.
   * @return A dátum éééé.hh.nn formátumban, null esetén null-t ad vissza.
   */
  public static String formatDay(Date date) {

    if (date == null) return null;
    return DAY_FORMAT.format(date);

  }
  
  /**
   * "hétfő", "kedd", ..., "vasárnap" formátumban adja vissza a dátumot.
   * @param date Dátum.
   * @return A dátum hét napja formátumban, null esetén null-t ad vissza.
   */
  public static String formatWeekDay(Date date) {

    if (date == null) return null;
    return WEEK_DAY_FORMAT.format(date);

  }
  
  /**
   * "éééé.hh.nn óó:pp:mm" string formátumban adja vissza a dátumot.
   * @param date Dátum.
   * @return A dátum éééé.hh.nn óó:pp:mm formátumban, null esetén null-t ad vissza.
   */
  public static String formatSec(Date date) {

    if (date == null) return null;
    return SEC_FORMAT.format(date);

  }
  
  /**
   * "óó:pp:mm" string formátumban adja vissza a dátumot.
   * @param date Dátum.
   * @return A dátum óó:pp:mm formátumban, null esetén null-t ad vissza.
   */
  public static String formatTime(Date date) {

    if (date == null) return null;
    return TIME_FORMAT.format(date);

  }
  
  /**
   * A paraméterben megadott formátumban adja vissza a dátumot.
   * @param date Dátum.
   * @param formatMask Formátum maszk.
   * @return A megadott formátumú string, de ha valamelyik paraméter null akkor null-t ad vissza.
   */
  public static String format(Date date, String formatMask) {

    if (date == null || formatMask == null) return null;
    SimpleDateFormat sdf = new SimpleDateFormat(formatMask);
    sdf.setLenient(false);
    return sdf.format(date);

  }
  
  /**
   * Az "éééé.hh.nn" formátumú stringet dátumra konvertálja. A dátum éjféli időpontot fog tartalmazni.
   * @param dateString "éééé.hh.nn" formátumú string.
   * @return A dátum, de ha a paraméter null akkor null-t ad vissza.
   * @throws ParseException Ha a string nem konvertálható dátumra.
   */
  public static Date parseDay(String dateString) throws ParseException {

    return parseWithCheck(dateString, DAY_FORMAT);

  }
  
  /**
   * "éééé.hh.nn óó:pp:mm" formátumú stringet dátum-ra konvertálja. Az ezredmásodperc 0 lesz.
   * @param dateString "éééé.hh.nn óó:pp:mm" formátumú string.
   * @return A dátum, de ha a paraméter null akkor null-t ad vissza.
   * @throws ParseException Ha a string nem konvertálható dátumra.
   */
  public static Date parseSec(String dateString) throws ParseException {

    return parseWithCheck(dateString, SEC_FORMAT);

  }
  
  /**
   * A megadott stringet a megadott formátummal dátum-ra konvertálja.
   * @param dateString Konvertálandó string.
   * @param formatMask Formátum maszk.
   * @return Dátum, de ha valamelyik paraméter null akkor null-t ad vissza.
   * @throws ParseException Ha a string nem konvertálható dátumra.
   */
  public static Date parse(String dateString, String formatMask) throws ParseException {

    SimpleDateFormat formatter = new SimpleDateFormat(formatMask);
    formatter.setLenient(false);
    return parseWithCheck(dateString, formatter);

  }
  
  /**
   * Leellenőrzi hogy az adott string "éééé.hh.nn" formátumú-e.
   * @param dateString A vizsgálandó string.
   * @return True: ha a string dátumra konvertálható. False: a string null vagy nem konvertálható dátumra.
   */
  public static boolean isDayString(String dateString) {

    if (dateString == null) return false;
    try {
      parseDay(dateString);
      return true;
    }
    catch (ParseException pe) {
      return false;
    }

  }
  
  /**
   * Dátum eltolása napokkal előre vagy vissza. A paraméterben kapott dátum nem változik.
   * @param date Eltolni kívánt dátum.
   * @param day Napok száma. Például: 3 = előre 3 nappal, -2 = vissza 2 nappal.
   * @return Eltolt dátum, de ha a dátum paraméter null akkor null-t ad vissza.
   */
  public static Date shiftDay(Date date, int day) {

    if (date == null) return null;
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DAY_OF_YEAR, day);
    return calendar.getTime();

  }
  
  /**
   * Dátum konvertálása éjfélre. A paraméterben kapott dátum nem változik.
   * @param date Dátum.
   * @return Éjfélre konvertált dátum, de ha a paraméter null akkor null-t ad vissza.
   */
  public static Date midnight(Date date) {

    if (date == null) return null;
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.MILLISECOND, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    return calendar.getTime();

  }
  
  /**
   * Dátum konvertálása java.sql.Date objektumra.
   * @param date Dátum.
   * @return java.sql.Date objektum, de ha a paraméter null akkor null-t ad vissza.
   */
  public static java.sql.Date toSqlDate(Date date) {

    if (date == null) return null;    
    return new java.sql.Date(date.getTime());

  }
  
  /**
   * Dátum konvertálása Timestamp objektumra.
   * @param date Dátum.
   * @return Timestamp objektum, de ha a paraméter null akkor null-t ad vissza.
   */
  public static Timestamp toTimestamp(Date date) {

    if (date == null) return null;    
    return new Timestamp(date.getTime());

  }
  
  /**
   * Dátum konvertálása Time objektumra.
   * @param date Dátum.
   * @return Time objektum, de ha a paraméter null akkor null-t ad vissza.
   */
  public static Time toTime(Date date) {

    if (date == null) return null;    
    return new Time(date.getTime());

  }
  
  /**
   * Megvizsgálja a paraméterként kapott évet hogy szökőév-e (366 napos-e) ?
   * @param year Vizsgálandó év.
   * @return True: szökőév, false: nem szökőév.
   */
  public static boolean isLeapYear(int year) {

    return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

  }
  
  /**
   * "éééé", vagyis év formátumban adja vissza a dátumot.
   * @param date Dátum.
   * @return A dátum "éééé" formátumban, null esetén null-t ad vissza.   
   */
  public static String formatOnlyYear(Date date) {

    if (date == null) return null;
    return ONLY_YEAR_FORMAT.format(date);

  }
  
  /**
   * "hh", vagyis hónap formátumban adja vissza a dátumot: "01", "02", ..., "12"
   * @param date Dátum.
   * @return A dátum "hh" formátumban, null esetén null-t ad vissza.   
   */
  public static String formatOnlyMonth(Date date) {

    if (date == null) return null;
    return ONLY_MONTH_FORMAT.format(date);

  }
  
  /**
   * "dd", vagyis nap formátumban adja vissza a dátumot: "01", "02", ..., "31"
   * @param date Dátum.
   * @return A dátum "dd" formátumban, null esetén null-t ad vissza.   
   */
  public static String formatOnlyDay(Date date) {

    if (date == null) return null;
    return ONLY_DAY_FORMAT.format(date);

  }
  
  /**
   * Vizsgálja hogy a paraméterként kapott dátum múltbéli-e.
   * @param dateString ÉÉÉÉ.HH.NN ÓÓ:PP:MM formában megadott dátum.
   * @return True: a paraméterként kapott dátum múltbéli. Null paraméter esetén false-t ad vissza.
   * @throws ParseException Ha a string nem konvertálható dátumra.
   */
  public static boolean isPast(String dateString) throws ParseException {

    if (dateString == null) return false;
    Date d = parseWithCheck(dateString, SEC_FORMAT);
    return (d.compareTo(new Date()) == -1);

  }
  
  /**
   * Dátum string parse szigorú vizsgálattal. 
   * A stringet dátummá alakítva, illetve onnan újra stringre formázva az eredeti stringet kell visszakapni.
   * Ha nem ugyanaz a string keletkezik akkor ParseException váltódik ki.
   */
  private static Date parseWithCheck(String dateString, @NonNull SimpleDateFormat formatter) throws ParseException {

    if (dateString == null) return null;
    Date date = formatter.parse(dateString);
    String dateStringForCheck = formatter.format(date);
    if (!dateString.equals(dateStringForCheck)) {
      throw new ParseException(dateString, 0);
    }
    return date;

  }
  
  // ====
}
