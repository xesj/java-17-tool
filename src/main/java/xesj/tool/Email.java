package xesj.tool;
import java.io.Serializable;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Az osztály email küldést biztosít.
 */
public class Email implements Serializable {

  /**
   * A levél tárgyának és szövegének karakter kódolása. 
   */
  private String charset;

  /**
   * Az üzenet.
   */
  private MimeMessage message;

  /**
   * Multipart.
   */
  private MimeMultipart multipart = new MimeMultipart();

  /**
   * Konstruktor.
   * @param mailServer Levelező szerver.
   * @param mailServerPort Levelező szerver port.
   * @param charset A levél tárgyának és szövegének karakter kódolása. 
   */
  public Email(String mailServer, String mailServerPort, String charset) {

    this.charset = charset;
    // mail-server beállítások
    Properties p = new Properties();
    p.setProperty("mail.smtp.host", mailServer);
    p.setProperty("mail.smtp.port", mailServerPort);
    // levél példány előállítása
    Session session = Session.getInstance(p);
    message = new MimeMessage(session);

  }

  /**
   * Levél feladójának meghatározása.
   * @param from Feladó.
   */
  public void setFrom(String from) throws Exception {

    message.setFrom(new InternetAddress(from));

  }

  /**
   * Levél címzettjének (to) hozzáadása.
   * @param to Címzett (to).
   */
  public void addTo(String to) throws Exception {

    message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

  }

  /**
   * Levél címzettjének (cc) hozzáadása.
   * @param cc Címzett (cc).
   */
  public void addCc(String cc) throws Exception {

    message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));

  }

  /**
   * Levél címzettjének (bcc) hozzáadása.
   * @param bcc Címzett (bcc).
   */
  public void addBcc(String bcc) throws Exception {

    message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));

  }

  /**
   * Levél tárgyának meghatározása.
   * @param subject Levél tárgya.
   */
  public void setSubject(String subject) throws Exception {

    message.setSubject(subject, charset);

  }

  /**
   * Levél szövegének meghatározása.
   * @param text Levél szövege.
   * @param subtype Levél mime típusa a "text/" után.
   *                Egyszerű szöveges levél esetén az értéke "plain". 
   *                Ekkor minden karakter úgy jelenik meg ahogy meg van adva.
   *                Html levél esetén az értéke "html". Ekkor html-ként értelmeződik a levél szöveg.
   */
  public void setText(String text, String subtype) throws Exception {

    MimeBodyPart partText = new MimeBodyPart();
    partText.setText(text, charset, subtype);
    multipart.addBodyPart(partText);

  }

  /**
   * Levélhez csatolandó fájl hozzáadása.
   * @param fileNameWithPath Csatoláshoz szükséges fájl név. 
   * @param fileNameInMail Levélben megjelenő fájlnév.
   */
  public void addAttachment(String fileNameWithPath, String fileNameInMail) throws Exception {

    MimeBodyPart partFile = new MimeBodyPart();
    DataSource dataSource = new FileDataSource(fileNameWithPath);
    partFile.setDataHandler(new DataHandler(dataSource));
    partFile.setFileName(fileNameInMail);
    multipart.addBodyPart(partFile);

  }

  /**
   * Levél szövegében lévő "inline" image hozzáadása (html levél esetén!)
   * @param fileNameWithPath Image csatoláshoz szükséges fájl név. 
   * @param componentId Az image azonosítója, így kell majd a levél szövegében hivatkozni rá:
   *                    <br>&lt;img src="cid:componentId"/&gt;
   */
  public void addInlineImage(String fileNameWithPath, String componentId) throws Exception {

    MimeBodyPart imagePart = new MimeBodyPart();
    imagePart.attachFile(fileNameWithPath);
    imagePart.setContentID("<" + componentId + ">");
    imagePart.setDisposition(MimeBodyPart.INLINE);
    multipart.addBodyPart(imagePart);

  }

  /**
   * Levél elküldése.
   */
  public void send() throws Exception {

    message.setContent(multipart);
    message.setSentDate(new Date());  // beállítjuk a DATE header-t, mivel lehet hogy ezt hiányolja néhány mailserver
    Transport.send(message);

  }

  // ====
}