package xesj.tool;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * File tool
 */
public class FileTool {

  /**
   * Visszaadja a temporális könyvtár nevét string-ként. 
   * Garantálja hogy a név egy path szeparátorral ér véget, így egy tetszőleges fájlnév illeszthető hozzá.
   * @return Temporális könyvtár neve.
   */
  public static String tmpDirName() {

    String tmpDirName = System.getProperty("java.io.tmpdir");
    if (!tmpDirName.endsWith(File.separator)) {
      tmpDirName += File.separator;
    }
    return tmpDirName;

  }

  /**
   * Visszaadja a temporális könyvtárat fájlként. 
   * Garantálja hogy a név egy path szeparátorral ér véget.
   * @return Temporális könyvtár.
   */
  public static File tmpDir() {

    return new File(tmpDirName());

  }

  /**
   * Visszaad egy nemlétező véletlen fájl-objektumot a temporális könyvtárban.
   * @param extension A fájl kiterjesztése "." nélkül, vagy null ha nincs kiterjesztés.
   * @return Egy nemlétező véletlen fájl-objektum a temporális könyvtárban.
   */
  public static File tmpFile(String extension) {

    extension = (extension == null ? "" : "." + extension);
    File tmpFile;
    do {
      String random = RandomTool.digits(16);
      tmpFile = new File(tmpDirName() + random + extension);
    } while (tmpFile.exists());  
    return tmpFile;

  }

  /**
   * Összehasonlít 2 fájlt a tartalmuk szerint. 
   * @param file1 1. összehasonlítandó file.
   * @param file2 2. összehasonlítandó file.
   * @return True: ha a paraméterek létező fájlok (nem könyvtárak), és tartalmuk megegyezik. Különben false.
   */
  public static boolean contentEqual(File file1, File file2) throws FileNotFoundException, IOException {

    FileInputStream is1 = null, is2 = null;
    try {
      is1 = new FileInputStream(file1);
      is2 = new FileInputStream(file2);
      if (file1.length() != file2.length()) {
        return false; // nem egyezőek
      } 
      int
        length,
        bufferSize = 1024;
      byte[] 
        buffer1 = new byte[bufferSize],
        buffer2 = new byte[bufferSize];
      while ((length = is1.read(buffer1)) > 0) {
        is2.read(buffer2);
        for (int i = 0; i < length; i++) {
          if (buffer1[i] != buffer2[i]) {
            return false; // nem egyezőek
          }
        }
      }
    }
    finally {
      try { is1.close(); } catch (Exception e) {}
      try { is2.close(); } catch (Exception e) {}
    }  
    return true; // egyezőek

  }

  // ====
}
