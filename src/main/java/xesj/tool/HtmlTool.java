package xesj.tool;

/**
 * Html tool
 */
public class HtmlTool {

  /**
   * A szövegen html escape-et hajt végre.<br/> 
   * Lecserélt karakterek: &amp; &quot; &#39; &lt; &gt; 
   * @param str Konvertálandó string.
   * @return Konvertált string, a null paraméter null-ra konvertálódik.
   */
  public static String escape(String str) {

    if (str == null) {
      return null;
    }
    
    return str
      .replaceAll("&", "&amp;")
      .replaceAll("\"", "&quot;")
      .replaceAll("<", "&lt;")
      .replaceAll(">", "&gt;")
      .replaceAll("'", "&#39;");

  }

  // ====
}
