package xesj.tool;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Http/https műveletekhez tartozó tool.
 * A "trust all" forráskód ötlete innen származik:<br/> 
 * https://code.google.com/p/misc-utils/wiki/JavaHttpsUrl
 */
public class HttpTool {

  /**
   * Minden tanúsítványban megbízó TrustManager példány (TAC).
   */
  private static final TrustManager[] 
    trustAllCerts = 
      new TrustManager[] { 
        new X509TrustManager() {
          @Override
          public void checkClientTrusted(final X509Certificate[] chain, final String authType) {
          }
          @Override
          public void checkServerTrusted(final X509Certificate[] chain, final String authType) {
          }
          @Override
          public X509Certificate[] getAcceptedIssuers() {
            return null;
          }
        } 
      };

  /**
   * TAC-ra épülő SSL context.
   */
  private static final SSLContext sslContextTAC; 

  /**
   * TAC-ra épülő SSL socket factory.
   */
  private static final SSLSocketFactory sslSocketFactoryTAC;

  /**
   * Minden host-ban megbízó HostnameVerifier példány.
   */
  private static final HostnameVerifier trustAllHosts = (String hostname, SSLSession session) -> true;  

  /**
   * Static
   */
  static {

    try {
      sslContextTAC = SSLContext.getInstance("SSL");
      sslContextTAC.init(null, trustAllCerts, new java.security.SecureRandom());
      sslSocketFactoryTAC = sslContextTAC.getSocketFactory();
    }
    catch (NoSuchAlgorithmException | KeyManagementException e) {
      throw new ExceptionInInitializerError(e);
    }

  }

  /**
   * A paraméterben kapott HttpsURLConnection-t úgy állítja be, hogy se a tanúsítványok(cert), se a host ne legyen ellenőrizve.
   * Így a metódus segítségével hibás tanúsítvánnyal rendelkező https url-ekre is ki lehet hívni.
   * Így használjuk:
   *   <pre>
   *   URL url = new URL("https://valami.hu/xy");
   *   HttpsURLConnection urlc = (HttpsURLConnection)url.openConnection();
   *   <b>HttpTool.trustAll(urlc);</b>
   *   InputStream is = urlc.getInputStream();
   *   </pre>
   * @param urlConnection Https URL kapcsolat.
   */
  public static void trustAll(HttpsURLConnection urlConnection) {

    urlConnection.setSSLSocketFactory(sslSocketFactoryTAC);
    urlConnection.setHostnameVerifier(trustAllHosts);

  }

  /**
   * A HttpsURLConnection default beállításait változtatja meg úgy, hogy se a tanúsítványok(cert), se a host ne legyen ellenőrizve.
   * Így a metódus segítségével hibás tanúsítvánnyal rendelkező https url-ekre is ki lehet hívni.
   */
  public static void trustAll() {

    HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactoryTAC);
    HttpsURLConnection.setDefaultHostnameVerifier(trustAllHosts);

  }

  /**
   * URL encode UTF-8 kódolással.
   * @param str Kódolandó string.
   * @return Kódolt string UTF-8 kódolással. Ha a paraméter null, akkor null-t ad vissza.
   */
  public static String encodeURL(String str) {

    if (str == null) return null;
    try {
      return URLEncoder.encode(str, "UTF-8");
    }
    catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }

  }    

  // ====
}
