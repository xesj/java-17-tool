package xesj.tool;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Number tool
 */
public class NumberTool {

  /**
   * Long adattípus minimális értéke BigInteger-ként.
   */
  public static final BigInteger LONG_MIN_VALUE_BIG_INTEGER = new BigInteger(String.valueOf(Long.MIN_VALUE));

  /**
   * Long adattípus maximális értéke BigInteger-ként.
   */
  public static final BigInteger LONG_MAX_VALUE_BIG_INTEGER = new BigInteger(String.valueOf(Long.MAX_VALUE));

  /**
   * Lebegőpontos érték kerekítése megadott tizedesjegyre.
   * @param value A kerekítendő érték, mely null is lehet.
   * @param scale Kerekítéskor a tizedesjegyek számának maximuma. 
   * @return Kerekített érték, de null érték esetén null a kerekítés eredménye.
   */
  public static Double round(Double value, int scale) {

    if (value == null) return null;
    BigDecimal big = new BigDecimal(value);
    big = big.setScale(scale, RoundingMode.HALF_UP);
    return big.doubleValue();

  }

  /**
   * Szám formázása string-re, ahol a tizedes jel és az ezres elválasztó a default locale-nak megfelelően jelenik meg 
   * (magyar locale esetén vessző és space).
   * @param value A formázandó érték, mely null is lehet.
   * @param isGroup Legyen-e ezres elválasztás space-szel ?
   * @param scale Tizedes jegyek számának maximuma.
   * @param isScaleRequired Kötelezően ki kell-e írni a scale-ben megadott tizedesjegyeket.
   * @return Formázott szám, de null paraméter esetén null-t ad vissza.
   */
  public static String format(Number value, boolean isGroup, int scale, boolean isScaleRequired) {

    return format(value, isGroup, scale, isScaleRequired, Locale.getDefault());

  }

  /**
   * Szám formázása string-re, ahol a tizedes jel és az ezres elválasztó a paraméterben megadott 
   * locale-nak megfelelően jelenik meg (magyar locale esetén vessző és space).
   * @param value A formázandó érték, mely null is lehet.
   * @param isGroup Legyen-e ezres elválasztás space-szel ?
   * @param scale Tizedes jegyek számának maximuma.
   * @param isScaleRequired Kötelezően ki kell-e írni a scale-ben megadott tizedesjegyeket.
   * @param locale Formázás a locale-nak megfelelően.
   * @return Formázott szám, de null paraméter esetén null-t ad vissza.
   */
  public static String format(Number value, boolean isGroup, int scale, boolean isScaleRequired, Locale locale) {

    if (value == null) return null;
    DecimalFormat decimalFormat = (DecimalFormat)DecimalFormat.getInstance(locale);

    // pattern összeállítása
    StringBuilder pattern = new StringBuilder("##0");
    if (isGroup) pattern.insert(0, ',');
    if (scale > 0) {
      pattern.append('.');
      char scaleChar = (isScaleRequired ? '0' : '#');
      for (int i = 0; i < scale; i++) pattern.append(scaleChar);  
    }  

    // adatformázás 
    decimalFormat.applyPattern(pattern.toString());
    return decimalFormat.format(value);

  }

  /**
   * Long -> BigDecimal konverzió. Null esetén null-t ad vissza.
   * @param lng Konvertálandó érték.
   * @return Konvertált érték. Null érték null-ra konvertálódik.
   */
  public static BigDecimal toBigDecimal(Long lng) {

    if (lng == null) {
      return null;
    }
    return new BigDecimal(lng);

  }

  /**
   * BigInteger -> BigDecimal konverzió. Null esetén null-t ad vissza.
   * @param bigInteger Konvertálandó érték.
   * @return Konvertált érték. Null érték null-ra konvertálódik.
   */
  public static BigDecimal toBigDecimal(BigInteger bigInteger) {

    if (bigInteger == null) {
      return null;
    }
    return new BigDecimal(bigInteger);

  }

  /**
   * Double -> BigDecimal konverzió. Null esetén null-t ad vissza.
   * @param db Konvertálandó érték.
   * @return Konvertált érték. Null érték null-ra konvertálódik.
   */
  public static BigDecimal toBigDecimal(Double db) {

    if (db == null) {
      return null;
    }
    return new BigDecimal(db); 

  }

  /**
   * Long -> BigInteger konverzió. Null esetén null-t ad vissza.
   * @param lng Konvertálandó érték.
   * @return Konvertált érték. Null érték null-ra konvertálódik.
   */
  public static BigInteger toBigInteger(Long lng) {

    if (lng == null) {
      return null;
    }
    return new BigInteger(lng.toString());

  }

  /**
   * BigDecimal -> BigInteger konverzió. Null esetén null-t ad vissza.
   * @param bigDecimal Konvertálandó érték.
   * @return Konvertált érték. Null érték null-ra konvertálódik.
   * @exception ArithmeticException Ha a konvertálandó érték nem egész szám.
   */
  public static BigInteger toBigInteger(BigDecimal bigDecimal) {

    if (bigDecimal == null) {
      return null;
    }
    try {
      return bigDecimal.toBigIntegerExact();
    }
    catch (ArithmeticException ae) {
      throw new ArithmeticException("Az érték nem egész szám.");
    }

  }

  /**
   * BigInteger -> Long konverzió. Null esetén null-t ad vissza.
   * @param bigInteger Konvertálandó érték.
   * @return Konvertált érték. Null érték null-ra konvertálódik.
   * @exception ArithmeticException Ha a konvertálandó érték nem fér bele a [Long.MIN_VALUE, Long.MAX_VALUE] tartományba.
   */
  public static Long toLong(BigInteger bigInteger) {

    if (bigInteger == null) {
      return null;
    }
    if (bigInteger.compareTo(LONG_MIN_VALUE_BIG_INTEGER) == -1 || bigInteger.compareTo(LONG_MAX_VALUE_BIG_INTEGER) == 1) {
      throw new ArithmeticException(
        "Az érték nem fér bele a Long adattípus értékkészletébe: [Long.MIN_VALUE, Long.MAX_VALUE]"
      );
    }   
    return bigInteger.longValue();

  }

  /**
   * BigDecimal -> Long konverzió. Null esetén null-t ad vissza.
   * @param bigDecimal Konvertálandó érték.
   * @return Konvertált érték. Null érték null-ra konvertálódik.
   * @exception ArithmeticException Ha a konvertálandó érték nem egész szám, 
   *            vagy nem fér bele a [Long.MIN_VALUE, Long.MAX_VALUE] tartományba.
   */
  public static Long toLong(BigDecimal bigDecimal) {

    if (bigDecimal == null) {
      return null;
    }
    try {
      return bigDecimal.longValueExact();
    }
    catch (ArithmeticException ae) {
      throw new ArithmeticException(
        "Az érték nem egész szám, vagy nem fér bele a Long adattípus értékkészletébe: [Long.MIN_VALUE, Long.MAX_VALUE]"
      );
    }

  }

  /**
   * BigDecimal -> Double konverzió. Null esetén null-t ad vissza.
   * @param bigDecimal Konvertálandó érték.
   * @return Konvertált érték. Null érték null-ra konvertálódik.
   */
  public static Double toDouble(BigDecimal bigDecimal) {

    if (bigDecimal == null) {
      return null;
    }
    return bigDecimal.doubleValue();

  }

  // ====
}
