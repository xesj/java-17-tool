package xesj.tool;
import java.security.SecureRandom;

/**
 * Véletlen értékek előállítása, egyenletes eloszlású valószínűséggel.
 */
public class RandomTool {
  
  /**
   * Kriptográfiának is megfelelő véletlenszám generátor
   */
  public static final SecureRandom secureRandom = new SecureRandom();

  /**
   * Számjegyek.
   */
  public static final String DIGITS = "0123456789";

  /**
   * Angol kisbetűk.
   */
  public static final String LOWERCASE_LETTERS = "abcdefghijklmnopqrstuvwxyz";

  /**
   * Angol nagybetűk.
   */
  public static final String UPPERCASE_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  /**
   * Speciális karakterek, melyek ascii kódjai: 33-47, 58-64, 91-96, 123-126
   */
  public static final String SPECIAL_CHARACTERS = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

  /**
   * Véletlen logikai érték előállítása.
   */
  public static boolean logic() {

    return secureRandom.nextBoolean();

  }

  /**
   * Egész véletlenszám előállítás a paraméterként megadott zárt intervallumban.
   * @param min A véletlenszám minimuma.
   * @param max A véletlenszám maximuma (a min értékénél nem lehet kisebb).
   * @return Az előállított véletlenszám.
   */
  public static int interval(int min, int max) {

    if (max < min) {
      throw new RuntimeException("A maximális érték kisebb a minimális értéknél.");
    }
    return (int)secureRandom.nextLong(min, max + 1L); 

  }

  /**
   * Véletlen karaktersorozat előállítása a megadott karakterekből.
   * @param characters Ezekből a karakterekből választ a véletlen generátor (legalább 1 karakterből kell állnia).
   * @param length Az előállítandó véletlen karaktersorozat hossza (legalább 0-nak kell lennie).
   * @return Az előállított véletlen karaktersorozat.
   */
  public static String fromCharacters(String characters, int length) {

    // ellenőrzés
    if (characters == null || characters.isEmpty()) {
      throw new RuntimeException("A string null vagy üres.");
    }
    if (length < 0) {
      throw new RuntimeException("A hossz kisebb mint 0.");
    }

    // karakterek kiválasztása
    StringBuilder sb = new StringBuilder(length);
    for (int i = 0; i < length; i++) {
      sb.append(characters.charAt(interval(0, characters.length() - 1)));
    }
    return sb.toString();

  }

  /**
   * Véletlen karaktersorozat előállítása 0-9 számjegyekből. 
   * @param length Az előállítandó karaktersorozat hossza (legalább 0-nak kell lennie).
   * @return Az előállított véletlen karaktersorozat.
   */
  public static String digits(int length) {

    return fromCharacters(DIGITS, length);

  }

  /**
   * Véletlen karaktersorozat előállítása az angol abc kis és nagybetűiből. 
   * @param length Az előállítandó karaktersorozat hossza (legalább 0-nak kell lennie).
   * @return Az előállított véletlen karaktersorozat.
   */
  public static String letters(int length) {

    return fromCharacters(LOWERCASE_LETTERS + UPPERCASE_LETTERS, length);

  }

  /**
   * Véletlen karaktersorozat előállítása számjegyek, angol abc kis és nagybetűk, speciális karakterekből. 
   * @param length Az előállítandó karaktersorozat hossza (legalább 0-nak kell lennie).
   * @return Az előállított véletlen karaktersorozat.
   */
  public static String allCharacters(int length) {

    return fromCharacters(DIGITS + LOWERCASE_LETTERS + UPPERCASE_LETTERS + SPECIAL_CHARACTERS, length);

  }

  // ====
}
