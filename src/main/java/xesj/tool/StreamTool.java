package xesj.tool;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

/**
 * Stream tool
 */
public class StreamTool {

  /**
   * Átmásolja az InputStream tartalmát az OutputStream-be. A stream-eket nem zárja le!
   * @param inputStream InputStream, ahonnan az adatok beolvasódnak.  
   * @param outputStream OutputStream, ahová az adatokat kiíródnak.
   * @param bufferSize Másolási buffer mérete byte-ban. Null esetén a default érték: 1024.
   */
  public static void copy(InputStream inputStream, OutputStream outputStream, Integer bufferSize) throws IOException {

    byte[] buffer = new byte[(bufferSize == null ? 1024 : bufferSize)];
    int length;
    while ((length = inputStream.read(buffer)) > 0) {
      outputStream.write(buffer, 0, length);
    } 

  }

  /**
   * String olvasása InputStream-ről, megadott karakterkódolással. Az InputStream-et nem zárja le!
   * @param inputStream InputStream, ahonnan az olvasás történik.  
   * @param encoding InputStream adatok karakter kódolása.  
   * @param bufferSize Olvasási buffer mérete karakterben. Null esetén a default érték: 1024.
   * @return A beolvasott string. 
   */
  public static String readString(InputStream inputStream, String encoding, Integer bufferSize) 
    throws UnsupportedEncodingException, IOException {

    char[] buffer = new char[(bufferSize == null ? 1024 : bufferSize)];
    StringBuilder stringBuilder = new StringBuilder();
    InputStreamReader reader = new InputStreamReader(inputStream, encoding);
    int counter;
    while ((counter = reader.read(buffer)) > 0) {
      stringBuilder.append(buffer, 0, counter);
    }
    return stringBuilder.toString();

  }

  /**
   * String kiírása OutputStream-re, megadott karakterkódolással. Az OutputStream-et nem zárja le!
   * @param text Kiírandó string.  
   * @param outputStream OutputStream, ahová a kiírás történik.  
   * @param encoding OutputStream adatok karakter kódolása.  
   */
  public static void writeString(String text, OutputStream outputStream, String encoding) 
    throws UnsupportedEncodingException, IOException {

    if (!StringTool.isNullOrEmpty(text)) {
      OutputStreamWriter writer = new OutputStreamWriter(outputStream, encoding);
      writer.write(text);
      writer.flush();
    }  

  }

  // ====
}
