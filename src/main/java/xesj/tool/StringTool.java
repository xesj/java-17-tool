package xesj.tool;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * String tool
 */
public class StringTool {

  /**
   * Objektum string-re konvertálása. 
   * @param obj String-re konvertálandó objektum.
   * @return Konvertált objektum. Ha az obj paraméter null, akkor null-t ad vissza.
   */
  public static String str(Object obj) {

    return (obj == null ? null : obj.toString());

  }

  /**
   * Objektum string-re konvertálása.
   * @param obj String-re konvertálandó objektum.
   * @param nullValue Ezt adja vissza ha az objektum null.
   * @return Konvertált objektum. Ha az obj paraméter null, akkor a nullValue-t adja vissza.
   */
  public static String str(Object obj, String nullValue) {

    return (obj == null ? nullValue : obj.toString());

  }

  /**
   * Objektum string-re konvertálása, és kiegészítése előtte és/vagy utána egy-egy string-gel, 
   * de csak akkor ha az objektum látható karaktert tartalmaz.<br/>
   * Ha az objektum üres(vagy null) akkor az empty-nek megfelelően értéket adja vissza.<br/>
   * Az objektum üresnek nevezhető ha csak láthatatlan karaktereket tartalmaz pl: space \t \r \n 
   * @param obj String-re konvertálandó és kiegészítendő objektum.
   * @param before A string elé írandó szöveg.
   * @param after A string mögé írandó szöveg.
   * @param emptyValue Üres objektum, vagy null esetén visszaadandó string(vagy null).
   * @return Kiegészített string.
   */
  public static String strExt(Object obj, String before, String after, String emptyValue) {

    if (obj == null || isNullOrTrimEmpty(obj.toString())) {
      return emptyValue;
    }
    else {
      return str(before, "") + obj.toString() + str(after, "");
    }  

  }

  /**
   * String sokszorozása.
   * @param str Sokszorozandó string.
   * @param repeat Sokszorozás száma.
   * @return Sokszorozott string. Ha az str paraméter null, akkor null-t ad vissza.
   */
  public static String multiply(String str, int repeat) {

    if (str == null) return null;
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < repeat; i++) {
      sb.append(str);
    }
    return sb.toString();

  }

  /**
   * Space sokszorozása.
   * @param repeat Sokszorozás száma.
   * @return Space repeat-szer.
   */
  public static String space(int repeat) {

    return multiply(" ", repeat);

  }

  /**
   * String feltöltése bármelyik irányból.
   * @param str Feltöltendő string.
   * @param length String kívánt hossza.
   * @param fill Feltöltő karakter.
   * @param left Balról történjen-e a feltöltés.
   * @return Feltöltött string. Ha az str paraméter null, akkor null-t ad vissza.
   */
  private static String pad(String str, int length, char fill, boolean left) {

    if (str == null) return null;
    StringBuilder buf = new StringBuilder(str);
    for (int i = str.length(); i < length; i++) {
      if (left) buf.insert(0, fill);
      else buf.append(fill);
    }
    return new String(buf);

  }

  /**
   * String feltöltése bal oldalról space-szel.
   * @param str Feltöltendő string.
   * @param length String kívánt hossza.
   * @return Feltöltött string. Ha az str paraméter null, akkor null-t ad vissza.
   */
  public static String leftPad(String str, int length) {

    return pad(str, length, ' ', true);

  }

  /**
   * String feltöltése bal oldalról adott karakterrel.
   * @param str Feltöltendő string.
   * @param length String kívánt hossza.
   * @param fill Feltöltő karakter.
   * @return Feltöltött string. Ha az str paraméter null, akkor null-t ad vissza.
   */
  public static String leftPad(String str, int length, char fill) {

    return pad(str, length, fill, true);

  }

  /**
   * String feltöltése jobb oldalról space-szel.
   * @param str Feltöltendő string.
   * @param length String kívánt hossza.
   * @return Feltöltött string. Ha az str paraméter null, akkor null-t ad vissza.
   */
  public static String rightPad(String str, int length) {

    return pad(str, length, ' ', false);

  }

  /**
   * String feltöltése jobb oldalról adott karakterrel.
   * @param str Feltöltendő string.
   * @param length String kívánt hossza.
   * @param fill Feltöltő karakter.
   * @return Feltöltött string. Ha az str paraméter null, akkor null-t ad vissza.
   */
  public static String rightPad(String str, int length, char fill) {

    return pad(str, length, fill, false);

  }

  /**
   * String levágás balról.
   * @param str Levágandó string.
   * @param trimCharacters Levágandó karakterek.
   * @return Levágott string. Ha az egyik paraméter null, akkor null-t ad vissza.
   */
  public static String leftTrim(String str, String trimCharacters) {

    if (str == null || trimCharacters == null) return null;
    int trimPosition = -1;
    for (int pos = 0; pos < str.length(); pos++) {
      boolean cut = false;
      for (int i = 0; i < trimCharacters.length(); i++) {
        if (str.charAt(pos) == trimCharacters.charAt(i)) {
          cut = true;
          break;
        }
      }
      if (!cut) break;
      trimPosition = pos;
    }
    return str.substring(trimPosition + 1);    

  }

  /**
   * String levágás jobbról.
   * @param str Levágandó string.
   * @param trimCharacters Levágandó karakterek.
   * @return Levágott string. Ha az egyik paraméter null, akkor null-t ad vissza.
   */
  public static String rightTrim(String str, String trimCharacters) {

    if (str == null || trimCharacters == null) return null;
    int trimPosition = str.length();
    for (int pos = str.length()-1; pos >= 0; pos--) {
      boolean cut = false;
      for (int i = 0; i < trimCharacters.length(); i++) {
        if (str.charAt(pos) == trimCharacters.charAt(i)) {
          cut = true;
          break;
        }
      }
      if (!cut) break;
      trimPosition = pos;
    }
    return str.substring(0, trimPosition);    

  }

  /**
   * String levágás balról és jobbról.
   * @param str Levágandó string.
   * @param trimCharacters Levágandó karakterek.
   * @return Levágott string. Ha az egyik paraméter null, akkor null-t ad vissza.
   */
  public static String trim(String str, String trimCharacters) {

    return rightTrim(leftTrim(str, trimCharacters), trimCharacters);

  }

  /**
   * String levágás jobbról, maximum hosszra, space mentén.
   * Ha a string hosszabb mint a megengedett akkor megkeresi a maximális hosszon belüli utolsó space-t és ott vágja el, 
   * majd kiegészíti "..."-tal.
   * Ha a maximális hosszon belül sehol nincs space, akkor a maximális hosszú stringet adja vissza "..."-tal kiegészítve.
   * @param str Levágandó string
   * @param maxLenght Megengedett maximális hossz (utána még lehet "..." is !)
   * @return Levágott string. Ha az str paraméter null, akkor null-t ad vissza.
   */
  public static String smartTrim(String str, int maxLenght) {

    if (str == null || str.length() <= maxLenght) return str;
    int pos = str.substring(0, maxLenght + 1).lastIndexOf(' ');
    return (pos == -1 ? str.substring(0, maxLenght) : str.substring(0, pos)) + "...";

  }

  /**
   * Megvizsgálja hogy a string int típussá átalakítható-e.
   * @param str Vizsgált string.
   * @return True: ha str nem null és int típussá alakítható, különben false.
   */
  public static boolean isIntString(String str) {

    if (str == null) return false;
    try {
      Integer.parseInt(str);
      return true;
    }
    catch (NumberFormatException e) {
      return false;
    }

  }

  /**
   * Megvizsgálja hogy a string long típussá átalakítható-e.
   * @param str Vizsgált string.
   * @return True: ha str nem null, és long típussá alakítható, különben false.
   */
  public static boolean isLongString(String str) {

    if (str == null) return false;
    try {
      Long.parseLong(str);
      return true;
    }
    catch (NumberFormatException e) {
      return false;
    }

  }

  /**
   * Megvizsgálja hogy a string double típussá átalakítható-e.
   * @param str Vizsgált string.
   * @return True: ha str nem null és double típussá alakítható, különben false.
   */
  public static boolean isDoubleString(String str) {

    if (str == null) return false;
    try {
      Double.parseDouble(str);
      return true;
    }
    catch (NumberFormatException e) {
      return false;
    }

  }

  /**
   * Integer-re konvertálás.
   * @param str Integer-re konvertálandó érték. 
   * @return A konvertált érték. Ha str null akkor null-t ad vissza.
   */
  public static Integer toInteger(String str) {

    return (str == null ? null : Integer.parseInt(str));

  }

  /**
   * Long-ra konvertálás.
   * @param str Long-ra konvertálandó érték.
   * @return A konvertált érték. Ha str null akkor null-t ad vissza.
   */
  public static Long toLong(String str) {

    return (str == null ? null : Long.parseLong(str));

  }

  /**
   * Double-re konvertálás.
   * @param str Double-re konvertálandó érték.
   * @return A konvertált érték. Ha str null akkor null-t ad vissza.
   */
  public static Double toDouble(String str) {

    return (str == null ? null : Double.parseDouble(str));

  }

  /**
   * Boolean-re konvertálás.
   * Kis/nagybetű függetlenül "true" esetén true, "false" esetén false, egyéb esetben exception keletkezik.
   * @param str Boolean-re konvertálandó érték. 
   * @return A konvertált érték. Ha str null akkor null-t ad vissza. 
   */
  public static Boolean toBoolean(String str) {

    if (str == null) return null;
    if (str.equalsIgnoreCase("true")) return true;
    if (str.equalsIgnoreCase("false")) return false;
    throw new RuntimeException("Nem konvertálható boolean értékre a string: '" + str + "'");

  }

  /**
   * MD5 típusú 16 byte-os hash készítése hexadecimális formában (32 karakteres string).
   * @param str Amiből a hash-t szeretnénk készíteni.
   * @return Hash hexadecimális formában (32 karakteres string). Ha str null akkor null-t ad vissza.
   */
  public static String md5hash(String str) {

    if (str == null) return null;
    MessageDigest messageDigest;
    try {
      messageDigest = MessageDigest.getInstance("MD5");
      messageDigest.update(str.getBytes("UTF-8"));
    }
    catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
    byte[] hash = messageDigest.digest();
    StringBuilder sb = new StringBuilder();
    for(byte b: hash) sb.append(String.format("%02x", b&0xff));
    return sb.toString();

  }  

  /**
   * Megvizsgálja hogy a string null-e vagy üres-e ?
   * @param str Vizsgálandó string.
   * @return True: ha a string null vagy üres, különben false.
   */
  public static boolean isNullOrEmpty(String str) {

    return (str == null || str.isEmpty());

  }

  /**
   * Megvizsgálja hogy a string null-e vagy trim()-melés után üres-e ?
   * @param str Vizsgálandó string.
   * @return True: ha a string null vagy trim()-melés után üres, különben false.
   */
  public static boolean isNullOrTrimEmpty(String str) {

    return (str == null || str.trim().isEmpty());

  }

  /**
   * Összehasonlít két objektumot egyenlőség szempontjából, a stringre konvertált alakjuk szerint.
   * @param a 1. összehasonlítandó objektum.
   * @param b 2. összehasonlítandó objektum.
   * @return True: ha string-re konvertált értékük megegyező, például mindkét paraméter null.
   */
  public static boolean equal(Object a, Object b) {

    if (a == null && b == null) return true;
    if (a == null || b == null) return false;
    return str(a).equals(str(b));

  }

  /**
   * Megvizsgálja hogy az első paraméter stringre konvertált alakja benne van-e a többi paraméter 
   * stringre konvertált alakjainak halmazában.<br/>
   * Például: in(null, 98, null, "abc", false) igaz !
   * @param element Vizsgálandó elem.
   * @param setElements Halmaz.
   * @return True: ha a vizsgálandó elem szerepel a halmazban, különben false.
   */
  public static boolean in(Object element, Object... setElements) {

    if (setElements == null) setElements = new Object[]{null};
    for (Object setElement: setElements) {
      if (equal(element, setElement)) {
        return true;
      }
    }
    return false;

  }

  // ====
}
