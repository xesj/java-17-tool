package xesj.tool;

/**
 * Thread tool
 */

public class ThreadTool {

  /**
   * Szál várakoztatása exception kiváltása nélkül. 
   * @param ms A szál várakozási ideje milliszekundumban megadva. 
   */
  public static void sleep(long ms) {

    try {
      Thread.sleep(ms);
    }
    catch (InterruptedException e) {
    }

  }

  // ====
}
