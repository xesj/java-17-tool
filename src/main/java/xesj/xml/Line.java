package xesj.xml;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import xesj.tool.StringTool;

/**
 * Egy ág a fa-struktúrában.
 */
public class Line {

  /** 
   * Az a fa melyhez az ág tartozik (a jelzők miatt szükséges, nem valós ág esetén null). 
   */
  private Tree tree;

  /** 
   * Jelzi hogy valóságos ág-e.
   */
  private boolean real; 

  /** 
   * Az ág neve, mely nem valós ág esetén null. 
   */
  private String name; 

  /**
   * Az ág belsejében lévő szöveg.
   */
  private String text;

  /** 
   * Szülő ág (ha nincs valódi szülő ág akkor a nem valós ág). 
   */
  private Line parent; 

  /** 
   * Gyerek ágak (ha nincs gyerek ág, vagy ez nem valós ág => üres List). 
   */
  List<Line> children = new ArrayList<>();

  /** 
   * Attribútumok (ha nincs attribútum, vagy nem valós ág => üres map). 
   */
  private Map<String,String> attrs = new HashMap<>(); 

  /** 
   * Az egyetlen nem valóságos ág. 
   */
  static final Line NOT_REAL = new Line(); 

  /**
   * Konstruktor: nem valós ág létrehozása.
   */
  private Line() {

    parent = this;

  }

  /**
   * Konstruktor: valós ág létrehozása node alapján.
   * @param tree Fa, melyhez az ág tartozik.
   * @param parent Szülő ág.
   * @param node Az ág alapjául szolgáló node.
   */
  Line(Tree tree, Line parent, Node node) {

    this.tree = tree;
    this.parent = parent;
    name = node.getNodeName();
    buildAttributes(this, node);
    buildChildren(tree, this, node);
    if (children.isEmpty()) {
      text = node.getTextContent();
    }  
    real = true;

  }

  /**
   * Konstruktor: valós ág létrehozása leíró adatok alapján.
   * @param tree Fa, melyhez az ág tartozik.
   * @param parent Szülő ág.
   * @param data Az ághoz tartozó leíró adatok.
   */
  Line(Tree tree, Line parent, String... data) {

    // paraméter ellenőrzés
    if (data == null || data.length == 0 || data[0] == null) {
      throw new RuntimeException("A főág nevének megadása kötelező");
    }
    if (data.length % 2 == 0) {
      throw new RuntimeException("Csak páratlan számú leíró adat lehet");
    }
    for (String s: data) {
      if (s == null) {
        throw new RuntimeException("Egyetlen leíró adat sem lehet null");
      }
    }
    //
    this.tree = tree;
    this.parent = parent;
    name = data[0];
    for (int i = 1; i < data.length; i += 2) {
      attrs.put(data[i], data[i+1]);
    }
    real = true;

  }

  /**
   * Egy ág gyerek ágainak meghatározása.
   * @param tree Fa, melyhez az ág tartozik.
   * @param parentLine Szülő ág.
   * @param parentNode Szülő node.
   */
  private static void buildChildren(Tree tree, Line parentLine, Node parentNode) {

    NodeList nodeList = parentNode.getChildNodes();
    for (int i = 0; i < nodeList.getLength(); i++) {
      Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Line l = new Line(tree, parentLine, node);
        parentLine.children.add(l);
      }
    }

  }

  /**
   * Egy ág attribútumainak meghatározása.
   */
  private static void buildAttributes(Line line, Node node) {

    NamedNodeMap nnm = node.getAttributes();
    for (int i = 0; i < nnm.getLength(); i++) {
      Node item = nnm.item(i);
      line.attrs.put(item.getNodeName(), item.getNodeValue());
    }

  }

  /**
   * A legelső gyerek ág lekérdezése melyre igazak a megadott feltételek, 
   * vagy ha nincs ilyen akkor a nem valós ágat adja vissza.
   * @param filter Szűrőfeltételek. 
   *   <ul>
   *     <li>
   *       Páratlan számú feltétel esetén az első az ág név szűrő, a többi páronként az attribútum név 
   *       és a hozzátartozó érték szűrő.
   *     </li>
   *     <li>Páros számú feltétel esetén páronként az attribútum név és a hozzátartozó érték szűrő.</li>
   *     <li>Ha az ág név szűrő == null, akkor az nem szűr.</li>
   *     <li>Ha az attribútum név szűrő == null, akkor az nem szűr (függetlenül az értéktől).</li>
   *     <li>Ha az attribútumhoz tartozó érték == null, akkor csak az attribútum név létezésére szűr.</li>
   *   </ul>
   */
  public Line child(String... filter) { 

    return child(1, filter);

  } 

  /**
   * Az N. gyerek ág lekérdezése melyre igazak a megadott feltételek, 
   * vagy ha nincs ilyen akkor a nem valós ágat adja vissza.
   * @param serial A feltételeknek megfelelő gyerek ágak közül hányadikat adja vissza (1-től indul a számozás).
   * @see #child(String... filter)  
   */
  public Line child(int serial, String... filter) { 

    int index = 0;
    for (Line line: children) {
      if (match(line, filter)) {
        index++;
        if (index == serial) return line;
      }
    } 
    return NOT_REAL;

  } 

  /**
   * Az összes ág lekérdezése melyre a feltételek igazak. Ha nincs ilyen, akkor üres lista.
   * @see #child(String... filter)  
   */
  public List<Line> children(String... filter) { 

    ArrayList<Line> lines = new ArrayList<>();
    for (Line line: children) {
      if (match(line, filter)) {
        lines.add(line);
      }
    }
    return lines;

  }

  /**
   * Szülő ág lekérdezése. Nem valós ág esetén önmagát adja vissza.
   */
  public Line parent() { 

    return parent; 

  }

  /**
   * A paraméterként kapott ág megfelel-e a szűrőfeltételeknek ?
   */
  private static boolean match(Line line, String... filter) {

    if (filter == null) return true;

    // node név ellenőrzése
    if (filter.length % 2 == 1) {
      if (filter[0] != null) {
        if (!filter[0].equals(line.name())) return false;
      }
    }

    // attribútumok ellenőrzése
    for (int i = filter.length % 2; i < filter.length; i += 2) {
      if (filter[i] != null) {
        if (filter[i+1] == null) {
          // eset: "x", null
          if (!line.isAttr(filter[i])) return false;
        }
        else {
          // eset: "x", "y"
          if (!line.isAttr(filter[i], filter[i+1])) return false;
        }
      }
    }

    // megfelelő
    return true;

  }

  /**
   * Az ág valódiságának lekérdezése.
   */
  public boolean isReal() { 

    return real; 

  }

  /**
   * Létezik ilyen attribútum ?  
   * @param attrName Az attribútum neve.
   * @return True: létezik az attribútum, false: nem létezik az attribútum vagy a paraméter == null.
   */
  public boolean isAttr(String attrName) { 

    if (!real || attrName == null) return false;
    return attrs.containsKey(attrName);

  }

  /**
   * Létezik ilyen attribútum az adott értékkel ?  
   * @param attrName Az attribútum neve.
   * @param attrValue Az attribútum értéke.
   * @return True: létezik. False: nem létezik, vagy az egyik paraméter null.
   */
  public boolean isAttr(String attrName, String attrValue) { 

    if (!real || attrName == null || attrValue == null) return false;
    return attrValue.equals(attrs.get(attrName));

  }

  /**
   * Attribútum értékének lekérdezése. 
   * @param name Az attribútum neve.
   */
  public String attr(String name) { 

    if (!real) return null;
    return attrs.get(name);

  }

  /**
   * Attribútumok lekérdezése. Nem valós ág esetén üres Map-et ad vissza.
   */
  public Map<String,String> attrs() { 

    return attrs; 

  }

  /**
   * Az ág nevének lekérdezése. Nem valós ág esetén null-t ad vissza.
   */
  public String name() {

    return name; 

  }

  /**
   * Az ág text-jének lekérdezése. Nem valós ág esetén null-t ad vissza.
   */
  public String text() {

    return text;

  }

  /**
   * Az ág text-jének írása. 
   * Csak valós ág esetén hajtható végre, illetve ha az ágnak nincs gyerek ága.
   * @return Az ágat magát adja vissza.
   */
  public Line text(String text) {

    if (!real) {
      throw new RuntimeException("Csak valós ágnak állítható be a text-je");
    }
    if (!children.isEmpty()) {
      throw new RuntimeException("Csak olyan ágnak állítható be a text-je, melynek nincs gyerek ága");
    }
    this.text = text;
    return this;

  }

  /**
   * Az ágat bejegyzi a hozzátartozó fában a megadott jelzéssel.
   * @param m Jelző.
   * @return Saját maga.
   */
  public Line mark(int m) {

    return mark(String.valueOf(m));

  }

  /**
   * Az ágat bejegyzi a hozzátartozó fában a megadott jelzéssel.
   * @param m Jelző.
   * @return Saját maga.
   */
  public Line mark(String m) {

    tree.marks.put(m, this);
    return this;

  }

  /**
   * Új ág létrehozása a gyerek ágak alatt utolsóként.
   * Csak valós ághoz szúrható be új gyerek ág.
   * A szülő ágnak a text-jébe null kerül !
   * @param data Név és attribútum adatok. Az első adat az ág neve, 
   *             a többi páronként az attribútum név és a hozzátartozó érték.
   * @return Az új ág.
   */
  public Line insertChild(String... data) {

    if (!real) throw new RuntimeException("Csak valós ágon hajtható végre az insertChild() művelet");
    Line line = new Line(tree, this, data);
    children.add(line);
    text = null; // ha már van gyerek ága this-nek, akkor nem lehet text-je
    return line;

  }

  /**
   * Új ág létrehozása az aktuális ágak alatt utolsóként.
   * A művelet egyenértékű a következővel: parent().insertChild()
   * Nem valós ágon illetve főágon nem hajtható végre.
   * @param data Név és attribútum adatok. Az első adat az ág neve, 
   *             a többi páronként az attribútum név és a hozzátartozó érték.
   * @return Az új ág.
   */
  public Line insertAfter(String... data) {

    if (!real) throw new RuntimeException("Csak valós ágon hajtható végre az insertAfter() művelet");
    if (tree.root == this) throw new RuntimeException("Főágon nem hajtható végre az insertAfter() művelet");
    return parent.insertChild(data);

  }

  /**
   * Az aktuális ág módosítása.
   * Csak valós ág módosítható !
   * @param data Név és attribútum adatok 
   *   <ul>
   *     <li>
   *       Páratlan számú adat esetén az első az ág új neve, 
   *       a többi páronként az új attribútum név és a hozzátartozó érték.
   *     </li>
   *     <li>Páros számú adat esetén páronként az új attribútum név és a hozzátartozó érték.</li>
   *     <li>Az ág neve nem lehet null.</li>
   *     <li>Az attribútum név nem lehet null.</li>
   *     <li>Az attribútum érték lehet null, ebben az esetben az attribútum törlődni fog.</li>
   *   </ul>
   * @return Az aktuális ág
   */
  public Line update(String... data) {

    if (!real) throw new RuntimeException("Csak valós ág módosítható");
    if (data == null || data.length == 0) return this; // nem módosít semmit
    if (data.length % 2 == 1) {
      // az ág nevének módosítása
      if (data[0] == null) throw new RuntimeException("Az ág új neve nem lehet null");
      name = data[0];
    }
    // az ág attribútumainak módosítása
    for (int i = data.length % 2; i < data.length; i += 2) {   
      String attrName = data[i];
      String attrValue = data[i+1];
      if (attrName == null) throw new RuntimeException("Az attribútum név nem lehet null");
      if (attrValue == null) {
        attrs.remove(attrName); // attribútum törlése
      }
      else {
        attrs.put(attrName, attrValue); // attribútum értékének módosítása, vagy új attribútum felvátele
      }
    }
    return this;

  }

  /**
   * Az aktuális ág törlése a gyerek ágakkal együtt. Az aktuális a szülő ág lesz. 
   * Nem valós ág, illetve a gyökér ág nem törölhető.
   * @return A szülő ág.
   */
  public Line delete() {

    if (!real) throw new RuntimeException("Csak valós ág törölhető");
    if (tree.root() == this) throw new RuntimeException("A gyökér ág nem törölhető");
    parent.children.remove(this);
    return parent;

  }

  /**
   * Az ág string-alakjának lekérdezése. 
   * Nem valós ág esetén: "&lt;&gt;"
   */  
  @Override
  public String toString() {

    StringBuilder s = new StringBuilder();
    s.append("<").append(StringTool.str(name,""));
    for (String key: attrs.keySet()) {
      s.append(" ").append(key).append("=\"").append(attrs.get(key)).append("\"");
    }
    s.append(">");
    return s.toString();

  }

  // ====
}
