package xesj.xml;

/**
 * Ágra vonatkozó szabály.
 */
public class LineRule {

  /**
   * Az ág elérési útja, mely kötelezően "/"-rel kezdődik, és nem "/"-rel végződik. 
   * Például: "/table/details".
   */
  private String path;

  /**
   * A szülő ág elérési útja, ha a szülő a gyökér elem , akkor: "".
   * Például: "/a/b/c" path esetén "/a/b", vagyis az utolsó "/" előtti rész.
   */
  private String parentPath;

  /**
   * A gyerek ág neve "/" jel nélkül.
   * Például: "/a/b/c" path esetén "c", vagyis az utolsó "/" utáni rész.
   */
  private String childName;

  /**
   * Az ág megengedett legkevesebb előfordulása. Ha nincs rá megkötés akkor null.
   */
  private Integer minOccur;

  /**
   * Az ág megengedett legtöbb előfordulása. Ha nincs rá megkötés akkor null.
   */
  private Integer maxOccur;

  /**
   * Konstruktor.
   * @param path Az ág elérési útja, mely kötelezően "/"-rel kezdődik, és nem "/"-rel végződik. Például: "/table/details".
   * @param minOccur Az ág megengedett legkevesebb előfordulása. Ha nincs rá megkötés akkor null.
   * @param maxOccur Az ág megengedett legtöbb előfordulása. Ha nincs rá megkötés akkor null.
   */
  public LineRule(String path, Integer minOccur, Integer maxOccur) {

    // paraméterek ellenőrzése
    if (path == null) throw new RuntimeException("A 'path' paraméter null.");
    if (!path.startsWith("/")) throw new RuntimeException("A 'path' paraméter nem '/'-rel kezdődik.");
    if (path.endsWith("/")) throw new RuntimeException("A 'path' paraméter '/'-rel végződik.");
    if (minOccur != null && minOccur < 0) throw new RuntimeException("A 'minOccur' paraméter negatív.");
    if (maxOccur != null && maxOccur < 0) throw new RuntimeException("A 'maxOccur' paraméter negatív.");
    if (minOccur != null && maxOccur != null && minOccur > maxOccur) {
      throw new RuntimeException("A 'minOccur' paraméter nagyobb mint a 'maxOccur' paraméter.");
    }

    // tárolás
    this.path = path;
    this.minOccur = minOccur;
    this.maxOccur = maxOccur;

    // path szétszedése
    int lastPosition = path.lastIndexOf('/');
    this.parentPath = path.substring(0, lastPosition);
    this.childName = path.substring(lastPosition + 1);

  }

  /**
   * Az ág elérési útjának lekérdezése.
   */  
  public String getPath() {

    return path;

  }

  /**
   * A szülő ág elérési útjának lekérdezése.
   */
  public String getParentPath() {

    return parentPath;

  }

  /**
   * A gyerek ág nevének lekérdezése.
   */
  public String getChildName() {

    return childName;

  }

  /**
   * Az ág megengedett legkevesebb előfordulásának lekérdezése.
   */
  public Integer getMinOccur() {

    return minOccur;

  }

  /**
   * Az ág megengedett legtöbb előfordulásának lekérdezése.
   */
  public Integer getMaxOccur() {

    return maxOccur;

  }

  // ====
}
