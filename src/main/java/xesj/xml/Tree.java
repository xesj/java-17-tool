package xesj.xml;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * XML-t modellező fa-struktúra.
 */
public class Tree {

  /** 
   * DocumentBuilderFactory (kizárólag a getDocumentBuilderFactory() metóduson keresztül szabad elérni !) 
   */
  private static DocumentBuilderFactory documentBuilderFactory;

  /** 
   * TransformerFactory (kizárólag a getTransformerFactory() metóduson keresztül szabad elérni !) 
   */
  private static TransformerFactory transformerFactory;

  /** 
   * A fa főága. 
   */
  Line root; 

  /** 
   * Jelzők. 
   */
  Map<String,Line> marks = new HashMap<>();

  /**
   * Újsor karakter.
   */
  static final String NL = "\n";

  /**
   * Konstruktor belső használatra.
   */
  private Tree() {
  }

  /**
   * Konstruktor.
   * Üres fa létrehozása a főág megadásával.
   * @param data Páratlan számú elem ahol az első a főág neve, a többi páronként az attribútum név és a hozzátartozó érték.
   */
  public Tree(String... data) throws ParserConfigurationException {

    root = new Line(this, Line.NOT_REAL, data);

  }

  /**
   * Konstruktor.
   * Fa-struktúra beolvasása XML dokumentumból.
   */
  public Tree(Document document) throws SAXException, IOException, ParserConfigurationException {

    build(document);

  } 

  /**
   * Konstruktor.
   * Fa-struktúra beolvasása XML fájlból.
   */
  public Tree(File file) throws SAXException, IOException, ParserConfigurationException {

    build(file);

  } 

  /**
   * Konstruktor.
   * Fa-struktúra beolvasása XML dokumentumot tartalmazó InputStream-ből.
   */
  public Tree(InputStream inputStream) throws SAXException, IOException, ParserConfigurationException {

    build(inputStream);

  } 

  /**
   * Konstruktor.
   * Fa-struktúra beolvasása XML dokumentumra mutató URL-ről.
   */
  public Tree(URL url) throws SAXException, IOException, ParserConfigurationException {

    build(url);

  } 

  /**
   * A teljes fa felépítése.
   * @param object az XML-objektum mely Document, File, InputStream, URL lehet.
   */
  private void build(Object object) throws SAXException, IOException, ParserConfigurationException {

    DocumentBuilder documentBuilder = getDocumentBuilderFactory().newDocumentBuilder();
    Document document = null;
    if (object instanceof Document) document = (Document)object;
    if (object instanceof File) document = documentBuilder.parse((File)object);
    if (object instanceof InputStream) document = documentBuilder.parse((InputStream)object);
    if (object instanceof URL) document = documentBuilder.parse(((URL)object).toString());
    Node rootNode = document.getDocumentElement();
    root = new Line(this, Line.NOT_REAL, rootNode);

  } 

  /**
   * Hozzáférés a DocumentBuilderFactory-hoz (kizárólag csak ezen metóduson keresztül történhet).
   */  
  private static DocumentBuilderFactory getDocumentBuilderFactory() throws ParserConfigurationException {

    if (documentBuilderFactory != null) return documentBuilderFactory;
    synchronized (Tree.class) {
      if (documentBuilderFactory != null) return documentBuilderFactory;
      documentBuilderFactory = DocumentBuilderFactory.newInstance();
      return documentBuilderFactory;
    }

  }

  /**
   * Hozzáférés a TransformerFactory-hoz (kizárólag csak ezen metóduson keresztül történhet).
   */  
  private static TransformerFactory getTransformerFactory() {

    if (transformerFactory != null) return transformerFactory;
    synchronized (Tree.class) {
      if (transformerFactory != null) return transformerFactory;
      transformerFactory = TransformerFactory.newInstance();
      return transformerFactory;
    }

  }

  /**
   * Főág lekérdezése.
   */
  public Line root() {

    return root;

  }

  /**
   * Fa string-re alakítása.
   */  
  @Override
  public String toString() {

    StringWriter stringWriter = new StringWriter();
    try {
      write(stringWriter);
    }
    catch (TransformerException | ParserConfigurationException e) {
      throw new RuntimeException(e);
    }
    return stringWriter.toString();

  }

  /**
   * Az adott jelzővel ellátott ágra ugrik, ha ilyen nincs akkor a nem valós ágra ugrik.
   * @param mark Jelző.
   * @return Az ág ahová az ugrás történt.
   */
  public Line jump(int mark) {

    return jump(String.valueOf(mark));

  }

  /**
   * Az adott jelzővel ellátott ágra ugrik, ha ilyen nincs akkor a nem valós ágra ugrik.
   * @param mark Jelző.
   * @return Az ág ahová az ugrás történt.
   */
  public Line jump(String mark) {

    Line line = marks.get(mark);
    return (line == null ? Line.NOT_REAL : line);

  }

  /**
   * Törli az összes jelzőt.
   */
  public void clearMarks() {

    marks.clear();

  }

  /**
   * Fájlba írja ki a fa tartalmát, XML formátumban. 
   */
  public void write(File file) 
    throws TransformerConfigurationException, TransformerException, ParserConfigurationException {

    writeInner(file);

  }

  /**
   * OutputStream-be írja ki a fa tartalmát, XML formátumban.
   */
  public void write(OutputStream outputStream) 
    throws TransformerConfigurationException, TransformerException, ParserConfigurationException {

    writeInner(outputStream);

  }

  /**
   * Writer-be írja ki a fa tartalmát, XML formátumban. 
   */
  public void write(Writer writer) 
    throws TransformerConfigurationException, TransformerException, ParserConfigurationException {

    writeInner(writer);

  }

  /**
   * Fájlba, OutputStream-be, vagy Writer-be írja ki a fa tartalmát, XML formátumban. 
   */
  private void writeInner(Object object) 
    throws TransformerConfigurationException, TransformerException, ParserConfigurationException {

    // document előállítása
    DocumentBuilder documentBuilder = getDocumentBuilderFactory().newDocumentBuilder();
    Document document = documentBuilder.newDocument();
    writeLineToDocument(root, document, document);

    // source előállítása
    Source source = new DOMSource(document);

    // result előállítása
    Result result = null;
    if (object instanceof File) result = new StreamResult((File)object);
    if (object instanceof OutputStream) result = new StreamResult((OutputStream)object);
    if (object instanceof Writer) result = new StreamResult((Writer)object);

    // transformer beállítás
    Transformer transformer = getTransformerFactory().newTransformer();
    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

    // írás
    transformer.transform(source, result);

  }

  /**
   * Egy ág beírása a dokumentumba.
   * Reurzívan önmagát hívja, így a teljes fát kiírja.
   */
  private static void writeLineToDocument(Line line, Node parentElement, Document document) {

    Element element = document.createElement(line.name());
    element.setTextContent(line.text());

    // attribútumok beállítása
    for (String key: line.attrs().keySet()) {
      element.setAttribute(key, line.attrs().get(key));
    }

    // hozzáadás a szülőhöz
    parentElement.appendChild(element);

    // gyerek ágak hozzáadása
    for (Line childLine: line.children) {
      writeLineToDocument(childLine, element, document);
    }

  }

  /**
   * Fa struktúrájának ellenőrzése.
   * Az ágakra vonatkozó szabályokat a "rules" paraméter szabályozza, mely szabályok hiányában null is lehet.
   * Ajánlott a szabályokat a fa összes ágára megadni, mert például "/a/b/c" -re vonatkozó 
   * "minOccur=1" szabály csak azt mondja ki, 
   * hogy "/a/b" ág esetén léteznie kell legalább egy "c" gyerekágnak is, 
   * de ennek ellenére lehetséges, hogy "/a/b" ág nem létezik.
   * Tehát javasolt "/a"-ra és "/a/b"-re vonatkozó szabályt is megadni.
   * @param rootName Gyökér ág neve. Ha bármilyen nevű lehet akkor null.
   * @param rules Ágakra vonatkozó szabályok. Szabályok hiányában null vagy üres lista.
   * @param only Csak a olyan ágak fordulhatnak elő melyeket a szabályok leírnak ?
   * @throws TreeCheckException Ellenőrzéskor felismert hiba.
   */
  public void check(String rootName, List<LineRule> rules, boolean only) throws TreeCheckException {

    if (rootName != null) {
      if (!root().name().equals(rootName)) throw new TreeCheckException("A főág neve nem <" + rootName + ">.");
    }  

    // szabályok ellenőrzése (a path-oknak egyedinek kell lenniük)
    if (rules != null) {
      Set<String> set = new HashSet<>();
      for (LineRule rule: rules) {
        if (!set.add(rule.getPath())) {
          throw new TreeCheckException(
            "Az ágakra vonatkozó szabályok között többször szerepel '" + rule.getPath() + "' elérési út."
          );
        }
      }
    }
    checkChildren(root(), "", rules, only);

  }

  /**
   * Gyerek ágak ellenőrzése.
   * @param parentLine Szülő ág.
   * @param parentPath Szülő ág elérési útja.
   * @param rules Ágakra vonatkozó szabályok.
   * @param only Csak a olyan ágak fordulhatnak elő melyeket a szabályok leírnak ?
   * @throws TreeCheckException Ellenőrzéskor felismert hiba.
   */
  private void checkChildren(Line parentLine, String parentPath, List<LineRule> rules, boolean only) 
    throws TreeCheckException {

    // map összeállítása az ág nevekkel, és előfordulásuk számval, például: {A:2,B:1}
    Map<String,Integer> map = new HashMap<>();
    for (Line line: parentLine.children()) {
      String name = line.name();
      checkChildren(line, parentPath + "/" + name, rules, only); // rekurzió
      Integer i = map.get(name);
      map.put(name, i == null ? 1 : i + 1);
    }

    // végigmenni a szabályokon és ellenőrizni 
    if (rules != null) {
      for (LineRule rule: rules) {
        if (rule.getParentPath().equals(parentPath)) {
          // erre az ágra vonatkozik a szabály
          Integer occur = map.get(rule.getChildName());
          if (occur == null) occur = 0;
          // minOccur ellenőrzés
          if (rule.getMinOccur() != null && rule.getMinOccur() > occur) {
            throw new TreeCheckException(
              "A(z) '" + rule.getPath() + "' ág megengedett legkevesebb előfordulása (" + 
                rule.getMinOccur() + ") nem teljesül."
            );
          }
          // maxOccur ellenőrzés
          if (rule.getMaxOccur() != null && rule.getMaxOccur() < occur) {
            throw new TreeCheckException(
              "A(z) '" + rule.getPath() + "' ág megengedett legtöbb előfordulása (" + rule.getMaxOccur() + ") nem teljesül."
            );
          }
          // eltávolítás a map-ből
          map.remove(rule.getChildName());
        }
      }
    }

    // ágak melyre nem volt szabály
    if (only && !map.isEmpty()) {
      throw new TreeCheckException(
        "A(z) '" + parentPath + "/" + map.keySet().iterator().next() + "' ág nem fordulhat elő."
      );
    }

  }

  // ====
}
