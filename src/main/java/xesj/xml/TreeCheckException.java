package xesj.xml;

/**
 * Fa ellenőrzésekor bekövetkezett exception.
 */
public class TreeCheckException extends Exception {

  /**
   * Konstruktor
   * @param message Hibaüzenet. 
   */
  public TreeCheckException(String message) {

    super(message);

  }

  // ====
}
