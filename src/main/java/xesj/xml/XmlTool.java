package xesj.xml;

/**
 * XML tool
 */
public class XmlTool {

  /**
   * A szövegen XML escape-et hajt végre. 
   */
  public static String escape(String str) {

    if (str == null) {
      return null;
    }
    
    return 
      str
        .replaceAll("&", "&amp;")
        .replaceAll("\"", "&quot;")
        .replaceAll("<", "&lt;")
        .replaceAll(">", "&gt;")
        .replaceAll("'", "&#39;");
    
  }

  // ====
}
