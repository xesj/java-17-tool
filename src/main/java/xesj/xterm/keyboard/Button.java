package xesj.xterm.keyboard;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

/**
 * Billentyűt (billentyűkombinációt) reprezentáló osztály.
 * Ilyen pédául az enter, ctrl-c, vagy ctrl-alt-delete.
 */
@Getter
public class Button {
  
  /**
   * Not Available jelzése a "code" mezőben.
   */
  public static final String NOT_AVAILABLE = "NA";
  
  /**
   * A billentyű azonosítója.
   */
  private ButtonId id;
  
  /**
   * A billentyű lenyomásakor keletkező kódok a billentyű bufferben. 
   * Lehetséges értékek:
   *   null         ->  Még nincs hozzárendelve a kód.
   *   "NA"         ->  Not Available (nem lehet hozzá kódot rendelni).
   *   "a,b,...,f"  ->  0 - 255 értékű kódok vesszővel elválasztva. 
   */
  private String code;
  
  /**
   * A "code" numerikus elemeinek száma. Ha a code null vagy "NA", akkor az értéke null. 
   */ 
  @JsonIgnore
  private Integer codeUnit;

  /**
   * Felhasználónak megjeleníthető név, például: 
   * "a", "Ű", "ctrl alt delete", "=", "7", "F12", "space", "enter", "backspace", "fel-nyíl", "page-up"
   */
  private String name;

  /**
   * A billentyű karakter megfelelője. Ha nem karakter, hanem funkció, akkor az értéke null.
   */
  private Character chr;
  
  /**
   * Konstruktor, a keyboard.json-ből való beolvasáshoz.
   */
  public Button() {
  }

  /**
   * Konstruktor.
   */
  public Button(ButtonId id, String code, String name, Character chr) {

    this.id = id;
    this.code = code;
    this.name = name;
    this.chr = chr;

  }
  
  /**
   * Code, codeUnit beállítása.
   */
  public void setCode(String code) {
    
    // Code mező beállítása
    this.code = code;
    
    // CodeUnit mező beállítása
    if (code == null || code.equals(NOT_AVAILABLE)) {
      this.codeUnit = null;
    }
    else {
      this.codeUnit = code.split(",", -1).length;
    }
    
  }

  /**
   * A billentyű karakter ?
   */
  @JsonIgnore
  public boolean isCharacter() {
    
    return (chr != null);
    
  }

  /**
   * A billentyű funkció ?
   */
  @JsonIgnore
  public boolean isFunction() {
    
    return (chr == null);
    
  }

  // ====
}
