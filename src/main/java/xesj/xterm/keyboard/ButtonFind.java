package xesj.xterm.keyboard;
import java.util.ArrayList;
import java.util.List;
import xesj.tool.ByteTool;

/**
 * A billentyű bufferben található kódok alapján azonosítja a billentyűket,
 * és a kódok azonosításához szükséges search-listát is összeállítja. 
 */
public class ButtonFind {
  
  /**
   * Search-lista. 
   * Ebben a sorrendben kell a kódokat keresni a billentyűzet buffer-ben.
   * A lista sorrendje: leghosszabb kódok,...,legrövidebb kódok.
   * Minden elem "," karakterrel végződik.
   */
  private static List<String> searchList = new ArrayList<>();
  
  /**
   * Search-lista összeállítása.
   */
  public static void createSearchList() {
    
    // Lista kiürítése
    searchList.clear();
    
    // Lista feltöltése
    for (int i = 10; i >= 1; i--) {
      for (Button button: Keyboard.getButtonList()) {
        if (button.getCodeUnit() != null && button.getCodeUnit() == i) {
          searchList.add(button.getCode() + ",");
        }
      }
    }
    
  }
  
  /**
   * Billentyű objektumok beolvasása a billentyű bufferből.
   * @param bufferArray Billentyű buffer.
   * @param count Billentyű buffer-ben lévő bájtok száma.
   * @return A lenyomott billentyű objektumok, vagy ha nincs ilyen, akkor üres lista.
   */
  static List<Button> find(byte[] bufferArray, int count) {

    // Előkészítés
    List<Button> buttonList = new ArrayList<>();
    
    // Van buffer adat ?
    if (count < 1) {
      return buttonList;
    }
    
    // Buffer adat string-re alakítása, a végén mindig van "," karakter: "27,27,91,65,"
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < count; i++) {
      sb.append(ByteTool.byteToInt(bufferArray[i]));
      sb.append(',');
    }
    String buffer = sb.toString();
    
    // Keresés
    boolean find;
    do {
      find = false;
      for (String search: searchList) {
        if (buffer.startsWith(search)) {
          Button button = Keyboard.getButton(search.substring(0, search.length() - 1));
          buttonList.add(button);
          buffer = buffer.substring(search.length());
          find = true;
          break;
        }
      }
    } while (!buffer.isEmpty() && find);
    
    // Válasz
    return buttonList;
    
  }
  
  // ====
}
