package xesj.xterm.keyboard;

/**
 * A lehetséges billentyűk (billentyűkombinációk).
 */
public enum ButtonId {
  
  // Kisbetű -------------------------------------------------------------------------------------------------------------------
  
  /** a */
  CHR_A,
  
  /** á */
  CHR_A2,
  
  /** b */
  CHR_B,        
  
  /** c */
  CHR_C,        
  
  /** d */
  CHR_D,        
  
  /** e */
  CHR_E,        
  
  /** é */
  CHR_E2,  
  
  /** f */
  CHR_F,   
  
  /** g */
  CHR_G, 
  
  /** h */
  CHR_H,   
  
  /** i */
  CHR_I,  
  
  /** í */
  CHR_I2,   
  
  /** j */
  CHR_J,    
  
  /** k */
  CHR_K,    
  
  /** l */
  CHR_L,     
  
  /** m */
  CHR_M,   
  
  /** n */
  CHR_N,  
  
  /** o */
  CHR_O,   
  
  /** ó */
  CHR_O2,  
  
  /** ö */
  CHR_O3,   
  
  /** ő */
  CHR_O4,   
  
  /** p */
  CHR_P,    
  
  /** q */
  CHR_Q,     
  
  /** r */
  CHR_R,    
  
  /** s */
  CHR_S,    
  
  /** t */
  CHR_T,   
  
  /** u */
  CHR_U,    
  
  /** ú */
  CHR_U2,    
  
  /** ü */
  CHR_U3,  
  
  /** ű */
  CHR_U4, 
  
  /** v */
  CHR_V, 
  
  /** w */
  CHR_W,  
  
  /** x */
  CHR_X,   
  
  /** y */
  CHR_Y,  
  
  /** z */
  CHR_Z,        

  // Nagybetű ------------------------------------------------------------------------------------------------------------------
  
  /** A */
  CHR_BIG_A,
  
  /** Á */
  CHR_BIG_A2,
  
  /** B */
  CHR_BIG_B,        
  
  /** C */
  CHR_BIG_C,        
  
  /** D */
  CHR_BIG_D,        
  
  /** E */
  CHR_BIG_E,        
  
  /** É */
  CHR_BIG_E2,  
  
  /** F */
  CHR_BIG_F,   
  
  /** G */
  CHR_BIG_G, 
  
  /** H */
  CHR_BIG_H,   
  
  /** I */
  CHR_BIG_I,  
  
  /** Í */
  CHR_BIG_I2,   
  
  /** J */
  CHR_BIG_J,    
  
  /** K */
  CHR_BIG_K,    
  
  /** L */
  CHR_BIG_L,     
  
  /** M */
  CHR_BIG_M,   
  
  /** N */
  CHR_BIG_N,  
  
  /** O */
  CHR_BIG_O,   
  
  /** Ó */
  CHR_BIG_O2,  
  
  /** Ö */
  CHR_BIG_O3,   
  
  /** Ő */
  CHR_BIG_O4,   
  
  /** P */
  CHR_BIG_P,    
  
  /** Q */
  CHR_BIG_Q,     
  
  /** R */
  CHR_BIG_R,    
  
  /** S */
  CHR_BIG_S,    
  
  /** T */
  CHR_BIG_T,   
  
  /** U */
  CHR_BIG_U,    
  
  /** Ú */
  CHR_BIG_U2,    
  
  /** Ü */
  CHR_BIG_U3,  
  
  /** Ű */
  CHR_BIG_U4, 
  
  /** V */
  CHR_BIG_V, 
  
  /** W */
  CHR_BIG_W,  
  
  /** X */
  CHR_BIG_X,   
  
  /** Y */
  CHR_BIG_Y,  
  
  /** Z */
  CHR_BIG_Z,        

  // Számjegy ------------------------------------------------------------------------------------------------------------------
  
  /** 0 */
  CHR_0,        
  
  /** 1 */
  CHR_1,        
  
  /** 2 */
  CHR_2,        
  
  /** 3 */
  CHR_3,        
  
  /** 4 */
  CHR_4,        
  
  /** 5 */
  CHR_5,        
  
  /** 6 */
  CHR_6,        
  
  /** 7 */
  CHR_7,        
  
  /** 8 */
  CHR_8,        
  
  /** 9 */
  CHR_9,        
  
  // Speciális karakter --------------------------------------------------------------------------------------------------------

  /** § */
  CHR_SECTION,        

  /** ~ */
  CHR_TILDE,        

  /** ' */
  CHR_APOSTROPHE,        

  /** " */
  CHR_QUOTATION,        

  /** + */
  CHR_PLUS,     
  
  /** ! */
  CHR_EXCLAMATION,

  /** % */
  CHR_PERCENT,

  /** / */
  CHR_SLASH,

  /** = */
  CHR_EQUAL,

  /** ` */
  CHR_BACKTICK,

  /** ( */
  CHR_LEFT_ROUND_BRACKET,
  
  /** ) */
  CHR_RIGHT_ROUND_BRACKET,
  
  /** \ */
  CHR_BACKSLASH,
  
  /** | */
  CHR_PIPE,
  
  /** [ */
  CHR_LEFT_SQUARE_BRACKET,

  /** ] */
  CHR_RIGHT_SQUARE_BRACKET,
  
  /** $ */
  CHR_DOLLAR,
  
  /** < */
  CHR_LESS,

  /** > */
  CHR_GREATER,

  /** # */
  CHR_HASH,

  /** & */
  CHR_AMPERSAND,

  /** @ */
  CHR_AT,

  /** { */
  CHR_LEFT_CURLY_BRACKET,

  /** } */
  CHR_RIGHT_CURLY_BRACKET,

  /** , */
  CHR_COMMA,

  /** ; */
  CHR_SEMICOLON,

  /** ? */
  CHR_QUESTION,

  /** . */
  CHR_DOT,

  /** : */
  CHR_COLON,

  /** - */
  CHR_MINUS,

  /** _ */
  CHR_UNDERSCORE,

  /** * */
  CHR_ASTERISK,

  /** space */
  CHR_SPACE,
  
  // Funkció -------------------------------------------------------------------------------------------------------------------
  
  /** ESC */
  FUN_ESC,

  /** F1 */
  FUN_F1,

  /** F2 */
  FUN_F2,

  /** F3 */
  FUN_F3,

  /** F4 */
  FUN_F4,

  /** F5 */
  FUN_F5,

  /** F6 */
  FUN_F6,

  /** F7 */
  FUN_F7,

  /** F8 */
  FUN_F8,

  /** F9 */
  FUN_F9,

  /** F10 */
  FUN_F10,

  /** F11 */
  FUN_F11,

  /** F12 */
  FUN_F12,

  /** TAB */
  FUN_TAB,

  /** BACKSPACE*/
  FUN_BACKSPACE,

  /** ENTER */
  FUN_ENTER,

  /** PRINT SCREEN */
  FUN_PRINT_SCREEN,

  /** PAUSE */
  FUN_PAUSE,

  /** INSERT */
  FUN_INSERT,

  /** DELETE */
  FUN_DELETE,

  /** HOME */
  FUN_HOME,

  /** END */
  FUN_END,

  /** PAGE UP */
  FUN_PAGE_UP,

  /** PAGE DOWN */
  FUN_PAGE_DOWN,

  /** LEFT */
  FUN_LEFT,

  /** RIGHT */
  FUN_RIGHT,

  /** UP */
  FUN_UP,

  /** DOWN */
  FUN_DOWN,

  // ALT funkció ---------------------------------------------------------------------------------------------------------------

  // CTRL funkció --------------------------------------------------------------------------------------------------------------

  /** CTRL a */
  CTRL_CHR_A,

  /** CTRL c */
  CTRL_CHR_C,

  /** CTRL q */
  CTRL_CHR_Q,

  /** CTRL v */
  CTRL_CHR_V,

  /** CTRL x */
  CTRL_CHR_X,

  // SHIFT funkció -------------------------------------------------------------------------------------------------------------

  // ALT CTRL funkció ----------------------------------------------------------------------------------------------------------

  // ALT SHIFT funkció ---------------------------------------------------------------------------------------------------------

  // CTRL SHIFT funkció --------------------------------------------------------------------------------------------------------

  // ALT CTRL SHIFT funkció ----------------------------------------------------------------------------------------------------

}
