package xesj.xterm.keyboard;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.NonNull;
import xesj.tool.ByteTool;
import xesj.xterm.terminal.Terminal;

/**
 * Billentyűzet kezelő osztály.
 */
public class Keyboard {
  
  /** 
   * Billentyű objektumok listája. 
   */
  @Getter
  private static List<Button> buttonList = new ArrayList<>(); 

  /** 
   * Billentyű-map id kulccsal. 
   */
  private static Map<ButtonId, Button> idMap = new HashMap<>();

  /** 
   * Billentyű-map code kulccsal.
   */
  private static Map<String, Button> codeMap = new HashMap<>();
  
  /** 
   * Ebbe olvassuk be a keyboard buffer tartalmát.
   */
  private static byte[] buffer = new byte[16 * 1024];

  /**
   * Billentyűzet használatának elkezdése. A keyboard.json alapján beállítja a billentyűkhöz tartozó kódokat,
   * és kiüríti a billentyűzet buffer-t.
   * @param keyboardFile A keyboard.json fájl. Kötelező megadni, de nem kötelező a fájlnak léteznie.
   */
  public static void start(@NonNull String keyboardFile) throws Exception {
    
    KeyboardJson.file = keyboardFile;
    KeyboardStart.start();
    clear();

  }
  
  /**
   * Billentyűzet buffer kiürítése várakozás nélkül.
   */
  public static void clear() throws IOException {    

    if (System.in.available() > 0) {
      System.in.read(buffer);
    }  

  }
  
  /**
   * Billentyű lekérdezése azonosító (id) alapján. Az idMap-ből keresi ki a billentyűt.
   * @param id Billentyű azonosító.
   */
  public static Button getButton(ButtonId id) {
    
    return idMap.get(id);
    
  }

  /**
   * Billentyű lekérdezése kód (code) alapján. A codeMap-ből keresi ki a billentyűt.
   * Ha a codeMap még nem tartalmaz ilyen kódú billentyűt, akkor null-t ad vissza.
   * @param code Billentyű kód.
   */
  public static Button getButton(String code) {
    
    return codeMap.get(code);
    
  }

  /**
   * Új azonosító (id) beszúrása az idMap-be. 
   * @param id Billentyű azonosító.
   * @param button Billentyű.
   * @throws RuntimeException Ha már létezik ilyen azonosító.
   */
  public static void putButton(ButtonId id, Button button) {
    
    // Ellenőrzés 
    if (idMap.containsKey(id)) {
      throw new RuntimeException("Ez a billentyű ID már létezik: " + id);
    }
    
    // Beszúrás
    idMap.put(id, button);
    
  }
  
  /**
   * Új kód (code) beszúrása a codeMap-be.  
   * @param code Billentyű kód.
   * @param button Billentyű.
   * @throws RuntimeException Ha már létezik ilyen kód.
   */
  public static void putButton(String code, Button button) {
    
    // Ellenőrzés
    if (codeMap.containsKey(code)) {
      throw new RuntimeException("Ez a billentyű kód már létezik: " + code);
    }
    
    // Beszúrás
    codeMap.put(code, button);
    
  }

  /**
   * Kód(ok) beolvasása a billentyűzetről várakozással.
   * Ha a keyboard buffer-ben már van adat, akkor várakozás nélkül ezekkel a kódokkal tér vissza, 
   * ha nincs, akkor megvárja amíg a felhasználó le nem nyom valamilyen billentyűt.
   * @return "n1,n2,..." formában a beolvasott kódok, ahol legalább az n1 létezik.
   */
  public static String readCode() throws IOException {
    
    // Beolvasás
    int count;
    do {
      count = System.in.read(buffer);
    } while (count < 1);  
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < count; i++) {
      if (i > 0) sb.append(',');
      sb.append(ByteTool.byteToInt(buffer[i]));
    }
    
    // Válasz
    return sb.toString();
    
  }
  
  /**
   * Billentyű objektumok beolvasása a billentyűzetről.
   * @param wait 
   *        True: mindenképp várjon billentyű lenyomásra, ha még nincs adat a keyboard buffer-ben. 
   *        False: nincs várakozás.
   * @return A lenyomott billentyű objektumok, vagy ha nincs ilyen, akkor üres lista.
   */
  public static List<Button> readButtons(boolean wait) throws IOException {
    
    // Ha tilos várakozni, és üres a buffer, akkor rögtön visszatérünk.
    if (!wait && System.in.available() == 0) {
      return new ArrayList<>();
    }
    
    // Buffer olvasása
    int count = System.in.read(buffer);
    
    // Find meghívása
    return ButtonFind.find(buffer, count);

  } 
  
  // ====
}
