package xesj.xterm.keyboard;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import xesj.tool.StringTool;

/**
 * A keyboard.json fájl reprezentációja. 
 * Ebből az objektumból lehet kiírni fájlba, illetve ide lehet beolvasni fájlból. 
 */
public class KeyboardJson {
  
  /**
   * A keyboard.json fájl neve.
   */
  static String file;
  
  /** 
   * Billentyű objektumok listája fájlba íráshoz, és fájlból olvasáshoz. 
   */
  public List<Button> buttonList; 

  /**
   * Hibaüzenet: nincs megadva a keyboard.json.
   */  
  private static final String KEYBOARD_JSON_FILE_NULL = 
    "A KeyboardJson osztály 'file' tagja nincs beállítva, az értéke null. " +
    "Ez úgy lehetséges, hogy nem volt meghívva a Keyboard osztály start() metódusa!";

  /**
   * Fájlból beolvasás. Feldolgozza a beolvasott adatokat. Azon billentyűk, melyeknek nem null a "code" mezője,
   * azoknál a "code" értékével frissíti a Keyboard osztályban található buttonList objektumok "code" mezőjét.
   * Frissíti a Keyboard osztályban található codeMap-et is, és a search-listát is.
   */
  public static void readFromFile() throws IOException {
    
    // Ellenőrzés
    if (file == null) {
      throw new RuntimeException(KEYBOARD_JSON_FILE_NULL);
    }

    // Beolvasás
    File f = new File(file);
    if (!f.exists()) return; // Még nemlétező fájl, nincs teendő 
    KeyboardJson json = new ObjectMapper().readValue(f, KeyboardJson.class);
    
    // Feldolgozás
    for (Button b: json.buttonList) {
      if (b.getCode() != null) {
        Button original = Keyboard.getButton(b.getId());
        original.setCode(b.getCode());
        if (!b.getCode().equals(Button.NOT_AVAILABLE)) {
          Keyboard.putButton(b.getCode(), original);
        }  
      }
    }
    
    // Search-lista előállítása
    ButtonFind.createSearchList();
    
  }

  /**
   * A Keyboard osztályban található buttonList objektum kiírása fájlba.
   */
  public static void writeToFile() throws IOException {
    
    // Ellenőrzés
    if (file == null) {
      throw new RuntimeException(KEYBOARD_JSON_FILE_NULL);
    }

    // Kiírás fájlba
    KeyboardJson json = new KeyboardJson();
    json.buttonList = Keyboard.getButtonList();
    new ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(new File(file), json);
    
  }
  
  /**
   * A Keyboard osztályban található buttonList objektum kódjainak kiírása text-fájlba úgy, hogy
   * a kódok abc-sorrendben szerepelnek. A fájl helye, és neve megegyezik a keyboard json fájl-lal,
   * csak a fájl kiterjesztése változik ".txt"-re.
   * @return A text fájl neve.
   */
  public static String writeToCodeOrderTxt() throws IOException {
    
    // Ellenőrzés
    if (file == null) {
      throw new RuntimeException(KEYBOARD_JSON_FILE_NULL);
    }

    // Kódok összegyűjtése
    int maxLength = 0;
    List<String> codeList = new ArrayList<>();
    for (Button b: Keyboard.getButtonList()) {
      String code = b.getCode();
      if (code != null && !code.equals(Button.NOT_AVAILABLE)) {
        if (code.length() > maxLength) maxLength = code.length();
        codeList.add(b.getCode());
      }
    }
    
    // Kódok sorbarendezése
    Collections.sort(codeList);
    
    // Text fájl meghatározása
    String txtFile;
    if (file.endsWith(".json")) {
      txtFile = file.substring(0, file.length() - 5) + ".txt";
    }
    else {
      txtFile = file + ".txt";
    }
    
    // Kiírás fájlba
    FileWriter fileWriter = new FileWriter(txtFile);
    for (String code: codeList) {
      fileWriter.write(StringTool.rightPad(code, maxLength + 5));
      Button b = Keyboard.getButton(code);
      fileWriter.write(b.getName());
      fileWriter.write("\n");
    }  
    fileWriter.close();
    
    // Válasz
    return txtFile;
    
  }  

  // ====
}
