package xesj.xterm.keyboard;
import static xesj.xterm.keyboard.ButtonId.*;

/**
 * Billentyűzet használatának elkezdése.
 */
public class KeyboardStart {

  /**
   * Start. Billentyű objektumok létrehozása.
   */
  static void start() throws Exception {
    
    // Kisbetű
    inner(CHR_A,  null, "a", 'a');
    inner(CHR_A2, null, "á", 'á');
    inner(CHR_B,  null, "b", 'b');       
    inner(CHR_C,  null, "c", 'c');       
    inner(CHR_D,  null, "d", 'd');       
    inner(CHR_E,  null, "e", 'e');       
    inner(CHR_E2, null, "é", 'é');  
    inner(CHR_F,  null, "f", 'f');  
    inner(CHR_G,  null, "g", 'g');
    inner(CHR_H,  null, "h", 'h');  
    inner(CHR_I,  null, "i", 'i'); 
    inner(CHR_I2, null, "í", 'í');  
    inner(CHR_J,  null, "j", 'j');   
    inner(CHR_K,  null, "k", 'k');   
    inner(CHR_L,  null, "l", 'l');    
    inner(CHR_M,  null, "m", 'm');  
    inner(CHR_N,  null, "n", 'n'); 
    inner(CHR_O,  null, "o", 'o');  
    inner(CHR_O2, null, "ó", 'ó');  
    inner(CHR_O3, null, "ö", 'ö');  
    inner(CHR_O4, null, "ő", 'ő');   
    inner(CHR_P,  null, "p", 'p');   
    inner(CHR_Q,  null, "q", 'q');    
    inner(CHR_R,  null, "r", 'r');   
    inner(CHR_S,  null, "s", 's');   
    inner(CHR_T,  null, "t", 't');  
    inner(CHR_U,  null, "u", 'u');   
    inner(CHR_U2, null, "ú", 'ú');   
    inner(CHR_U3, null, "ü", 'ü');  
    inner(CHR_U4, null, "ű", 'ű');
    inner(CHR_V,  null, "v", 'v');
    inner(CHR_W,  null, "w", 'w'); 
    inner(CHR_X,  null, "x", 'x');  
    inner(CHR_Y,  null, "y", 'y'); 
    inner(CHR_Z,  null, "z", 'z');   
    
    // Nagybetű
    inner(CHR_BIG_A,  null, "A", 'A');
    inner(CHR_BIG_A2, null, "Á", 'Á');
    inner(CHR_BIG_B,  null, "B", 'B');        
    inner(CHR_BIG_C,  null, "C", 'C');        
    inner(CHR_BIG_D,  null, "D", 'D');      
    inner(CHR_BIG_E,  null, "E", 'E');        
    inner(CHR_BIG_E2, null, "É", 'É'); 
    inner(CHR_BIG_F,  null, "F", 'F');   
    inner(CHR_BIG_G,  null, "G", 'G'); 
    inner(CHR_BIG_H,  null, "H", 'H');   
    inner(CHR_BIG_I,  null, "I", 'I');  
    inner(CHR_BIG_I2, null, "Í", 'Í');  
    inner(CHR_BIG_J,  null, "J", 'J');    
    inner(CHR_BIG_K,  null, "K", 'K');    
    inner(CHR_BIG_L,  null, "L", 'L');     
    inner(CHR_BIG_M,  null, "M", 'M');   
    inner(CHR_BIG_N,  null, "N", 'N');  
    inner(CHR_BIG_O,  null, "O", 'O');   
    inner(CHR_BIG_O2, null, "Ó", 'Ó'); 
    inner(CHR_BIG_O3, null, "Ö", 'Ö');  
    inner(CHR_BIG_O4, null, "Ő", 'Ő');  
    inner(CHR_BIG_P,  null, "P", 'P');    
    inner(CHR_BIG_Q,  null, "Q", 'Q');     
    inner(CHR_BIG_R,  null, "R", 'R');    
    inner(CHR_BIG_S,  null, "S", 'S');    
    inner(CHR_BIG_T,  null, "T", 'T');   
    inner(CHR_BIG_U,  null, "U", 'U');    
    inner(CHR_BIG_U2, null, "Ú", 'Ú');    
    inner(CHR_BIG_U3, null, "Ü", 'Ü');  
    inner(CHR_BIG_U4, null, "Ű", 'Ű'); 
    inner(CHR_BIG_V,  null, "V", 'V'); 
    inner(CHR_BIG_W,  null, "W", 'W');  
    inner(CHR_BIG_X,  null, "X", 'X');   
    inner(CHR_BIG_Y,  null, "Y", 'Y');  
    inner(CHR_BIG_Z,  null, "Z", 'Z');        

    // Számjegy
    inner(CHR_0, null, "0", '0');        
    inner(CHR_1, null, "1", '1');        
    inner(CHR_2, null, "2", '2');        
    inner(CHR_3, null, "3", '3');        
    inner(CHR_4, null, "4", '4');        
    inner(CHR_5, null, "5", '5');        
    inner(CHR_6, null, "6", '6');        
    inner(CHR_7, null, "7", '7');        
    inner(CHR_8, null, "8", '8');        
    inner(CHR_9, null, "9", '9');    

    // Speciális karakter
    inner(CHR_SECTION,              null, "§",     '§');        
    inner(CHR_TILDE,                null, "~",     '~');        
    inner(CHR_APOSTROPHE,           null, "'",     '\'');        
    inner(CHR_QUOTATION,            null, "\"",    '"');       
    inner(CHR_PLUS,                 null, "+",     '+');
    inner(CHR_EXCLAMATION,          null, "!",     '!');
    inner(CHR_PERCENT,              null, "%",     '%');
    inner(CHR_SLASH,                null, "/",     '/');
    inner(CHR_EQUAL,                null, "=",     '=');
    inner(CHR_BACKTICK,             null, "`",     '`');
    inner(CHR_LEFT_ROUND_BRACKET,   null, "(",     '(');
    inner(CHR_RIGHT_ROUND_BRACKET,  null, ")",     ')');
    inner(CHR_BACKSLASH,            null, "\\",    '\\');
    inner(CHR_PIPE,                 null, "|",     '|');
    inner(CHR_LEFT_SQUARE_BRACKET,  null, "[",     '[');
    inner(CHR_RIGHT_SQUARE_BRACKET, null, "]",     ']');
    inner(CHR_DOLLAR,               null, "$",     '$');
    inner(CHR_LESS,                 null, "<",     '<');
    inner(CHR_GREATER,              null, ">",     '>');
    inner(CHR_HASH,                 null, "#",     '#');
    inner(CHR_AMPERSAND,            null, "&",     '&');
    inner(CHR_AT,                   null, "@",     '@');
    inner(CHR_LEFT_CURLY_BRACKET,   null, "{",     '{');
    inner(CHR_RIGHT_CURLY_BRACKET,  null, "}",     '}');
    inner(CHR_COMMA,                null, ",",     ',');
    inner(CHR_SEMICOLON,            null, ";",     ';');
    inner(CHR_QUESTION,             null, "?",     '?');
    inner(CHR_DOT,                  null, ".",     '.');
    inner(CHR_COLON,                null, ":",     ':');
    inner(CHR_MINUS,                null, "-",     '-');
    inner(CHR_UNDERSCORE,           null, "_",     '_');
    inner(CHR_ASTERISK,             null, "*",     '*');
    inner(CHR_SPACE,                null, "space", ' ');
    
    // Funkció
    inner(FUN_ESC,                  null, "Esc",          null);
    inner(FUN_F1,                   null, "F1",           null); 
    inner(FUN_F2,                   null, "F2",           null);
    inner(FUN_F3,                   null, "F3",           null);
    inner(FUN_F4,                   null, "F4",           null);
    inner(FUN_F5,                   null, "F5",           null);
    inner(FUN_F6,                   null, "F6",           null);
    inner(FUN_F7,                   null, "F7",           null);
    inner(FUN_F8,                   null, "F8",           null);
    inner(FUN_F9,                   null, "F9",           null);
    inner(FUN_F10,                  null, "F10",          null);
    inner(FUN_F11,                  null, "F11",          null);
    inner(FUN_F12,                  null, "F12",          null);
    inner(FUN_TAB,                  null, "Tab",          null);
    inner(FUN_BACKSPACE,            null, "Backspace",    null);
    inner(FUN_ENTER,                null, "Enter",        null);
    inner(FUN_PRINT_SCREEN,         null, "Print Screen", null);
    inner(FUN_PAUSE,                null, "Pause",        null);
    inner(FUN_INSERT,               null, "Insert",       null);
    inner(FUN_DELETE,               null, "Delete",       null);
    inner(FUN_HOME,                 null, "Home",         null);
    inner(FUN_END,                  null, "End",          null);
    inner(FUN_PAGE_UP,              null, "Page Up",      null);
    inner(FUN_PAGE_DOWN,            null, "Page Down",    null);
    inner(FUN_LEFT,                 null, "Left",         null);
    inner(FUN_RIGHT,                null, "Right",        null);
    inner(FUN_UP,                   null, "Up",           null);
    inner(FUN_DOWN,                 null, "Down",         null);

    // ALT funkció
    // ...

    // CTRL funkció
    inner(CTRL_CHR_A,               null, "Ctrl a",       null);
    inner(CTRL_CHR_C,               null, "Ctrl c",       null);
    inner(CTRL_CHR_Q,               null, "Ctrl q",       null);
    inner(CTRL_CHR_V,               null, "Ctrl v",       null);
    inner(CTRL_CHR_X,               null, "Ctrl x",       null);

    // SHIFT funkció
    // ...

    // ALT CTRL funkció
    // ...

    // ALT SHIFT funkció
    // ...

    // CTRL SHIFT funkció
    // ...

    // ALT CTRL SHIFT funkció
    // ...
    
    // Beolvasás a keyboard.json fájlból, és a kapott adatok feldolgozása
    KeyboardJson.readFromFile();
    
  }
  
  /**
   * Belső eljárás.
   * @param id Billentyű azonosító.
   * @param code Billentyű kód.
   * @param name Billentyű megnevezés.
   * @param chr Billentyű karakter megfelelője. Ha nem karakter, akkor null.
   */
  private static void inner(ButtonId id, String code, String name, Character chr) {

    Button button = new Button(id, code, name, chr);
    Keyboard.getButtonList().add(button);
    Keyboard.putButton(id, button);
    
  }
  
  // ====
}
