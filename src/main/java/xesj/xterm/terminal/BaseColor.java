package xesj.xterm.terminal;

/**
 * A 8 alapszín, és a világos változatuk.
 */
public enum BaseColor {
  
  /**
   * Fekete
   */
  BLACK, 

  /**
   * Piros
   */
  RED, 

  /**
   * Zöld
   */
  GREEN, 
  
  /**
   * Sárga
   */
  YELLOW, 

  /**
   * Kék
   */
  BLUE, 
  
  /**
   * Magenta
   */
  MAGENTA, 

  /**
   * Ciánkék
   */
  CYAN, 
  
  /**
   * Fehér
   */
  WHITE,
  
  /**
   * Világos fekete
   */
  LIGHT_BLACK, 

  /**
   * Világos piros
   */
  LIGHT_RED, 

  /**
   * Világos zöld
   */
  LIGHT_GREEN, 
  
  /**
   * Világos sárga
   */
  LIGHT_YELLOW, 

  /**
   * Világos kék
   */
  LIGHT_BLUE, 
  
  /**
   * Világos magenta
   */
  LIGHT_MAGENTA, 

  /**
   * Világos ciánkék
   */
  LIGHT_CYAN, 
  
  /**
   * Világos fehér
   */
  LIGHT_WHITE
  
}
