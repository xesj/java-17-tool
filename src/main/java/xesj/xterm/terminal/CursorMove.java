package xesj.xterm.terminal;

/**
 * Cursor mozgatás irányok.
 */
public enum CursorMove {
  
  /** 
   * Fel
   */
  UP,
  
  /** 
   * Le
   */
  DOWN,
  
  /** 
   * Balra
   */
  LEFT, 

  /** 
   * Jobbra
   */
  RIGHT, 

  /** 
   * Sor elejére
   */
  ROWSTART, 
  
  /** 
   * Fel, és a sor elejére
   */
  UP_ROWSTART, 
  
  /** 
   * Le, és a sor elejére
   */
  DOWN_ROWSTART
  
}
