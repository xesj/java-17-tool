package xesj.xterm.terminal;

/**
 * A cursor pozíciója. 
 */
public class CursorPosition {
  
  /**
   * Sor, mely 1-től indul.
   */
  public int row;

  /**
   * Oszlop, mely 1-től indul.
   */
  public int column;

}
