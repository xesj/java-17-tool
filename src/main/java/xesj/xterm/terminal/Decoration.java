package xesj.xterm.terminal;

/**
 * Betű dekoráció.
 */
public enum Decoration {
  
  /**
   * Vastag betű
   */
  BOLD,

  /**
   * Dőlt betű
   */
  ITALIC,
  
  /**
   * Aláhúzott betű
   */
  UNDERLINE, 
  
  /**
   * Ellentétes megjelenés (betűszín - háttérszín megcserélése)
   */
  OPPOSITE
  
}
