package xesj.xterm.terminal;

/**
 * Terminál ablak scroll irányok.
 */
public enum Scroll {
  
  /**
   * Fel
   */
  UP,
  
  /**
   * Le
   */
  DOWN
  
}
