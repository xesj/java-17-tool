package xesj.xterm.terminal;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import lombok.NonNull;
import xesj.xterm.keyboard.Keyboard;

/**
 * Terminál kezelő osztály.
 */
public class Terminal {
  
  /**
   * CSI: Control Sequence Introducer = ESC [
   */
  public static final String CSI = "\u001b[";
  
  /**
   * Cursor a sor elejére, és egy sorral le.
   */
  public static final String RN = "\r\n";
  
  /**
   * Kiírás a terminálra sorváltás nélkül. Ezt kell használni a System.out.print() helyett, de valójában ennek a megfelelője. 
   * Ha a kiíráskor elérjük a sor végét, akkor a kiírás a következő sorban folytatódik.
   * @param text A kiírandó szöveg.
   */
  public static void print(String text) {
    
    System.out.print(text);
    
  }

  /**
   * Kiírás a terminálra sorváltással. Ezt kell használni a System.out.println() helyett, de valójában ennek a megfelelője.
   * A sorváltáshoz a "\r\n" karaktereket használja.
   * @param text A kiírandó szöveg.
   */
  public static void printRn(String text) {
    
    System.out.print(text);
    System.out.print(RN);
    
  }
  
  /**
   * Terminál ablak törlése.
   * @param cursorStartPositon 
   *        True: a törlés után a cursor a bal-felső sarokba mozog. 
   *        False: nincs cursor mozgatás.
   */
  public static void clear(boolean cursorStartPositon) {

    print(CSI + "2J");
    if (cursorStartPositon) setCursorPosition(1, 1);
    
  }
  
  /**
   * Cursor pozíciójának beállítása.
   * Ha túl nagyok az értékek, akkor az utolsó sorba, és/vagy az utolsó oszlopba megy a cursor. 
   * @param row Sor száma, mely 1-től kezdődik.
   * @param column Oszlop száma, mely 1-től kezdődik.
   * @throws RuntimeException Ha a sor, vagy az oszlop értéke 1-nél kisebb. 
   */
  public static void setCursorPosition(int row, int column) {
    
    // Paraméterek vizsgálata
    if (row < 1) throw new RuntimeException("A sor 1-nél kisebb: " + row);
    if (column < 1) throw new RuntimeException("Az oszlop 1-nél kisebb: " + column);
    
    // Pozicionálás
    print(CSI + row + ";" + column + "H");
    
  }

  /**
   * Cursor pozíciójának beállítása.
   * Ha túl nagyok az értékek, akkor az utolsó sorba, és/vagy az utolsó oszlopba megy a cursor. 
   * @param cursorPosition Cursor pozíció objektum.
   * @throws RuntimeException Ha a sor, vagy az oszlop értéke 1-nél kisebb. 
   */
  public static void setCursorPosition(CursorPosition cursorPosition) {
    
    setCursorPosition(cursorPosition.row, cursorPosition.column);
    
  }
  
  /**
   * Cursor pozíciójának lekérdezése. 
   * A lekérdezés előtt törli a billentyű buffer-t, mivel a lekérdezés eredménye is ott képződik.
   */
  public static CursorPosition getCursorPosition() throws IOException {
    
    // Előkészítés
    String[] array;
    int pos27, pos91, pos59, pos82;
    CursorPosition cursorPosition = new CursorPosition();
    
    // Keyboard buffer törlése
    Keyboard.clear();

    // Query cursor position (QCP), és eredményének beolvasása a keyboard buffer-ből 
    do {
      print(CSI + "6n"); // QCP
      String codes = Keyboard.readCode();
      array = codes.split(",", -1);
      List<String> list = Arrays.asList(array);
      pos27 = list.indexOf("27"); // Esc
      pos91 = list.indexOf("91"); // "["
      pos59 = list.indexOf("59"); // ";"
      pos82 = list.indexOf("82"); // "R"
    }
    while (!(pos27 > -1 && pos91 > -1 && pos59 > -1 && pos82 > -1 && pos27 + 1 == pos91 && pos91 < pos59 && pos59 < pos82));

    // Row meghatározása
    for (int i = pos91 + 1; i < pos59; i++) {
      int power = (int)Math.pow(10, pos59 - i - 1);
      int value = power * (Integer.parseInt(array[i]) - 48);
      cursorPosition.row += value;
    }

    // Column meghatározása
    for (int i = pos59 + 1; i < pos82; i++) {
      int power = (int)Math.pow(10, pos82 - i - 1);
      int value = power * (Integer.parseInt(array[i]) - 48);
      cursorPosition.column += value;
    }
    
    // Válasz
    return cursorPosition;
    
  }
  
  /**
   * Cursor pozíciójának mentése. 
   * A mentett pozícióra visszaállni a restoreCursorPosition() metódussal lehet.
   */
  public static void saveCursorPosition() {
    
    print(CSI + "s");
    
  }

  /**
   * Cursor pozíciójának visszaállítása. 
   * Az utolsó saveCursorPosition() által mentett pozícióra állítja vissza a cursort.
   */
  public static void restoreCursorPosition() {
    
    print(CSI + "u");
    
  }

  /**
   * Cursor mozgatása az adott oszlopba úgy, hogy a sor nem változik.
   * @param column Oszlop száma, mely 1-től kezdődik. Ha túl nagy az értéke, akkor az utolsó oszlopba megy a cursor. 
   * @throws RuntimeException Ha a "column" paraméter értéke 1-nél kisebb. 
   */
  public static void setCursorColumn(int column) {
    
    // Paraméterek vizsgálata
    if (column < 1) throw new RuntimeException("A 'column' paraméter értéke 1-nél kisebb: " + column);
    
    // Pozicionálás
    print(CSI + column + "G");
    
  }
  
  /**
   * Cursor mozgatása. Ha a cursor a kért irányba mozogva eléri a terminál ablak szélét, akkor ott marad, nem mozog tovább.
   * @param move A cursor mozgatásának iránya.
   * @param unit A mozgatás sorainak/oszlopainak száma. Legkisebb értéke 0 lehet.
   * @throws RuntimeException Ha a "unit" paraméter értéke 0-nál kisebb. 
   */
  public static void cursorMove(@NonNull CursorMove move, int unit) {
    
    // Paraméterek vizsgálata
    if (unit < 0) throw new RuntimeException("A 'unit' paraméter értéke 0-nál kisebb: " + unit);
    
    // Mozgatás
    switch (move) {
      case UP:
        print(CSI + unit + "A");
        break;
      case DOWN:
        print(CSI + unit + "B");
        break;
      case RIGHT:
        print(CSI + unit + "C");
        break;
      case LEFT:
        print(CSI + unit + "D");
        break;
      case DOWN_ROWSTART:
        print(CSI + unit + "E");
        break;
      case UP_ROWSTART:
        print(CSI + unit + "F");
        break;
      case ROWSTART:
        print("\r");
        break;
    }
    
  }

  /**
   * Cursor elrejtése, hogy ne látszódjon a terminál ablakban.
   */  
  public static void cursorHide() {

    print(CSI + "?25l");
    
  }

  /**
   * Cursor megjelenítése, hogy látszódjon a terminál ablakban.
   */  
  public static void cursorShow() {

    print(CSI + "?25h");

  }
  
  /**
   * Terminál ablak egy részének törlése. A cursor pozíciója nem változik.
   * @param part A terminál ablak azon része, mely törlésre kerül.
   */
  public static void clearPart(@NonNull TerminalPart part) {
    
    // Törlés
    switch (part) {
      case CURSOR_TO_END:
        print(CSI + "0J");
        break;
      case START_TO_CURSOR:
        print(CSI + "1J");
        break;
      case CURSOR_TO_ROWEND:
        print(CSI + "0K");
        break;
      case ROWSTART_TO_CURSOR:
        print(CSI + "1K");
        break;
      case CURSOR_ROW:
        print(CSI + "2K");
        break;
    }
    
  }
  
  /**
   * Terminál ablak scrollozása felfelé, vagy lefelé. Valójában a karakterek scrollozása történik meg.
   * A scrollozás hatására az egyik oldalon eltűnő sorok elvesznek, egy ellentétes irányú scroll sem hozza őket vissza. 
   * A másik oldalon új üres sorok jelennek meg az ablakban. A cursor pozíciója nem változik.
   * @param scroll Scroll irányának meghatározása, mely csak felfelé, vagy lefelé lehetséges.
   * @param row Hány sornyi scrollozás történjen.
   * @throws RuntimeException Ha a "row" paraméter értéke 0-nál kisebb. 
   */
  public static void scroll(@NonNull Scroll scroll, int row) {
    
    // Paraméterek vizsgálata
    if (row < 0) throw new RuntimeException("A 'row' paraméter értéke 0-nál kisebb: " + row);

    // Scroll
    switch (scroll) {
      case UP:
        print(CSI + row + "S"); // Felfelé
        break;
      case DOWN:
        print(CSI + row + "T"); // Lefelé
        break;
    }

  }
  
  /**
   * Betűszín beállítása a szín nevének megadásával. 
   * Ezzel a metódussal csak a 8 alapszín, és világos változata állítható be.
   * A beállítás az ezután kiírt karakterek betűszínére van hatással.
   * A beállítás addig marad érvényben, amíg be nem állítunk más betűszínt, vagy nem hajtunk végre egy reset() metódust.  
   * @param baseColor Betűszín.
   */
  public static void setColor(@NonNull BaseColor baseColor) {
    
    // Színbeállítás
    switch (baseColor) {
      case BLACK:
        print(CSI + "30m");
        break;
      case RED:
        print(CSI + "31m");
        break;
      case GREEN:
        print(CSI + "32m");
        break;
      case YELLOW:
        print(CSI + "33m");
        break;
      case BLUE:
        print(CSI + "34m");
        break;
      case MAGENTA:
        print(CSI + "35m");
        break;
      case CYAN:
        print(CSI + "36m");
        break;
      case WHITE:
        print(CSI + "37m");
        break;
      case LIGHT_BLACK:
        print(CSI + "90m");
        break;
      case LIGHT_RED:
        print(CSI + "91m");
        break;
      case LIGHT_GREEN:
        print(CSI + "92m");
        break;
      case LIGHT_YELLOW:
        print(CSI + "93m");
        break;
      case LIGHT_BLUE:
        print(CSI + "94m");
        break;
      case LIGHT_MAGENTA:
        print(CSI + "95m");
        break;
      case LIGHT_CYAN:
        print(CSI + "96m");
        break;
      case LIGHT_WHITE:
        print(CSI + "97m");
        break;
    }
    
  }
  
  /**
   * Betűszín beállítása a szín 0 - 255 közötti kódjának megadásával.
   * A beállítás az ezután kiírt karakterek betűszínére van hatással.
   * A beállítás addig marad érvényben, amíg be nem állítunk más betűszínt, vagy nem hajtunk végre egy reset() metódust.  
   * @param color Betűszín kódja.
   * @throws RuntimeException Ha a "color" paraméter értéke nem 0 - 255 között van. 
   */
  public static void setColor(int color) {
    
    // Paraméterek vizsgálata
    if (color < 0 || color > 255) throw new RuntimeException("A 'color' paraméter csak 0 - 255 közötti érték lehet!");
    
    // Színbeállítás
    print(CSI + "38;5;" + color + "m");
    
  }

  /**
   * Betűszín beállítása a szín RGB kódjának megadásával.
   * A beállítás az ezután kiírt karakterek betűszínére van hatással.
   * A beállítás addig marad érvényben, amíg be nem állítunk más betűszínt, vagy nem hajtunk végre egy reset() metódust.  
   * @param red Piros szín komponens 0 - 255 közötti értéke.
   * @param green Zöld szín komponens 0 - 255 közötti értéke.
   * @param blue Kék szín komponens 0 - 255 közötti értéke.
   * @throws RuntimeException Ha a "red", "green", vagy "blue" paraméterek valamelyike nem 0 - 255 értékű. 
   */
  public static void setColor(int red, int green, int blue) {
    
    // Paraméterek vizsgálata
    if (red < 0 || red > 255) throw new RuntimeException("A 'red' paraméter csak 0 - 255 közötti érték lehet!");
    if (green < 0 || green > 255) throw new RuntimeException("A 'green' paraméter csak 0 - 255 közötti érték lehet!");
    if (blue < 0 || blue > 255) throw new RuntimeException("A 'blue' paraméter csak 0 - 255 közötti érték lehet!");
    
    // Színbeállítás
    print(CSI + "38;2;" + red + ";" + green + ";" + blue + "m");
    
  }
  
  /**
   * Betű háttérszín beállítása a szín nevének megadásával.
   * Ezzel a metódussal csak a 8 alapszín, és világos változata állítható be.
   * A beállítás az ezután kiírt karakterek betű háttérszínére van hatással.
   * A beállítás addig marad érvényben, amíg be nem állítunk más betű háttérszínt, vagy nem hajtunk végre egy reset() metódust.  
   * @param baseColor Betű háttérszín.
   */
  public static void setBackgroundColor(@NonNull BaseColor baseColor) {
    
    // Színbeállítás
    switch (baseColor) {
      case BLACK:
        print(CSI + "40m");
        break;
      case RED:
        print(CSI + "41m");
        break;
      case GREEN:
        print(CSI + "42m");
        break;
      case YELLOW:
        print(CSI + "43m");
        break;
      case BLUE:
        print(CSI + "44m");
        break;
      case MAGENTA:
        print(CSI + "45m");
        break;
      case CYAN:
        print(CSI + "46m");
        break;
      case WHITE:
        print(CSI + "47m");
        break;
      case LIGHT_BLACK:
        print(CSI + "100m");
        break;
      case LIGHT_RED:
        print(CSI + "101m");
        break;
      case LIGHT_GREEN:
        print(CSI + "102m");
        break;
      case LIGHT_YELLOW:
        print(CSI + "103m");
        break;
      case LIGHT_BLUE:
        print(CSI + "104m");
        break;
      case LIGHT_MAGENTA:
        print(CSI + "105m");
        break;
      case LIGHT_CYAN:
        print(CSI + "106m");
        break;
      case LIGHT_WHITE:
        print(CSI + "107m");
        break;
    }
    
  }
  
  /**
   * Betű háttérszín beállítása a szín 0 - 255 kódjának megadásával.
   * A beállítás az ezután kiírt karakterek betű háttérszínére van hatással.
   * A beállítás addig marad érvényben, amíg be nem állítunk más betű háttérszínt, vagy nem hajtunk végre egy reset() metódust.  
   * @param color Betű háttérszín kódja.
   * @throws RuntimeException Ha a "color" paraméter nem 0 - 255 közötti érték. 
   */
  public static void setBackgroundColor(int color) {
    
    // Paraméterek vizsgálata
    if (color < 0 || color > 255) throw new RuntimeException("A 'color' paraméter csak 0 - 255 közötti érték lehet!");
    
    // Színbeállítás
    print(CSI + "48;5;" + color + "m");
    
  }
  
  /**
   * Betű háttérszín beállítása a háttérszín RGB kódjának megadásával.
   * A beállítás az ezután kiírt karakterek betű háttérszínére van hatással.
   * A beállítás addig marad érvényben, amíg be nem állítunk más betű háttérszínt, vagy nem hajtunk végre egy reset() metódust.  
   * @param red Piros szín komponens 0 - 255 közötti értéke.
   * @param green Zöld szín komponens 0 - 255 közötti értéke.
   * @param blue Kék szín komponens 0 - 255 közötti értéke.
   * @throws RuntimeException Ha a "red", a "green", vagy a "blue" paraméterek valamelyike nem 0 - 255 közötti érték. 
   */
  public static void setBackgroundColor(int red, int green, int blue) {
    
    // Paraméterek vizsgálata
    if (red < 0 || red > 255) throw new RuntimeException("A 'red' paraméter csak 0 - 255 közötti érték lehet!");
    if (green < 0 || green > 255) throw new RuntimeException("A 'green' paraméter csak 0 - 255 közötti érték lehet!");
    if (blue < 0 || blue > 255) throw new RuntimeException("A 'blue' paraméter csak 0 - 255 közötti érték lehet!");
    
    // Színbeállítás
    print(CSI + "48;2;" + red + ";" + green + ";" + blue + "m");
    
  }
  
  /**
   * Betű dekoráció (pl. vastag, dőlt, aláhúzott betű) beállítása.
   * A betű dekorációkból egyszerre többet is be lehet állítani, a metódus többszöri meghívásával.
   * A beállítás az ezután kiírt karakterekre van hatással.
   * A beállítás addig marad érvényben, amíg be nem állítunk más dekorációt, vagy nem hajtunk végre egy reset() metódust. 
   * @param decoration Dekoráció.
   */
  public static void setDecoration(@NonNull Decoration decoration) {
    
    // Dekoráció
    switch (decoration) {
      case BOLD:
        print(CSI + "1m");
        break;
      case ITALIC:
        print(CSI + "3m");
        break;
      case UNDERLINE:
        print(CSI + "4m");
        break;
      case OPPOSITE:
        print(CSI + "7m");
        break;
    }
    
  }
  
  /**
   * Törli a betűszín, a betű háttérszín, és a betű dekoráció beállításait, így a terminál az alapbeállításra áll. 
   */
  public static void reset() {
    
    print(CSI + "0m");
    
  }
  
  /**
   * Terminál méretének lekérdezése.<br/> 
   * Törli a billentyű buffer-t. A cursor pozícióját nem módosítja.<br/> 
   * @return A terminál méretét reprezentáló objektum.
   */
  public static TerminalSize getSize() throws IOException {
    
    // A cursor jelenlegi pozíciójának mentése
    CursorPosition originalCursorPosition = getCursorPosition();

    // A cursor mozgatása a jobb alsó sarokba
    Terminal.setCursorPosition(1024, 1024);
    CursorPosition cornerCursorPosition = getCursorPosition();
    
    // A cursor visszaállítása az eredeti pozícióba
    setCursorPosition(originalCursorPosition);
    
    // Válasz
    return new TerminalSize(cornerCursorPosition);

  }  
  
  // ====
}
