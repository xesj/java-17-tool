package xesj.xterm.terminal;

/**
 * Terminál ablak részei.
 */
public enum TerminalPart {
  
  /** 
   * A cursor pozíciójától, a terminál ablak végéig (a cursor pozíciója is benne van).
   */
  CURSOR_TO_END,
  
  /** 
   * A terminál ablak elejétől a cursor pozíciójáig (a cursor pozíciója is benne van).
   */
  START_TO_CURSOR,
  
  /** 
   * A cursor pozíciójától a sor végéig (a cursor pozíciója is benne van).
   */
  CURSOR_TO_ROWEND, 

  /** 
   * A sor elejétől a cursor pozíciójáig (a cursor pozíciója is benne van).
   */
  ROWSTART_TO_CURSOR, 

  /** 
   * A cursor sora.
   */
  CURSOR_ROW
  
}
