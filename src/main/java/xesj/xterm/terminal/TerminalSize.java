package xesj.xterm.terminal;

/**
 * A terminál mérete. 
 */
public class TerminalSize {
  
  /**
   * Sorok száma.
   */
  public int row;

  /**
   * Oszlopok száma.
   */
  public int column;
  
  /**
   * Konstruktor.
   * @param cursorPosition A jobb alsó sarokban lévő cursor pozíciója.
   */  
  public TerminalSize(CursorPosition cursorPosition) {
    
    row = cursorPosition.row;
    column = cursorPosition.column;
    
  }
  
  // ====
}
