package test.xesj;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Saját logger formázó
 */
public class MyLoggerFormatter extends Formatter {

  /**
   * Logolás formázása. 
   */
  @Override
  public String format(LogRecord record) {
    
    return record.getMessage() + "\n";
    
  }

  // ====
}
