package test.xesj;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import xesj.tool.FileTool;

/**
 * Teszteléshez tartozó Init osztály.
 */
public class TestInit implements Serializable {

  /** Singleton példány. Módosítása tilos ! */
  private static volatile TestInit instance;

  /** Temporális könyvtár */
  private String tmpDirectory;

  /** Log könyvtár */
  private String logDirectory;

  /**
   * Konstruktor
   * PRIVATE !
   */
  private TestInit() {

    tmpDirectory =  FileTool.tmpDirName();
    logDirectory = FileTool.tmpDirName() + "java-17-tool/log";
    System.out.println("LOG DIRECTORY: " + logDirectory);
    // közös 
    new File(tmpDirectory).mkdirs();
    new File(logDirectory).mkdirs();

  }

  /**
   * Init példány lekérdezés.
   * MÓDOSÍTÁSA TILOS !
   */
  public static TestInit getInstance() {

    if (instance == null) {
      synchronized (TestInit.class) {
        if (instance == null) {
          instance = new TestInit();
        }
      }
    }
    return instance;

  }

  /**
   * Újrainicializálás.
   * MÓDOSÍTÁSA TILOS !
   */
  public static synchronized void reInit() {

    instance = new TestInit();

  }

  /**
   * Logger előállítása. 
   * @param cls Logolást végző osztály.
   * @param fileName Log-fájl neve.
   */
  public Logger getLogger(Class cls, String fileName) {

    Logger logger = Logger.getLogger(cls.getName());
    FileHandler handler; 
    try {
      handler = new FileHandler(logDirectory + File.separatorChar + fileName);
    } 
    catch (IOException | SecurityException e) {
      throw new RuntimeException(e);
    }  
    handler.setFormatter(new MyLoggerFormatter());
    logger.addHandler(handler);
    logger.setUseParentHandlers(false);
    return logger;

  }

  /**
   * Getter metódusok.
   */
  public String getTmpDirectory() {
    return tmpDirectory;
  }
  
  public String getLogDirectory() {
    return logDirectory;
  }

  // ====
}
