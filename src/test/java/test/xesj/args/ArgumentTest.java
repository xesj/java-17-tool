package test.xesj.args;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import test.xesj.TestInit;
import test.xesj.args.other.AET;
import xesj.args.ArgumentException;
import xesj.args.ArgumentHandler;
import xesj.args.ArgumentRule;
import xesj.args.HelpException;
import xesj.tool.DateTool;
import xesj.tool.LocaleTool;

/**
 * Argument teszt
 */
public class ArgumentTest {

  /**
   * Logger
   */
  private static final Logger logger = TestInit.getInstance().getLogger(ArgumentTest.class, "argument-test-log.txt");

  /**
   * Argumentum tömb előállítása stringből.
   * Ha a paraméter null, akkor null-t ad vissza.
   */
  private String[] array(String str) {

    if (str == null) return null;
    List<String> list = new ArrayList<>();
    StringTokenizer tokenizer = new StringTokenizer(str, " ");
    while (tokenizer.hasMoreTokens()) {
      list.add(tokenizer.nextToken());
    }
    return list.toArray(new String[0]);

  }
  
  /**
   * 1-es ArgumentHandler előállítása.
   */
  private ArgumentHandler argumentHandler1(boolean required) {

    return new ArgumentHandler()
      .addRule(new ArgumentRule("string", String.class, null, required).enabledValues("árvíztűrő", "ÁRVÍZTŰRŐ"))
      .addRule(new ArgumentRule("integer", Integer.class, "-754", required))
      .addRule(new ArgumentRule("long", Long.class, null, required))
      .addRule(new ArgumentRule("BIGINTEGER", BigInteger.class, "11111222223333344444555556666677777", required))
      .addRule(new ArgumentRule("double", Double.class, null, required).enabledValues("67.25", "-987.5"))
      .addRule(new ArgumentRule("BIGDECIMAL", BigDecimal.class, "-11111222223333344444555556666677777.88888", required))
      .addRule(new ArgumentRule("date", Date.class, null, required).datePattern(DateTool.DAY_FORMAT))
      .addRule(new ArgumentRule("boolean", Boolean.class, "Yes", required).trueValues("i", "IGEN", "Yes"));

  }

  /**
   * Helyes argumentumok tesztje. 
   */
  @Test
  public void helyesArgumentumTest() throws HelpException, ArgumentException {
    
    Locale.setDefault(LocaleTool.LOCALE_HU);

    // első teszt: csak azok a kötelező argumentumok vannak kitöltve melyeknek nincs default értéke, magyar locale
    ArgumentHandler handler = argumentHandler1(true);
    handler.check(array("string=árvíztűrő long=-567890123456789 double=67.25 date=2016.02.18"));
    assertEquals(handler.getString("string"), "árvíztűrő");
    assertEquals((int)handler.getInteger("integer"), -754);
    assertEquals((long)handler.getLong("long"), -567890123456789L);
    assertEquals(handler.getBigInteger("BIGINTEGER").toString(), "11111222223333344444555556666677777");
    assertEquals(handler.getDouble("double"), 67.25);
    assertEquals(handler.getBigDecimal("BIGDECIMAL").toString(), "-11111222223333344444555556666677777.88888");
    assertEquals(DateTool.formatDay(handler.getDate("date")), "2016.02.18");
    assertTrue(handler.getBoolean("boolean"));

    // második teszt: nincs kötelező argumentum, nincs semmi kitöltve
    handler = argumentHandler1(false);
    handler.check(array(""));
    assertEquals((int)handler.getInteger("integer"), -754);
    assertEquals(handler.getBigInteger("BIGINTEGER").toString(), "11111222223333344444555556666677777");
    assertEquals(handler.getBigDecimal("BIGDECIMAL").toString(), "-11111222223333344444555556666677777.88888");
    assertTrue(handler.getBoolean("boolean"));

    // harmadik teszt: nincs kötelező argumentum, default értékek felülírása, angol locale
    Locale.setDefault(Locale.ENGLISH);
    handler = argumentHandler1(false);
    handler.check(array("boolean=IGEN BIGDECIMAL=1984.5321 BIGINTEGER=-456756456465643232 integer=908070"));
    assertNull(handler.getString("string"));
    assertEquals((int)handler.getInteger("integer"), 908070);
    assertNull(handler.getLong("long"));
    assertEquals(handler.getBigInteger("BIGINTEGER").toString(), "-456756456465643232");
    assertNull(handler.getDouble("double"));
    assertEquals(handler.getBigDecimal("BIGDECIMAL").toString(), "1984.5321");
    assertNull(handler.getDate("date"));
    assertTrue(handler.getBoolean("boolean"));

    // 4. teszt: üres handler, üres argumentum
    handler = new ArgumentHandler().check(array(""));

  }

  /**
   * Boolean teszt
   */
  @Test
  public void booleanTest() throws HelpException, ArgumentException {

    // +---------------+
    // | Helyes esetek |
    // +---------------+

    // igaz és hamis értékek adottak, argumentum: hamis
    ArgumentHandler handler = 
      new ArgumentHandler().addRule(
        new ArgumentRule("01", Boolean.class, "Yes", true).trueValues("i", "IGEN", "Yes").falseValues("n")
      );
    handler.check(array("01=n"));
    assertFalse(handler.getBoolean("01"));

    // igaz és hamis értékek adottak, argumentum: igaz
    handler = new ArgumentHandler().addRule(
      new ArgumentRule("02", Boolean.class, "Yes", true).trueValues("i", "IGEN", "Yes").falseValues("n")
    );
    handler.check(array("02=IGEN"));
    assertTrue(handler.getBoolean("02"));

    // igaz és hamis értékek adottak, argumentum: null
    handler = new ArgumentHandler().addRule(
      new ArgumentRule("03", Boolean.class, null, false).trueValues("i", "IGEN", "Yes").falseValues("n")
    );
    handler.check(array(""));
    assertNull(handler.getBoolean("03"));

    // csak hamis értékek adottak, argumentum: hamis
    handler = new ArgumentHandler().addRule(
      new ArgumentRule("04", Boolean.class, null, true).falseValues("n", "NOT")
    );
    handler.check(array("04=NOT"));
    assertFalse(handler.getBoolean("04"));

    // csak hamis értékek adottak, argumentum: null
    handler = new ArgumentHandler().addRule(
      new ArgumentRule("05", Boolean.class, null, false).falseValues("n", "NOT")
    );
    handler.check(array(""));
    assertNull(handler.getBoolean("05"));

    // csak igaz értékek adottak, argumentum: igaz
    handler = new ArgumentHandler().addRule(
      new ArgumentRule("06", Boolean.class, null, true).trueValues("i", "IGEN", "Yes")
    );
    handler.check(array("06=i"));
    assertTrue(handler.getBoolean("06"));

    // csak igaz értékek adottak, argumentum: null
    handler = new ArgumentHandler().addRule(
      new ArgumentRule("07", Boolean.class, null, false).trueValues("y")
    );
    handler.check(array(""));
    assertNull(handler.getBoolean("07"));

    // +---------------------------------------+
    // | Hibás esetek szabály összeállításakor |
    // +---------------------------------------+

    // nincs se trueValues(), falseValues() megadva
    try {
      handler = new ArgumentHandler().addRule(new ArgumentRule("a", Boolean.class, null, false));
      fail();
    }
    catch (RuntimeException e) {
    }

    // trueValues() üres
    try {
      handler = new ArgumentHandler().addRule(new ArgumentRule("a", Boolean.class, null, false).trueValues());
      fail();
    }
    catch (RuntimeException e) {
    }

    // falseValues() null-t tartalmaz
    try {
      handler = new ArgumentHandler().addRule(new ArgumentRule("a", Boolean.class, null, false).falseValues("F", null));
      fail();
    }
    catch (RuntimeException e) {
    }

    // trueValues() és falseValues() elemei ütköznek
    try {
      handler = new ArgumentHandler().addRule(
        new ArgumentRule("gdf", Boolean.class, null, false).trueValues("Y", "true", "x").falseValues("x", "false")
      );
      fail();
    }
    catch (RuntimeException e) {
    }

    // enabledValues() nem hívható
    try {
      handler = new ArgumentHandler().addRule(new ArgumentRule("gdf", Boolean.class, null, false).enabledValues("sss"));
      fail();
    }
    catch (RuntimeException e) {
    }

    // datePattern() nem hívható
    try {
      handler = new ArgumentHandler().addRule(
        new ArgumentRule("gdf", Boolean.class, null, false).datePattern(DateTool.DAY_FORMAT)
      );
      fail();
    }
    catch (RuntimeException e) {
    }

  }

  /**
   * Date teszt
   */
  @Test
  public void dateTest() throws HelpException, ArgumentException {

    ArgumentHandler handler;

    // +---------------------------------------+
    // | Hibás esetek szabály összeállításakor |
    // +---------------------------------------+

    // nem hívható trueValues()
    try {
      handler = new ArgumentHandler().addRule(
        new ArgumentRule("kelt", Date.class, null, false).trueValues("Y", "true", "x")
      );
      fail();
    }
    catch (RuntimeException e) {
    }

    // nincs megadva dátum formátum
    try {
      handler = new ArgumentHandler().addRule(new ArgumentRule("kelt", Date.class, null, false));
      fail();
    }
    catch (RuntimeException e) {
    }

    // dátum formátum null
    try {
      handler = new ArgumentHandler().addRule(new ArgumentRule("kelt", Date.class, null, false).datePattern(null));
      fail();
    }
    catch (RuntimeException e) {
    }

  }

  /**
   * enabledValues() teszt
   */
  @Test
  public void enabledValuesTest() {

    ArgumentRule rule;

    // +---------------+
    // | Helyes esetek |
    // +---------------+

    rule = new ArgumentRule("kelt", Long.class, null, false);
    rule.enabledValues().enabledValues("abc", "DEF").enabledValues(".").enabledValues("q", "q", "q");

    // nem szerepelhet null az értékek között
    try {
      rule.enabledValues((String)null);
      fail();
    }
    catch (RuntimeException e) {
    }
    try {
      rule.enabledValues("aa", null);
      fail();
    }
    catch (RuntimeException e) {
    }

  }

  /**
   * 2-es ArgumentHandler előállítása.
   */
  private ArgumentHandler argumentHandler2() {

    return new ArgumentHandler()
      .addRule(
        new ArgumentRule("string", String.class, null, true)
        .enabledValues("árvíztűrő", "ÁRVÍZTŰRŐ")
      )
      .addRule(
        new ArgumentRule("double", Double.class, null, true)
        .enabledValues("67.25", "-987.5", "3,5")
        .description("Százalékban mért pontosság, 2 tizedesjeggyel")
      )
      .addRule(
        new ArgumentRule("boolean", Boolean.class, "Yes", false)
        .trueValues("igen", "IGEN", "Yes")
        .falseValues("nem")
      );

  }

  /**
   * HelpException teszt.
   */
  @Test
  public void helpExceptionTest() throws ArgumentException {

    ArgumentHandler handler;

    // handler-1
    handler = argumentHandler1(true);
    logger.info("----- HELP-1 SZÖVEG KEZDETE -----");
    logger.info(handler.help() + "----- HELP-1 SZÖVEG VÉGE -----");

    // handler-2
    handler = argumentHandler2();
    try {
      handler.check(array("?"));
      fail();
    }
    catch (HelpException ex) {
      logger.info("----- HELP-2 SZÖVEG KEZDETE -----");
      logger.info(handler.help() + "----- HELP-2 SZÖVEG VÉGE -----");
    }

    // handler-3 (szabály nélküli üres help)
    handler = new ArgumentHandler();
    logger.info("----- HELP-3 SZÖVEG KEZDETE (ÜRES !) -----");
    logger.info(handler.help() + "----- HELP-3 SZÖVEG VÉGE -----");

  }

  /**
   * ArgumentException esetek
   */
  @Test
  public void argumentExceptionTest() throws HelpException {

    final AET[] aetArray = {
      new AET("zizi", "zizi", ArgumentException.Type.EQUAL_SIGN),
      new AET("double=67.25 string=árvíztűrő double=-987.5", "double", ArgumentException.Type.MULTIPLE),
      new AET("double=67.25 ee=34 string=árvíztűrő", "ee", ArgumentException.Type.APPLICABLE),
      new AET("double=-987.5", "string", ArgumentException.Type.REQUIRED),
      new AET("string=ÁRVÍZTŰRŐ", "double", ArgumentException.Type.REQUIRED),
      new AET("double=10 string=árvíztűrő", "double", ArgumentException.Type.VALUE_GRANT),
      new AET("double=-987.5 string=árvíz boolean=nem", "string", ArgumentException.Type.VALUE_GRANT),
      new AET("double=3,5 string=árvíztűrő", "double", ArgumentException.Type.VALUE_CONVERT),
      new AET("string=ÁRVÍZTŰRŐ double=67.25 boolean=XX", "boolean", ArgumentException.Type.VALUE_CONVERT),};
    for (int i = 0; i < aetArray.length; i++) {
      AET aet = aetArray[i];
      ArgumentHandler handler = argumentHandler2();
      try {
        handler.check(array(aet.args));
        fail("Nem keletkezett ArgumentException AET[" + i + "] tömbelemnél");
      }
      catch (ArgumentException e) {
        assertEquals(e.getArgumentName(), aet.exceptionArgumentName);
        assertEquals(e.getExceptionType(), aet.exceptionType);
        try {
          handler.getString("string");
          fail();
        }
        catch (RuntimeException re) {
        }
      }
    }

  }

  /**
   * RuntimeException esetek
   */
  @Test
  public void runtimeExceptionTest() throws HelpException, ArgumentException {

    // +--------------------+
    // | ArgumentRule hibák |
    // +--------------------+

    // 'name' paraméter nem lehet null
    try {
      new ArgumentRule(null, String.class, null, false);
      fail();
    }
    catch (RuntimeException e) {
    }

    // 'type' paraméter nem lehet null
    try {
      new ArgumentRule("abc", null, null, false);
      fail();
    }
    catch (RuntimeException e) {
    }

    // 'type' paraméter típusa nem támogatott
    try {
      new ArgumentRule("abc", AET.class, null, false);
      fail();
    }
    catch (RuntimeException e) {
    }

    // dátum típusú argumentumnál kötelező a dátum formátum megadása
    try {
      new ArgumentHandler().addRule(new ArgumentRule("abc", Date.class, null, false));
      fail();
    }
    catch (RuntimeException e) {
    }

    // boolean típusú argumentumnál kötelező az igaznak, vagy hamisnak értékelendő string-ek megadása
    try {
      new ArgumentHandler().addRule(new ArgumentRule("ABC", Boolean.class, null, false));
      fail();
    }
    catch (RuntimeException e) {
    }

    // +-----------------------+
    // | ArgumentHandler hibák |
    // +-----------------------+

    // add() hiba: add() metódus nem hívható meg check() után
    try {
      new ArgumentHandler().check(array("")).addRule(new ArgumentRule("x", String.class, null, false));
      fail();
    }
    catch (RuntimeException e) {
    }

    // add() hiba: 'rule' paraméter nem lehet null
    try {
      new ArgumentHandler().addRule(null);
      fail();
    }
    catch (RuntimeException e) {
    }

    // add() hiba: az argumentum kezelő már tartalmaz ilyen nevű szabályt
    try {
      new ArgumentHandler()
        .addRule(new ArgumentRule("XY", String.class, null, false))
        .addRule(new ArgumentRule("XY", String.class, null, false));
      fail();
    }
    catch (RuntimeException e) {
    }

    // check() hiba: argumentum tömb nem lehet null
    try {
      new ArgumentHandler().check(null);
      fail();
    }
    catch (RuntimeException e) {
    }

    // check() hiba: a metódus csak egyszer hívható meg
    try {
      new ArgumentHandler().check(array("")).check(array(""));
      fail();
    }
    catch (RuntimeException e) {
    }

    // getXXX() hiba: a metódus nem hívható meg check() előtt
    try {
      new ArgumentHandler().getLong("t");
      fail();
    }
    catch (RuntimeException e) {
    }

    // getXXX() hiba: argumentum név nem használható
    try {
      new ArgumentHandler().check(array("")).getDate("asdf");
      fail();
    }
    catch (RuntimeException e) {
    }

    // getXXX() hiba: argumentum nem megfelelő típusú
    try {
      new ArgumentHandler()
        .addRule(new ArgumentRule("XY", String.class, null, false))
        .check(array("XY=67"))
        .getLong("XY");
      fail();
    }
    catch (RuntimeException e) {
    }

  }

  // ====
}
