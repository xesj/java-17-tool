package test.xesj.args.other;
import xesj.args.ArgumentException;

/**
 * AET
 */
public class AET {

  public String args;
  public String exceptionArgumentName;
  public ArgumentException.Type exceptionType;

  /**
   * Konstruktor
   */
  public AET(String args, String exceptionArgumentName, ArgumentException.Type exceptionType) {

    this.args = args;
    this.exceptionArgumentName = exceptionArgumentName;
    this.exceptionType = exceptionType;

  }

  // ====
}
