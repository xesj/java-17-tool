package test.xesj.json;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigInteger;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import xesj.json.JsonConversionException;
import xesj.json.JsonNodeReader;

/**
 * JsonNodeReader teszt
 */
public class JsonNodeReaderTest {
  
  public static final String JSON = """
    {
      "nincsAdat": null, 
      "szoveg": "árvíztűrő tükörfúrógép",
      "szoveg-ures": "",
      "logika-igaz": true, 
      "logika-hamis": false,
      "tort": 3.14, 
      "intSzam": 48,
      "longSzam": 9223372036854775807,
      "bigintSzam": 5564712635174523674623512835218635267816546783645278635462782767354278,
      "tomb": [7, "7", null, "macska"]
    }
    """;

  /**
   * Reader
   */
  private JsonNodeReader getReader() throws JsonProcessingException {

    return new JsonNodeReader(new ObjectMapper().readTree(JSON));
    
  }
  
  /**
   * Metódus teszt: exist()
   */
  @Test
  public void existTest() throws JsonProcessingException {
    
    JsonNodeReader reader = getReader();
    
    // True értékek
    assertTrue(reader.exist("nincsAdat"));
    assertTrue(reader.exist("tomb", 0));
    assertTrue(reader.exist("tomb", 2));
    assertTrue(reader.exist(new Object[]{"tomb", 2}));

    // False értékek
    assertFalse(reader.exist("nincsadat"));
    assertFalse(reader.exist("tomb", "2"));
    assertFalse(reader.exist("tomb", -1));
    assertFalse(reader.exist("tomb", 333));
    assertFalse(reader.exist(new Object[]{"tomb", 333}));

  }

  /**
   * Metódus teszt: getInteger()
   */
  @Test
  public void getIntegerTest() throws JsonProcessingException {
    
    JsonNodeReader reader = getReader();
    
    // Null értékek
    assertNull(reader.getInteger("nincsIlyenKulcs"));
    assertNull(reader.getInteger("tomb", 2));
    assertNull(reader.getInteger("tomb", 555));
    assertNull(reader.getInteger("nincsAdat"));
    
    // Valós értékek
    assertEquals(48, reader.getInteger("intSzam"));
    assertEquals(7, reader.getInteger("tomb", 0));
    
    // JsonConversionException
    assertThrows(JsonConversionException.class, () -> { reader.getInteger("szoveg"); });
    assertThrows(JsonConversionException.class, () -> { reader.getInteger("szoveg-ures"); });
    assertThrows(JsonConversionException.class, () -> { reader.getInteger("logika-igaz"); });
    assertThrows(JsonConversionException.class, () -> { reader.getInteger("logika-hamis"); });
    assertThrows(JsonConversionException.class, () -> { reader.getInteger("tort"); });
    assertThrows(JsonConversionException.class, () -> { reader.getInteger("longSzam"); });
    assertThrows(JsonConversionException.class, () -> { reader.getInteger("bigintSzam"); });
    assertThrows(JsonConversionException.class, () -> { reader.getInteger("tomb", 1); });
    assertThrows(JsonConversionException.class, () -> { reader.getInteger("tomb", 3); });

  }

  /**
   * Metódus teszt: getLong()
   */
  @Test
  public void getLongTest() throws JsonProcessingException {
    
    JsonNodeReader reader = getReader();
    
    // Null értékek
    assertNull(reader.getLong("nincsIlyenKulcs"));
    assertNull(reader.getLong("tomb", 2));
    assertNull(reader.getLong("tomb", 555));
    assertNull(reader.getLong("nincsAdat"));
    
    // Valós értékek
    assertEquals(48L, reader.getLong("intSzam"));
    assertEquals(Long.MAX_VALUE, reader.getLong("longSzam"));
    assertEquals(7L, reader.getLong("tomb", 0));
    
    // JsonConversionException
    assertThrows(JsonConversionException.class, () -> { reader.getLong("szoveg"); });
    assertThrows(JsonConversionException.class, () -> { reader.getLong("szoveg-ures"); });
    assertThrows(JsonConversionException.class, () -> { reader.getLong("logika-igaz"); });
    assertThrows(JsonConversionException.class, () -> { reader.getLong("logika-hamis"); });
    assertThrows(JsonConversionException.class, () -> { reader.getLong("tort"); });
    assertThrows(JsonConversionException.class, () -> { reader.getLong("bigintSzam"); });
    assertThrows(JsonConversionException.class, () -> { reader.getLong("tomb", 1); });
    assertThrows(JsonConversionException.class, () -> { reader.getLong("tomb", 3); });

  }

  /**
   * Metódus teszt: getDouble()
   */
  @Test
  public void getDoubleTest() throws JsonProcessingException {
    
    JsonNodeReader reader = getReader();
    
    // Null értékek
    assertNull(reader.getDouble("nincsIlyenKulcs"));
    assertNull(reader.getDouble("tomb", 2));
    assertNull(reader.getDouble("tomb", 555));
    assertNull(reader.getDouble("nincsAdat"));
    
    // Valós értékek
    assertEquals(3.14, reader.getDouble("tort"));
    assertEquals(48.0, reader.getDouble("intSzam"));
    assertEquals(Long.MAX_VALUE, reader.getDouble("longSzam"));
    assertEquals(7.0, reader.getDouble("tomb", 0));
    
    // JsonConversionException
    assertThrows(JsonConversionException.class, () -> { reader.getDouble("szoveg"); });
    assertThrows(JsonConversionException.class, () -> { reader.getDouble("szoveg-ures"); });
    assertThrows(JsonConversionException.class, () -> { reader.getDouble("logika-igaz"); });
    assertThrows(JsonConversionException.class, () -> { reader.getDouble("logika-hamis"); });
    assertThrows(JsonConversionException.class, () -> { reader.getDouble("bigintSzam"); });
    assertThrows(JsonConversionException.class, () -> { reader.getDouble("tomb", 1); });
    assertThrows(JsonConversionException.class, () -> { reader.getDouble("tomb", 3); });

  }

  /**
   * Metódus teszt: getBigInteger()
   */
  @Test
  public void getBigIntegerTest() throws JsonProcessingException {
    
    JsonNodeReader reader = getReader();
    
    // Null értékek
    assertNull(reader.getBigInteger("nincsIlyenKulcs"));
    assertNull(reader.getBigInteger("tomb", 2));
    assertNull(reader.getBigInteger("tomb", 555));
    assertNull(reader.getBigInteger("nincsAdat"));
    
    // Valós értékek
    assertEquals(new BigInteger("48"), reader.getBigInteger("intSzam"));
    assertEquals(new BigInteger("9223372036854775807"), reader.getBigInteger("longSzam"));
    assertEquals(
      new BigInteger("5564712635174523674623512835218635267816546783645278635462782767354278"), 
      reader.getBigInteger("bigintSzam")
    );
    assertEquals(new BigInteger("7"), reader.getBigInteger("tomb", 0));
    
    // JsonConversionException
    assertThrows(JsonConversionException.class, () -> { reader.getBigInteger("szoveg"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBigInteger("szoveg-ures"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBigInteger("logika-igaz"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBigInteger("logika-hamis"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBigInteger("tort"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBigInteger("tomb", 1); });
    assertThrows(JsonConversionException.class, () -> { reader.getBigInteger("tomb", 3); });

  }

  /**
   * Metódus teszt: getString()
   */
  @Test
  public void getStringTest() throws JsonProcessingException {
    
    JsonNodeReader reader = getReader();
    
    // Null értékek
    assertNull(reader.getString("nincsIlyenKulcs"));
    assertNull(reader.getString("tomb", 2));
    assertNull(reader.getString("tomb", 555));
    assertNull(reader.getString("nincsAdat"));
    
    // Valós értékek
    assertEquals("árvíztűrő tükörfúrógép", reader.getString("szoveg"));
    assertEquals("", reader.getString("szoveg-ures"));
    assertEquals("7", reader.getString("tomb", 1));
    assertEquals("macska", reader.getString("tomb", 3));
    
    // JsonConversionException
    assertThrows(JsonConversionException.class, () -> { reader.getString("logika-igaz"); });
    assertThrows(JsonConversionException.class, () -> { reader.getString("logika-hamis"); });
    assertThrows(JsonConversionException.class, () -> { reader.getString("tort"); });
    assertThrows(JsonConversionException.class, () -> { reader.getString("intSzam"); });
    assertThrows(JsonConversionException.class, () -> { reader.getString("longSzam"); });
    assertThrows(JsonConversionException.class, () -> { reader.getString("bigintSzam"); });
    assertThrows(JsonConversionException.class, () -> { reader.getString("tomb", 0); });

  }

  /**
   * Metódus teszt: getBoolean()
   */
  @Test
  public void getBooleanTest() throws JsonProcessingException {
    
    JsonNodeReader reader = getReader();
    
    // Null értékek
    assertNull(reader.getBoolean("nincsIlyenKulcs"));
    assertNull(reader.getBoolean("tomb", 2));
    assertNull(reader.getBoolean("tomb", 555));
    assertNull(reader.getBoolean("nincsAdat"));
    
    // Valós értékek
    assertTrue(reader.getBoolean("logika-igaz"));
    assertFalse(reader.getBoolean("logika-hamis"));
    
    // JsonConversionException
    assertThrows(JsonConversionException.class, () -> { reader.getBoolean("szoveg"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBoolean("szoveg-ures"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBoolean("tort"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBoolean("intSzam"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBoolean("longSzam"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBoolean("bigintSzam"); });
    assertThrows(JsonConversionException.class, () -> { reader.getBoolean("tomb", 0); });
    assertThrows(JsonConversionException.class, () -> { reader.getBoolean("tomb", 1); });
    assertThrows(JsonConversionException.class, () -> { reader.getBoolean("tomb", 3); });

  }
  
  /**
   * Metódus teszt: getJsonNode()
   */
  @Test
  public void getJsonNodeTest() throws JsonProcessingException {
    
    JsonNode node = getReader().getJsonNode();
    assertEquals(48, node.path("intSzam").asInt());
  
  }
  
  /**
   * Hibás path teszt
   */
  @Test
  public void badPath() throws JsonProcessingException {

    JsonNodeReader reader = getReader();

    // Rossz adattípus a path-ban
    assertThrows(RuntimeException.class, () -> { reader.exist("a", null); });
    assertThrows(RuntimeException.class, () -> { reader.exist("a", true); });
    
  }
  
  /**
   * Path tömbként megadva 
   */
  @Test
  public void arrayPath() throws JsonProcessingException {

    JsonNodeReader reader = getReader();

    assertEquals(48, reader.getInteger(new Object[]{"intSzam"}));
    assertEquals(7L, reader.getLong(new Object[]{"tomb", 0}));
    assertNull(reader.getLong(new Object[]{"tomb", 777}));
    
  }
  
  // ==== 
}
