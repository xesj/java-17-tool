package test.xesj.shell;
import test.xesj.shell.other.Masodik;
import test.xesj.shell.other.ElsoResultSetConverter;
import test.xesj.shell.other.Elso;
import test.xesj.shell.other.EgyebANC;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.xesj.TestInit;
import xesj.shell.Database;
import xesj.shell.MyException;
import xesj.shell.NullStrategy;
import xesj.shell.SQL;
import xesj.shell.Shell;
import xesj.shell.ShellException;
import xesj.shell.ShellMap;
import xesj.shell.generator.Gtool;
import xesj.tool.DateTool;
import xesj.tool.StringTool;

/**
 * Shell teszt H2 memória adatbázis használatával.
 */
public class ShellTest {

  /** Adatbázis shell */
  private static Shell shell;
  
  /**
   * Változók az ELSO táblához.
   */
  private final long idA = -84, idB = 5, idC = 100, idD = 9988; // azonosítók
  private final long nagyEgesz = Long.MAX_VALUE, irszam = 9876;
  private final BigDecimal tizedes = new BigDecimal("78.13");
  private final String rovidNev = "árvíztűrő ÁRVÍZTŰRŐ", hosszuNev = StringTool.multiply("hosszú árvíztűrőgép ", 100);
  private final File file;
  private final java.util.Date datum = new Date();
  private final Timestamp idopont = new Timestamp(new Date().getTime());
  private final boolean igazHamis = true;
  private final BigDecimal flt = new BigDecimal("148.25");

  /**
   * Változók a MASODIK táblához.
   */
  private final long evA = 1968, evB = 2000, evC = 2013, evD = 2020; // azonosítók
  private final String kodA = "x2", kodB = "x2", kodC = "ma", kodD = "fu"; // azonosítók
  private final String szoveg = "ÁRVÍZTŰRŐ";
  private final int szazalek = 67;

  /**
   * Egyéb változók.
   */
  private long tmpl;

  /**
   * Logger
   */
  private static final Logger logger = TestInit.getInstance().getLogger(ShellTest.class, "shell-test-log.txt");

  /**
   * BeforeAll: Adatbázis kapcsolat létrehozása, adatbázis objektumok létrehozása
   */
  @BeforeAll 
  public static void beforeAll() throws ClassNotFoundException, SQLException {

    shell = getShell();
    
    // Adatbázis objektumok törlése
    shell.sql("DROP TABLE IF EXISTS elso CASCADE").execute();
    shell.sql("DROP TABLE IF EXISTS masodik CASCADE").execute();
    shell.commit();
    
    // Adatbázis objektumok létrehozása
    shell.sql(
      "create table elso(" +
      "  id           smallint not null," +
      "  nagy_egesz   bigint," +
      "  irszam       numeric(4)," +
      "  tizedes      numeric(12,2)," +
      "  rovid_nev    varchar(100)," +
      "  hosszu_nev   text," +
      "  bin          blob," +
      "  datum        date," +
      "  idopont      timestamp," +
      "  igaz_hamis   boolean," +
      "  flt          float8" +
      ")").execute();
    shell.sql("alter table elso add constraint pk_elso primary key(id)").execute();
    shell.sql(
      "create table masodik(" +
      "  szoveg     text," +
      "  kod        varchar(2) not null," +
      "  ev         numeric(4) not null," +
      "  szazalek   bigint" +
      ")").execute();
    shell.sql("alter table masodik add constraint pk_masodik primary key(ev,kod)").execute();
    shell.commit();

  }

  /**
   * AfterAll: Adatbázis kapcsolat lezárása.
   */
  @AfterAll
  public static void afterAll() throws ClassNotFoundException, SQLException {

    shell.close();

  }
  
  /**
   * Minden teszt eljárás előtt: ELSO és MASODIK tábla kiürítése.
   */
  @BeforeEach
  public void beforeEach() {

    shell.sql("DELETE FROM elso").execute();
    shell.sql("DELETE FROM masodik").execute();
    shell.commit();

  }
  
  /**
   * Konstruktor
   */
  public ShellTest() throws URISyntaxException {

    file = new File(this.getClass().getResource("porsche.jpg").toURI());

  }

  /**
   * Adatbázis Connection létrehozása
   */  
  private static Connection getConnection() throws ClassNotFoundException, SQLException {

    Class.forName("org.h2.Driver");
    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1", "sa", "");
    connection.setAutoCommit(false);
    return connection;

  }

  /**
   * Adatbázis Shell létrehozása
   */  
  private static Shell getShell() throws ClassNotFoundException, SQLException {

    return new Shell(getConnection(), new Database("H2", NullStrategy.SET_NULL_WITH_NULL_TYPE));

  } 
  
  /**
   * SQL-insert teszt: ELSO feltoltese idA sorral.
   */
  @Test
  public void insert_elso_a_test() {

    tmpl = shell.sql(
      "INSERT INTO elso(id, nagy_egesz, irszam, tizedes, rovid_nev, hosszu_nev, bin, datum, idopont, igaz_hamis, flt) " +
      "VALUES(:id, :nagy_egesz, :irszam, :tizedes, :rovid_nev, :hosszu_nev, :bin, :datum, :idopont, :igaz_hamis, :flt)")
      .setParameter("id", idA)
      .setParameter("nagy_egesz", nagyEgesz)
      .setParameter("irszam", irszam)
      .setParameter("tizedes", tizedes)
      .setParameter("rovid_nev", rovidNev)
      .setParameter("hosszu_nev", hosszuNev)
      .setParameter("bin", file)
      .setParameter("datum", datum)
      .setParameter("idopont", idopont)
      .setParameter("igaz_hamis", igazHamis)
      .setParameter("flt", flt)
      .execute();
    assertEquals(1, tmpl);

  }
  
  /**
   * SQL-insert teszt: ELSO feltoltese idB sorral.
   */
  @Test
  public void insert_elso_b_test() {

    String tmps = "második";
    tmpl = shell.sql("INSERT INTO elso(id, rovid_nev, irszam) VALUES(:dd, :aa, :zz)")
      .setAllParameters(idB, tmps, 5000 + tmps.length()).execute();
    assertEquals(1, tmpl);

  }
  
  /**
   * SQL-insert teszt: ELSO feltoltese idC sorral.
   */
  @Test
  public void insert_elso_c_test() {

    tmpl = shell.sql(
      "INSERT INTO elso(id, nagy_egesz, tizedes, hosszu_nev, bin, datum, idopont, igaz_hamis) " +
      "VALUES(:1, :2, :3, :4||':konstan''s', :5, :6, :7, :8)")
      .setAllParameters(
        idC, 9988776655443322L, 1234567890.48, "ÉÁŰ éáű ŐÚ őú ÖÜÓ öüó", file, new Date(), 
        new Timestamp(new Date().getTime()), false
      )
      .execute();
    assertEquals(1, tmpl);

  }
  
  /**
   * SQL-insert teszt: MASODIK feltoltese evA sorral.
   */
  @Test
  public void insert_masodik_a_test() {

    shell.sql("INSERT INTO masodik(ev, kod, szoveg, szazalek) VALUES(:x, :x, :x, :x)")
      .setAllParameters(evA, kodA, szoveg, szazalek).execute();

  }
  
  /**
   * SQL-insert teszt: MASODIK feltoltese evB sorral.
   */
  @Test
  public void insert_masodik_b_test() {

    shell.sql("INSERT INTO masodik(ev, kod, szoveg, szazalek) VALUES(:ev, :kod, :2, :1)")
      .setParameter(2, null)
      .setParameter("ev", evB)
      .setParameter(1, 33)
      .setParameter("kod", kodB)
      .execute();

  }
  
  /**
   * SQL-insert teszt: MASODIK feltoltese evC sorral.
   */
  @Test
  public void insert_masodik_c_test() {

    shell.sql("INSERT INTO masodik(ev, kod, szoveg, szazalek) VALUES(:ev, :kod, :szoveg, :szazalek)")
      .setAllParameters(evC, kodC, "árvíztűrő", null).execute();

  }
  
  /**
   * Metódus exception teszt: setParameter()
   * Hiba: nemlétező paraméter.
   */
  @Test
  public void setParameterExceptionTest() {

    ShellException e = assertThrows(
      ShellException.class, 
      () -> { shell.sql("INSERT INTO masodik(ev, kod) VALUES(:x, :y)").setParameter("z", evA); }
    );
    assertEquals("Nem létezik 'z' nevű paraméter.", e.getMessage());

  }    
  
  /**
   * Metódus exception teszt: setAllParameters()
   * Hiba: kevés paraméter.
   */
  @Test
  public void setAllParametersExceptionTest() {

    ShellException e = assertThrows(
      ShellException.class,
      () -> { shell.sql("INSERT INTO masodik(ev, kod, szoveg, szazalek) VALUES(:x, :x, :x, :x)")
        .setAllParameters(evA, kodA, szoveg); }
    );
    assertEquals(
      "Az SQL-parancs 4 paraméterrel rendelkezik, de a setAllParameters() metódusban 3 paraméter van megadva.", 
      e.getMessage()
    );

  }
  
  /**
   * Metódus exception teszt: execute()
   * Hiba: üres SQL-parancs.
   */
  @Test
  public void executeExceptionTest1() {

    ShellException e = assertThrows(
      ShellException.class,
      () -> { shell.sql("").execute(); }
    );
    assertEquals("Az SQL parancs nem lehet null, vagy üres.", e.getMessage());

  }
  
  /**
   * Metódus exception teszt: execute()
   * Hiba: nincs megadva minden paraméter.
   */
  @Test
  public void executeExceptionTest2() {

    ShellException e = assertThrows(
      ShellException.class,
      () -> { shell.sql("INSERT INTO masodik(ev, kod) VALUES(:x, :y)").setParameter("x", evA).execute(); }
    );
    assertEquals("A(z) 'y' paraméterhez még nincs paraméter-objektum hozzárendelve.", e.getMessage());

  }   
  
  /**
   * Metódus teszt: getRow() paraméter nélkül.
   */
  @Test
  public void getRowEmptyTest() {

    // előkészítés
    insert_elso_a_test();
    insert_masodik_b_test();

    // Beolvasás az ELSO táblából ShellMap-be.
    ShellMap smap = shell.sql("SELECT * FROM elso WHERE id = :1")
      .setParameter(1, idA)
      .setMaximumRows(20)
      .getRow();
    assertEquals(nagyEgesz, (long)smap.getLong("nagy_egesz"));
    assertEquals(irszam, (long)smap.getLong("irszam"));
    assertEquals(tizedes, smap.getBigDecimal("tizedes"));
    assertEquals(rovidNev, smap.getString("rovid_nev"));
    assertEquals(hosszuNev, smap.getString("hosszu_nev"));
    assertEquals(file.length(), smap.getFile("bin").length());
    assertEquals(DateTool.midnight(datum), smap.getDate("datum"));
    assertEquals(idopont, smap.getTimestamp("idopont"));
    assertEquals(igazHamis, (boolean)smap.getBoolean("igaz_hamis"));
    assertEquals(flt, smap.getBigDecimal("flt"));

    // Beolvasás a MASODIK táblából ShellMap-be.
    smap = shell.sql("SELECT kod, ev, szoveg, szazalek FROM masodik WHERE kod = :k and ev = :e")
      .setParameter("e", evB)
      .setParameter("k", kodB)
      .getRow();
    assertNull(smap.getAny("szoveg"));
    assertEquals(33L, (long)smap.getLong("szazalek"));
    assertEquals(evB, (long)smap.getLong("ev"));

  }
  
  /**
   * Metódus teszt: getRow() DIC paraméterel.
   */
  @Test
  public void getRowDICTest() {

    // előkészítés
    insert_elso_a_test();
    insert_elso_b_test();

    // beolvasás
    Elso elso = shell.sql("SELECT * FROM elso WHERE id = :1").setParameter(1, idA).getRow(Elso.class);
    assertEquals(tizedes, elso.tizedes);
    assertEquals(igazHamis, (boolean)elso.igaz_hamis);
    assertEquals(flt, elso.flt);
    elso = shell.sql("SELECT * FROM elso WHERE id = :1").setParameter(1, idB).getRow(Elso.class);
    assertNull(elso.nagy_egesz);
    assertNull(elso.nagy_egesz);

  }  
  
  /**
   * Metódus teszt: getRow() DIO paraméterel.
   */
  @Test
  public void getRowDIOTest() {

    // előkészítés
    insert_elso_a_test();

    // beolvasás
    Elso elso = new Elso();
    shell.sql("SELECT * FROM elso WHERE id = :1").setParameter(1, idA).getRow(elso);
    assertEquals(tizedes, elso.tizedes);
    assertEquals(igazHamis, (boolean)elso.igaz_hamis);

  }
  
  /**
   * Metódus teszt: getRow() ResultSetConverter paraméterel.
   */
  @Test
  public void getRowConverterTest() {

    // előkészítés
    insert_elso_a_test();

    // beolvasás
    Elso elso = shell.sql("SELECT * FROM elso WHERE id = :1").setParameter(1, idA).getRow(new ElsoResultSetConverter());
    assertEquals(rovidNev, elso.rovid_nev);
    assertNull(elso.idopont);

  }  
  
  /**
   * Metódus exception teszt: getRow()
   * Hiba: Nem ANC-be olvasunk be.
   */
  @Test
  public void getRowExceptionTest1() {    

    ShellException e = assertThrows(
      ShellException.class,
      () -> { shell.sql("SELECT * FROM elso WHERE id = :1").setParameter(1, idA).getRow(String.class); }
    );
    assertEquals("A(z) 'java.lang.String' osztály egyetlen @Column annotációt sem tartalmaz.", e.getMessage());

  } 
  
  /**
   * Metódus exception teszt: getRow()
   * Hiba: Az ANC-ben hibás/nemlétező oszlopnév található.
   */
  @Test
  public void getRowExceptionTest2() {    

    // előkészítés
    insert_elso_a_test();

    // beolvasás
    assertThrows(
      RuntimeException.class,
      () -> { shell.sql("SELECT * FROM elso WHERE id = :1").setParameter(1, idA).getRow(EgyebANC.class); }
    );

  }  
  
  /**
   * Metódus teszt: getList() paraméter nélkül.
   */
  @Test
  public void getListEmptyTest() {

    // előkészítés
    insert_elso_a_test();
    insert_elso_b_test();
    insert_elso_c_test();

    // beolvasás
    List<ShellMap> list = shell.sql("SELECT * FROM elso WHERE id > :x").setAllParameters(0).getList();
    assertEquals(2, list.size());
    for (ShellMap sm: list) {
      long id = sm.getLong("id");
      String nev = (String)sm.getAny("rovid_nev");
      assertTrue(id == 5 && nev.equals("második") || id == 100 && nev == null);
    }

  }
  
  /**
   * Metódus teszt: getList() DIC paraméterrel.
   */
  @Test
  public void getListDICTest() {

    // előkészítés
    insert_elso_a_test();
    insert_elso_b_test();
    insert_elso_c_test();

    // beolvasás
    List<Elso> list = shell.sql("SELECT * FROM elso WHERE id > :x").setAllParameters(0).getList(Elso.class);
    assertEquals(2, list.size());
    for (Elso elso: list) {
      assertTrue(elso.id == 5 && elso.rovid_nev.equals("második") || elso.id == 100 && elso.rovid_nev == null);
    }

  }
  
  /**
   * Metódus teszt: getList() ResultSetConverter paraméterrel.
   */
  @Test
  public void getListConverterTest() {

    // előkészítés
    insert_elso_a_test(); 
    insert_elso_b_test(); 
    insert_elso_c_test();

    // beolvasás
    List<Elso> list = shell.sql("SELECT * FROM elso WHERE id > :x")
      .setParameter("x", idA).getList(new ElsoResultSetConverter());
    assertEquals(2, list.size());
    for (Elso elso: list) {
      assertTrue(elso.id == 5 && elso.rovid_nev.equals("második") || elso.id == 100 && elso.rovid_nev == null);
    }

  } 
  
  /**
   * Metódus teszt: isMoreRows()
   */
  @Test
  public void isMoreRowsTest() {

    // előkészítés
    insert_elso_a_test(); 
    insert_elso_b_test(); 
    insert_elso_c_test();

    // beolvasás
    shell.sql("SELECT * FROM elso").setMaximumRows(2).getList();
    assertTrue(shell.lastSQL().isMoreRows());
    shell.sql("SELECT * FROM elso").setMaximumRows(3).getList();
    assertFalse(shell.lastSQL().isMoreRows());

  }
  
  /**
   * Metódus teszt: isParameter()
   */
  @Test
  public void isParameterTest() {

    SQL sql = shell.sql("UPDATE elso SET rovid_nev = :pa, irszam = :22");
    assertTrue(sql.isParameter("pa"));
    assertTrue(sql.isParameter(22));
    assertFalse(sql.isParameter(":pa"));

  }
  
  /**
   * Metódus teszt: isParameterSet()
   */
  @Test
  public void isParameterSetTest() {

    SQL sql = shell.sql("UPDATE elso SET rovid_nev = :pa, irszam = :22");
    sql.setParameter("pa", "beállítva");
    assertTrue(sql.isParameterSet("pa"));
    assertFalse(sql.isParameterSet(22));
    assertFalse(sql.isParameterSet("22"));
    assertFalse(sql.isParameterSet(":pa"));

  }
  
  /**
   * Metódus teszt: getJdbcSql()
   */
  @Test
  public void getJdbcSqlTest() {

    assertEquals("DELETE FROM elso", 
       shell.sql("DELETE FROM elso").getJdbcSql());
    assertEquals("SELECT 'hh24:mi:ss' FROM elso WHERE datum = ?", 
       shell.sql("SELECT 'hh24:mi:ss' FROM elso WHERE datum = :x").getJdbcSql());
    assertEquals("SELECT 'hh24:mi:ss' FROM elso WHERE datum = ?", 
       shell.sql("SELECT 'hh24:mi:ss' FROM elso WHERE datum = :x234").getJdbcSql());
    assertEquals("SELECT   ?||'hh24'':mi:''ss' +  ? FROM elso WHERE datum = ?", 
       shell.sql("SELECT   :ggg||'hh24'':mi:''ss' +  :hhhhh FROM elso WHERE datum = :x").getJdbcSql());

  }
  
  /**
   * Metódus teszt: getObject()
   */
  @Test
  public void getObjectTest() {

    // előkészítés
    insert_elso_a_test();
    insert_masodik_a_test();

    // Beolvasás az ELSO táblából.
    Elso elsoNull = shell.getObject(Elso.class, 637652);
    assertNull(elsoNull);
    Elso elso1 = shell.getObject(Elso.class, idA);
    assertEquals(idA, (long)elso1.id);
    assertEquals(nagyEgesz, (long)elso1.nagy_egesz);
    assertEquals(irszam, (long)elso1.irszam);
    assertEquals(tizedes, elso1.tizedes);
    assertEquals(rovidNev, elso1.rovid_nev);
    assertEquals(hosszuNev, elso1.hosszu_nev);
    assertEquals(file.length(), elso1.bin.length());
    assertEquals(DateTool.midnight(datum), elso1.datum);
    assertEquals(idopont, elso1.idopont);
    assertEquals(igazHamis, (boolean)elso1.igaz_hamis);

    // Beolvasás a MASODIK táblából DIC-be.
    Masodik masodik = shell.getObject(Masodik.class, kodA, evA);
    assertEquals(evA, (long)masodik.ev);
    assertEquals(kodA, masodik.kod);
    assertEquals(szazalek, (long)masodik.szazalek);
    assertEquals(szoveg, masodik.szoveg);

    // Beolvasás a MASODIK táblából DIO-ba.
    masodik.free = "szabad";
    masodik = shell.getObject(masodik, kodA, evA);
    assertEquals("szabad", masodik.free);

  }
  
  /**
   * Metódus exception teszt: getObject()
   * Hiba: Nem DIC/DIO-ba olvasunk be.
   */
  @Test
  public void getObjectExceptionTest1() {    

    ShellException e = assertThrows(
      ShellException.class,
      () -> { shell.getObject(String.class, 1); }
    );
    assertEquals("A(z) 'java.lang.String' osztály egyetlen @Column annotációt sem tartalmaz.", e.getMessage());

  }  
  
  /**
   * Metódus exception teszt: getObject()
   * Hiba: Nem ANC-be olvasunk be.
   */
  @Test
  public void getObjectExceptionTest2() {    

    ShellException e = assertThrows(
      ShellException.class,
      () -> { shell.getObject(EgyebANC.class, 1); }
    );
    assertEquals(
      "A(z) 'test.xesj.shell.other.EgyebANC' osztály a @Table/@View és @Id annotáció egyikét nem tartalmazza.", 
      e.getMessage()
    );

  }      
  
  /**
   * Metódus exception teszt: getObject()
   * Hiba: Nem megfelelő számú paraméter a primary key megadáshoz.
   */
  @Test
  public void getObjectExceptionTest3() {    

    ShellException e = assertThrows(
      ShellException.class,
      () -> { shell.getObject(Masodik.class, 9564); }
    );
    assertEquals(
      "Az SQL-parancs 2 paraméterrel rendelkezik, de a setAllParameters() metódusban 1 paraméter van megadva.", 
      e.getMessage()
    );

  }
  
  /**
   * Metódus teszt: insertObject()
   */
  @Test
  public void insertObjectTest() {    

    // Insert az ELSO táblába.
    Elso elso = new Elso();
    elso.id = idD;
    elso.irszam = 8321L;
    elso.hosszu_nev = "insertObject()-tel keletkezett árvíztűrő";
    shell.insertObject(elso);
    shell.commit();

    // Insert visszaellenőrzése.
    Elso elso2 = shell.getObject(Elso.class, idD);
    assertEquals("insertObject()-tel keletkezett árvíztűrő", elso2.hosszu_nev);
    assertEquals(8321L, (long)elso2.irszam);
    assertNull(elso2.datum);

    // Insert a MASODIK táblába.
    Masodik masodik = new Masodik(evA, kodA);
    masodik.ev = evD;
    masodik.kod = kodD;
    shell.insertObject(masodik);
    shell.commit();

  } 
  
  /**
   * Metódus teszt: updateObject()
   */
  @Test
  public void updateObjectTest() {   

    // előkészítés
    insert_elso_c_test();
    insert_masodik_a_test();
    insert_masodik_b_test();
    insert_masodik_c_test();

    // Update az ELSO táblán.
    Elso elso = shell.getObject(Elso.class, idC);
    elso.hosszu_nev = elso.hosszu_nev + " + updateObject()";
    elso.igaz_hamis = true;
    shell.updateObject(elso);

    // Update a MASODIK táblán.
    Masodik masodik = new Masodik(5381L, "wq");
    tmpl = shell.updateObject(masodik);
    assertEquals(0, tmpl);
    masodik = new Masodik(evC, kodC);
    masodik.szazalek = 50L;
    tmpl = shell.updateObject(masodik);
    assertEquals(1, tmpl);
    ShellMap smap = shell.sql("SELECT sum(szazalek) szum1, sum(ev) szum2 FROM masodik").getRow();
    BigDecimal 
      szum1 = smap.getBigDecimal("szum1"),
      szum2 = smap.getBigDecimal("szum2");
    assertEquals(new BigDecimal(150), szum1);
    assertEquals(new BigDecimal(5981), szum2);
    shell.commit();

  }
  
  /**
   * Metódus teszt: deleteObject()
   */
  @Test
  public void deleteObjectTest() {

    // előkészítés
    insert_elso_c_test();
    insert_masodik_c_test();

    // Delete az ELSO táblán.
    Elso elso = shell.getObject(Elso.class, idC);
    tmpl = shell.deleteObject(elso);
    assertEquals(1, tmpl);

    // Delete a MASODIK táblán.
    tmpl = shell.deleteObject(new Masodik(evC, kodC));
    assertEquals(1, tmpl);
    tmpl = shell.deleteObject(new Masodik(7639L, "o8"));
    assertEquals(0, tmpl);
    shell.commit();

  }  
  
  /**
   * Metódus teszt: rollback()
   */
  @Test
  public void rollbackTest() {

    insert_elso_a_test();
    shell.commit();
    insert_elso_b_test();
    insert_elso_c_test();
    shell.rollback();
    tmpl = shell.sql("SELECT count(1) c FROM elso").getRow().getLong("c");
    assertEquals(1, tmpl);

  }
  
  /**
   * Metódus teszt: close()
   * Ellenőrzés: a close() végrehajt-e rollback()-et is. 
   */
  @Test
  public void closeRollbackTest() throws ClassNotFoundException, SQLException {

    Shell tmpshell = null;
    try {
      tmpshell = getShell();
      tmpshell.sql("INSERT INTO masodik(ev, kod, szoveg, szazalek) VALUES(:x, :x, :x, :x)")
        .setAllParameters(evA, kodA, szoveg, szazalek).execute();
    }
    finally {
      try { tmpshell.close(); } catch (Exception e) {}
    }
    tmpl = shell.sql("SELECT count(1) c FROM masodik").getRow().getLong("c");
    assertEquals(0, tmpl);

  }
  
  /**
   * Metódus teszt: createClassName()
   */
  @Test
  public void createClassNameTest() {

    assertEquals("Telepules", Gtool.createClassName("telepules"));
    assertEquals("Telepules", Gtool.createClassName("tELEPULES"));
    assertEquals("TelepulesEv", Gtool.createClassName("telepules_ev"));
    assertEquals("ElesTelepulesEv", Gtool.createClassName("eles.telepules_ev"));
    assertEquals("ElesSchemaTelepulesEv", Gtool.createClassName("eles_schema.telepules_ev"));
    assertEquals("ElesSchemaTelepulesEv", Gtool.createClassName("ELES_SCHEMA.TELEPULES_EV"));

  }
  
  /**
   * Metódus teszt: checkClassName()
   */
  @Test
  public void checkClassNameTest() throws Exception {

    Gtool.checkClassName("teszt", "Telepules");
    Gtool.checkClassName("teszt", "_Telepules_123_abc_");

  }  
  
  /**
   * Metódus exception teszt: checkClassName()
   */
  @Test
  public void checkClassNameExceptionTest1() {

    MyException e = assertThrows(
      MyException.class,
      () -> { Gtool.checkClassName("teszt", "0_Telepules_123_abc_"); }
    );
    assertEquals("A <name>teszt</name> taghoz tartozó <class_name> tag számmal kezdődik: '0'", e.getMessage());

  }  
  
  /**
   * Metódus exception teszt: checkClassName()
   */
  @Test
  public void checkClassNameExceptionTest2() {

    MyException e = assertThrows(
      MyException.class,
      () -> { Gtool.checkClassName("teszt", "Telepulesí_123_abc_"); }
    );
    assertEquals("A <name>teszt</name> taghoz tartozó <class_name> tag hibás karaktert tartalmaz: 'í'", e.getMessage());

  }
  
  /**
   * Metódus teszt: Shell.info(), SQL.info(), ShellMap.info()
   */
  @Test
  public void infoTest() throws Exception {

    // előkészítés
    insert_elso_a_test();

    // Shell info
    logger.info(shell.info().toString());

    // SQL info
    shell.sql("SELECT * FROM elso WHERE :a = :b").setAllParameters(0, 1).getList();
    logger.info(shell.lastSQL().info().toString());

    // SQL info
    shell.getObject(Elso.class, idA);
    logger.info(shell.lastSQL().info().toString());

    // ShellMap info
    ShellMap smap = shell.sql("SELECT * FROM elso WHERE id = :1").setParameter(1, idA).getRow();
    logger.info(smap.info().toString());

  }  
  
  /**
   * Cross-conversion teszt.
   */
  @Test
  public void crossConversionTest() {

    // előkészítés
    insert_elso_a_test();

    // nagy_egesz oszlop kiolvasása mindhárom módon
    ShellMap smap = shell.sql("SELECT nagy_egesz FROM elso").getRow();
    assertEquals(nagyEgesz, (long)smap.getLong("nagy_egesz"));
    assertEquals(new BigInteger(String.valueOf(nagyEgesz)), smap.getBigInteger("nagy_egesz"));
    assertEquals(new BigDecimal(nagyEgesz), smap.getBigDecimal("nagy_egesz"));

    // irszam^4 oszlop kiolvasása mindhárom módon   
    smap = shell.sql("SELECT irszam * irszam * irszam * irszam x FROM elso").getRow();
    assertEquals(new BigInteger(String.valueOf(irszam)).pow(4), smap.getBigInteger("x"));
    assertEquals(new BigDecimal(new BigInteger(String.valueOf(irszam)).pow(4)), smap.getBigDecimal("x"));

    // tizedes oszlop kiolvasása mindhárom módban
    smap = shell.sql("SELECT tizedes FROM elso").getRow();
    assertEquals(tizedes, smap.getBigDecimal("tizedes"));
    try {
      smap.getLong("tizedes");
      fail();
    }
    catch (ArithmeticException ae) {}
    try {
      smap.getBigInteger("tizedes");
      fail();
    }
    catch (ArithmeticException ae) {}

    // 100 * tizedes oszlop kiolvasása mindhárom módban
    smap = shell.sql("SELECT 100 * tizedes x FROM elso").getRow();
    assertEquals(tizedes.multiply(new BigDecimal("100")).longValue(), (long)smap.getLong("x"));
    assertEquals(tizedes.multiply(new BigDecimal("100")).toBigIntegerExact(), smap.getBigInteger("x"));
    assertEquals(tizedes.multiply(new BigDecimal("100")), smap.getBigDecimal("x"));

  }
  
  /**
   * Try with resources teszt.
   */
  @Test
  public void tryWithResourcesTest() throws ClassNotFoundException, SQLException {

    // előkészítés
    insert_elso_a_test();

    // tesztelés
    for (int i = 0; i < 100; i++) {
      try (Shell sh = getShell()) {
        sh.sql("SELECT count(1) c FROM elso").getRow().getLong("c");
        try (SQL sql = sh.sql("SELECT * FROM elso")) {
          sql.getResultSet().next();
        }
      }
    }

  }
  
  // ====
}
