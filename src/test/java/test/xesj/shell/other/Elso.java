package test.xesj.shell.other;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import xesj.shell.annotation.Column;
import xesj.shell.annotation.Id;
import xesj.shell.annotation.Table;
/**
 * @author Shell DIC generátor
 * @version 2.1
 */
@Table("elso")
public class Elso implements Serializable {
  @Column @Id
  public Long id;
  @Column
  public Long nagy_egesz;
  @Column
  public Long irszam;
  @Column
  public BigDecimal tizedes;
  @Column
  public String rovid_nev;
  @Column
  public String hosszu_nev;
  @Column
  public File bin;
  @Column
  public Date datum;
  @Column
  public Timestamp idopont;
  @Column
  public Boolean igaz_hamis;
  @Column
  public BigDecimal flt;
}
/*
Elso elso = new Elso();
elso.id =
elso.nagy_egesz =
elso.irszam =
elso.tizedes =
elso.rovid_nev =
elso.hosszu_nev =
elso.bin =
elso.datum =
elso.idopont =
elso.igaz_hamis =
elso.pont =
elso.flt =
*/
