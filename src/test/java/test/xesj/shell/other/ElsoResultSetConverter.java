package test.xesj.shell.other;
import java.sql.ResultSet;
import java.sql.SQLException;
import xesj.shell.ResultSetConverter;

/**
 * Elso Resultset Converter
 */
public class ElsoResultSetConverter implements ResultSetConverter<Elso>{

  /**
   * convert()
   */
  @Override
  public Elso convert(ResultSet resultSet, int rowNumber) {

    Elso elso = new Elso();
    try {
      elso.id = resultSet.getLong("id");
      elso.rovid_nev = resultSet.getString("rovid_nev");
      elso.hosszu_nev = "ROW-NUMBER: " + rowNumber + " " + resultSet.getString("hosszu_nev");
    } 
    catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return elso;

  }

  // ====
}
