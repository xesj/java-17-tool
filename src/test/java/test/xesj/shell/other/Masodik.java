package test.xesj.shell.other;
import xesj.shell.annotation.Column;
import xesj.shell.annotation.Id;
import xesj.shell.annotation.Table;

/**
 * Második
 */
@Table("masodik")
public class Masodik {

  @Id(2) @Column
  public Long ev;

  @Column
  public String szoveg;

  @Column @Id(1)
  public String kod;

  @Column 
  public Long szazalek;

  // nem oszlop
  public String free;

  /**
   * Konstruktor
   */
  public Masodik() {}

  /**
   * Konstruktor
   */
  public Masodik(Long ev, String kod) {

    this.ev = ev;
    this.kod = kod;

  }

  /**
   * toString()
   */
  @Override
  public String toString() {

    return ev + "|" + szoveg + "|" + kod + "|" + szazalek;

  }

  // ==== 
}
