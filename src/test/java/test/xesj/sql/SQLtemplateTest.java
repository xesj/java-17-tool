package test.xesj.sql;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import test.xesj.TestInit;
import xesj.sql.SQLtemplate;
import xesj.sql.SQLtemplateException;

/**
 * SQLtemplate osztály tesztelő
 */
public class SQLtemplateTest {
  
  private static final String sor3A = """
    Első sor
    Második sor
    Harmadik (utolsó sor)
    """;

  private static final String sor3B = """
    Első sor
    Második sor (nem utolsó)

    """;
  
  private static final String sql1 = """
    SELECT 
      egy, ketto, {harom}
    FROM                       --(f2)
      {TÁBla}                  --(f2)
    WHERE {harom} > {TÁBla}    --(where)
    ORDER BY ketto             --(order)                     
    """;
  
  /** Logger */
  private static final Logger logger = TestInit.getInstance().getLogger(SQLtemplateTest.class, "sql-template-test-log.txt");
  
  /**
   * Konstruktor teszt
   */
  @Test
  public void constructorTeszt() {
    
    // Null template
    assertThrows(
      SQLtemplateException.class,
      () -> { new SQLtemplate(null); }
    );
    
    // Üres template
    assertEquals(1, new SQLtemplate("").getLineCount());
    assertEquals("", new SQLtemplate("").toString());

    // Template sorok számának ellenőrzése
    assertEquals(3, new SQLtemplate(sor3A).getLineCount());
    assertEquals(3, new SQLtemplate(sor3B).getLineCount());
    logger.info("sor3A --->" + new SQLtemplate(sor3A).toString() + "<--- sor3A");
    logger.info("sor3B --->" + new SQLtemplate(sor3B).toString() + "<--- sor3B");
    
  }
  
  /**
   * replace() teszt
   */
  @Test
  public void replaceTeszt() {
    
    // Null target
    assertThrows(
      SQLtemplateException.class,
      () -> { new SQLtemplate(sql1).replace(null, "abc", true); }
    );

    // Üres target
    assertThrows(
      SQLtemplateException.class,
      () -> { new SQLtemplate(sql1).replace("", "abc", true); }
    );
    
    logger.info("sql1 nincs replace --->" + 
      new SQLtemplate(sql1).replace("harom", "HÁROM_NEVŰ", false).replace("table", "TÁBLNÉV", false).toString()
    );
    logger.info("sql1 replace --->" + 
      new SQLtemplate(sql1).replace("harom", "HÁROM_NEVŰ").replace("TÁBla", "public.SZEMELY").toString()
    );
    logger.info("sql1 csak táblanév replace --->" + 
      new SQLtemplate(sql1).replace("harrrrrrom", "HÁROM_NEVŰ").replace("TÁBla", "public.SZEMELY").toString()
    );
    
  }
  
  /**
   * delete() teszt
   */
  @Test
  public void deleteTeszt() {

    new SQLtemplate(sql1);
    new SQLtemplate(sql1).delete("order");

    
    // Null marker
    assertThrows(
      SQLtemplateException.class,
      () -> { new SQLtemplate(sql1).delete(null, true); }
    );

    // Üres target
    assertThrows(
      SQLtemplateException.class,
      () -> { new SQLtemplate(sql1).delete(""); }
    );
  
    logger.info("sql1 nincs delete --->" + 
      new SQLtemplate(sql1).delete("f2", false).delete("where", false).toString()
    );
    logger.info("sql1 delete order --->" + 
      new SQLtemplate(sql1).delete("order").delete("f2", false).toString()
    );
    logger.info("sql1 delete minden --->" + 
      new SQLtemplate(sql1).delete("f2").delete("where").delete("order", true).delete("nincs").toString()
    );
    
  }
    
  // ====
}
