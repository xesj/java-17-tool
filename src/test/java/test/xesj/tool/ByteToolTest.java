package test.xesj.tool;
import java.text.ParseException;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static xesj.tool.ByteTool.*;

/**
 * Byte tool teszt
 */
public class ByteToolTest {

  /**
   * Metódus teszt: byteToHex()
   */
  @Test
  public void byteToHexTest() throws ParseException {

    assertNull(byteToHex(null));
    assertEquals("80", byteToHex((byte)-128));
    assertEquals("FE", byteToHex((byte)-2));
    assertEquals("FF", byteToHex((byte)-1));
    assertEquals("00", byteToHex((byte)0));
    assertEquals("01", byteToHex((byte)1));
    assertEquals("0F", byteToHex((byte)15));
    assertEquals("10", byteToHex((byte)16));
    assertEquals("7F", byteToHex((byte)127));

  }

  /**
   * Metódus teszt: hexToByte()
   */
  @Test
  public void hexToByteTest() {    

    // null érték
    assertNull(hexToByte(null));

    // 1 karakteres értékek 
    assertEquals((byte)0, (byte)hexToByte("0"));
    assertEquals((byte)10, (byte)hexToByte("a"));
    assertEquals((byte)15, (byte)hexToByte("f"));

    // 2 karakteres értékek
    assertEquals((byte)-128, (byte)hexToByte("80"));
    assertEquals((byte)-2, (byte)hexToByte("fE"));
    assertEquals((byte)-1, (byte)hexToByte("ff"));
    assertEquals((byte)0, (byte)hexToByte("00"));
    assertEquals((byte)1, (byte)hexToByte("01"));
    assertEquals((byte)15, (byte)hexToByte("0F"));
    assertEquals((byte)16, (byte)hexToByte("10"));
    assertEquals((byte)127, (byte)hexToByte("7f"));

  }

  /**
   * Metódus exception teszt: hexToByte()
   * Hiba: túl hosszú paraméter.
   */
  @Test 
  public void hexToByteExceptionTest1() {

    assertThrows(
      NumberFormatException.class,
      () -> { hexToByte("300"); }
    );

  }

  /**
   * Metódus exception teszt: hexToByte()
   * Hiba: túl rövid paraméter.
   */
  @Test 
  public void hexToByteExceptionTest2() {

    assertThrows(
      NumberFormatException.class,
      () -> { hexToByte(""); }
    );

  }

  /**
   * Metódus exception teszt: hexToByte()
   * Hiba: nem konvertálható paraméter.
   */
  @Test 
  public void hexToByteExceptionTest3() {

    assertThrows(
      NumberFormatException.class,
      () -> { hexToByte("x0"); }
    );

  }

  /**
   * Metódus teszt: byteToBin()
   */
  @Test
  public void byteToBinTest() {    

    assertNull(byteToBin(null));
    assertEquals("10000000", byteToBin((byte)-128));
    assertEquals("11111110", byteToBin((byte)-2));
    assertEquals("11111111", byteToBin((byte)-1));
    assertEquals("00000000", byteToBin((byte)0));
    assertEquals("00000001", byteToBin((byte)1));
    assertEquals("00000010", byteToBin((byte)2));
    assertEquals("00000111", byteToBin((byte)7));
    assertEquals("01111111", byteToBin((byte)127));

  }

  /**
   * Metódus teszt: binToByte()
   */
  @Test
  public void binToByteTest() {    

    assertNull(binToByte(null));
    assertEquals((byte)-128, (byte)binToByte("10000000"));
    assertEquals((byte)-2,   (byte)binToByte("11111110"));
    assertEquals((byte)-1,   (byte)binToByte("11111111"));
    assertEquals((byte)0,    (byte)binToByte(       "0"));
    assertEquals((byte)0,    (byte)binToByte("00000000"));
    assertEquals((byte)1,    (byte)binToByte(       "1"));
    assertEquals((byte)1,    (byte)binToByte(     "001"));
    assertEquals((byte)7,    (byte)binToByte("00000111"));
    assertEquals((byte)8,    (byte)binToByte(    "1000"));
    assertEquals((byte)127,  (byte)binToByte("01111111"));
    assertEquals((byte)127,  (byte)binToByte( "1111111"));

  }
  
  /**
   * Metódus teszt: byteToInt()
   */
  @Test
  public void byteToIntTest() {

    assertNull(byteToInt(null));
    assertEquals(0, byteToInt((byte)0));
    assertEquals(1, byteToInt((byte)1));
    assertEquals(23, byteToInt((byte)23));
    assertEquals(126, byteToInt((byte)126));
    assertEquals(127, byteToInt((byte)127));
    assertEquals(128, byteToInt((byte)-128));
    assertEquals(129, byteToInt((byte)-127));
    assertEquals(254, byteToInt((byte)-2));
    assertEquals(255, byteToInt((byte)-1));
    
  }

  /**
   * Metódus exception teszt: binToByte()
   * Hiba: nem konvertálható paraméter.
   */
  @Test 
  public void binToByteExceptionTest1() {

    assertThrows(
      NumberFormatException.class,
      () -> { binToByte("0x1111"); }
    );

  }

  /**
   * Metódus exception teszt: binToByte()
   * Hiba: túl hosszú paraméter.
   */
  @Test 
  public void binToByteExceptionTest2() {

    assertThrows(
      NumberFormatException.class,
      () -> { binToByte("100000000"); }
    );

  }

  /**
   * Metódus exception teszt: binToByte()
   * Hiba: túl rövid paraméter.
   */
  @Test 
  public void binToByteExceptionTest3() {

    assertThrows(
      NumberFormatException.class,
      () -> { binToByte(""); }
    );

  }

  /**
   * Metódus teszt: intToByte()
   */
  @Test
  public void intToByteTest() {

    assertNull(intToByte(null));
    assertEquals((byte)0, intToByte(0));
    assertEquals((byte)1, intToByte(1));
    assertEquals((byte)23, intToByte(23));
    assertEquals((byte)126, intToByte(126));
    assertEquals((byte)127, intToByte(127));
    assertEquals((byte)-128, intToByte(128));
    assertEquals((byte)-127, intToByte(129));
    assertEquals((byte)-2, intToByte(254));
    assertEquals((byte)-1, intToByte(255));
    
  }
  
  /**
   * Metódus teszt: hexToByteArray()
   */
  @Test
  public void hexToByteArrayTest() {
    
    // Helyes konkrét esetek 
    assertNull(hexToByteArray(null));
    assertArrayEquals(new byte[0], hexToByteArray(""));
    assertArrayEquals(new byte[]{1, -1, 127, 0, -128}, hexToByteArray("01fF7f0080"));
    
    // Hibás esetek
    String[] badHexValues = {"0", "111", "0g", "01234X"};
    for (String badHex: badHexValues) {
      assertThrows(NumberFormatException.class, () -> { hexToByteArray(badHex); });
    }
    
    // Konverzió oda-vissza
    String[] hexValues = {
      "00", "ff", "0000000000", "a431", "1111", "CAFEbabe", 
      "ABCDEF0123456789abcdef", "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad"
    };
    for (String hex: hexValues) {
      assertEquals(hex.toUpperCase(), byteArrayToHex(hexToByteArray(hex)));
    }
    
  }

  /**
   * Metódus teszt: byteArrayToHex()
   */
  @Test
  public void byteArrayToHexTest() {
    
    // Helyes konkrét esetek 
    assertNull(byteArrayToHex(null));
    assertEquals("", byteArrayToHex(new byte[0]));
    assertEquals("01FF7F0080", byteArrayToHex(new byte[]{1, -1, 127, 0, -128}));
    
    // Konverzió oda-vissza
    // Ez már meg van valósítva a hexToByteArrayTest()-nél
    
  }
  
  // ====
}
