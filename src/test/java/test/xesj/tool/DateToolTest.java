package test.xesj.tool;
import java.text.ParseException;
import java.util.Date;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import static xesj.tool.DateTool.*;

/**
 * Date tool teszt
 */
public class DateToolTest {

  /**
   * Metódus teszt: formatTime()
   */
  @Test
  public void formatTimeTest() throws ParseException {

    assertNull(formatTime(null));
    assertEquals(formatTime(parseSec("1968.07.22 07:23:01")), "07:23:01");
    assertEquals(formatTime(parseSec("2017.11.04 13:09:59")), "13:09:59");

  }

  /**
   * Metódus teszt: formatWeekDay()
   */
  @Test
  public void formatWeekDayTest() throws ParseException {

    assertNull(formatWeekDay(null));
    assertEquals(formatWeekDay(parseDay("1968.07.22")), "hétfő");
    assertEquals(formatWeekDay(parseSec("1980.12.30 05:48:12")), "kedd");
    assertEquals(formatWeekDay(parseDay("2007.06.13")), "szerda");
    assertEquals(formatWeekDay(parseDay("2010.07.01")), "csütörtök");
    assertEquals(formatWeekDay(parseDay("2014.08.08")), "péntek");
    assertEquals(formatWeekDay(parseDay("2222.10.05")), "szombat");
    assertEquals(formatWeekDay(parseSec("2500.10.31 23:59:59")), "vasárnap");

  }
  
  /**
   * Metódus teszt: isLeapYear()
   */
  @Test
  public void isLeapYearTest() throws ParseException {    

    assertFalse(isLeapYear(1));
    assertTrue(isLeapYear(4));
    assertFalse(isLeapYear(10));
    assertFalse(isLeapYear(1800));
    assertFalse(isLeapYear(1900));
    assertTrue(isLeapYear(1996));
    assertTrue(isLeapYear(2000));
    assertFalse(isLeapYear(2100));
    assertTrue(isLeapYear(2400));

  }
  
  /**
   * Metódusok teszt: formatOnlyYear(), formatOnlyMonth(), formatOnlyDay()
   */
  @Test
  public void formatOnlyYearMonthDayTest() throws ParseException {
    
    assertNull(formatOnlyYear(null));
    assertNull(formatOnlyMonth(null));
    assertNull(formatOnlyDay(null));

    Date date = parseDay("2014.09.04");
    assertEquals("2014", formatOnlyYear(date));
    assertEquals("09", formatOnlyMonth(date));
    assertEquals("04", formatOnlyDay(date));

    date = parseDay("2000.12.31");
    assertEquals("2000", formatOnlyYear(date));
    assertEquals("12", formatOnlyMonth(date));
    assertEquals("31", formatOnlyDay(date));

  }

  /**
   * Metódus teszt: isPast()
   */
  @Test
  public void isPastTest() throws ParseException {    

    assertFalse(isPast(null));
    assertFalse(isPast("2999.01.01 00:00:00"));
    assertFalse(isPast("2999.12.31 23:59:59"));
    assertTrue(isPast("1015.12.31 23:59:59"));
    assertTrue(isPast("2015.01.31 23:59:59"));

  }

  /**
   * Metódus exception teszt: isPast()
   * Hiba: nem értelmezhető paraméter.
   */
  @Test
  public void isPastExceptionTest() throws ParseException {

    assertThrows(
      ParseException.class,
      () -> { isPast("rossz"); }
    );

  }

  /**
   * Metódus exception teszt: parseDay()
   * Hiba: hibás dátum string.
   */
  @ParameterizedTest
  @ValueSource(
    strings = {"2017.11.1x", "2017.1.11", "201.11.11", "1.1.1", "1.1.1x", " 2017.11.11", "2017.11.11   ", "xxxx.11.11"}
  )
  public void parseDayExceptionTest(String dateString) throws ParseException {

    assertThrows(
      ParseException.class,
      () -> { parseDay(dateString); }
    );

  }

  /**
   * Metódus exception teszt: parse()
   * Hiba: hibás dátum string.
   */
  @ParameterizedTest
  @CsvSource(value = {"201|yyyy", "2017.1|yyyy.mm", "2017.1x|yyyy.mm", "xxxx|yyyy"}, delimiter = '|')
  public void parseExceptionTest(String dateString, String formatMask) throws ParseException {

    assertThrows(
      ParseException.class,
      () -> { parse(dateString, formatMask); }
    );

  }
  
  // ====
}
