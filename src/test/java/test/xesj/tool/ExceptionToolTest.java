package test.xesj.tool;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static xesj.tool.ExceptionTool.*;

/**
 * Exception tool teszt
 */
public class ExceptionToolTest {

  /**
   * Metódus teszt: getRootCauseTest()
   */
  @Test
  public void getRootCauseTest() {

    // előkészítés
    Throwable t1 = new RuntimeException("lánc 1. elem");
    Throwable t2 = new RuntimeException("lánc 2. elem", t1);
    Throwable t3 = new Exception("lánc 3. elem", t2);

    // tesztelés
    assertNull(getRootCause(null));
    assertEquals(getRootCause(t1), t1);
    assertEquals(getRootCause(t2), t1);
    assertEquals(getRootCause(t3), t1);

  }

  // ====
}
