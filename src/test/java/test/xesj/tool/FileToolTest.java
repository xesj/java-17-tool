package test.xesj.tool;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import test.xesj.TestInit;
import static xesj.tool.FileTool.*;
import xesj.tool.StreamTool;

/**
 * File tool teszt
 */
public class FileToolTest {

  /**
   * Logger
   */
  private static final Logger logger = TestInit.getInstance().getLogger(FileToolTest.class, "file-tool-test-log.txt");

  /**
   * Metódus teszt: tmpDir(), tmpDirName()
   */
  @Test
  public void tmpDirTest() throws IOException, URISyntaxException {

    logger.info("tmpDirName() -> " + tmpDirName());
    logger.info("tmpDir()     -> " + tmpDir());

  }

  /**
   * Metódus teszt: tmpFile()
   */
  @Test
  public void tmpFileTest() {

    for (int i = 0; i < 3; i++) {
      File tmpFile = tmpFile(null);
      logger.info("Temporális fájl kiterjesztés nélkül: " + tmpFile.getAbsolutePath());
    }
    for (int i = 0; i < 3; i++) {
      File tmpFile = tmpFile("jpg");
      logger.info("Temporális fájl kiterjesztéssel:     " + tmpFile.getAbsolutePath());
    }

  }

  /**
   * Metódus teszt: contentEquals()
   */
  @Test
  public void contentEqualsTest() throws IOException, URISyntaxException {

    File 
      file1 = new File(this.getClass().getResource("adatmodell.jpg").toURI()),
      file2 = new File(this.getClass().getResource("auto.jpg").toURI()),
      file2m = new File(this.getClass().getResource("auto_masolat.jpg").toURI()),
      fileUres = new File(this.getClass().getResource("ures.xy").toURI()),
      dir = tmpDir();

    // false esetek
    assertFalse(contentEqual(file1, file2));
    assertFalse(contentEqual(fileUres, file1));
    assertFalse(contentEqual(file1, fileUres));

    // true esetek
    assertTrue(contentEqual(file2m, file2));
    assertTrue(contentEqual(file2, file2m));
    assertTrue(contentEqual(file2, file2));
    assertTrue(contentEqual(fileUres, fileUres));

  }

  /**
   * Metódus exception teszt: contentEquals()
   * Hiba: a paraméter null.
   */
  @Test
  public void contentEqualsExceptionTest1() throws IOException  {

    assertThrows(
      NullPointerException.class,
      () -> { contentEqual(null, null); }
    );

  }

  /**
   * Metódus exception teszt: contentEquals()
   * Hiba: a paraméter könyvtár.
   */
  @Test
  public void contentEqualsExceptionTest2() throws IOException {

    assertThrows(
      FileNotFoundException.class,
      () -> { contentEqual(tmpDir(), tmpDir());}
    );

  }

  /**
   * Metódus exception teszt: contentEquals()
   * Hiba: az egyik paraméter nemlétező fájl.
   */
  @Test
  public void contentEqualsExceptionTest3() throws IOException {

    assertThrows(
      FileNotFoundException.class,
      () -> {
        File 
          letezo = tmpFile("txt"),
          nemLetezo = tmpFile("txt");
        try (FileOutputStream fileOutputStream = new FileOutputStream(letezo)) {
          StreamTool.writeString("abc", fileOutputStream, "UTF-8");
        }
        contentEqual(letezo, nemLetezo);
      }  
    );

  }

  // ====
}
