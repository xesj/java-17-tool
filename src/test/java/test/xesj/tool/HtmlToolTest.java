package test.xesj.tool;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static xesj.tool.HtmlTool.*;

/**
 * Html tool teszt
 */
public class HtmlToolTest {

  /**
   * Metódus teszt: escape()
   */
  @Test
  public void escapeTest() {

    assertNull(escape(null));
    assertEquals("", escape(""));
    assertEquals("árvíztűrő", escape("árvíztűrő"));
    assertEquals("a &amp; &quot; &lt; &gt; b &#39;", escape("a & \" < > b '"));

  }

  // ==== 
}
