package test.xesj.tool;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static xesj.tool.HttpTool.*;

/**
 * Http tool teszt
 */
public class HttpToolTest {

  /**
   * Metódus teszt: encodeURL()
   */
  @Test
  public void encodeURLTest() {

    assertNull(null);
    assertEquals("%2F", encodeURL("/"));
    assertEquals("%C3%A1rv%C3%ADzt%C5%B1r%C5%91", encodeURL("árvíztűrő"));

  }

  // ====
}
