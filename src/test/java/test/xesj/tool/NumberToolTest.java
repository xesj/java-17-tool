package test.xesj.tool;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import xesj.tool.LocaleTool;
import static xesj.tool.NumberTool.*;

/**
 * Number tool teszt
 */
public class NumberToolTest {

  /**
   * Metódus teszt: round()
   */
  @Test
  public void roundTest() {

    double deltaZero = 0.0;
    double d;
    Double D; 
    d = 1.0 / 3.0; // 0.333333333333
    assertEquals(0.33, round(d,2), deltaZero);
    assertEquals(0.0, round(d,0), deltaZero);
    d = 1.0 / 2.0; // 0.5
    assertEquals(1.0, round(d, 0), deltaZero);
    assertEquals(0.5, round(d, 1), deltaZero);
    D = Double.valueOf("0.987");
    assertEquals(1.0, round(D, 1), deltaZero);
    assertEquals(0.99, round(D, 2), deltaZero);

  }

  /**
   * Metódus teszt: format()
   * A replaceAll() metódus a 160-as karaktert space-re cseréli (160 = non breaking space), 
   * mert ez a karakter a magyar group separator.
   */
  @Test
  public void formatTest() {

    // +------------------------+
    // | Default: magyar locale |
    // +------------------------+

    Locale.setDefault(LocaleTool.LOCALE_HU);

    // null formázás
    assertNull(format(null, true, 2, false));

    // double formázás
    double x = 2045345634;
    assertEquals(
      format(x, true, 2, false).replaceAll("\u00A0", " "),
      "2 045 345 634"
    ); 

    // double formázás
    x = 0.25;
    assertEquals(format(x, false, 2, true), "0,25");

    // BigInteger formázás
    BigInteger bigi = new BigInteger("12345678900112233445566778899");
    assertEquals(
      format(bigi, false, 20, true).replaceAll("\u00A0", " "),
      "12345678900112233445566778899,00000000000000000000"
    ); 

    // BigInteger formázás
    assertEquals(
      format(bigi, true, 7, true).replaceAll("\u00A0", " "),
      "12 345 678 900 112 233 445 566 778 899,0000000"
    ); 

    // BigDecimal formázás
    BigDecimal bigd = new BigDecimal("12345678900112233445566778899.4321");
    assertEquals(
      format(bigd, false, 7, true).replaceAll("\u00A0", " "),
      "12345678900112233445566778899,4321000"
    ); 

    // BigDecimal formázás
    assertEquals(
      format(bigd, true, 2, false).replaceAll("\u00A0", " "),
      "12 345 678 900 112 233 445 566 778 899,43"
    ); 

    // long formázás
    long l = 222223333344444L;
    assertEquals(
      format(l, true, 2, false).replaceAll("\u00A0", " "),
      "222 223 333 344 444"
    ); 

    // long formázás
    assertEquals(
      format(l, true, 8, true).replaceAll("\u00A0", " "),
      "222 223 333 344 444,00000000"
    ); 

    // +-----------------------+
    // | Default: angol locale |
    // +-----------------------+

    Locale.setDefault(Locale.ENGLISH);
    assertEquals(
      format(l, true, 8, true),
      "222,223,333,344,444.00000000"
    ); 

    // long formázás, paraméterben megadott magyar locale segítségével
    assertEquals(
      format(l, true, 8, true, LocaleTool.LOCALE_HU).replaceAll("\u00A0", " "),
      "222 223 333 344 444,00000000"
    ); 

    // long formázás, paraméterben megadott német locale segítségével
    assertEquals(
      format(l, true, 8, true, Locale.GERMANY),
      "222.223.333.344.444,00000000"
    ); 

  }

  /**
   * Metódus teszt: toBigDecimal()
   */
  @Test
  public void toBigDecimalTest() {

    // Long -> BigDecimal konverzió
    assertNull(toBigDecimal((Long)null));
    long[] longArray = {0, -2345, Long.MIN_VALUE, Long.MAX_VALUE};
    for (long l: longArray) {
      assertEquals(l, toBigDecimal(l).longValue());
    }  

    // BigInteger -> BigDecimal konverzió
    assertNull(toBigDecimal((BigInteger)null));
    String[] stringArray = {"0", "1", "-2345", "111222333444555666777888999000", "-111222333444555666777888999000"};
    for (String s: stringArray) {
      assertEquals(s, toBigDecimal(new BigInteger(s)).toString());
    }

    // Double -> BigDecimal konverzió
    assertNull(toBigDecimal((Double)null));
    assertEquals(new BigDecimal("114237607567234.25"), toBigDecimal(114237607567234.25));
    assertEquals(new BigDecimal("-671760761567235.5"), toBigDecimal(-671760761567235.5));

  }

  /**
   * Metódus teszt: toBigInteger()
   */
  @Test
  public void toBigIntegerTest() {

    // Long -> BigInteger konverzió
    assertNull(toBigInteger((Long)null));
    long[] longArray = {0, 578, -1, -2345, Long.MIN_VALUE, Long.MAX_VALUE};
    for (long l: longArray) {
      assertEquals(Long.toString(l), toBigInteger(l).toString());
    }  

    // BigDecimal -> BigInteger konverzió
    assertNull(toBigInteger((BigDecimal)null));
    String[] stringArray = {
      "0", "1", "-2345", "111222333444555666777888999000", "-111222333444555666777888999000", "67.000", 
      "-987512351674523671256374672634575235.000"
    };
    for (String s: stringArray) {
      BigDecimal bigDecimal = new BigDecimal(s);
      String bigDecimalStr = bigDecimal.setScale(0).toString();
      assertEquals(bigDecimalStr, toBigInteger(bigDecimal).toString());
    }

  }

  /**
   * Metódus exception teszt: toBigInteger()
   */
  @Test
  public void toBigIntegerExceptionTest() {

    String[] stringArray = {
      "0.1", "-1.5", "-2345.48", "111222333444555666777888999000.0001", "-111222333444555666777888999000.88", 
      "-987512351674523671256374672634575235.9"
    };
    for (String s: stringArray) {
      try {
        toBigInteger(new BigDecimal(s));
        fail("Nem keletkezett ArithmeticException, ennél az értéknél: " + s);
      }
      catch (ArithmeticException ae) {
      }
    }

  }

  /**
   * Metódus teszt: toLong()
   */
  @Test
  public void toLongTest() {

    // BigInteger -> Long konverzió
    assertNull(toLong((BigInteger)null));
    String[] stringArray1 = {
      "0", "1", "-2345", "34560923834342", String.valueOf(Long.MIN_VALUE), String.valueOf(Long.MAX_VALUE)
    };
    for (String s: stringArray1) {
      BigInteger bigInteger = new BigInteger(s);
      assertEquals(Long.valueOf(s), toLong(bigInteger));
    }

    // BigDecimal -> Long konverzió
    assertNull(toLong((BigDecimal)null));
    String[] stringArray2 = {
      "0", "1", "-2345", "34560923834342", String.valueOf(Long.MIN_VALUE), String.valueOf(Long.MAX_VALUE)
    };
    for (String s: stringArray2) {
      Long lng = Long.valueOf(s);
      assertEquals(lng, toLong(new BigDecimal(s)));
      assertEquals(lng, toLong(new BigDecimal(s + ".0")));
      assertEquals(lng, toLong(new BigDecimal(s + ".00000000000000000000000000000000000000000000000000000000000000000")));
    }

  }
  /**
   * Metódus exception teszt: toLong()
   */
  @Test
  public void toLongExceptionTest() {

    // BigInteger -> Long konverzió
    String[] stringArray1 = {
      "111222333444555666777888999000", "-111222333444555666777888999000", "-9223372036854775809", "9223372036854775808"
    };
    for (String s: stringArray1) {
      try {
        toLong(new BigInteger(s));
        fail("Nem keletkezett ArithmeticException, ennél az értéknél: " + s);
      }
      catch (ArithmeticException ae) {
      }
    }

    // BigDecimal -> Long konverzió
    String[] stringArray2 = {
      "0.1", "-22.34",
      "111222333444555666777888999000.5", "-111222333444555666777888999000.000000000000000000000000000000000000001", 
      "-9223372036854775809", "9223372036854775808"
    };
    for (String s: stringArray2) {
      try {
        toLong(new BigDecimal(s));
        fail("Nem keletkezett ArithmeticException, ennél az értéknél: " + s);
      }
      catch (ArithmeticException ae) {
      }
    }

  }

  /**
   * Metódus teszt: toDouble()
   */
  @Test
  public void toDoubleTest() {

    // BigDecimal -> Double konverzió
    assertNull(toDouble((BigDecimal)null));
    assertEquals(1423760761256723465.25, toDouble(new BigDecimal("1423760761256723465.25")));
    assertEquals(-671423760761256723465.5, toDouble(new BigDecimal("-671423760761256723465.5")));

  }

  // ====
}
