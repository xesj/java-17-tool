package test.xesj.tool;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import test.xesj.TestInit;
import xesj.tool.RandomTool;
import static xesj.tool.RandomTool.*;

/**
 * Random tool teszt
 */
public class RandomToolTest {

  /**
   * Ismétlések száma.
   */
  private static final int REPEAT = 100;

  /**
   * Logger
   */
  private static final Logger logger = TestInit.getInstance().getLogger(RandomToolTest.class, "random-tool-test-log.txt");

  /**
   * Metódus teszt: interval()
   */
  @Test
  public void intervalTest() {

    int[] cases = {
      1, 3,  
      -700, -100,  
      3, 3,  
      0, 0,  
      -53, -53,  
      -13000, 5,  
      -50, 50, 
      -2, 90, 
      0, Integer.MAX_VALUE,
      Integer.MAX_VALUE - 5, Integer.MAX_VALUE,  
      Integer.MIN_VALUE, Integer.MIN_VALUE + 3,
      Integer.MIN_VALUE, Integer.MAX_VALUE
    };
    // Ellenőrzés: a tartományba esik-e, és egyenletes-e az eloszlása
    logger.info("\ninterval():");
    logger.info("-----------");
    for (int index = 0; index < cases.length; index += 2) {
      
      Map<Integer, Integer> dispersalMap = new HashMap<>();
      int min = cases[index], max = cases[index+1];
      for (int repeat = 0; repeat < REPEAT; repeat++) {
        int rnd = interval(min, max);
        assertTrue(min <= rnd && rnd <= max);
        // Eloszlás gyűjtés 
        if (dispersalMap.containsKey(rnd)) {
          dispersalMap.put(rnd, dispersalMap.get(rnd) + 1);
        }
        else {
          dispersalMap.put(rnd, 1);
        }
      } 
      // Eloszlás kiírás
      logger.info("Eloszlás: " + min + " ... " + max);
      Set<Integer> dispersalKeySet = dispersalMap.keySet();
      List<Integer> dispersalKeyList = new ArrayList<>(dispersalKeySet); 
      Collections.sort(dispersalKeyList);
      for (int random: dispersalKeyList) {
        logger.info(random + " => " + (100.0 * dispersalMap.get(random) / REPEAT) + "%");
      }
      logger.info("");

    } // End for

  }

  /**
   * Metódus teszt: letters()
   */
  @Test
  public void lettersTest() {    

    logger.info("\nletters(9):");
    logger.info("---------");
    for (int i = 0; i < 5; i++) logger.info(letters(9));

  }

  /**
   * Metódus teszt: digits()
   */
  @Test
  public void digitsTest() {    

    logger.info("\ndigits(22):");
    logger.info("----------------------");
    for (int i = 0; i < 5; i++) logger.info(digits(22));

  }

  /**
   * Metódus teszt: allCharacters()
   */
  @Test
  public void allCharactersTest() {    

    logger.info("\nallCharacters(25):");
    logger.info("-------------------------");
    for (int i = 0; i < 5; i++) logger.info(allCharacters(25));

  }

  /**
   * Metódus teszt: fromCharacters()
   */
  @Test
  public void fromCharactersTest() {    

    logger.info("\nfromCharacters(11):");
    logger.info("-----------");
    for (int i = 0; i < 5; i++) logger.info(fromCharacters("őÍ", 11));

    // length = 0 eset
    assertEquals("", fromCharacters("%/F+", 0));
    assertEquals("", fromCharacters(RandomTool.SPECIAL_CHARACTERS, 0));

  }

  /**
   * Metódus teszt: logic()
   */
  @Test
  public void logicTest() {    

    int logicTrue = 0, logicFalse = 0;
    for (int i = 0; i < REPEAT; i++) {
      if (logic()) logicTrue++;
      else logicFalse++;
    }
    logger.info("\nlogic():");
    logger.info("--------");
    logger.info("Eloszlás: true/false");
    logger.info("true  => " + (100.0 * logicTrue / REPEAT) + "%");
    logger.info("false => " + (100.0 * logicFalse / REPEAT) + "%");

  }

  /**
   * Metódus exception teszt: fromCharacters()
   * A string null.
   */
  @Test
  public void fromCharactersExceptionTest1() {

    assertThrows(
      RuntimeException.class,
      () -> { fromCharacters(null, 5); }
    );

  }

  /**
   * Metódus exception teszt: fromCharacters()
   * A string üres.
   */
  @Test
  public void fromCharactersExceptionTest2() {

    assertThrows(
      RuntimeException.class,
      () -> { fromCharacters("", 5); }
    );

  }
  
  /**
   * Metódus exception teszt: fromCharacters()
   * A hossz kisebb mint 0.
   */
  @Test
  public void fromCharactersExceptionTest3() {

    assertThrows(
      RuntimeException.class,
      () -> { fromCharacters("ABC", -1); }
    );

  }

  // ====
}
