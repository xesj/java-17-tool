package test.xesj.tool;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import test.xesj.TestInit;
import xesj.tool.FileTool;
import static xesj.tool.FileTool.tmpFile;
import xesj.tool.StreamTool;
import static xesj.tool.StreamTool.*;
import xesj.tool.StringTool;
import static xesj.tool.FileTool.contentEqual;

/**
 * Stream tool teszt
 */
public class StreamToolTest {

  /**
   * Logger
   */
  private static final Logger logger = TestInit.getInstance().getLogger(StreamToolTest.class, "stream-tool-test-log.txt");

  /**
   * Metódus teszt: copy() 
   */
  @Test
  public void copyTest() throws URISyntaxException, FileNotFoundException, IOException {

    int[] bufferSizes = new int[]{1, 456, 1024, 123456};
    String[] fileNames = new String[]{"adatmodell.jpg", "ures.xy", "auto.jpg"};
    for (int bufferSize: bufferSizes) {
      for (String fileName: fileNames) {

        // input fájl
        File inputFile = new File(this.getClass().getResource(fileName).toURI());
        FileInputStream inputStream = new FileInputStream(inputFile);

        // output fájl
        File outputFile = new File(FileTool.tmpDirName() + fileName + "_copy_" + bufferSize);
        FileOutputStream outputStream = new FileOutputStream(outputFile);

        // másolás
        copy(inputStream, outputStream, bufferSize);

        // másolás ellenőrzése
        if (!FileTool.contentEqual(inputFile, outputFile)) {
          fail("A fájl méretek nem egyeznek: " + outputFile);
        }
      }  
    }   

  }

  /**
   * Metódus teszt: readString(), writeString() 
   */
  @Test
  public void readStringAndWriteStringTest() throws IOException {

    String uresString = "";
    String rovidString = "Első sor\nmásodik sor\nharmadik sor, árvíztűrő tükörfúrógép + ÁRVÍZTŰRŐ TÜKÖRFÚRÓGÉP";
    String hosszuString = StringTool.multiply("őű3456789\n", 1000);
    String charsetUTF8 = "UTF-8";
    String charsetISO2 = "ISO-8859-2";

    // uresString kiírása + beolvasása   
    File file = tmpFile("uresString.txt");
    logger.info("readStringAndWriteStringTest() -> " + file);
    try (FileOutputStream fos = new FileOutputStream(file)) {
      StreamTool.writeString(uresString, fos, charsetUTF8);
    }
    try (FileInputStream fis = new FileInputStream(file)) {
      String result = StreamTool.readString(fis, charsetUTF8, 13);
      assertEquals(uresString, result);
    }    
    assertEquals(0, file.length());

    // rovidString kiírása + beolvasása
    file = tmpFile("rovidString");
    logger.info("readStringAndWriteStringTest() -> " + file);
    try (FileOutputStream fos = new FileOutputStream(file)) {
      StreamTool.writeString(rovidString, fos, charsetISO2);
    }
    try (FileInputStream fis = new FileInputStream(file)) {
      String result = StreamTool.readString(fis, charsetISO2, null);
      assertEquals(rovidString, result);
    }    

    // hosszuString kiírása + beolvasása
    file = tmpFile("hosszuString");
    logger.info("readStringAndWriteStringTest() -> " + file);
    try (FileOutputStream fos = new FileOutputStream(file)) {
      StreamTool.writeString(hosszuString, fos, charsetUTF8);
    }
    try (FileInputStream fis = new FileInputStream(file)) {
      String result = StreamTool.readString(fis, charsetUTF8, 45);
      assertEquals(hosszuString, result);
    }    

    // null string esetén üres fájl jön létre ?
    file = tmpFile("nullString");
    logger.info("readStringAndWriteStringTest() -> " + file);
    try (FileOutputStream fos = new FileOutputStream(file)) {
      StreamTool.writeString(null, fos, charsetUTF8);
    }
    assertEquals(0, file.length());

    // két különböző karakterterkódolással írt fájl nem egyező tartalmú ?
    File file1 = tmpFile("utf8.txt");
    File file2 = tmpFile("iso2.txt");
    try (FileOutputStream fos = new FileOutputStream(file1)) {
      StreamTool.writeString(rovidString, fos, charsetUTF8);
    }
    try (FileOutputStream fos = new FileOutputStream(file2)) {
      StreamTool.writeString(rovidString, fos, charsetISO2);
    }
    assertFalse(contentEqual(file1, file2));

  }

  // ====
}
