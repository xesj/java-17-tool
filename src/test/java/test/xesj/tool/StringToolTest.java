package test.xesj.tool;
import java.awt.Color;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static xesj.tool.StringTool.*;
import xesj.tool.StringTool;

/**
 * String tool teszt
 */
public class StringToolTest {

  /**
   * Metódus teszt: smartTrim()
   */
  @Test
  public void smartTrimTest() {

    // Segédlet:
    // ________________________1111111111
    // _______________1234567890123456789                 
    final String s = "egy kettő három négy öt hat hét";
    assertNull(smartTrim(null, 9));
    assertEquals("...", smartTrim(s, 0));
    assertEquals("e...", smartTrim(s, 1));
    assertEquals("eg...", smartTrim(s, 2));
    assertEquals("egy...", smartTrim(s, 3));
    assertEquals("egy...", smartTrim(s, 4));
    assertEquals("egy...", smartTrim(s, 5));
    assertEquals("egy...", smartTrim(s, 6));
    assertEquals("egy...", smartTrim(s, 7));
    assertEquals("egy...", smartTrim(s, 8));
    assertEquals("egy kettő...", smartTrim(s, 9));
    assertEquals("egy kettő...", smartTrim(s, 10));
    assertEquals("egy kettő...", smartTrim(s, 11));

  }

  /**
   * Metódus teszt: multiply()
   */
  @Test
  public void multiplyTest() {

    assertNull(multiply(null, 54));
    assertEquals(multiply("", 3), "");
    assertEquals(multiply("asd", -3), "");
    assertEquals(multiply("asd", 3), "asdasdasd");
    assertEquals(multiply("&nbsp; ", 4), "&nbsp; &nbsp; &nbsp; &nbsp; ");

  }

  /**
   * Metódus teszt: space()
   */
  @Test
  public void spaceTest() {    

    assertEquals("", space(0));
    assertEquals("", space(-3));
    assertEquals("   ", space(3));

  }

  /**
   * Metódus teszt: str()
   */
  @Test
  public void strTest() {    

    assertNull(str(null));
    assertNull(str(null, null));
    assertEquals("", str(null, ""));
    assertEquals("--", str(null, "--"));

    assertEquals("-59", str(-59));
    assertEquals("-59", str(-59, null));
    assertEquals("-59", str(-59, "??"));
    assertEquals("false", str(false));
    assertEquals("false", str(false, "?"));

  }

  /**
   * Metódus teszt: strExt()
   */
  @Test
  public void strExtTest() {    

    assertNull(strExt("  ", "előtte", "utána", null));
    assertNull(strExt(null, "előtte", "utána", null));
    assertEquals("n.a.", strExt("\t  \r\n ", "előtte", "utána", "n.a."));
    assertEquals("", strExt(null, "előtte", "utána", ""));
    assertEquals("380 Ft", strExt(380, null, " Ft", null));
    assertEquals("380 Ft", strExt(380, null, " Ft", "-"));
    assertEquals("Ára: 380 Ft", strExt(380, "Ára: ", " Ft", "-"));
    assertEquals("Ára: 380 Ft", strExt(380, "Ára: ", " Ft", "-"));
    assertEquals("Eredmény: 78.3%", strExt(78.3, "Eredmény: ", "%", "-"));

  }

  /**
   * Metódus teszt: md5hash()
   */
  @Test
  public void md5hashTest() {    

    assertNull(md5hash(null));
    String[] jelszavak = {"árvíztűrő", "titok", "", "éá57bvsG6!+=C6HgFöüó|kskjhcbnisanuierwomnxciwonusiu", "198546"};
    for (String jelszo: jelszavak) {
      assertEquals(md5hash(jelszo), md5hashTesztelo(jelszo));
    }

  }

  /**
   * Metódus teszt: isNullOrEmpty()
   */
  @Test
  public void isNullOrEmptyTest() {    

    assertTrue(isNullOrEmpty(null));
    assertTrue(isNullOrEmpty(""));
    assertFalse(isNullOrEmpty(" "));
    assertFalse(isNullOrEmpty("abc"));

  }

  /**
   * Metódus teszt: isNullOrTrimEmpty()
   */
  @Test
  public void isNullOrTrimEmptyTest() {    

    assertTrue(isNullOrTrimEmpty(null));
    assertTrue(isNullOrTrimEmpty(""));
    assertTrue(isNullOrTrimEmpty(" "));
    assertTrue(isNullOrTrimEmpty(" \r \n \t "));
    assertFalse(isNullOrTrimEmpty("."));
    assertFalse(isNullOrTrimEmpty("/"));

  }

  /**
   * Metódus teszt: equals()
   */
  @Test
  public void equalsTest() {    

    assertTrue(StringTool.equal(null, null));
    assertTrue(StringTool.equal("-23", -23));
    assertTrue(StringTool.equal("", "" + ""));
    assertTrue(StringTool.equal("éáű", "éá" + "ű"));
    assertFalse(StringTool.equal(null, "null"));
    assertFalse(StringTool.equal("null", null));
    assertFalse(StringTool.equal(-3, "-4"));
    assertFalse(StringTool.equal("", " "));
    assertFalse(StringTool.equal("\t", " "));

  }

  /**
   * Metódus teszt: in()
   */
  @Test
  public void inTest() {    

    // false esetek
    assertFalse(in(7));
    assertFalse(in(null));
    assertFalse(in(null, "", 2, true));
    assertFalse(in(5, "", -2, true, null, false));
    assertFalse(in(3, 0, 1, 2, 4, 5, 6));
    assertFalse(in("Cápa", "macska", "delfin", "harkály", "cápa", "denevér"));
    assertFalse(in("null", (Object)null));
    assertFalse(in(Color.GREEN, Color.RED, Color.GRAY, Color.BLUE));
    assertFalse(in(true, false));
    assertFalse(in(false, true, true));
    // true esetek
    assertTrue(in(5, -67, 5L));
    assertTrue(in("cápa", null, "macska", "delfin", "harkály", "cápa", "denevér"));
    assertTrue(in(null, (Object)null));
    assertTrue(in(null, null, null));
    assertTrue(in(Color.GREEN, Color.RED, Color.GREEN, Color.BLUE));
    assertTrue(in(false, false));
    assertTrue(in(true, false, true, true));
    assertTrue(in(7 * 8, null, 50 + 6));

  }

  /**
   * Egy másik eljárás az md5 alakítására, mellyel összevetjük a valódi eljárás eredményét.
   */
  private static String md5hashTesztelo(String be) {

    try {
      StringBuilder sb = new StringBuilder();
      MessageDigest md = MessageDigest.getInstance("MD5");
      md.update(be.getBytes("UTF-8"));
      byte[] coded = md.digest();
      for (int i = 0; i < coded.length; i++) {
        byte b = coded[i];
        String s = Integer.toHexString(b < 0 ? (256 + b) : b);
        if (s.length() == 1) sb.append("0");
        sb.append(s);
      }
      return sb.toString();
    }
    catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }

  }

  // ====
}
