package test.xesj.xml;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Document;
import test.xesj.TestInit;
import xesj.xml.Tree;
import xesj.xml.LineRule;
import xesj.xml.TreeCheckException;
import xesj.xml.XmlTool;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * XML teszt
 */
public class XmlTest {

  /**
   * Kiíráshoz fix szöveg.
   */
  private static final String FILE_OUT = "A fájl írás megtörtént: ";

  /**
   * Ellenőrzéshez használt tree.
   */
  private static Tree checkmeTree;

  /**
   * Logger
   */
  private static final Logger logger = TestInit.getInstance().getLogger(XmlTest.class, "xml-test-log.txt");

  /**
   * BeforeAll
   */
  @BeforeAll
  public static void beforeAll() throws Exception {

    URL url = XmlTest.class.getResource("checkme.xml");
    checkmeTree = new Tree(url);

  }

  /**
   * Output fájl előállítása a fájl név alapján a temporális könyvtárba
   * @param fileName Fájl név.
   */
  private static String outFile(String fileName) { 

    return TestInit.getInstance().getTmpDirectory() + File.separatorChar + fileName;

  }  

  /**
   * Metódus teszt: XmlTool.escape()
   */
  @Test
  public void escapeTest() {

    assertNull(XmlTool.escape(null));
    assertEquals("", XmlTool.escape("")); 
    assertEquals("árvíztűrő", XmlTool.escape("árvíztűrő"));
    assertEquals("a &amp; &quot; &lt; &gt; b &#39;", XmlTool.escape("a & \" < > b '"));

  }

  /**
   * Teszt: XML beolvasása
   */
  @Test
  public void xmlBeolvasasTest() throws Exception {

    InputStream is;
    URL url;
    Tree tree;
    String outFile;

    // szemelyek.xml (InputStream-ről olvasás)
    is = this.getClass().getResourceAsStream("szemelyek.xml");
    tree = new Tree(is);
    logger.info("----- szemelyek.xml begin -----");
    logger.info(tree.toString());
    logger.info("----- szemelyek.xml end -----");

    // tanusitvany.xml (URL-ről olvasás)
    url = this.getClass().getResource("tanusitvany.xml");
    tree = new Tree(url);
    assertEquals(
      "Lakó- és szállásjellegű", 
      tree.root().child("tanusitvany").child("epulet").child("epulet_rendeltetese").text()
    );
    assertEquals(
      "Vérmező u.4.", 
      tree.root().child("tanusitvany").child("megrendelo").child("cim").child("utca_hsz").text()
    );
    assertEquals(
      "tanusitvany.pdf", 
      tree.root().child("tanusitvany").child("pdf_tanusitvany").attr("fajlnev")
    );

    // soap.xml (URL-ről olvasás)
    url = this.getClass().getResource("soap.xml");
    tree = new Tree(url);
    assertTrue(tree.root().isAttr("xmlns:soap12", "http://schemas.xmlsoap.org/wsdl/soap12/"));
    assertFalse(tree.root().isAttr("xmlns:soap12", "http://schemas.xmlsoap.org/wsdl/soap12"));
    assertEquals(
      "https://teszt-alk.etdr.gov.hu/Services/VatiHandler.svc",
      tree.root().child("wsdl:service").child("wsdl:port").child("wsa10:EndpointReference").child("wsa10:Address").text()
    );  
    assertEquals(10, tree.root().children("wsdl:message").size());
    assertEquals(1, tree.root().children("wsdl:message", "name", "OEENYResponse").size());
    assertEquals(10, tree.root().children("wsdl:message", "name", null).size());
    assertFalse(tree.root().child("wsdl").isReal());
    assertEquals(4, tree.root().child("wsdl:types").child().children().size());

    // File-ból olvasás
    tree = new Tree(new File(this.getClass().getResource("szemelyek.xml").toURI()));
    outFile = outFile("file_bol_szemelyek.xml");
    tree.write(new File(outFile));
    logger.info(FILE_OUT + outFile);

    // Document-ből olvasás
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document = builder.parse(this.getClass().getResourceAsStream("szemelyek.xml"));
    tree = new Tree(document);
    outFile = outFile("document_bol_szemelyek.xml");
    tree.write(new File(outFile));
    logger.info(FILE_OUT + outFile);

  }

  /**
   * Teszt: XML kiírása
   */
  @Test
  public void xmlKiirasTest() throws Exception {

    InputStream is = this.getClass().getResourceAsStream("szemelyek.xml");
    Tree tree = new Tree(is);

    // kiírás File-ba
    String outFile = outFile("szemelyek_fajlba.xml");
    tree.write(new File(outFile));
    logger.info(FILE_OUT + outFile);

    // kiírás OutputStream-be
    outFile = outFile("szemelyek_outputstream_be.xml");
    tree.write(new FileOutputStream(outFile));
    logger.info(FILE_OUT + outFile);

    // kiírás Writer-be    
    StringWriter sw = new StringWriter();
    tree.write(sw);
    logger.info("A Writer-be kiírás megtörtént");

  }

  /**
   * Teszt: XML olvasó metódusok
   */
  @Test
  public void xmlOlvasoMetodusokTest() throws Exception {

    InputStream is = this.getClass().getResourceAsStream("szemelyek.xml");
    Tree tree = new Tree(is);

    // Nem valós ág tesztelése
    assertFalse(tree.root().parent().isReal());
    assertFalse(tree.root().parent().parent().child().isReal());
    assertFalse(tree.root().child("nincs").isReal());
    assertFalse(tree.root().child("keszult").isReal());
    assertFalse(tree.root().child("szemely", "id", "13").isReal());
    assertFalse(tree.root().child("szemely", "xx", null).isReal());
    assertFalse(tree.root().child("szemely", "xx", null).parent().parent().isReal());

    // Tree.root(), Line.parent(), Line.child(), Line.children()
    assertEquals(tree.root().child(), tree.root().child(3).parent().child("h:keszult"));
    assertEquals(tree.root().child(), tree.root().child("xmlns:h","http://www.w3.org/TR/html4/"));
    assertEquals(tree.root().child(3).child(2), tree.root().child("szemely", "id", "20").child("szuletett"));
    assertEquals(tree.root().child(3).child(2), tree.root().child("szemely", "kod", null).child("szuletett"));
    assertEquals(tree.root().child(3).child(2), tree.root().child("szemely", "kod", "őű").child("szuletett"));
    assertEquals(tree.root().child(2).child(2), tree.root().child("szemely", null, "őű").child("szuletett"));
    assertEquals(tree.root().children().get(2), tree.root().child("szemely", "kod", "őű", "furcsa", null));
    assertEquals(0, tree.root().children("aa").size());
    assertEquals(2, tree.root().children("szemely").size());
    assertEquals(1, tree.root().children("szemely", "furcsa", null, "kod", "őű").size());
    assertEquals(0, tree.root().parent().children().size());
    assertEquals(tree.root().child((String) null), tree.root().children((String) null).get(0));
    assertEquals(tree.root().child(null, null), tree.root().children((String[]) null).get(0));

    // Tree.jump(), Line.mark() 
    tree.root().child().mark("k").parent().child(3).mark(99);
    assertEquals(tree.jump("k"), tree.root().child(1));
    assertEquals(tree.jump(99), tree.root().child("szemely", "id", "20"));
    assertFalse(tree.jump(7).isReal());
    assertTrue(tree.jump(99).isReal());
    tree.clearMarks(); // jelzések törlése
    assertFalse(tree.jump(99).isReal());

    // Line.attr(), Line.attrs(), Line.isAttr()
    tree.root().child(3).mark(0);
    assertNull(tree.jump(0).attr(null));
    assertNull(tree.jump(0).attr("aa"));
    assertEquals("őű", tree.jump(0).attr("kod"));
    assertEquals("őű", tree.jump(0).attrs().get("kod"));
    assertTrue(tree.jump(0).isAttr("kod"));
    assertTrue(tree.jump(0).isAttr("kod", "őű"));
    assertFalse(tree.jump(0).isAttr("www"));
    assertFalse(tree.jump(0).isAttr("kod", "yy"));
    assertFalse(tree.jump(0).isAttr("kod", null));
    assertFalse(tree.jump(0).isAttr(null, "őű"));
    assertFalse(tree.jump(0).isAttr(null, null));
    assertEquals("http://www.w3.org/TR/html4/", tree.root().child().attr("xmlns:h"));

    // Line.isReal()
    assertTrue(tree.jump(0).isReal());
    assertFalse(tree.jump(100).isReal());

    // Line.name()
    assertEquals("gyoker", tree.root().name());
    assertEquals("h:keszult", tree.root().child().name());

    // Line.text()
    assertEquals("2013.07.12", tree.root().child().text());
    assertEquals("Árvíztűrő Tükörfúrógép", tree.root().child("szemely", "id", "20").child("nev").text());
    assertNull(tree.root().parent().text());

    // Line.toString()
    assertEquals("<szemely id=\"10\">", tree.root().child("szemely").toString());
    assertEquals("<>", tree.root().parent().toString());
    assertEquals("<nev>", tree.root().child("szemely", "id", "20").child("nev").toString());

  }

  /**
   * Teszt: XML író metódusok teszt.
   */
  @Test
  public void xmlIroMetodusokTest() throws Exception {

    Tree tree = new Tree("gyoker", "gyid", "10", "szin", "piros");

    // Line.insertChild(), Line.insertAfter(), Line.update(), Line.delete(), Line.text(...)
    tree.root().text("gyökér szövege");
    tree.root()
      .insertChild("kulso")
      .insertChild("belso1", "id", "rossz", "torlendo", "törlendő").text("ez a belso1 text-je")
      .insertAfter("belso2").text("ez a belso2 text-je")
      .insertAfter("belso3").text("ez a belso3 text-je");
    assertNull(tree.root().text());
    assertEquals("rossz", tree.root().child().child().attr("id"));
    tree.root().child().child().update("id", "jo", "uj", "brandnew", "torlendo", null);
    assertEquals("jo", tree.root().child().child().attr("id"));

    // Line.delete()
    tree.root().insertChild("torlendo1").insertChild("torlendo2").insertChild("torlendo3");
    tree.root().child("torlendo1").delete();
    assertEquals(1, tree.root().children().size());
    String outFile = outFile("modositott.xml");
    tree.write(new FileOutputStream(outFile));
    logger.info(FILE_OUT + outFile);

  }  

  /**
   * Konstruktor exception teszt.
   * Hiba: nincs főág megadva.
   */
  @Test  
  public void constructorExceptionTest1() throws ParserConfigurationException {

    assertThrows(
      RuntimeException.class,
      () -> { new Tree(); }
    );

  }    

  /**
   * Konstruktor exception teszt.
   * Hiba: nincs főág megadva.
   */
  @Test  
  public void constructorExceptionTest2() throws ParserConfigurationException {

    assertThrows(
      RuntimeException.class,
      () -> { new Tree(new String[]{}); }
    );

  }    

  /**
   * Konstruktor exception teszt.
   * Hiba: a főág neve null.
   */
  @Test  
  public void constructorExceptionTest3() throws ParserConfigurationException {

    assertThrows(
      RuntimeException.class,
      () -> { new Tree(new String[]{null}); }
    );

  }    

  /**
   * Konstruktor exception teszt.
   * Hiba: nem páratlan számú paraméter van megadva.
   */
  @Test  
  public void constructorExceptionTest4() throws ParserConfigurationException {

    assertThrows(
      RuntimeException.class,
      () -> { new Tree("gyoker", "id"); }
    );

  }    

  /**
   * Konstruktor exception teszt.
   * Hiba: a szin attribútumnak nincs értéke.
   */
  @Test  
  public void constructorExceptionTest5() throws ParserConfigurationException {

    assertThrows(
      RuntimeException.class,
      () -> { new Tree("gyoker", "id", "10", "szin", null); }
    );

  }    

  /**
   * Metódus exception teszt: insertChild()
   * Hiba: nem valós ághoz nem lehet beszúrni.
   */
  @Test  
  public void insertChildExceptionTest() throws ParserConfigurationException {

    assertThrows(
      RuntimeException.class,
      () -> {
        Tree tree = new Tree("gyoker");
        tree.root().parent().insertChild("allat", "nev", "bodri");
      }
    );  

  }    

  /**
   * Metódus exception teszt: insertAfter()
   * Hiba: főágon nem hajtható végre insertAfter().
   */
  @Test  
  public void insertAfterExceptionTest1() throws ParserConfigurationException {

    assertThrows(
      RuntimeException.class,
      () -> {
        Tree tree = new Tree("gyoker");
        tree.root().insertAfter("xxx");
      }
    );

  }    

  /**
   * Metódus exception teszt: insertAfter()
   * Hiba: nem valós ágon nem hajtható végre insertAfter().
   */
  @Test  
  public void insertAfterExceptionTest2() throws ParserConfigurationException {

    assertThrows(
      RuntimeException.class,
      () -> {
        Tree tree = new Tree("gyoker");
        tree.root().parent().insertAfter("xxx");
      }
    );

  }    

  /**
   * Metódus exception teszt: delete()
   * Hiba: főág nem törölhető.
   */
  @Test  
  public void deleteExceptionTest1() throws ParserConfigurationException {

    assertThrows(
      RuntimeException.class,
      () -> {
        Tree tree = new Tree("gyoker");
        tree.root().delete();
      }
    );

  }    
  /**
   * Metódus exception teszt: delete()
   * Hiba: csak valós ág törölhető.
   */
  @Test  
  public void deleteExceptionTest2() throws ParserConfigurationException {

    assertThrows(
      RuntimeException.class,
      () -> {
        Tree tree = new Tree("gyoker");
        tree.root().parent().delete();
      }
    );

  }    

  /**
   * Teszt: Óriás XML készítése. 
   * kb. 100.000 ágból álló 5 Mbyte-os xml, a végén egy óriási text tartalom
   */
  @Test
  public void oriasFaTest() throws Exception {

    Tree tree = new Tree("big");

    // ágak létrehozása
    for (int i = 1; i <= 10000; i++) {
      tree.root().insertChild("kulso_" + i).mark(0);
      for (int j = 1; j <= 10; j++) {
        tree.jump(0).insertChild("belso_" + j).mark(0);
      }
      tree.jump(0).text("Big XML árvíztűrő tükörfúrógép " + i);
    }

    // óriás szöveg beszúrása
    StringBuilder bigstr = new StringBuilder();
    for (int i = 1; i < 1000; i++) {
      bigstr.append(
        "<absvdio768e9dhjeakdhew7hdajklhvcjksdhjaksdhdjakhdwqejk chjsdkl " +
          "hdjkasui!%/=+!%/289374ö92ö238477283e678363782678nxjwüöűasűcá>\n"
      );
    }
    tree.root().insertChild("sokadat").text(bigstr.toString());
    File file = new File(outFile("orias.xml"));
    tree.write(file);
    logger.info(FILE_OUT + file);

  }

  /**
   * Metódus teszt: Tree.check()
   */
  @Test
  public void checkTest() throws Exception {

    // helyes tree
    List<LineRule> rules = Arrays.asList(
      new LineRule("/a", 2, 2), 
      new LineRule("/b", 1, 1),
      new LineRule("/ce", 1, 1),
      new LineRule("/ce/de", 3, 3)
    );
    checkmeTree.check("gyoker", rules, true);

    // helyes tree: '/a' és '/b' ágakon kívül más is lehet
    checkmeTree.check("gyoker", Arrays.asList(new LineRule("/a", 1, null), new LineRule("/b", 1, null)), false);

  }

  /**
   * Metódus exception teszt: Tree.check()
   * Hiba: a gyökér ág neve hibás.
   */
  @Test
  public void checkExceptionTest1() throws TreeCheckException {  

    TreeCheckException e = assertThrows(
      TreeCheckException.class,
      () -> { checkmeTree.check("jolenne", null, true); }
    );
    assertEquals("A főág neve nem <jolenne>.", e.getMessage());

  }

  /**
   * Metódus exception teszt: Tree.check()
   * Hiba: hiányzik a '/x' ág
   */
  @Test
  public void checkExceptionTest2() throws TreeCheckException {  

    TreeCheckException e = assertThrows(
      TreeCheckException.class,
      () -> { checkmeTree.check("gyoker", Arrays.asList(new LineRule("/x", 1, null)), false); }
    );
    assertEquals("A(z) '/x' ág megengedett legkevesebb előfordulása (1) nem teljesül.", e.getMessage());

  }

  /**
   * Metódus exception teszt: Tree.check()
   * Hiba: hiányzik a '/ce/x' ág
   */
  @Test
  public void checkExceptionTest3() throws TreeCheckException {  

    TreeCheckException e = assertThrows(
      TreeCheckException.class,
      () -> { checkmeTree.check("gyoker", Arrays.asList(new LineRule("/ce/x", 1, null)), false); }
    );
    assertEquals("A(z) '/ce/x' ág megengedett legkevesebb előfordulása (1) nem teljesül.", e.getMessage());

  }    

  /**
   * Metódus exception teszt: Tree.check()
   * Hiba: túl sok a '/a' ág
   */
  @Test
  public void checkExceptionTest4() throws TreeCheckException {  

    TreeCheckException e = assertThrows(
      TreeCheckException.class,
      () -> { checkmeTree.check("gyoker", Arrays.asList(new LineRule("/a", null, 1)), false); }
    );
    assertEquals("A(z) '/a' ág megengedett legtöbb előfordulása (1) nem teljesül.", e.getMessage());

  }    

  /**
   * Metódus exception teszt: Tree.check()
   * Hiba: '/a' és '/b' ágakon kívül nem lehet más a gyökérben
   */
  @Test
  public void checkExceptionTest5() throws TreeCheckException {  

    TreeCheckException e = assertThrows(
      TreeCheckException.class,
      () -> { 
        checkmeTree.check("gyoker", Arrays.asList(new LineRule("/a", null, null), new LineRule("/b", null, null)), true); 
      }
    );
    assertEquals("A(z) '/ce/de' ág nem fordulhat elő.", e.getMessage());

  }

  /**
   * Teszt: string-re alakítás.
   */
  @Test
  public void toStringTest() throws ParserConfigurationException, TransformerException {

    Tree tree = new Tree("gyoker");
    tree.root().insertChild("elso", "id", "1");
    tree.root().insertChild("masodik", "a", "10", "b", "20");
    tree.root().child("masodik").insertChild("legbelso");
    logger.info("TREE1:\n" + tree);
    logger.info("TREE2:\n" + tree);
    logger.info("TREE3:\n" + tree);

  }

  // ====
}