--encoding: UTF-8

--id-prefix: pre1.

--id: első


SELECT 
  id,                       --block: c
  felnev,                   --block: c
  'árvíztűrő tükörfúrógép' jelszo                    --block: c
FROM al_szemely
WHERE true 

  AND id > 0                --block: where1
  ;
  
--id: elsődleges


SELECT     
  upper(felnev)             --block: columns
FROM al_szemely
WHERE true 
  AND id > 0                --block: where1
  AND felnev ILIKE 'a%'
ORDER BY 1;  

--id-prefix: pre2:

SELECT 'EZ SEM JELENHET MEG';

--id: második
SELECT     
  upper(felnev)             --block: columns
FROM tábla
WHERE true 
  AND id > 0                --block: where1
  AND felnev ILIKE 'a%'
  AND 
   --block-begin: where2
    (a > 12
    OR
    b < 76)
   --block-end: where2
ORDER BY sorrend;  

--id:  
select 'EZ NEM JELENHET MEG'; 


