--encoding: UTF-8

--id: id1

SELECT ... --block-begin:                    b      
FROM ...   
WHERE ...  
ORDER BY   --block-end: b 

--id: id2

SELECT ... --block-begin: cc
FROM ...   --block-end: CC

