--encoding: UTF-8

--id: id1

SELECT ... --block-begin: b
FROM ...   
WHERE ...  --block: b
ORDER BY   --block-end: b

--id: id2

SELECT ...
FROM ... 

