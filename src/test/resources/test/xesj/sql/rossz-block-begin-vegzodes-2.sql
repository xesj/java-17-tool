--encoding: UTF-8

--id: id1

SELECT ... --block-begin: b
FROM ... 
WHERE ...  --block-end: b

--id: id2

SELECT ... 
FROM ... 

--id: id3

SELECT ... --block-begin: b
FROM ...