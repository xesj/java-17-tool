--coding: UTF-8

--id-base: teszt.

--id: részletek
SELECT 
  id,                       --row: c
  felnev,                   --row: c
  jelszo                    --row: c
FROM al_szemely
WHERE true 
  AND id > 0;               --row: where1
  
--id: nagybetűs
SELECT     
  upper(felnev)             --row: columns
FROM al_szemely
WHERE true 
  AND id > 0                --row: where1
  AND felnev ILIKE 'a%'
ORDER BY 1;  

--id: replace-table
SELECT     
  upper(felnev)             --row: columns
FROM {tábla}
WHERE true 
  AND id > 0                --row: where1
  AND felnev ILIKE 'a%'
  AND 
   --row-start: where2
    (a > 12
    OR
    b < 76)
   --row-end: where2
ORDER BY {sorrend};  


